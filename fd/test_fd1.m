clear all
close all

% Unit interval domain
a = 1;

% Boundary type
bdtype = [1 1];

% RHS function
rhsfunc = @(x) (pi*pi)*sin(pi*x);

% Exact solution
exactfunc = @(x) sin(pi*x);
exactderfunc = @(x) pi*cos(pi*x);

% Boundary conditions
diri = [exactfunc(0) exactfunc(a)];
neu = [-exactderfunc(0) exactderfunc(a)];

% Initial mesh size
N = 4;
% Number of refinements
K = 10;

% Result vectors
NN = zeros(K,1);
hh = zeros(K,1);
err = zeros(K,1);

for k=1:K
    NN(k) = N;
    hh(k) = a/(N-1);

    % Assemble discretization matrix and RHS vector
    rhs = fd1_eval( a, N, bdtype, rhsfunc );
    [Ah,bh] = fd1_lapl( a, N, bdtype, rhs, diri, neu );
    
    % Solve
    u = Ah\bh;

    % Extend to boundary
    uext = fd1_extbd( u, bdtype, diri );
    
    % Calculate error
    exact = arrayfun( exactfunc, hh(k)*(0:N-1) )';
    err(k) = norm( uext - exact, inf);

    N = 2*N;
end


% Line fitting for order estimation
p = polyfit(log(hh), log(err),1);
pp=exp(p(1)*log(hh)+p(2));
loglog( hh, err, 'o', hh, pp, '-');
title(sprintf('$\\alpha=%.4f$, $C=%.4f$',p(1),exp(p(2))),'Interpreter','LaTex')
axis equal

% 
% Generate LaTeX table
input.data = [NN hh err];
input.tableColLabels = {'$N$','$h$','$\|u_h-\overline{u}_h\|_\infty$'};
input.dataFormatMode = 'column';
input.dataFormat = {'%d',1,'%.6f',1,'%.4e',1};
input.tableColumnAlignment = 'c';
input.tableBorders = 1;
input.tableCaption = 'A $-u''''(x)=\pi^2\sin \pi x$, $u(0)=0$, $u(1)=1$ inhomogén Dirichlet-peremértékfeladat véges differenciás diszkretizációjának abszolút hibája. (Generálta: \lstinline|test_fd1.m|)';
input.tableLabel = 'fd1';
latex = latexTable(input);
