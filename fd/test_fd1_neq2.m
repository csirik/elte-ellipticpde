clear all

% Unit interval domain
a = 1;
epsilon = 0.001;

bdtype = [1 1];

% RHS function
rhsfunc = @(x) 1;
%f = @(x) 2;

% Exact solution
exact = @(x) x - ( exp(x/epsilon) - 1 ) / ( exp(1/epsilon) - 1 );
exactder = @(x) 1 - (1/epsilon) * exp(x/epsilon) / ( exp(1/epsilon) - 1 );
%exact = @(x) x*(1-x);

% Boundary conditions
diri = [exact(0) exact(a)];
neu = [-exactder(0) exactder(a)];

N = 1000;
center = 0.5;
if 1
    % Equidistant
    X = linspace(0, a, N);
elseif 0
    % Non-equidistant grid
    gridfunc = @(X) (1/pi)*asin( X ) + 0.5*ones(size(X)) ;
    X = gridfunc( linspace(-1, 1, N) );
    X = sort(X);
elseif 0
    % local refinement
    delta = 0.05;
    Ncoarse = 20;
    Nfine = 100;
    X = [linspace(0, center-delta, Ncoarse) linspace(center-delta, center+delta, Nfine) linspace(center+delta, a, Ncoarse)];
    X = unique(sort(X));
elseif 0
    % Non-equidistant grid
    gridfunc = @(X) 0.5*X.^3 + 0.5*ones(size(X)) ;
    X = gridfunc( linspace(-1, 1, N) );
    X = sort(X);
end

% Assemble and solve

Ah = epsilon*fd1_asm_neq( X, bdtype ) - ;
Fh = fd1_rhs_neq( X, bdtype, rhsfunc, diri, neu );

u = Ah\Fh;

uext = fd1_extbd( u, bdtype, diri );

err = uext-arrayfun( exact, X)';
fprintf('absolute error: %.4e\n', norm(err,inf))
fprintf('effective resolution:  %.4e\n', min((X(2:N) - X(1:N-1))'))
% plot( X, zeros(size(X)), 'ro' );
% return 

if 1

hold on 
plot( X, uext, 'o' )
plot( X, arrayfun( exact, X )', 'r' );
hold off

else
    
hold on 
plot( X, err, 'ro' );
hold off    
    
end