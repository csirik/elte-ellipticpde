function [v,X] = fd1_eval( a, N, bdtype, func )
% Evaluate function on an equidistant grid with boundary conditions.
%
% INPUT
% a - Length of the interval.
% N - Number of nodes including the endpoints.
% bdtype - Two-element vector specifying the type of boundary for
%          left and right boundary points, respectively. 
%          The permitted boundary types are:
%          1 = Dirichlet
%          2 = Neumann
%          E.g. bdype = [1 1] means pure Dirichlet boundary conditons.
% func - Univariate function
%
% OUTPUT
% v - The evaluated function valeus.
% X - The grid points.
%
% AUTHOR
% Csirik Mihaly, ELTE, csirik@gmail.com
%
% CC0
% To the extent possible under law, the author has waived all copyright and related 
% or neighboring rights to this file.
%

%
% Check input for errors
%

if N < 3 || a <= 0 || any(bdtype ~= 1 & bdtype ~= 2) || length(bdtype) ~= 2
    fprintf( 'fd1_eval: Invalid parameters specified.\n' );
    return;
end

%
% Begin function
%

h = a / ( N - 1 );

if all(bdtype == 1)
    % pure Dirichlet
    X = h*(1:N-2);
elseif any(bdtype == 1) && any(bdtype == 2)
    % mixed
    if bdtype(1) == 1
        % Dirichlet-Neumann
        X = h*(1:N-1);
    else
        % Neumann-Dirichlet
        X = h*(0:N-2);
    end
else
    % pure Neumann
    X = h*(0:N-1);
end

v = arrayfun( func, X )';

end

