clear all

a1 = 1;
a2 = 1;

start = 0;
Nsteps = 10;
step = 100;
results = zeros(Nsteps, 4);

for n=1:Nsteps
    
    N = start + n*step;

    N1 = N;
    N2 = N;

    results(n,1) = N;
    results(n,2) = 1/(N-1);

    bd = [ 1 1 1 1 ];
    
    tic
    Ak = fd2_asm( a1, a2, N1, N2, bd );
    %[~,INTERIOR] = lexicographic( N1, N2 );
    %Ak = A( :, INTERIOR );
    results(n,3) = toc;

    tic
    As = fd2_asm_slow( a1, a2, N1, N2 );
    results(n,4) = toc;
    
    if max(max(Ak-As)) > 0
        fprintf('ERROR!\n');
    end
    
    fprintf('%d/%d\n',n,Nsteps);
end

% Generate LaTeX table
input.data = results;
input.tableColLabels = {'$N=N_1=N_2$','$h=h_1=h_2$','$\Delta t$ [sec]','$\Delta t_{slow}$ [sec]'};
input.dataFormatMode = 'column';
input.dataFormat = {'%d',1,'%.5f',1,'%.3f',2};
input.tableColumnAlignment = 'c';
input.tableBorders = 1;
input.tableCaption = 'Az \texttt{fd2\_asm} és az \texttt{fd2\_asm\_slow} program futási idejének összehasonlítása. (Generálta: \texttt{test\_fd2\_asm.m})';
input.tableLabel = 'fd2asm';

latex = latexTable(input);