function u = fd2_lex_to_grid( U, Nx, Ny, bdtype )
% Convert a lexicographically ordered vector representation of a 
% grid function to a matrix representation suitable for plotting with surf.
%
% INPUT
% U - Lexicographically ordered vector
% Nx- Number of grid points along the x axis, including the endpoints.
% Ny- Number of grid points along the y axis, including the endpoints.
% bdtype - Four-element vector specifying the type of boundary for
%          boundary edges, in the following order:
%
%          +---3---+
%          |       |
%          4       2
%          |       |
%          +---1---+
%
%          The permitted boundary types are:
% 
%          1 = Dirichlet
%          2 = Neumann
%          E.g. bdype = [1 1 1 1] means pure Dirichlet boundary conditons.
%
% OUTPUT
% u - The matrix representation of the grid function.
%
% AUTHOR
% Csirik Mihaly, ELTE, csirik@gmail.com
%
% CC0
% To the extent possible under law, the author has waived all copyright and related 
% or neighboring rights to this file.
%

[ imin, imax, jmin, jmax ] = fd2_dofs( Nx, Ny, bdtype );
isize = imax-imin+1;
jsize = jmax-jmin+1;
u = reshape( U, isize, jsize);

end

