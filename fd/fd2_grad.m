function [ux,uy] = fd2_grad( u, a, b, Nx, Ny )
% Compute the gradient of a grid function u by central differencing.
%
% INPUT
% u - Grid function given on the full grid.
% a - Length along the x axis of the rectangle.
% b - Length along the y axis of the rectangle.
% Nx- Number of grid points along the x axis, including the endpoints.
% Ny- Number of grid points along the y axis, including the endpoints.
%
% OUTPUT
% ux,uy - The components of the gradient.
%
% AUTHOR
% Csirik Mihaly, ELTE, csirik@gmail.com
%
% CC0
% To the extent possible under law, the author has waived all copyright and related 
% or neighboring rights to this file.
%

if Nx < 3 || Ny < 3 || size(u,1) ~= Nx || size(u,2) ~= Ny
    fprintf( 'fd2_grad: Invalid parameters specified.\n' );
    return;
end

ux = (0.5*(Nx-1)/a) * ( u(3:Nx,2:Ny-1) - u(1:Nx-2,2:Ny-1) );
uy = (0.5*(Ny-1)/b) * ( u(2:Nx-1,3:Ny) - u(2:Nx-1,1:Ny-2) );

end

