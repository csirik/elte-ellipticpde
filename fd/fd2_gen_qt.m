function fd2_gen_qt( depthfun, mindepth, maxdepth, regular, bdtype )

    global ni
    global qt

    qt = struct;
    qt.depth = 0;
    qt.indx = [1 2 3 4];
    qt.leaf = 1;
    qt.parent = 0;
    qt.children = [0 0 0 0];
    qt.neighbors = cell(4);
    qt.nodes = [0 0; 1 0; 1 1; 0 1];
    qt.bdmarker = bdtype;

    ni = 4;

    for depth=1:maxdepth+1
        fprintf('>> Pass %d/%d\n', depth, maxdepth);
        for i=1:length(qt)
            center = mean( qt(i).nodes, 1 );
            if ( depthfun(center(1),center(2)) > 0 && qt(i).depth < maxdepth && qt(i).leaf == 1 ) || ( qt(i).depth < mindepth && qt(i).leaf == 1 )
                neighbors = qt(i).neighbors; 

                if regular
                    for side=1:4
                        if length(neighbors{side}) > 0 && qt(neighbors{side}(1)).depth + 1 == qt(i).depth 
                            j = neighbors{side}(1);
                            qt = [ qt  split(qt(j), j) ];
                            update_subcell_neighbors( j );
                            %qt(j).neighbors = cell(4);
                            qt(j).leaf = 0;
                        end
                    end
                end

                qt = [ qt  split(qt(i), i) ];
                update_subcell_neighbors( i );
                %qt(i).neighbors = cell(4);
                qt(i).leaf = 0;
            end
        end
    end
    
    leafindx = [qt.leaf] == 1;
    leafindx = reshape([qt(leafindx).indx], 1, 4*nnz(leafindx));
    [uli,ulii] = unique( leafindx );
    indexmap(uli) = 1:length(uli);
    
    for i=1:length(qt)
        if qt(i).leaf == 1
            for j=1:4
                qt(i).indx(j) = indexmap(qt(i).indx(j));
            end
        end
    end
 end

function update_subcell_neighbors(i)
    global qt
    global ni
    nqt = length(qt);
    neighbors = qt(i).neighbors; 
    
    % SW        
    qt(nqt-3).neighbors{1} = outer_side_neighbors( i, 1, 1 );
    qt(nqt-3).neighbors{2} = nqt-2;
    qt(nqt-3).neighbors{3} = nqt;
    qt(nqt-3).neighbors{4} = outer_side_neighbors( i, 4, 1 );
    
    qt(nqt-3).indx(1) = qt(i).indx(1);
    qt(nqt-3).indx(2) = ni+1; ni = ni+1;
    qt(nqt-3).indx(3) = ni+1; ni = ni+1;
    qt(nqt-3).indx(4) = ni+1; ni = ni+1;
    
    % SE
    qt(nqt-2).neighbors{1} = outer_side_neighbors( i, 1, 2 );
    qt(nqt-2).neighbors{2} = outer_side_neighbors( i, 2, 1 );
    qt(nqt-2).neighbors{3} = nqt-1;
    qt(nqt-2).neighbors{4} = nqt-3;
    
    qt(nqt-2).indx(1) = qt(nqt-3).indx(2);
    qt(nqt-2).indx(2) = qt(i).indx(2);
    qt(nqt-2).indx(3) = ni+1; ni = ni+1;
    qt(nqt-2).indx(4) = qt(nqt-3).indx(3);

    % NE
    qt(nqt-1).neighbors{1} = nqt-2;
    qt(nqt-1).neighbors{2} = outer_side_neighbors( i, 2, 2 );
    qt(nqt-1).neighbors{3} = outer_side_neighbors( i, 3, 2 );
    qt(nqt-1).neighbors{4} = nqt;
    
    qt(nqt-1).indx(1) = qt(nqt-3).indx(3);
    qt(nqt-1).indx(2) = qt(nqt-2).indx(3);
    qt(nqt-1).indx(3) = qt(i).indx(3);
    qt(nqt-1).indx(4) = ni+1; ni = ni+1;

    % NW
    qt(nqt-0).neighbors{1} = nqt-3;
    qt(nqt-0).neighbors{2} = nqt-1;
    qt(nqt-0).neighbors{3} = outer_side_neighbors( i, 3, 1 );
    qt(nqt-0).neighbors{4} = outer_side_neighbors( i, 4, 2 );

    qt(nqt-0).indx(1) = qt(nqt-3).indx(4);
    qt(nqt-0).indx(2) = qt(nqt-3).indx(3);
    qt(nqt-0).indx(3) = qt(nqt-1).indx(4);
    qt(nqt-0).indx(4) = qt(i).indx(4);

    % S
    if length(neighbors{1}) > 0
        if qt(neighbors{1}(1)).depth == qt(i).depth + 1
            qt(neighbors{1}(1)).neighbors{3} = nqt-3;
            qt(neighbors{1}(2)).neighbors{3} = nqt-2;
            qt(nqt-3).indx(2) = qt(neighbors{1}(1)).indx(3);
            qt(nqt-2).indx(1) = qt(neighbors{1}(1)).indx(3);
        else
            qt(neighbors{1}(1)).neighbors{3} = [nqt-3 nqt-2];
        end
    end

    % E
    if length(neighbors{2}) > 0 
        if qt(neighbors{2}(1)).depth == qt(i).depth + 1
            qt(neighbors{2}(1)).neighbors{4} = nqt-2;
            qt(neighbors{2}(2)).neighbors{4} = nqt-1;
            qt(nqt-2).indx(3) = qt(neighbors{2}(1)).indx(4);
            qt(nqt-1).indx(2) = qt(neighbors{2}(1)).indx(4);
        else
            qt(neighbors{2}(1)).neighbors{4} = [nqt-2 nqt-1];
        end
    end

    % N
    if length(neighbors{3}) > 0 
        if qt(neighbors{3}(1)).depth == qt(i).depth + 1
            qt(neighbors{3}(1)).neighbors{1} = nqt;
            qt(neighbors{3}(2)).neighbors{1} = nqt-1;
            qt(nqt-0).indx(3) = qt(neighbors{3}(1)).indx(2);
            qt(nqt-1).indx(4) = qt(neighbors{3}(1)).indx(2);
        else
            qt(neighbors{3}(1)).neighbors{1} = [nqt nqt-1];
        end
    end

    % W
    if length(neighbors{4}) > 0
        if qt(neighbors{4}(1)).depth == qt(i).depth + 1
            qt(neighbors{4}(1)).neighbors{2} = nqt-3;
            qt(neighbors{4}(2)).neighbors{2} = nqt;
            qt(nqt-0).indx(1) = qt(neighbors{4}(1)).indx(3);
            qt(nqt-3).indx(4) = qt(neighbors{4}(1)).indx(3);
        else
            qt(neighbors{4}(1)).neighbors{2} = [nqt-3 nqt];
        end
    end
end

function children = split(parent, parentindex)
    children = [];
    newnodes = zeros( 9, 2 );
    newnodes([1 3 9 7],:) = parent.nodes([1 2 3 4],:);
    newnodes(2,:) = 0.5*( newnodes(1,:) + newnodes(3,:) );
    newnodes(4,:) = 0.5*( newnodes(1,:) + newnodes(7,:) );
    newnodes(5,:) = 0.25*( newnodes(1,:) + newnodes(3,:) + newnodes(7,:) + newnodes(9,:) );
    newnodes(6,:) = 0.5*( newnodes(3,:) + newnodes(9,:) );
    newnodes(8,:) = 0.5*( newnodes(7,:) + newnodes(9,:) );

    nindx = [ 1 2 5 4; 2 3 6 5; 5 6 9 8; 4 5 8 7];

    for k=1:4
        newchildren = struct;
        newchildren.depth = parent.depth + 1;
        newchildren.indx = [0 0 0 0];
        newchildren.leaf = 1;
        newchildren.parent = parentindex;
        newchildren.children = [0 0 0 0];
        newchildren.neighbors = cell(4);
        newchildren.nodes = newnodes(nindx(k,:),:);
        children = [children newchildren];
    end
    
    children(1).bdmarker = [parent.bdmarker(1) 0 0 parent.bdmarker(4)];
    children(2).bdmarker = [parent.bdmarker(1) parent.bdmarker(2) 0 0];
    children(3).bdmarker = [0 parent.bdmarker(2) parent.bdmarker(3) 0];
    children(4).bdmarker = [0 0 parent.bdmarker(3) parent.bdmarker(4)];
end

function neighbors = outer_side_neighbors( i, side, subside )
    global qt
    neighbors = [];
    if length(qt(i).neighbors{side}) == 1
        neighbors = qt(i).neighbors{side}(1);
    elseif length(qt(i).neighbors{side}) == 2
        neighbors = qt(i).neighbors{side}(subside);
    end
end
