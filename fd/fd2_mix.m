function [A,rhs] = fd2_mix( a, b, Nx, Ny, bdtype, dirifunc, neufunc )
% Discretize 2D mixed derivative using the 4-point stencil.
%
% fd2_mix( a, b, Nx, Ny, bdtype, rhsin, dirifunc, neufunc )
%
% Construct 2D finite difference matrix and rhs vector for the mixed
% derivative on an equidistant, rectangular grid with various boundary conditions.
% The 9-point stencil
%           [  -1 0 1   ]
%           [   0 0 0   ]
%           [   1 0 -1  ]
% is used for the mixed derivative and central differencing for the Neumann boundary condtion.
%
% INPUT
% a - Length along the x axis of the rectangle.
% b - Length along the y axis of the rectangle.
% Nx- Number of grid points along the x axis, including the endpoints.
% Ny- Number of grid points along the y axis, including the endpoints.
% bdtype - Four-element vector specifying the type of boundary for
%          boundary edges, in the following order:
%
%          +---3---+
%          |       |
%          4       2
%          |       |
%          +---1---+
%
%          The permitted boundary types are:
% 
%          1 = Dirichlet
%          2 = Neumann
%          E.g. bdype = [1 1 1 1] means pure Dirichlet boundary conditons.
% rhsfunc - bivariate RHS function f of Poisson's equation
% dirifunc - bivariate Dirichlet boundary function
% neufunc - bivariate Neumann boundary function
%
% OUTPUT
% A - The constructed discretization matrix.
% RHS - The constructed right-hand side vector.
%
% AUTHOR
% Csirik Mihaly, ELTE, csirik@gmail.com
%
% CC0
% To the extent possible under law, the author has waived all copyright and related 
% or neighboring rights to this file.
%

%
% Check input for errors
%
if Nx < 3 || Ny < 3 || a <= 0 || b <= 0 || ...
        any(bdtype ~= 1 & bdtype ~= 2) || length(bdtype) ~= 4
    fprintf( 'fd2_lapl: Invalid parameters specified.\n' );
    return;
end

%
% Begin function
%

hx = a / (Nx-1);
hy = b / (Ny-1);
hxinv = (Nx-1) / a;
hyinv = (Ny-1) / b;
hxinv2 = hxinv*hxinv;
hyinv2 = hyinv*hyinv;

% all grid points
[X,Y] = ndgrid( hx*(0:Nx-1), hy*(0:Ny-1) );
% evaluate dirifunc and neufunc on the full grid
% therefore you can index it using the absolute indices below
dirixy = arrayfun( dirifunc, X, Y );
neuxy = arrayfun( neufunc, X, Y );

% interior indices
[ imin, imax, jmin, jmax ] = fd2_dofs( Nx, Ny, bdtype );
isize = imax-imin+1;
jsize = jmax-jmin+1;

% boundary contributions
beta = zeros( isize, jsize );
alpha = zeros( isize, jsize );

% Prepare factors for the Kronecker product
Cx = fd1_diff( a, Nx, bdtype([2 4]) );
Cy = fd1_diff( b, Ny, bdtype([1 3]) );
Ix = speye( size( Cx ) ); 
Iy = speye( size( Cy ) );


if bdtype(1) == 1
    % bottom side is Dirichlet 
    beta( 1:isize, 1 ) = beta( 1:isize, 1 ) + dirixy( (imin:imax)+1, 1 ) - dirixy( (imin:imax)-1, 1 );
    alpha( 1:isize, 1 ) = alpha( 1:isize, 1 ) + 1;
elseif bdtype(1) == 2
    % bottom side is Neumann
    Iy(1,1) = 0.5;
    alpha( 1:isize, 1 ) = 0.5 * alpha( 1:isize, 1 );
    beta( 1:isize, 1 ) = beta( 1:isize, 1 ) + 2 * hyinv * neuxy( imin:imax, 1 );
end

if bdtype(2) == 1
    % right side is Dirichlet
    beta( isize, 1:jsize ) = beta( isize, 1:jsize ) + dirixy( Nx, (jmin:jmax)-1 ) - dirixy( Nx, (jmin:jmax)+1 );
    alpha( isize, 1:jsize ) = alpha( isize, 1:jsize ) + 1;
elseif bdtype(2) == 2
    % right side is Neumann
    Ix(end,end) = 0.5;
    alpha( isize, 1:jsize ) = 0.5 * alpha( isize, 1:jsize );
    beta( isize, 1:jsize ) = beta( isize, 1:jsize ) + 2 * hxinv * neuxy( Nx, jmin:jmax );
end

if bdtype(3) == 1
    % top side is Dirichlet
    beta( 1:isize, jsize ) = beta( 1:isize, jsize ) + dirixy( (imin:imax)-1, Ny ) - dirixy( (imin:imax)+1, Ny );
    alpha( 1:isize, jsize ) = alpha( 1:isize, jsize ) + 1;
elseif bdtype(3) == 2
    % top side is Neumann
    Iy(end,end) = 0.5;
    alpha( 1:isize, jsize ) = 0.5 * alpha( 1:isize, jsize );
    beta( 1:isize, jsize ) = beta( 1:isize, jsize ) + 2 * hyinv * neuxy( imin:imax, Ny );
end

if bdtype(4) == 1
    % left side is Dirichlet
    beta( 1, 1:jsize ) = beta( 1, 1:jsize ) + dirixy( 1, (jmin:jmax)+1 ) - dirixy( 1, (jmin:jmax)-1 );
    alpha( 1, 1:jsize ) = alpha( 1, 1:jsize ) + 1;
elseif bdtype(4) == 2
    % left side is Neumann
    Ix(1,1) = 0.5;
    alpha( 1, 1:jsize ) = 0.5 * alpha( 1, 1:jsize );
    beta( 1, 1:jsize ) = beta( 1, 1:jsize ) + 2 * hxinv * neuxy( 1, jmin:jmax );
end


%
% Assemble discretization matrix
%
A = kron( Cx, Cy );

%
% Assemble right-hand side vector
%
if bdtype(1) == 1 && bdtype(4) == 1
    % lower left corner is Dirichlet
    beta(1,1) = beta(1,1) + dirixy(1,1);
end

if bdtype(1) == 1 && bdtype(2) == 1
    % lower right corner is Dirichlet
    beta(end,1) = beta(end,1) - dirixy(end,1);
end

if bdtype(2) == 1 && bdtype(3) == 1
    % upper right corner is Dirichlet
    beta(end,end) = beta(end,end) + dirixy(end,end);
end

if bdtype(4) == 1 && bdtype(3) == 1
    % upper left corner is Dirichlet
    beta(1,end) = beta(1,end) - dirixy(1,end);
end


rhs = reshape( -0.25*hxinv*hyinv*beta, isize*jsize, 1 );

end

