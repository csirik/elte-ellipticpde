function uext = fd1_extbd( u, bdtype, diri )
% Extend a grid function defined only on the free grid points (degrees of freedom) to the whole grid.
%
% INPUT
% a - Length of the interval.
% N - Number of nodes including the endpoints.
% bdtype - Two-element vector specifying the type of boundary for
%          left and right boundary points, respectively. 
%          The permitted boundary types are:
%          1 = Dirichlet
%          2 = Neumann
%          E.g. bdype = [1 1] means pure Dirichlet boundary conditons.
%
% diri - Two-dimensional vector containing Dirichlet boundary data.
%
% OUTPUT
% uext - Grid function on the full grid.
%
% AUTHOR
% Csirik Mihaly, ELTE, csirik@gmail.com
%
% CC0
% To the extent possible under law, the author has waived all copyright and related 
% or neighboring rights to this file.
%

%
% Check input for errors
%
if any(bdtype ~= 1 & bdtype ~= 2) || length(bdtype) ~= 2 || ...
        length(diri) ~= 2
    fprintf( 'fd1_extbd: Invalid parameters specified.\n' );
    return;
end;

uext = u;

if bdtype(1) == 1
    % Left boundary point is Dirichlet
    uext = [diri(1); uext];
end

if bdtype(2) == 1
    % Right boundary point is Dirichlet
    uext = [uext; diri(2)];
end
