clear all

if 0
    a = 1;
    bdtype = [1 2];
    exact = @(x) exp(x);
    exactder = @(x) exp(x);
   
    rhsfunc = @(x) -exp(x);
end

if 0
    a = 1;
    bdtype = [1 2];
    exact = @(x) x*(1-x);
    exactder = @(x) 1-2*x;
   
    rhsfunc = @(x) 2;
end

if 1
    % Unit interval domain
    a = 1;
    L = 1000;

    bdtype = [1 1];

    % RHS function
    rhsfunc = @(x) 2 * L^3 * ( x - 0.5 ) / ( ( 1 + L^2 * ( x - 0.5 )^2 )^2 );

    % Exact solution
    exact = @(x) atan( L*( x - 0.5 ) );
    exactder = @(x) L / ( 1 + L^2 * ( x - 0.5 )^2 );
end


diri = [exact(0) exact(1)];
neu = [-exactder(0) exactder(1)];

% Initial mesh size
N = 10;
% Number of refinements
K = 5;

% Result vectors
NN = zeros(K,1);
hh = zeros(K,1);
err = zeros(K,1);

for k=1:K
    NN(k) = N;
    hh(k) = a/(N-1);
    
    % Assemble
    rhs = fd1_eval( a, N, bdtype, rhsfunc );
    [Ah,bh] = fd1_lapl_4( a, N, bdtype, rhs, diri, neu );

    % Solve
    u = Ah\bh;

    % Extend to boundary
    uext = fd1_extbd( u, bdtype, diri );
    
    % Calculate error
    err(k) = norm( uext - arrayfun( exact, hh(k)*(0:N-1) )', inf);

    N = 2*N;
end

%plot( (0:N/2-1)/(N/2-1), uext, 'o' )
%return

% Line fitting for order estimation
p = polyfit(log(hh), log(err),1);
pp=exp(p(1)*log(hh)+p(2));
loglog( hh, err, 'o', hh, pp, '-');
title(sprintf('$\\alpha=%.4f$, $C=%.4f$',p(1),exp(p(2))),'Interpreter','LaTex')
axis equal


% 
% Generate LaTeX table
input.data = [NN hh err];
input.tableColLabels = {'$N$','$h$','$\|u_h-\overline{u}_h\|_\infty$'};
input.dataFormatMode = 'column';
input.dataFormat = {'%d',1,'%.6f',1,'%.4e',1};
input.tableColumnAlignment = 'c';
input.tableBorders = 1;
input.tableCaption = [ 'A $-u''''=-e^x$, $u(0)=1$, $u(1)=e$ Dirichlet-peremértékfeladat' ...
' negyedrendű diszkretizációjának abszolút hibája. (Generálta: \lstinline|test_fd1_hi.m|)'];
input.tableLabel = 'fd1';
latex = latexTable(input);

