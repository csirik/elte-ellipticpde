function T = tridiag( v, N )
    if length(v) ~= 3 || N <= 0
        fprintf('Invalid arguments specified.\n');
        T = [];
        return
    end
    e = ones( N, 1 );
    T = spdiags( e*v, -1:1, N, N );
end