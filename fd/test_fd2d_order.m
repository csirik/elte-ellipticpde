clear all

% Unit square domain
a = 1;

% RHS function
f = @(x1,x2) (2*pi*pi)*sin(pi*x1)*sin(pi*x2);
% Homogeneous Dirichlet boundary
g = @(x1,x2) 0;
% Exact solution
exact = @(x1,x2) sin(pi*x1)*sin(pi*x2);

% Initial mesh size
N = 3;
% Number of refinements
K = 10;

% Result vectors
NN = zeros(K,1);
hh = zeros(K,1);
err = zeros(K,1);
 
for k=1:K
    NN(k) = N;
    hh(k) = 1/(N-1);
    INDX = natindx( N-2, N-2 );

    [A,F] = fd2dsasm( a, a, N, N, f, g, INDX );
    U = A\F;
    Z = fderr( a, a, N, N, U, exact, INDX );
    err(k) = max(max(Z));
    
    N = 2*N - 1;
end

% Line fitting for order estimation
p = polyfit(log(hh), log(err),1);
pp=exp(p(1)*log(hh)+p(2));
loglog( hh, err, 'o', hh, pp, '-');
title(sprintf('$\\alpha=%.4f$, $C=%.4f$',p(1),exp(p(2))),'Interpreter','LaTex')
axis equal

% Generate LaTeX table
input.data = [NN hh err];
input.tableColLabels = {'$N$','$h$','$\|u_h-\overline{u}_h\|_\infty$'};
input.dataFormatMode = 'column';
input.dataFormat = {'%d',1,'%.8f',1,'%.4e',1};
input.tableColumnAlignment = 'c';
input.tableBorders = 1;
input.tableCaption = 'A $-\lapl u=4\pi^2\sin(\pi x_1)sin(\pi x_2)$ homogén Dirichlet-peremértékfeladat 5-pontos véges differenciás diszkretizációja.';

latex = latexTable(input);
