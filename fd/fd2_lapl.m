function [A,rhs] = fd2_lapl( a, b, Nx, Ny, bdtype, rhsin, dirifunc, neufunc )
% Discretize 2D negative Laplacian using the 9-point stencil.
%
% fd2_lapl( a, b, Nx, Ny, bdtype, rhsin, dirifunc, neufunc )
%
% Construct 2D finite difference matrix and rhs vector for the negative Laplacian 
% on an equidistant, rectangular grid with various boundary conditions.
% The 5-point stencil
%           [    -1      ]
%           [  -1 4 -1   ]
%           [    -1      ]
% is used for the Laplacian and central differencing for the Neumann boundary condtion.
%
% INPUT
% a - Length along the x axis of the rectangle.
% b - Length along the y axis of the rectangle.
% Nx- Number of grid points along the x axis, including the endpoints.
% Ny- Number of grid points along the y axis, including the endpoints.
% bdtype - Four-element vector specifying the type of boundary for
%          boundary edges, in the following order:
%
%          +---3---+
%          |       |
%          4       2
%          |       |
%          +---1---+
%
%          The permitted boundary types are:
% 
%          1 = Dirichlet
%          2 = Neumann
%          E.g. bdype = [1 1 1 1] means pure Dirichlet boundary conditons.
% rhsfunc - bivariate RHS function f of Poisson's equation
% dirifunc - bivariate Dirichlet boundary function
% neufunc - bivariate Neumann boundary function
%
% OUTPUT
% A - The constructed system matrix.
% RHS - The constructed right-hand side vector.
%
% AUTHOR
% Csirik Mihaly, ELTE, csirik@gmail.com
%
% CC0
% To the extent possible under law, the author has waived all copyright and related 
% or neighboring rights to this file.
%

%
% Check input for errors
%
if Nx < 3 || Ny < 3 || a <= 0 || b <= 0 || ...
        any(bdtype ~= 1 & bdtype ~= 2) || length(bdtype) ~= 4
    fprintf( 'fd2_lapl: Invalid parameters specified.\n' );
    return;
end

%
% Begin function
%

% auxiliary quantities
hx = a / (Nx-1);
hy = b / (Ny-1);
hxinv = (Nx-1) / a;
hyinv = (Ny-1) / b;
hxinv2 = hxinv*hxinv;
hyinv2 = hyinv*hyinv;

% all grid points
[X,Y] = ndgrid( hx*(0:Nx-1), hy*(0:Ny-1) );
% evaluate dirifunc and neufunc on the full grid
% therefore you can index it using the absolute indices below
dirixy = arrayfun( dirifunc, X, Y );
neuxy = arrayfun( neufunc, X, Y );

% interior indices
[ imin, imax, jmin, jmax ] = fd2_dofs( Nx, Ny, bdtype );
isize = imax-imin+1;
jsize = jmax-jmin+1;

% auxiliary coefficients for rhsin
alpha = ones( isize, jsize );

% boundary contributions accumulator for RHS assembly
beta = zeros( isize, jsize );

%
% Prepare factors for the Kronecker product
%
Sx = fd1_lapl( a, Nx, bdtype([4 2]) );
Sy = fd1_lapl( b, Ny, bdtype([1 3]) );
Ix = speye( size( Sx ) ); 
Iy = speye( size( Sy ) );

%
% Modify Ix and Iy according to boundary conditions
%

if bdtype(1) == 2
    % bottom side is Neumann
    Iy(1,1) = 0.5;
end

if bdtype(2) == 2
    % right side is Neumann
    Ix(end,end) = 0.5;
end

if bdtype(3) == 2
    % top side is Neumann
    Iy(end,end) = 0.5;
end

if bdtype(4) == 2
    % left side is Neumann
    Ix(1,1) = 0.5;
end

%
% Assemble discretization matrix
%
A = kron( Iy, Sx ) + kron( Sy, Ix );

%
% Assemble RHS
%

if bdtype(1) == 1
    % bottom side is Dirichlet 
    beta( 1:isize, 1 ) = beta( 1:isize, 1 ) + hyinv2 * dirixy( imin:imax, 1 );
elseif bdtype(1) == 2
    % bottom side is Neumann
    alpha( 1:isize, 1 ) = 0.5 * alpha( 1:isize, 1 );
    beta( 1:isize, 1 ) = beta( 1:isize, 1 ) + 2 * hyinv * neuxy( imin:imax, 1 );
end

if bdtype(2) == 1
    % right side is Dirichlet
    beta( isize, 1:jsize ) = beta( isize, 1:jsize ) + hxinv2 * dirixy( Nx, jmin:jmax );
elseif bdtype(2) == 2
    % right side is Neumann
    alpha( isize, 1:jsize ) = 0.5 * alpha( isize, 1:jsize );
    beta( isize, 1:jsize ) = beta( isize, 1:jsize ) + 2 * hxinv * neuxy( Nx, jmin:jmax );
end

if bdtype(3) == 1
    % top side is Dirichlet
    beta( 1:isize, jsize ) = beta( 1:isize, jsize ) + hyinv2 * dirixy( imin:imax, Ny );
elseif bdtype(3) == 2
    % top side is Neumann
    alpha( 1:isize, jsize ) = 0.5 * alpha( 1:isize, jsize );
    beta( 1:isize, jsize ) = beta( 1:isize, jsize ) + 2 * hyinv * neuxy( imin:imax, Ny );
end

if bdtype(4) == 1
    % left side is Dirichlet
    beta( 1, 1:jsize ) = beta( 1, 1:jsize ) + hxinv2 * dirixy( 1, jmin:jmax );
elseif bdtype(4) == 2
    % left side is Neumann
    alpha( 1, 1:jsize ) = 0.5 * alpha( 1, 1:jsize );
    beta( 1, 1:jsize ) = beta( 1, 1:jsize ) + 2 * hxinv * neuxy( 1, jmin:jmax );
end

%
% Final assembly of the right-hand side vector
%
rhs = alpha .* (  beta + fd2_lex_to_grid( rhsin, Nx, Ny, bdtype ) );
rhs = reshape( rhs, isize*jsize, 1 );

end

