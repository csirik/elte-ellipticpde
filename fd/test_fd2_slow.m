clear all
a = 1;
b = 1;

Nx = 10;
Ny = 10;


hx = a / (Nx-1);
hy = b / (Ny-1);

% RHS function
rhsfunc = @(x,y) -4;
%rhsfunc = @(x,y) (13*pi*pi)*sin(3*pi*x)*sin(2*pi*y);
%rhsfunc = @(x,y) 0;
%rhsfunc = @(x,y) double((x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) < 0.01);


% Dirichlet boundary
dirifunc = @(x,y) (x-0.5)*(x-0.5) + (y-0.5)*(y-0.5);
%dirifunc = @(x,y) x+y;
%dirifunc = @(x,y) 0;
%dirifunc = @(x,y) (0.5*sign(sin(10*pi*(x+0.5)))+0.5);
%neufunc = @(x,y) 1;
%neufunc = @(x,y) -2*pi*sin(3*pi*x);
%neufunc = @(x,y) -pi*sin(2*pi*x)*cos(pi*y);
neufunc = @(x,y) 1;

%dirifunc = @(x,y) sin(pi*x)+sin(pi*y);

% Exact solution
%exact = @(x,y) sin(3*pi*x)*sin(2*pi*y);
exact = @(x,y) (x-0.5)*(x-0.5) + (y-0.5)*(y-0.5);

bdtype = [2 2 1 2];

bdchar = ['D' 'N'];
fprintf('+----%c----+\n|         |\n%c         %c\n|         |\n+----%c----+\n', ...
    bdchar(bdtype(3)), bdchar(bdtype(4)), bdchar(bdtype(2)), bdchar(bdtype(1)) );

% Assemble
rhs = fd2_eval( a, b, Nx, Ny, bdtype, rhsfunc );
[Ah,Fh] = fd2_lapl( a, b, Nx, Ny, bdtype, rhs, dirifunc, neufunc );
[Ahs,Fhs] = fd2_lapl_slow( a, b, Nx, Ny, bdtype, rhs, dirifunc, neufunc );

fprintf('Aerr=%f, Ferr=%f\n', norm(Ah-Ahs, inf), norm(Fh-Fhs, inf));

%return

%Ah = Ahs;
%Fh = Fhs;

%return
% Solve
U = Ah\Fh;

% Make it plottable
u = fd2_lex_to_grid( U, Nx, Ny, bdtype );
u = fd2_extbd( u, a, b, Nx, Ny, bdtype, dirifunc );

% Grid
[X, Y] = ndgrid( hx*(0:Nx-1), hy*(0:Ny-1) );

% Error function
ERR = u-arrayfun( exact, X, Y );
%surf( X, Y, arrayfun( exact, X, Y ) )
%surf( X, Y, ERR )
%return

hold on
if 1
surf( X, Y, u )
else
contour( X, Y, u, 50 )
[ux,uy] = fd2_grad( u, a, b, Nx, Ny );
quiver( XINT, YINT, ux, uy, 'LineWidth', 0.5 )
%surf( X, Y, ERR )
end
hold off

fprintf('errmax=%e\n', norm(ERR(:),inf));
%axis equal
%zlim([0 1])
%surf( Xfull, Yfull, arrayfun( dirifunc, Xfull, Yfull ) )

