clear all
N = 32;

rhsfunc = @(x) 4*pi*pi*sin(2*pi*x);

rhsf = fftshift( fft( arrayfun( rhsfunc, (0:N-1)/N ) ) );
fprintf('mean of rhsfunc = %f\n', rhsf(1) );
uf = rhsf(2:end) ./ ( ( 2 - 2 * cos( 2*pi*(1:N-1)/N ) ) );
uf = [0 uf];
u = ifft( ifftshift( uf ) )

hold on
plot( (0:N)/N, sin(2*pi*(0:N)/N), 'r' )
plot( (0:N)/N, [u u(1)], 'o' )
hold off