function [v] = fd2_eval( a, b, Nx, Ny, bdtype, func )
% Evaluate function on an equidistant grid with boundary conditions.
%
% INPUT
% a - Length along the x axis of the rectangle.
% b - Length along the y axis of the rectangle.
% Nx- Number of grid points along the x axis, including the endpoints.
% Ny- Number of grid points along the y axis, including the endpoints.
% bdtype - Four-element vector specifying the type of boundary for
%          boundary edges, in the following order:
%
%          +---3---+
%          |       |
%          4       2
%          |       |
%          +---1---+
%
%          The permitted boundary types are:
% 
%          1 = Dirichlet
%          2 = Neumann
%          E.g. bdype = [1 1 1 1] means pure Dirichlet boundary conditons.
% func - bivariate RHS function f of Poisson's equation
%
% OUTPUT
% v - The evaluated function values.
% X - The constructed right-hand side vector.
%
% AUTHOR
% Csirik Mihaly, ELTE, csirik@gmail.com
%
% CC0
% To the extent possible under law, the author has waived all copyright and related 
% or neighboring rights to this file.
%

%
% Check input for errors
%
if Nx < 3 || Ny < 3 || a <= 0 || b <= 0 || ...
        any(bdtype ~= 1 & bdtype ~= 2) || length(bdtype) ~= 4
    fprintf( 'fd2_eval: Invalid parameters specified.\n' );
    return;
end

%
% Begin function
%

hx = a / (Nx-1);
hy = b / (Ny-1);

% interior indices
[ imin, imax, jmin, jmax ] = fd2_dofs( Nx, Ny, bdtype );
% dof grid points
[XDOF, YDOF] = ndgrid( hx*(imin-1:imax-1), hy*(jmin-1:jmax-1) );
% evaulate
v = arrayfun( func, XDOF, YDOF );

end

