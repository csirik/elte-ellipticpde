function plot_fd2mesh( a, b, Nx, Ny, bdtype )

hx = a / (Nx-1);
hy = b / (Ny-1);

[X,Y] = meshgrid(1:Nx,1:Ny);

X = hx*( X - ones(size(X)) );
Y = hy*( Y - ones(size(Y)) );

hold on

surf( X, Y, zeros(size(X)), 'LineWidth',2 )
colormap([1  1  1])

plot( X(2:Ny-1, 2:Nx-1), Y(2:Ny-1, 2:Nx-1), 'ks', 'MarkerSize', 10, 'MarkerFaceColor', [1,1,1] )

marker = {'ko', 'ks'};

plot( X(1, 1:Nx), Y(1, 1:Nx), marker{bdtype(1)}, 'MarkerSize', 10, 'MarkerFaceColor', [1,1,1] )
plot( X(1:Ny, Nx), Y(1:Ny, Nx), marker{bdtype(2)}, 'MarkerSize', 10, 'MarkerFaceColor', [1,1,1] )
plot( X(Ny, 1:Nx), Y(Ny, 1:Nx), marker{bdtype(3)}, 'MarkerSize', 10, 'MarkerFaceColor', [1,1,1] )
plot( X(1:Ny, 1), Y(1:Ny, 1), marker{bdtype(4)}, 'MarkerSize', 10, 'MarkerFaceColor', [1,1,1] )

view(2)
axis equal

hold off

end

