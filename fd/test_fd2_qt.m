clear all

global qt
mindepth = 2;
maxdepth = 3;

depthfun = @(x1,x2) 0.25 - (x1-0.5)^2 - (x2-0.5)^2;
%depthfun = @(x1,x2) 2*x1 + x2 - 1.2;
%depthfun = @(x1,x2) 1;
%depthfun = @(x1,x2) (x1-0.5)^2 + (x2-0.5)^2 < 0.25 && (x1-0.5)^2 + (x2-0.5)^2 > 0.1;
%depthfun = @(x1,x2) abs(x1-1)+abs(x2-1)<1;
depthfun = @(x1,x2) 1 - x1^2 - x2^2;

bdtype = [1 1 1 1];
fd2_gen_qt( depthfun, mindepth, maxdepth, 1, bdtype );


hold on
axis equal

depthcolor = [ 1 0 0; 0 1 0; 0 0 1; 0 1 1; 1 1 0 ];
arrow = @(x,y,c) quiver( x(1),x(2),y(1)-x(1),y(2)-x(2),0,'color',c,'LineWidth',2); 
sidemid = [1 2; 2 3; 3 4; 1 4];
sidecolor = ['r' 'g' 'b' 'k'];
nodelabeloffset = [ 1 1; -1 1; -1 -1; 1 -1 ];

for c=1:length(qt)
    if qt(c).leaf == 1
        rectangle( 'Position', [qt(c).nodes(1,1) qt(c).nodes(1,2) qt(c).nodes(2,1)-qt(c).nodes(1,1) qt(c).nodes(4,2)-qt(c).nodes(1,2)] );
        center = mean(qt(c).nodes,1);
        %text( center(1), center(2), num2str(qt(c).depth), 'FontSize', 12, 'FontWeight', 'Bold', 'Color', 'Black' )
        if depthfun(center(1),center(2)) > 0
           % plot( center(1), center(2), '*' )
        end
        if 0
        for node=1:4
            s = 0.08*(qt(c).nodes(2,1)-qt(c).nodes(1,1));
            text( qt(c).nodes(node,1)+s*nodelabeloffset(node,1), qt(c).nodes(node,2)+s*nodelabeloffset(node,2), num2str(qt(c).indx(node)), ...
                'FontSize', 8, 'Color', 'Black' )
        end
        end
        
        if 1
        %text( center(1), center(2), num2str(c), 'FontSize', 8, 'Color', 'Black' )
        

        for side=1:4
            sidecenter = 0.5*( qt(c).nodes(sidemid(side,1),:) + qt(c).nodes(sidemid(side,2),:) );
            %scatter( sidecenter(1)+0.01*rand(), sidecenter(2)+0.01*rand(), sidecolor(side))
            bdchar = [' ', 'D', 'N'];
            text( sidecenter(1), sidecenter(2), bdchar(1+qt(c).bdmarker(side)), 'FontSize', 8, 'Color', 'Black' )

            for subside=1:length(qt(c).neighbors{side})
                %sidecenter = 0.5*( qt(c).nodes(sidemid(side,1),:) + qt(c).nodes(sidemid(side,2),:) );
                %sidecenter = mean( qt(c).nodes )
                if 0 
                neighindex = qt(c).neighbors{side}(subside);
                center = mean( qt(neighindex).nodes );
                arrow( sidecenter, center, sidecolor(side));
                else
                end
            end
        end
        end
    end
end

hold off