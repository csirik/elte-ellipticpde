clear all

% Unit interval domain
a = 1;
L = 1000;

bdtype = [1 1];

% RHS function
rhsfunc = @(x) 2 * L^3 * ( x - 0.5 ) / ( ( 1 + L^2 * ( x - 0.5 )^2 )^2 );
%f = @(x) 2;

% Exact solution
exact = @(x) atan( L*( x - 0.5 ) );
exactder = @(x) L / ( 1 + L^2 * ( x - 0.5 )^2 );
%exact = @(x) x*(1-x);

% Boundary conditions
diri = [exact(0) exact(a)];
neu = [-exactder(0) exactder(a)];

N = 101;
center = 0.5;
if 0
    % Equidistant
    X = linspace(0, a, N);
elseif 0
    % Non-equidistant grid
    gridfunc = @(X) (1/pi)*asin( X ) + 0.5*ones(size(X)) ;
    X = gridfunc( linspace(-1, 1, N) );
    X = sort(X);
elseif 0
    % local refinement
    delta = 0.05;
    Ncoarse = 20;
    Nfine = 100;
    X = [linspace(0, center-delta, Ncoarse) linspace(center-delta, center+delta, Nfine) linspace(center+delta, a, Ncoarse)];
    X = unique(sort(X));
elseif 1
    % Non-equidistant grid
    gridfunc = @(X) 0.1*X.^3 + 0.5*ones(size(X)) ;
    X = gridfunc( linspace(-1, 1, N) );
    X = sort(X);
end

% Assemble
[Ah,Fh] = fd1_lapl_neq( X, bdtype, rhsfunc, diri, neu );
% Solve
u = Ah\Fh;
% Extend to boundary
uext = fd1_extbd( u, bdtype, diri );

err = uext-arrayfun( exact, X)';
fprintf('absolute error: %.4e\n', norm(err,inf))
fprintf('effective resolution:  %.4e\n', min((X(2:end) - X(1:end-1))'))
% plot( X, zeros(size(X)), 'ro' );
% return 

if 1

hold on 
plot( X, uext, 'o' )
plot( X, arrayfun( exact, X )', 'r' );
hold off

else
    
hold on 
plot( X, err, 'ro' );
hold off    
    
end