function [A,rhs] = fd2_lapl_slow( a, b, Nx, Ny, bdtype, rhsin, dirifunc, neufunc )
% Discretize 2D negative Laplacian using the 9-point stencil.
%
% fd2_lapl_slow( a, b, Nx, Ny, bdtype, rhsin, dirifunc, neufunc )
%
% WARNING! THIS IS THE SLOW, EDUCATIONAL VERSION EMPLOYING NODE-BASED
% ASSEMBLY. DO NOT USE THIS IN PRACTICE.
%
% Construct 2D finite difference matrix and rhs vector for the negative Laplacian 
% on an equidistant, rectangular grid with various boundary conditions.
% The 5-point stencil
%           [    -1      ]
%           [  -1 4 -1   ]
%           [    -1      ]
% is used for the Laplacian and central differencing for the Neumann boundary condtion.
%
% INPUT
% a - Length along the x axis of the rectangle.
% b - Length along the y axis of the rectangle.
% Nx- Number of grid points along the x axis, including the endpoints.
% Ny- Number of grid points along the y axis, including the endpoints.
% bdtype - Four-element vector specifying the type of boundary for
%          boundary edges, in the following order:
%
%          +---3---+
%          |       |
%          4       2
%          |       |
%          +---1---+
%
%          The permitted boundary types are:
% 
%          1 = Dirichlet
%          2 = Neumann
%          E.g. bdype = [1 1 1 1] means pure Dirichlet boundary conditons.
% rhsfunc - bivariate RHS function f of Poisson's equation
% dirifunc - bivariate Dirichlet boundary function
% neufunc - bivariate Neumann boundary function
%
% OUTPUT
% A - The constructed system matrix.
% RHS - The constructed right-hand side vector.
%
% AUTHOR
% Csirik Mihaly, ELTE, csirik@gmail.com
%
% CC0
% To the extent possible under law, the author has waived all copyright and related 
% or neighboring rights to this file.
%

%
% Check input for errors
%
if Nx < 4 || Ny < 4 || a <= 0 || b <= 0 || ...
        any(bdtype ~= 1 & bdtype ~= 2) || length(bdtype) ~= 4
    fprintf( 'fd2_lapl: Invalid parameters specified.\n' );
    return;
end

%
% Begin function
%

hx = a / (Nx-1);
hy = b / (Ny-1);
hxinv = (Nx-1) / a;
hyinv = (Ny-1) / b;
hxinv2 = hxinv*hxinv;
hyinv2 = hyinv*hyinv;

% Grid points
[X,Y] = ndgrid( hx*(0:Nx-1), hy*(0:Ny-1) );

% interior indices
[ imin, imax, jmin, jmax ] = fd2_dofs( Nx, Ny, bdtype );
isize = imax-imin+1;
jsize = jmax-jmin+1;
ndofs = isize * jsize;

%
% Initialize boundary marker
%
bdmarker = zeros( Nx, Ny );
% sides as indexes in the grid
sides{1,1} = 1:Nx; sides{1,2} = 1;
sides{2,1} = Nx; sides{2,2} = 1:Ny;
sides{3,1} = 1:Nx; sides{3,2} = Ny;
sides{4,1} = 1; sides{4,2} = 1:Ny;

% mark Neumann sides first
for neu=find(bdtype==2)
    bdmarker(sides{neu,1}, sides{neu,2}) = 2;
end

% then Dirichlet
for diri=find(bdtype==1)
    bdmarker(sides{diri,1}, sides{diri,2}) = 1;
end

% Construct lexicographic index map
INDXF = reshape( 1:Nx*Ny, Nx, Ny );

INDX = zeros( Nx, Ny );
ii = 1;
for j = jmin:jmax
    for i = imin:imax
        if bdmarker(i,j) == 0 || bdmarker(i,j) == 2
            INDX(i,j) = ii;
            ii = ii + 1;
        end
    end
end

%INDX
%INDXF
%fullbdmarker

%
% Assemble system matrix and RHS vector
%
A = spalloc( ndofs, ndofs, 5*ndofs );
rhs = zeros( ndofs, 1 );

for j=jmin:jmax
    for i=imin:imax
        if bdmarker( INDXF(i,j) ) == 2
            %
            % Neumann-Neumann corner
            %
            if ( i == 1 && j == 1 ) || ( i == 1 && j == Ny ) || ...
                    ( i == Nx && j == 1 ) || ( i == Nx && j == Ny )
                
                if i == 1
                    % left side corner
                    A( INDX(i,j), INDX(i+1,j) ) = - 0.5 * hxinv2;
                end
                
                if i == Nx
                    % right side corner
                    A( INDX(i,j), INDX(i-1,j) ) = - 0.5 * hxinv2;
                end

                if j == 1
                    % bottom side corner
                    A( INDX(i,j), INDX(i,j+1) ) = - 0.5 * hyinv2;
                end
                
                if j == Ny
                    % top side corner
                    A( INDX(i,j), INDX(i,j-1) ) = - 0.5 * hyinv2;
                end
                
                % Diagonal
                A( INDX(i,j), INDX(i,j) ) = 0.5 * ( hxinv2 + hyinv2 );
                % RHS
                rhs( INDX(i,j) ) = 0.25 * rhsin(INDX(i,j)) ...
                        + 0.5 * ( hxinv + hyinv ) * neufunc( X(i,j), Y(i,j) );
            else
                %
                % Noncorner Neumann or Dirichlet-Neumann corner
                %
                if i == 1
                    % on left side
                    if bdmarker( INDXF(i,j-1) ) == 1
                        % bottom left Neumann-Dirichlet
                        A( INDX(i,j), INDX(i+1,j) ) = - hxinv2;
                        A( INDX(i,j), INDX(i,j+1) ) = - 0.5 * hyinv2;
                        rhs( INDX(i,j) ) = 0.5 * rhsin( INDX(i,j) ) + ...
                            0.5 * hyinv2 * dirifunc( X(i,j-1), Y(i,j-1) ) + hxinv * neufunc( X(i,j), Y(i,j) );   
                    elseif bdmarker( INDXF(i,j+1) ) == 1
                        % top left Neumann-Dirichlet
                        A( INDX(i,j), INDX(i+1,j) ) = - hxinv2;
                        A( INDX(i,j), INDX(i,j-1) ) = - 0.5 * hyinv2;
                        rhs( INDX(i,j) ) = 0.5 * rhsin( INDX(i,j) ) + ...
                            0.5 * hyinv2 * dirifunc( X(i,j+1), Y(i,j+1) ) + hxinv * neufunc( X(i,j), Y(i,j) );   
                    else
                        % noncorner
                        A( INDX(i,j), INDX(i+1,j) ) = - hxinv2;
                        A( INDX(i,j), INDX(i,j-1) ) = - 0.5 * hyinv2;
                        A( INDX(i,j), INDX(i,j+1) ) = - 0.5 * hyinv2;
                        rhs( INDX(i,j) ) = 0.5 * rhsin( INDX(i,j) ) + hxinv * neufunc( hx*(i-1), hy*(j-1) );
                    end
                elseif i == Nx
                    % on right side
                    if bdmarker( INDXF(i,j-1) ) == 1
                        % bottom right Neumann-Dirichlet
                        A( INDX(i,j), INDX(i-1,j) ) = - hxinv2;
                        A( INDX(i,j), INDX(i,j+1) ) = - 0.5 * hyinv2;
                        rhs( INDX(i,j) ) = 0.5 * rhsin( INDX(i,j) ) + ...
                            0.5 * hyinv2 * dirifunc( X(i,j-1), Y(i,j-1) ) + hxinv * neufunc( X(i,j), Y(i,j) );   
                    elseif bdmarker( INDXF(i,j+1) ) == 1
                        % top right Neumann-Dirichlet
                        A( INDX(i,j), INDX(i-1,j) ) = - hxinv2;
                        A( INDX(i,j), INDX(i,j-1) ) = - 0.5 * hyinv2;
                        rhs( INDX(i,j) ) = 0.5 * rhsin( INDX(i,j) ) + ...
                            0.5 * hyinv2 * dirifunc( X(i,j+1), Y(i,j+1) ) + hxinv * neufunc( X(i,j), Y(i,j) );   
                    else
                        % noncorner
                        A( INDX(i,j), INDX(i-1,j) ) = - hxinv2;
                        A( INDX(i,j), INDX(i,j-1) ) = - 0.5 * hyinv2;
                        A( INDX(i,j), INDX(i,j+1) ) = - 0.5 * hyinv2;
                        rhs( INDX(i,j) ) = 0.5 * rhsin( INDX(i,j) ) + hxinv * neufunc( X(i,j), Y(i,j) );
                    end
                elseif j == 1
                    % on bottom side
                    if bdmarker( INDXF(i-1,j) ) == 1
                        % bottom left Neumann-Dirichlet
                        A( INDX(i,j), INDX(i+1,j) ) = - 0.5 * hxinv2;
                        A( INDX(i,j), INDX(i,j+1) ) = - hyinv2;
                        rhs( INDX(i,j) ) = 0.5 * rhsin( INDX(i,j) ) + ...
                            0.5 * hxinv2 * dirifunc( X(i-1,j), Y(i-1,j) ) + hyinv * neufunc( X(i,j), Y(i,j) );   
                    elseif bdmarker( INDXF(i+1,j) ) == 1
                        % bottom right Neumann-Dirichlet
                        A( INDX(i,j), INDX(i-1,j) ) = - 0.5 * hxinv2;
                        A( INDX(i,j), INDX(i,j+1) ) = - hyinv2;
                        rhs( INDX(i,j) ) = 0.5 * rhsin( INDX(i,j) ) + ...
                            0.5 * hxinv2 * dirifunc( X(i+1,j), Y(i-1,j) ) + hyinv * neufunc( X(i,j), Y(i,j) );   
                    else
                        % noncorner
                        A( INDX(i,j), INDX(i-1,j) ) = - 0.5 * hxinv2;
                        A( INDX(i,j), INDX(i+1,j) ) = - 0.5 * hxinv2;
                        A( INDX(i,j), INDX(i,j+1) ) = - hyinv2;
                        rhs( INDX(i,j) ) = 0.5 * rhsin( INDX(i,j) ) + hxinv * neufunc( hx*(i-1), hy*(j-1) );                    
                    end
                elseif j == Ny
                    % on top side
                    if bdmarker( INDXF(i-1,j) ) == 1
                        % top left Neumann-Dirichlet
                        A( INDX(i,j), INDX(i+1,j) ) = - 0.5 * hxinv2;
                        A( INDX(i,j), INDX(i,j-1) ) = - hyinv2;
                        rhs( INDX(i,j) ) = 0.5 * rhsin( INDX(i,j) ) + ...
                            0.5 * hxinv2 * dirifunc( X(i-1,j), Y(i-1,j) ) + hyinv * neufunc( X(i,j), Y(i,j) );   
                    elseif bdmarker( INDXF(i+1,j) ) == 1
                        % top right Neumann-Dirichlet
                        A( INDX(i,j), INDX(i-1,j) ) = - 0.5 * hxinv2;
                        A( INDX(i,j), INDX(i,j-1) ) = - hyinv2;
                        rhs( INDX(i,j) ) = 0.5 * rhsin( INDX(i,j) ) + ...
                            0.5 * hxinv2 * dirifunc( X(i+1,j), Y(i-1,j) ) + hyinv * neufunc( X(i,j), Y(i,j) );   
                    else
                        % noncorner
                        A( INDX(i,j), INDX(i-1,j) ) = - 0.5 * hxinv2;
                        A( INDX(i,j), INDX(i+1,j) ) = - 0.5 * hxinv2;
                        A( INDX(i,j), INDX(i,j-1) ) = - hyinv2;
                        rhs( INDX(i,j) ) = 0.5 * rhsin( INDX(i,j) ) + hyinv * neufunc( hx*(i-1), hy*(j-1) );                    
                    end
                end
                A( INDX(i,j), INDX(i,j) ) = hxinv2 + hyinv2;
            end
        else
            %
            % Dirichlet
            %
            rhs( INDX(i,j) ) = rhsin( INDX(i,j) );
                        
            if bdmarker(INDXF(i-1,j)) == 1
                % left neighbor is Dirichlet    
                rhs( INDX(i,j) ) = rhs( INDX(i,j) ) + hxinv2 * dirifunc( X(i-1,j), Y(i-1,j) );
            else
                A( INDX(i,j), INDX(i-1,j) ) = - hxinv2;
            end
            
            if bdmarker(INDXF(i+1,j)) == 1
                % right neighbor is Dirichet
                rhs( INDX(i,j) ) = rhs( INDX(i,j) ) + hxinv2 * dirifunc( X(i+1,j), Y(i+1,j) );
            else
                A( INDX(i,j), INDX(i+1,j) ) = - hxinv2;
            end
            
            if bdmarker(INDXF(i,j-1)) == 1
                % bottom neighbor is Dirichlet
                rhs( INDX(i,j) ) = rhs( INDX(i,j) ) + hyinv2 * dirifunc( X(i,j-1), Y(i,j-1) );
            else
                A( INDX(i,j), INDX(i,j-1) ) = - hyinv2;
            end
            
            if bdmarker(INDXF(i,j+1)) == 1
                % top neighbor is Dirichlet
                rhs( INDX(i,j) ) = rhs( INDX(i,j) ) + hyinv2 * dirifunc( X(i,j+1), Y(i,j+1) );
            else
                A( INDX(i,j), INDX(i,j+1) ) = - hyinv2;
            end
            A( INDX(i,j), INDX(i,j) ) = 2.0 * hxinv2 + 2.0 * hyinv2;
        end
    end
end

end

