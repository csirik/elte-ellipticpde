function [ imin, imax, jmin, jmax ] = fd2_dofs( Nx, Ny, bdtype )
% Return the DOF grid point index intervals on a 2D grid.
%
% INPUT
% Nx- Number of grid points along the x axis, including the endpoints.
% Ny- Number of grid points along the y axis, including the endpoints.
% bdtype - Four-element vector specifying the type of boundary for
%          boundary edges, in the following order:
%
%          +---3---+
%          |       |
%          4       2
%          |       |
%          +---1---+
%
%          The permitted boundary types are:
% 
%          1 = Dirichlet
%          2 = Neumann
%          E.g. bdype = [1 1 1 1] means pure Dirichlet boundary conditons.
%
% OUTPUT
% imin, imax, jmin, jmax - The bounds for the the grid indexes occupied by
%                          DOFS.
%
% AUTHOR
% Csirik Mihaly, ELTE, csirik@gmail.com
%
% CC0
% To the extent possible under law, the author has waived all copyright and related 
% or neighboring rights to this file.
%

%
% Check input for errors
%
if Nx < 3 || Ny < 3 || any(bdtype ~= 1 & bdtype ~= 2) || length(bdtype) ~= 4
    fprintf( 'fd2_dofs: Invalid parameters specified.\n' );
    return;
end

imin = 1;
imax = Nx;
jmin = 1;
jmax = Ny;

if bdtype(1) == 1
    % bottom is Dirichlet
    jmin = 2;
end

if bdtype(2) == 1
    % right is Dirichlet
    imax = Nx-1;
end

if bdtype(3) == 1
    % top is Dirichlet
    jmax = Ny-1;
end

if bdtype(4) == 1
    % left is Dirichlet
    imin = 2;
end
end

