function [A,rhs] = fd1_lapl_4( a, N, bdtype, varargin )
% Discretize negative second derivative using the fourth order stencil.
%
% fd1_lapl_4( a, N, bdtype )
% fd1_lapl_4( a, N, bdtype, rhs, diri, neu )
%
% Construct 1D finite difference matrix and RHS vector for the negative 
% Laplacian on an equidistant grid with various boundary conditions.
% The fourth order is used for the Laplacian and  fourth order central 
% differencing for the Neumann boundary condtion.
%
% INPUT
% a - Length of the interval.
% N - Number of nodes including the endpoints.
% bdtype - Two-element vector specifying the type of boundary for
%          left and right boundary points, respectively. 
%          The permitted boundary types are:
%          1 = Dirichlet
%          2 = Neumann
%          E.g. bdype = [1 1] means pure Dirichlet boundary conditons.
%
% OPTIONAL INTPUT IF RHS VECTOR IS NEEDED
% rhs - Right-hand side vector
% diri - 2-dimensional vector containing the Dirichlet boundary value
% neu - 2-dimensional vector containing the Neumann boundary value
%
% OUTPUT
% A - The constructed system matrix.
% RHS - The constructed right hand side vector or empty.
%
% AUTHOR
% Csirik Mihaly, ELTE, csirik@gmail.com
%
% CC0
% To the extent possible under law, the author has waived all copyright and related 
% or neighboring rights to this file.
%
A = [];
rhs = [];

%
% Check input for errors
%
Aonly = 0;

if nargin == 3
    Aonly = 1;
    rhs = [];
elseif nargin == 6
    rhs = varargin{1};
    diri = varargin{2};
    neu = varargin{3};
else
	fprintf( 'fd1_lapl_4: Invalid parameters specified.\n' );
    return;
end

if ~Aonly 
    if (length(diri) ~= 2 || length(neu) ~= 2)
        fprintf( 'fd1_lapl: Invalid parameters specified.\n' );
        return;
    end
end

if N < 6 || a <= 0 || any(bdtype ~= 1 & bdtype ~= 2) || length(bdtype) ~= 2
    fprintf( 'fd1_lapl_4: Invalid parameters specified.\n' );
    return;
end

%
% Begin function
%

hinv = ( N - 1 ) / a;
hinv2 = hinv * hinv;

e = ones( N, 1 );
A = spdiags( e*(1/12)*[1 -16 30 -16 1], -2:2, N, N );

if all(bdtype == 1)
    % pure Dirichlet
    A = A(2:N-1, 2:N-1);
    A(1,1:4) = (1/12) * [20 -6 -4 1];
    A(end,end:-1:end-3) = (1/12) * [20 -6 -4 1];
    
    if ~Aonly
        rhs(1) = rhs(1)         + (11/12)*hinv2*diri(1);
        rhs(2) = rhs(2)         - (1/12) *hinv2*diri(1);
        rhs(end-1) = rhs(end-1) - (1/12) *hinv2*diri(2);
        rhs(end) = rhs(end)     + (11/12)*hinv2*diri(2);
    end
elseif any(bdtype == 1) && any(bdtype == 2)
    % mixed
    if bdtype(1) == 1
        % Dirichlet-Neumann
        A = A(2:N, 2:N);
        A(1,1:4) = (1/12) * [20 -6 -4 1];
        A(end,end:-1:end-3) = (1/18) * [ 85 -108 27 -4 ];
        A(end-1,end:-1:end-3) = (1/18) * [ -29 54 -27 2 ];
        
        if ~Aonly
            rhs(1) = rhs(1)         + (11/12)*hinv2*diri(1);
            rhs(2) = rhs(2)         - (1/12) *hinv2*diri(1);
            rhs(end-1) = rhs(end-1) - (6/18) *hinv*neu(2);
            rhs(end) = rhs(end)     + (66/18)*hinv*neu(2);
        end
    else
        % Neumann-Dirichlet
        A = A(1:N-1, 1:N-1);
        A(1,1:4) = (1/18) * [ 85 -108 27 -4 ];
        A(2,1:4) = (1/18) * [ -29 54 -27 2 ];
        A(end,end:-1:end-3) = (1/12) * [20 -6 -4 1];
        
        if ~Aonly
            rhs(1) = rhs(1)         + (66/18)*hinv*neu(1);
            rhs(2) = rhs(2)         - (6/18) *hinv*neu(1);
            rhs(end-1) = rhs(end-1) - (1/12) *hinv2*diri(2);
            rhs(end) = rhs(end)     + (11/12)*hinv2*diri(2);
        end
    end
else
    % pure Neumann
    A(1,1:4) = (1/18) * [ 85 -108 27 -4 ];
    A(2,1:4) = (1/18) * [ -29 54 -27 2 ];
    A(end,end:-1:end-3) = (1/18) * [ 85 -108 27 -4 ];
    A(end-1,end:-1:end-3) = (1/18) * [ -29 54 -27 2 ];
    
    if ~Aonly
        rhs(1) = rhs(1)         + (66/18)*hinv*neu(2);
        rhs(2) = rhs(2)         - (6/18) *hinv*neu(2);
        rhs(end-1) = rhs(end-1) - (6/18) *hinv*neu(2);
        rhs(end) = rhs(end)     + (66/18)*hinv*neu(2);
    end
end

hinv = ( N - 1 ) / a;
A = hinv*hinv * A;

end

