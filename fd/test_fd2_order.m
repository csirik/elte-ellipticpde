clear all

% Unit square domain
a = 1;

rhsfunc = @(x,y) (5*pi*pi)*sin(2*pi*x)*sin(pi*y);
dirifunc = @(x,y) 0;
neufunc = @(x,y) -pi*sin(2*pi*x);
exact = @(x,y) sin(2*pi*x)*sin(pi*y);
bdtype = [2 1 1 1];

% Initial mesh size
N = 8;
% Number of refinements
K = 6;

% Result vectors
NN = zeros(K,1);
hh = zeros(K,1);
err = zeros(K,1);
 
for k=1:K
    fprintf('%d/%d\n', k, K);
    NN(k) = N;
    hh(k) = a/(N-1);
    

    rhs = fd2_eval( a, a, N, N, bdtype, rhsfunc );
    [Ah,bh] = fd2_lapl( a, a, N, N, bdtype, rhs, dirifunc, neufunc );

    U = Ah\bh;

    u = fd2_lex_to_grid( U, N, N, bdtype );
    u = fd2_extbd( u, a, a, N, N, bdtype, dirifunc );

    [X, Y] = ndgrid( a*(0:N-1)/(N-1), a*(0:N-1)/(N-1) );
    ERR = u-arrayfun( exact, X, Y );
    
    err(k) = norm( ERR(:), inf );
    
    N = 2*N;
end

%surf( X, Y, u )
%return

% Line fitting for order estimation
p = polyfit(log(hh), log(err),1);
pp=exp(p(1)*log(hh)+p(2));
loglog( hh, err, 'o', hh, pp, '-');
title(sprintf('$\\alpha=%.4f$, $C=%.4f$',p(1),exp(p(2))),'Interpreter','LaTex')
axis equal

% Generate LaTeX table
input.data = [NN hh err];
input.tableColLabels = {'$N$','$h$','$\|u_h-\overline{u}_h\|_\infty$'};
input.dataFormatMode = 'column';
input.dataFormat = {'%d',1,'%.8f',1,'%.4e',1};
input.tableColumnAlignment = 'c';
input.tableBorders = 1;
input.tableCaption = 'A $-\lapl u=5\pi^2\sin(2\pi x)\sin(\pi y)$ homogén Dirichlet-peremértékfeladat 5-pontos véges differenciás diszkretizációja.';

latex = latexTable(input);
