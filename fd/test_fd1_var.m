clear all

a = 1;

if 0
    exact = @(x) x*(1-x);
    exactder = @(x) 1-2*x;
    coefffunc = @(x) 1+x*x;

    rhsfunc = @(x) 6*x*x - 2*x + 2;
end

if 1
    exact = @(x) exp(x);
    exactder = @(x) exp(x);
    
    coefffunc = @(x) 1+x*x;
    
    rhsfunc = @(x) -exp(x)*(x+1)*(x+1);
end

if 0
    exact = @(x) exp(x);
    exactder = @(x) exp(x);

    coefffunc = @(x) 1;
    
    rhsfunc = @(x) -exp(x);
end


bdtype = [1 2];
diri = [exact(0) exact(1)];
neu = [-exactder(0) exactder(1)];

% Initial mesh size
N = 4;
% Number of refinements
K = 10;

% Result vectors
NN = zeros(K,1);
hh = zeros(K,1);
err = zeros(K,1);

for k=1:K
    NN(k) = N;
    hh(k) = a/(N-1);

    % Calculate diffusion coefficent vector
    coeff = arrayfun( coefffunc, (0.5+(-1:N-1))/(N-1) )';

    % Assemble
    [Ah,bh] = fd1_lapl_var( a, N, bdtype, coeff, rhsfunc, diri, neu );
    
    % Solve
    u = Ah\bh;

    % Extend to boundary
    uext = fd1_extbd( u, bdtype, diri );
    
    % Calculate error
    err(k) = norm( uext - arrayfun( exact, hh(k)*(0:N-1) )', inf );

    N = 2*N;
end

% % 
% % bh = arrayfun( rhsfunc, h*(1:N-2) )';
% % bh(1) = bh(1) + (N-1)*(N-1)*coeff(1)*exact(0);
% % bh(N-2) = bh(N-2) + (N-1)*(N-1)*coeff(N-1)*exact(a);
% 
% u = Ah\bh;
% u = [ exact(0); u ];
% figure(1)
% plot( (0:N/2-1)/(N/2-1), uext )
% % 
% 
% norm( u-arrayfun( exact, h*(0:N-1) )', inf )

%figure(2)
% Line fitting for order estimation
p = polyfit(log(hh), log(err),1);
pp=exp(p(1)*log(hh)+p(2));
loglog( hh, err, 'o', hh, pp, '-');
title(sprintf('$\\alpha=%.4f$, $C=%.4f$',p(1),exp(p(2))),'Interpreter','LaTex')
axis equal


% 
% Generate LaTeX table
input.data = [NN hh err];
input.tableColLabels = {'$N$','$h$','$\|u_h-\overline{u}_h\|_\infty$'};
input.dataFormatMode = 'column';
input.dataFormat = {'%d',1,'%.6f',1,'%.4e',1};
input.tableColumnAlignment = 'c';
input.tableBorders = 1;
input.tableCaption = [ 'A $-((1+x^2)u'')''=-e^x(1+x)^2$, $-u''(0)=-1$, $u(1)=e$ Neumann-Dirichlet-peremértékfeladat' ...
' diszkretizációjának abszolút hibája (Pontos megoldás: $e^x$). (Generálta: \lstinline|test_fd1_var.m|)'];
input.tableLabel = 'fd1';
latex = latexTable(input);

