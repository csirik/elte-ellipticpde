function [A,rhs] = fd1_lapl_neq( X, bdtype, varargin )
% Discretize negative second derivative on a non-equidistant grid.
% 
% fd1_lapl_neq( a, N, bdtype )
% fd1_lapl_neq( a, N, bdtype, rhs, diri, neu )
%
% Construct 1D finite difference matrix and rhs vectorfor the negative Laplacian 
% a non-equidistant grid with various boundary conditions using the 
% grid point approach.
%
% INPUT
% X - An N dimensional vector specifying the grid points.
% bdtype - Two-element vector specifying the type of boundary for
%          left and right boundary points, respectively. 
%          The permitted boundary types are:
%          1 = Dirichlet
%          2 = Neumann
%          E.g. bdype = [1 1] means pure Dirichlet boundary conditons.
%
% OPTIONAL INTPUT IF RHS VECTOR IS NEEDED
% rhsfunc - Right-hand side function of Poisson's equation
% diri - 2-dimensional vector containing the Dirichlet boundary values
% neu - 2-dimensional vector containing the Neumann boundary values
%
% OUTPUT
% A - The constructed system matrix.
% RHS - The constructed right hand side vector or empty.
%
% AUTHOR
% Csirik Mihaly, ELTE, csirik@gmail.com
%
% CC0
% To the extent possible under law, the author has waived all copyright and related 
% or neighboring rights to this file.
%
N = length( X );

A = [];
rhs = [];

%
% Check input for errors
%

Aonly = 0;

if nargin == 2
    Aonly = 1;
elseif nargin == 5
    rhsfunc = varargin{1};
    diri = varargin{2};
    neu = varargin{3};
else
	fprintf( 'fd1_lapl_neq: Invalid parameters specified.\n' );
    return;
end

if ~Aonly 
    if (length(diri) ~= 2 || length(neu) ~= 2)
        fprintf( 'fd1_lapl_neq: Invalid parameters specified.\n' );
        return;
    end
end


if N < 3 || any(bdtype ~= 1 & bdtype ~= 2) || length(bdtype) ~= 2
    fprintf( 'fd1_asm_neq_neq: Invalid parameters specified.\n' );
    return;
end;


%
% Begin function
%

% Grid spacings
H = (X(2:N) - X(1:N-1))';
Hinv = 1 ./ H(1:N-1);

D = spdiags( 2./( H(2:N-1) + H(1:N-2) ), 0, N-2, N-2);
A = D * spdiags( [ -Hinv(2:end) Hinv(1:end-1)+Hinv(2:end) -Hinv(1:end-1) ], -1:1, N-2, N-2 );

if all(bdtype == 1)
    % pure Dirichlet
    % NO modification required to A
    if ~Aonly
        rhs = arrayfun( rhsfunc, X(2:N-1) )';
        rhs(1) = rhs(1) + D(1,1) * Hinv(1) * diri(1);
        rhs(end) = rhs(end) + D(end,end) * Hinv(end) * diri(2);
    end
elseif any(bdtype == 1) && any(bdtype == 2)
    % mixed
    if bdtype(1) == 1
        % Dirichlet-Neumann
        A = [ A [zeros(N-3,1); -Hinv(end)*D(end,end) ];
              zeros(1, N-3) -Hinv(end)*Hinv(end) Hinv(end)*Hinv(end) ];
        if ~Aonly
            rhs = arrayfun( rhsfunc, X(2:N) )';
            rhs(1) = rhs(1) + D(1,1) * Hinv(1) * diri(1);
            rhs(end) = rhs(end) +  2 * Hinv(end) * neu(2);
        end
    else
        % Neumann-Dirichlet
        A = [ Hinv(1)*Hinv(1) -Hinv(1)*Hinv(1) zeros(1, N-3); ...
              [-Hinv(1)*D(1,1); zeros(N-3,1)]  A ];
        if ~Aonly
            rhs = arrayfun( rhsfunc, X(1:N-1) )';
            rhs(1) = rhs(1) + 2 * Hinv(1) * neu(1);
            rhs(end) = rhs(end) + D(end) * Hinv(end) * diri(2);
        end
    end
else
    % pure Neumann
    A = [ Hinv(1)*Hinv(1) -Hinv(1)*Hinv(1) zeros(1, N-2); ...
          [-Hinv(1)*D(1,1); zeros(N-3,1)]  A [zeros(N-3,1); -Hinv(end)*D(end,end)]; ...
          zeros(1, N-2) -Hinv(end)*Hinv(end) Hinv(end)*Hinv(end) ];
    if ~Aonly
    	rhs = arrayfun( rhsfunc, X )';
        rhs(1) = rhs(1) + 2 * Hinv(1) * neu(1);
        rhs(end) = rhs(end) +  2 * Hinv(end) * neu(2);
    end
end
    
end