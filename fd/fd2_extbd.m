function uext = fd2_extbd( u, a, b, Nx, Ny, bdtype, dirifunc )
% Extend a grid function defined only on the free grid points 
% (degrees of freedom) to the whole grid.
%
% INPUT
% u - Grid function defined on free grid points.
% a - Length along the x axis of the rectangle.
% b - Length along the y axis of the rectangle.
% Nx- Number of grid points along the x axis, including the endpoints.
% Ny- Number of grid points along the y axis, including the endpoints.
% bdtype - Four-element vector specifying the type of boundary for
%          boundary edges, in the following order:
%
%          +---3---+
%          |       |
%          4       2
%          |       |
%          +---1---+
%
%          The permitted boundary types are:
% 
%          1 = Dirichlet
%          2 = Neumann
%          E.g. bdype = [1 1 1 1] means pure Dirichlet boundary conditons.
% dirifunc - bivariate Dirichlet function.
%
% OUTPUT
% uext - The grid function u extended to the whole grid.
%
% AUTHOR
% Csirik Mihaly, ELTE, csirik@gmail.com
%
% CC0
% To the extent possible under law, the author has waived all copyright and related 
% or neighboring rights to this file.
%

hx = a / (Nx-1);
hy = b / (Ny-1);

% all grid points
[X,Y] = ndgrid( hx*(0:Nx-1), hy*(0:Ny-1) );
% evaluate dirifunc and neufunc on the full grid
% therefore you can index it using the absolute indices below
uext = arrayfun( dirifunc, X, Y );
[ imin, imax, jmin, jmax ] = fd2_dofs( Nx, Ny, bdtype );

uext( imin:imax, jmin:jmax ) = u;

end

