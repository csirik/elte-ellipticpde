clear all
a = 1;
b = 1;

Nx = 50;
Ny = 50;


hx = a / (Nx-1);
hy = b / (Ny-1);

% RHS function
%rhsfunc = @(x,y) -4;
rhsfunc = @(x,y) (5*pi*pi)*sin(2*pi*x)*sin(pi*y);
%rhsfunc = @(x,y) 0;
%rhsfunc = @(x,y) double((x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) < 0.01);


% Dirichlet boundary
%dirifunc = @(x,y) (x-0.5)*(x-0.5) + (y-0.5)*(y-0.5);
%dirifunc = @(x,y) x+y;
dirifunc = @(x,y) 0;
%dirifunc = @(x,y) (0.5*sign(sin(10*pi*(x+0.5)))+0.5);
neufunc = @(x,y) -2*(y-0.5);
neufunc = @(x,y) 0;
neufunc = @(x,y) -pi*sin(2*pi*x)*cos(pi*y);

%dirifunc = @(x,y) sin(pi*x)+sin(pi*y);

% Exact solution
exact = @(x,y) sin(2*pi*x)*sin(pi*y);
%exact = @(x,y) (x-0.5)*(x-0.5) + (y-0.5)*(y-0.5);

bdtype = [1 1 1 1];

rhs = fd2_eval( a, b, Nx, Ny, bdtype, rhsfunc );
[Ah,Fh] = fd2_lapl( a, b, Nx, Ny, bdtype, rhs, dirifunc, neufunc );

%condest(Ah)

U = Ah\Fh;

u = fd2_lex_to_grid( U, Nx, Ny, bdtype );

u = fd2_extbd( u, a, b, Nx, Ny, bdtype, dirifunc );

%plot_fd2mesh( a, b, Nx, Ny, bdtype )


[X, Y] = ndgrid( hx*(0:Nx-1), hy*(0:Ny-1) );
[XINT, YINT] = ndgrid( hx*(1:Nx-2), hy*(1:Ny-2) );

ERR = u-arrayfun( exact, X, Y );
%surf( X, Y, arrayfun( exact, X, Y ) )

hold on
%surf( X, Y, u )
if 0
plot3( X, Y, u, '.', 'Color', 'Blue' )
elseif 1
surf( X, Y, u,'FaceLighting','phong','FaceColor','interp',...
      'EdgeColor','none','AmbientStrength',0.5 )
material dull 
camlight left
shading interp
colormap hsv
elseif 0
surf( X, Y, u )
end

view(30, 30)

%contour( X, Y, u, 50 )
%[ux,uy] = fd2_grad( u, a, b, Nx, Ny );
%quiver( XINT, YINT, ux, uy, 'LineWidth', 0.5 )
%surf( X, Y, ERR )
hold off

fprintf('errmax=%e\n', max(max(ERR)));
%axis equal
%zlim([0 1])
%surf( Xfull, Yfull, arrayfun( dirifunc, Xfull, Yfull ) )

