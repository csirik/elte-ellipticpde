clear all

% Unit interval domain
a = 1;
L = 1000;

bdtype = [1 1];

% RHS function
rhsfunc = @(x) 2 * L^3 * ( x - 0.5 ) / ( ( 1 + L^2 * ( x - 0.5 )^2 )^2 );

% Exact solution
exact = @(x) atan( L*( x - 0.5 ) );
exactder = @(x) L/( 1+ L^2*( x - 0.5 )^2 );

% Boundary conditions
diri = [exact(0) exact(a)];
neu = [-exactder(0) exactder(a)];

N = 101;
h = a/(N-1);

rhs = fd1_eval( a, N, bdtype, rhsfunc );
[Ah,Fh] = fd1_lapl( a, N, bdtype, rhs, diri, neu );

u = Ah\Fh;

uext = fd1_extbd( u, bdtype, diri );

err = uext-arrayfun( exact, h*(0:N-1))';
fprintf('absolute error: %.4e\n', norm(err,inf))

XX = h*(0:N-1);

if 1

hold on 
plot( XX, uext, 'o' )
plot( XX, arrayfun( exact, h*(0:N-1) )', 'r' );
hold off

else

hold on 
plot( XX, err, 'ro' );
hold off    
    
end