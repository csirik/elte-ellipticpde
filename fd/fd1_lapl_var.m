function [A,rhs] = fd1_lapl_var( a, N, bdtype, coeff, varargin )
% Discretize the operator -(k(x)u')' using a second order stencil.
% 
% fd1_lapl_var( a, N, bdtype )
% fd1_lapl_var a, N, bdtype, rhs, diri, neu )
%
% Construct 1D finite difference matrix and rhs vector for the negative Laplacian 
% with variable coefficents, i.e. -(ku')' on an equidistant grid with 
% various boundary conditions using the (-1, 2, -1) stencil.
%
% INPUT
% a - Length of the interval.
% N - Number of nodes including the endpoints.
% bdtype - Two-element vector specifying the type of boundary for
%          left and right boundary points, respectively. 
%          The permitted boundary types are:
%          1 = Dirichlet
%          2 = Neumann
%          E.g. bdype = [1 1] means pure Dirichlet boundary conditons.
% coeff - An N+1 dimensional vector specifying the values of the
%         coefficient function at the midpoints of the cells in the grid
%         including the auxiliary cells at the ends.
%
% OPTIONAL INTPUT IF RHS VECTOR IS NEEDED
% rhsfunc - Right-hand side function of Poisson's equation
% diri - 2-dimensional vector containing the Dirichlet boundary values
% neu - 2-dimensional vector containing the Neumann boundary values
%
% OUTPUT
% A - The constructed system matrix.
% RHS - The constructed right hand side vector or empty.
%
% AUTHOR
% Csirik Mihaly, ELTE, csirik@gmail.com
%
% CC0
% To the extent possible under law, the author has waived all copyright and related 
% or neighboring rights to this file.
%

A = [];
rhs = [];

%
% Check input for errors
%

Aonly = 0;

if nargin == 4
    Aonly = 1;
    rhs = [];
elseif nargin == 7
    rhsfunc = varargin{1};
    diri = varargin{2};
    neu = varargin{3};
else
	fprintf( 'fd1_lapl_var: Invalid parameters specified.\n' );
    return;
end


%
% Check input for errors
%
if ~Aonly 
    if (length(diri) ~= 2 || length(neu) ~= 2)
        fprintf( 'fd1_lapl_var: Invalid parameters specified.\n' );
        return;
    end
end

if N < 3 || a <= 0 || length(coeff) ~= N+1 || ...
        any(bdtype ~= 1 & bdtype ~= 2) || length(bdtype) ~= 2
    fprintf( 'fd1_lapl_var: Invalid parameters specified.\n' );
    return;
end;

%
% Begin function
%

h = a / ( N - 1 );
hinv = ( N - 1 ) / a;
hinv2 = hinv * hinv;

A = spdiags( [-coeff(3:N) coeff(3:N)+coeff(2:N-1) -coeff(2:N-1)], -1:1, N-2, N-2 );

if all(bdtype == 1)
    % pure Dirichlet
    % NO modification to A
    if ~Aonly
        rhs = arrayfun( rhsfunc, h*(1:N-2) )';
        rhs(1) = rhs(1) + hinv2 * coeff(3) * diri(1);
        rhs(end) = rhs(end) + hinv2 * coeff(N) * diri(2);
    end
elseif any(bdtype == 1) && any(bdtype == 2)
    % mixed
    if bdtype(1) == 1
        % Dirichlet-Neumann
        A = [ A [zeros(N-3,1); -coeff(N)];
              zeros(1, N-3) -coeff(N) coeff(N) ];
        if ~Aonly
            rhs = arrayfun( rhsfunc, h*(1:N-1) )';
            rhs(1) = rhs(1) + hinv2 * coeff(3) * diri(1);
            rhs(end) = coeff(N) / ( coeff(N) + coeff(N+1) ) * rhs(end) + ...
                2 * coeff(N) * coeff(N+1) / ( coeff(N) + coeff(N+1) ) * hinv * neu(2);
        end
    else
        % Neumann-Dirichlet
        A = [ coeff(2) -coeff(2) zeros(1, N-3); ...
              [-coeff(2); zeros(N-3,1)]  A ];
        if ~Aonly
            rhs = arrayfun( rhsfunc, h*(0:N-2) )';
            rhs(1) = coeff(2) / ( coeff(1) + coeff(2) ) * rhs(1) + ...
                2 * ( coeff(1) * coeff(2) / ( coeff(1) + coeff(2)) ) * hinv * neu(1);
            rhs(end) = rhs(end) + hinv2 * coeff(N) * diri(2);
        end
    end
else
    % pure Neumann
    A = [ coeff(2) -coeff(2) zeros(1, N-2); ...
          [-coeff(2); zeros(N-3,1)]  A [zeros(N-3,1); -coeff(N)]; ...
          zeros(1, N-2) -coeff(N) coeff(N) ];
    if ~Aonly
        rhs = arrayfun( rhsfunc, h*(0:N-1) )';
        rhs(1) = coeff(3) / ( coeff(1) + coeff(2) ) * rhs(1) + ...
                2 * ( coeff(1) * coeff(3) / ( coeff(1) + coeff(2)) ) * hinv * neu(1);
        rhs(end) = coeff(N-2) / ( coeff(N-1) + coeff(N) ) * rhs(end) + ...
                2 * coeff(N-2) * coeff(N) / ( coeff(N-1) + coeff(N) ) * hinv * neu(2);
    end
end

A = hinv*hinv * A;

end