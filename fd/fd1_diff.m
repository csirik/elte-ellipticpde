function [A,rhs] = fd1_diff( a, N, bdtype, varargin )
% Discretize first derivative using central differencing.
%
% fd1_diff( a, N, bdtype )
% fd1_diff( a, N, bdtype, rhsin, diri, neu )
%
% Construct 1D finite difference matrix and optionally the RHS vector for the first derivative on an equidistant grid with various boundary conditions. Second order central differencing is used.
%
% INPUT
% a - Length of the interval.
% N - Number of nodes including the endpoints.
% bdtype - Two-element vector specifying the type of boundary for
%          left and right boundary points, respectively. 
%          The permitted boundary types are:
%          1 = Dirichlet
%          2 = Neumann
%          E.g. bdype = [1 1] means pure Dirichlet boundary conditons.
%
% OPTIONAL INTPUT IF RHS VECTOR IS NEEDED
% rhsin - Initial rhs vector
% diri - 2-dimensional vector containing the Dirichlet boundary values
% neu - 2-dimensional vector containing the Neumann boundary values
%
% OUTPUT
% A - The constructed system matrix.
% rhs - The constructed right hand side vector or empty.
%
% AUTHOR
% Csirik Mihaly, ELTE, csirik@gmail.com
%
% CC0
% To the extent possible under law, the author has waived all copyright and related 
% or neighboring rights to this file.
%
A = [];
rhs = [];

%
% Check input for errors
%

Aonly = 0;

if nargin == 3
    Aonly = 1;
    rhs = [];
elseif nargin == 6
    rhsfunc = varargin{1};
    diri = varargin{2};
    neu = varargin{3};
else
	fprintf( 'fd1_diff Invalid parameters specified.\n' );
    return;
end


if N < 3 || a <= 0 || any(bdtype ~= 1 & bdtype ~= 2) || length(bdtype) ~= 2
    fprintf( 'fd1_diff: Invalid parameters specified.\n' );
    return;
end

if ~Aonly 
    if (length(diri) ~= 2 || length(neu) ~= 2)
        fprintf( 'fd1_diff: Invalid parameters specified.\n' );
        return;
    end
end

%
% Begin function
%

hinvhalf = 0.5 * ( N - 1 ) / a;

A = tridiag( [-1 0 1], N );

if all(bdtype == 1)
    % pure Dirichlet
    A = A(2:N-1, 2:N-1);
    
    if ~Aonly
        rhs(1) = rhs(1) + hinvhalf * diri(1);
        rhs(end) = rhs(end) + hinvhalf * diri(2);
    end
elseif any(bdtype == 1) && any(bdtype == 2)
    % mixed
    if bdtype(1) == 1
        % Dirichlet-Neumann
        A = A(2:N, 2:N);
        A(N-1, :) = [ zeros(1, N-3) -1 1 ];
        
        if ~Aonly
            rhs = arrayfun( rhsfunc, h*(1:N-1) )';
            rhs(1) = rhs(1) + hinv2 * diri(1);
            rhs(end) = 0.5 * rhs(end) + hinvhalf * neu(2);
        end
    else
        % Neumann-Dirichlet
        A = A(1:N-1, 1:N-1);
        A(1, :) = [ 1 -1 zeros(1, N-3) ];
        
        if ~Aonly
            rhs = arrayfun( rhsfunc, h*(0:N-2) )';
            rhs(1) = 0.5 * rhs(1) + hinvhalf * neu(1);
            rhs(end) = rhs(end) + hinv2 * diri(2);
        end
    end
else
    % pure Neumann
    A(1, :) = [ 1 -1 zeros(1, N-2) ];
    A(N, :) = [ zeros(1, N-2) -1 1 ];
    
    if ~Aonly
        rhs = arrayfun( rhsfunc, h*(0:N-1) )';
        rhs(1) = 0.5 * rhs(1) + hinvhalf * neu(1);
        rhs(end) = 0.5 * rhs(end) + hinvhalf * neu(2);
    end
end

A = hinvhalf * A;
end

