function [ rhsfunc, dirifunc, neufunc, exactfunc ] = atp2_lapl_rect_eigen( a, b, bdtype, k, l )

if any(bdtype == [1 1 1 1])
    exactfunc = @(x,y) sin( pi*k*x/a ) * sin( pi*l*y/b );
    rhsfunc = @(x,y) pi*pi*( k^2/a^2 + l^2/b^2 ) * sin( pi*k*x/a ) * sin( pi*l*y/b );
    dirifunc = @(x,y) 0;
    neufunc = @(x,y) 0;
end

end

