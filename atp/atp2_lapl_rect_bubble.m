function [ rhsfunc, dirifunc, neufunc, exactfunc ] = atp2_lapl_rect_bubble( xmin, xmax, ymin, ymax, p )

exactfunc = @(x,y) 2^(4*p) * (x-xmin)^p * (xmax-x)^p * (y-ymin)^p * (ymax-y)^p;
% Generated with:
% syms x y p xmin xmax ymin ymax
% exactfunc(x,y) = 2^(4*p) * (x-xmin)^p * (xmax-x)^p * (y-ymin)^p * (ymax-y)^p;
% -laplacian(exactfunc, [x y])
rhsfunc = @(x,y) 2*2^(4*p)*p^2*(xmax - x)^p*(x - xmin)^p*(ymax - y)^(p - 1)*(y - ymin)^(p - 1) + ...
    2*2^(4*p)*p^2*(xmax - x)^(p - 1)*(x - xmin)^(p - 1)*(ymax - y)^p*(y - ymin)^p - ...
    2^(4*p)*p*(xmax - x)^p*(x - xmin)^p*(ymax - y)^p*(y - ymin)^(p - 2)*(p - 1) - ...
    2^(4*p)*p*(xmax - x)^p*(x - xmin)^p*(ymax - y)^(p - 2)*(y - ymin)^p*(p - 1) - ...
    2^(4*p)*p*(xmax - x)^p*(x - xmin)^(p - 2)*(ymax - y)^p*(y - ymin)^p*(p - 1) - ...
    2^(4*p)*p*(xmax - x)^(p - 2)*(x - xmin)^p*(ymax - y)^p*(y - ymin)^p*(p - 1);
dirifunc = @(x,y) 0;
neufunc = @(x,y) 0; % TODO
end

