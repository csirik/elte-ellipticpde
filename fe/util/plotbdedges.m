function plotbdedges( verts, edges, trineigh, edgemarker )

cmap = hsv(size(edges, 1)); 

char = ['D' 'N'];
col = [1 0 0; 0 0 1];

for e=1:size(edges, 1)
    if trineigh(e,2) == 0
        X = [verts( edges(e,1), 1 );
             verts( edges(e,2), 1 ) ];
        Y = [verts( edges(e,1), 2 );
             verts( edges(e,2), 2 ) ];
        if isempty(edgemarker)
            plot( X, Y,'Color','k','LineWidth',1.5 )
        else
            plot( X, Y, '-s','Color',col(edgemarker(e),:),'LineWidth',2.5 )
            text( (X(1)+X(2))/2, (Y(1)+Y(2))/2, char(edgemarker(e)), 'Color', col(edgemarker(e),:), 'FontWeight', 'Bold' )
        end
    end
end

end

