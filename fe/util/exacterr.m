function [ err ] = exacterr( verts, tris, U, exact )
err = U - arrayfun( exact, verts(:,1), verts(:,2) );


