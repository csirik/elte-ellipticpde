function plotbasis( verts, tris, i )

if length(i) == 1
    u = zeros( 1, length(verts) );
    u(i) = 1;
    i = u;
end

hold on
axis equal 

trimesh(tris, verts(:,1), verts(:,2));
plotvertstris( verts, tris );
%plotbdedges( fine_verts, fine_edges, fine_trineigh, edgemarker )

trisurf(tris, verts(:,1), verts(:,2), i)

hold off

end

