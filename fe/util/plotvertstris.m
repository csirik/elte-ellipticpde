function plotvertstris( verts, tris )

for v=1:size(verts, 1)
    plot( verts(v,1), verts(v,2), '-o', 'LineWidth',2.5 )
    text( verts(v,1), verts(v,2), num2str(v), 'FontSize', 12, 'FontWeight', 'Bold' )
end

for t=1:size(tris, 1)
    X = ( verts(tris(t,1),1) + verts(tris(t,2),1) + verts(tris(t,3),1) )/3.0;
    Y = ( verts(tris(t,1),2) + verts(tris(t,2),2) + verts(tris(t,3),2) )/3.0;
    text( X, Y, num2str(t), 'FontSize', 12, 'FontWeight', 'Bold', 'Color', 'Green' )
end

end

