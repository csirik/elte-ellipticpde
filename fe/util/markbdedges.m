function edgemarker = markbdedges( verts, edges, trineigh, edgemarker, poly, bdtype )

if isempty(edgemarker)
    edgemarker = zeros( size(edges, 1), 1 );
end

for e=1:size(edges, 1)
    if trineigh(e,2) == 0
        edgebary = [0.5*(verts( edges(e,1), 1 ) + verts( edges(e,2), 1 ));...
                    0.5*(verts( edges(e,1), 2 ) + verts( edges(e,2), 2))];
        if isempty(poly)
            edgemarker(e) = bdtype;
        elseif inpoly( [edgebary(1), edgebary(2)], poly ) 
           edgemarker(e) = bdtype;
        end
    end
end

end

