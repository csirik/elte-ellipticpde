function bary= tribary( verts, tris )

bary = ( verts(tris(:,1),:) + verts(tris(:,2),:) + verts(tris(:,3),:) )/3;

end

