%
% fe2_stiff_aniso_d - Dense-assemble anisotropic first-order finite element stiffness matrix
%               SLOW -- USE THE VECTORIZED VERSION!
%
% INPUT
% verts       - Nx2 array of points, i.e. nodes of the triangle mesh
% tris        - Mx3 array of indices into verts specifying the triangles
% B           - 2x2 constant coefficient matrix
%
% OUTPUT
%             - A the assembled extended stiffness matrix
%             To select the actual A use A(~marker,~marker)
%
% AUTHORS
% Csirik Mihaly, ELTE
%
function A = fe2_stiff_aniso_d( verts, tris, B )

% Number of elements
nel = size( tris, 1 );
% Number of vertices (nodes)
nv = size( verts, 1 );
% Output matrix
A = zeros( nv, nv );

% Derivatives of local shape functions on the reference element
Dlambda = [ -1 1 0;
            -1 0 1];

%
% Assemble element-wise
%
for el=1:nel
    p = verts( tris( el, : ), : ); % vertices
    Aff = [ p( 2, : ) - p( 1, : ); p( 3, : ) - p( 1, :) ]; % affine map matrix
    iAff = inv( Aff );
    jac = abs( det( Aff ) );
    
    for vi=1:3
        for vj=1:3
           A(tris( el, vi ),tris( el, vj )) = A(tris( el, vi ),tris( el, vj )) + ...
               0.5 * jac * ( B*iAff*Dlambda(:,vi) )' * ( iAff*Dlambda(:,vj) );
        end
    end
end

