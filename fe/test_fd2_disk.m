%
% First order 2D finit element demo
%
% Pure Dirichlet problem for the Poisson equation with exact solution 
% u(x,y) = x^2 + y^2.
%

clear all
close all

addpath('mesh2d')
addpath('util')

%
% Generate mesh: unit disk B(0,1)
%
dtheta = pi/10;
theta  = (-pi:dtheta:pi-dtheta)';
node   = [cos(theta) sin(theta)];

hdata = [];
hdata.hmax = 0.1;
options = [];
options.output = false;

[verts,tris] = mesh2d(node, [], hdata, options);
[edges, triedges, trineigh, boundary] = connectivity(verts, tris);

% Mark all boundary edges Dirichlet
edgemarker = [];
edgemarker = markbdedges( verts, edges, trineigh, edgemarker, [], 1 );

%
% Set up problem
%
rhsfunc = @(x,y) -4;
dirifunc = @(x,y) 1;
neufunc = @(x,y) 0;
exactfunc = @(x,y) x^2 + y^2;

%
% Assembly
%
A = fe2_stiff_v( verts, tris );
F = fe2_load_v( verts, tris, quad2_3point(), rhsfunc );
[U,F,dof,nondof] = fe2_boundary( verts, edges, edgemarker, A, F, dirifunc, neufunc );

% Solve
U(dof) = A(dof,dof)\F(dof);

ERR = exacterr(verts, tris, U, exactfunc );
norm(ERR, 'inf')

% Plot
hold on
Du = fe2_grad( verts, tris, U );
bary = tribary( verts, tris );

scalegrad = 1000;

quiver(bary(:,1), bary(:,2), scalegrad*Du(:,1), scalegrad*Du(:,2))

%trisurf(tris, verts(:,1), verts(:,2), U)
trimesh(tris, verts(:,1), verts(:,2), U)


%tricontour(verts, tris, U, 100)
hold off