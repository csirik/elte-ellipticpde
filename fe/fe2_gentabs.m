clear all

lambda{1} = @(x,y) x.^0 - x - y;
lambda{2} = @(x,y) x;
lambda{3} = @(x,y) y;

for i=1:3
    phi{i} = @(x,y) lambda{i}(x,y)*( 2*lambda{i}(x,y) - 1 );
end
% phi12
phi{4} = @(x,y) 4*lambda{1}(x,y)*lambda{2}(x,y);
% phi23
phi{5} = @(x,y) 4*lambda{2}(x,y)*lambda{3}(x,y);
% phi13
phi{6} = @(x,y) 4*lambda{1}(x,y)*lambda{3}(x,y);

Dphi = cell(6, 2);

Dphi{1}{1} = @(x,y) -3*x.^0+4*x+4*y;
Dphi{1}{2} = @(x,y) -3*x.^0+4*x+4*y;
Dphi{2}{1} = @(x,y) 4*x-x.^0;
Dphi{2}{2} = @(x,y) 0*x.^0;
Dphi{3}{1} = @(x,y) 0*x.^0;
Dphi{3}{2} = @(x,y) 4*y-x.^0;
Dphi{4}{1} = @(x,y) 4*(x.^0-2*x-y);
Dphi{4}{2} = @(x,y) -4*x;
Dphi{5}{1} = @(x,y) 4*y;
Dphi{5}{2} = @(x,y) 4*x;
Dphi{6}{1} = @(x,y) -4*y;
Dphi{6}{2} = @(x,y) 4*(x.^0-2*y-x);

intdphidphi = zeros(2,2,6,6);

for i=1:2
    for j=1:2
        for k=1:6
            for l=1:6
                dphidphi = @(x,y)  Dphi{k}{i}(x,y) .* Dphi{l}{j}(x,y); 
                intdphidphi(i,j,k,l) = quad2d( dphidphi, 0, 1, 0, @(x) 1-x );
            end
        end
    end
end

format long
r = reshape( intdphidphi, [36 4] )
isequal( intdphidphi, reshape( r, [2 2 6 6] ) ) 


