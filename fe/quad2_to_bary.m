function [ lambda ] = quad2_to_bary( XW )

nxw = size( XW, 1 );
lambda = zeros( nxw, 3 );

lambda(:,1) = ones(nxw, 1) - XW(:,1) - XW(:,2);
lambda(:,2) = XW(:,1);
lambda(:,3) = XW(:,2);

end

