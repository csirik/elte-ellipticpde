%
% First order 2D finite element demo
%
% Pure Dirichlet problem for the Poisson equation with exact solution 
%

clear all
close all

addpath('mesh2d')
addpath('util')
addpath('../atp')

k = 3;
l = 5;

%
% Generate mesh: L-shaped domain [0,2]x[0,2]\[1,2]x[1,2]
%
node = [
     0   0
     2   0
     2   1
     1   1
     1   2
     0   2
         ];
hdata = [];
hdata.hmax = 0.1;
options = [];
options.output = false;

[verts,tris] = mesh2d(node, [], hdata, options);
[edges, triedges, trineigh, boundary] = connectivity(verts, tris);

% Mark all boundary edges Dirichlet
edgemarker = [];
edgemarker = markbdedges( verts, edges, trineigh, edgemarker, [], [], 1 );

%
% Set up problem
%
[ rhsfunc, dirifunc, neufunc, exactfunc ] = atp2_lapl_rect_eigen( 2, 2, [1 1 1 1], 2, 2 );


%
% Assembly
%
A = fe2_stiff_v( verts, tris );
F = fe2_load_v( verts, tris, quad2_3point(), rhsfunc );
[U,F,dof,nondof] = fe2_boundary( verts, edges, edgemarker, A, F, dirifunc, neufunc );

% Solve
U(dof) = A(dof,dof)\F(dof);

ERR = exacterr(verts, tris, U, exactfunc );
norm(ERR, 'inf')

% Plot
trisurf(tris, verts(:,1), verts(:,2), U)
