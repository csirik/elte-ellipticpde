
%
% fe2_stiff_aniso_v - Fast, vectorized sparse assemble of first-order anisotropic finite element 
%               stiffness matrix
%
% INPUT
% verts       - Nx2 array of points, i.e. nodes of the triangle mesh
% tris        - Mx3 array of indices into verts specifying the triangles
% B           - 2x2 constant coefficient matrix
%
% OUTPUT
%             - The assembled extended stiffness matrix
%             To select the actual A, use masking such as
%             A(dof,dof) with dof obtained from fe2_boundary
%
% AUTHORS
% Csirik Mihaly, ELTE
%
% Based on the iFEM code by Long Chen.
%
function A = fe2_stiff_aniso_v( verts, tris, B )

% Number of elements
nel = size( tris, 1 );
% Number of vertices (nodes)
nv = size( verts, 1 );
% Output matrix
A = sparse( nv, nv );

% Prepare anisotropy matrix
R = [0 -1; 1 0 ];
M = R' * B * R;

% 
% Precompute edges and areas
%
edges = zeros( nel, 2, 3 );

edges(:,:,1) = verts(tris(:,3),:)-verts(tris(:,2),:);
edges(:,:,2) = verts(tris(:,1),:)-verts(tris(:,3),:);
edges(:,:,3) = verts(tris(:,2),:)-verts(tris(:,1),:);

areas = 0.5*abs(edges(:,2,3).*edges(:,1,2)-edges(:,1,3).*edges(:,2,2));

%
% Assemble stiffness matrix
%
for i=1:3
    for j=1:3
        % Accumulate
        Meiej = 0;
        for k=1:2
            for l=1:2
                Meiej = Meiej + M(k,l)*edges(:,k,i).*edges(:,l,j);
            end
        end
        Aij = Meiej ./ ( 4*areas );
        A = A + sparse(tris(:,i),tris(:,j),Aij,nv,nv);
    end
end
