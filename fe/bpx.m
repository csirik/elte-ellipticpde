function Bv = bpx( v )

global interp
global Ahcoarsest
global smoothers

levels = size( interp, 1 );

res = cell( 1, levels );
err = cell( 1, levels );

res{levels} = v;

for k=levels:-1:2
    err{k} = smoothers{k} * res{k};
    res{k-1} = interp{k-1}' * res{k};
end

err{1} = Ahcoarsest\res{1};
%err{1} = smoothers{1}*res{1};

for k=2:levels
    err{k} = err{k} + interp{k-1} * err{k-1};
end

Bv = err{levels};

end

