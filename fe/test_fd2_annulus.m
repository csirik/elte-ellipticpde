%
% First order 2D finite element demo
%

clear all
close all

addpath('mesh2d')
addpath('util')

bdtype = 'mixed';

%
% Generate mesh: annulus B(0,2)\B(0,1)
%
dtheta = pi/20;
theta  = (-pi:dtheta:pi)';
node   = [cos(theta) sin(theta); 2*cos(theta) 2*sin(theta)];

hdata = [];
hdata.hmax = 0.1;
options = [];
options.output = false;
[verts,tris] = mesh2d(node, [], hdata, options);
[edges, triedges, trineigh, boundary] = connectivity(verts, tris);

% Mark boundary edges
switch bdtype
    case 'diri'
        % All boundary edges are Dirichlet
        edgemarker = [];
        edgemarker = markbdedges( verts, edges, trineigh, edgemarker, [], 1 );
    case 'mixed'
        % Outer boundary edges are Neumann
        edgemarker = [];
        edgemarker = markbdedges( verts, edges, trineigh, edgemarker, [], 2 );
    
        % Inner boundary edges are Dirichlet
        poly = [-1 -1; 1 -1; 1 1; -1 1; -1 -1];
        edgemarker = markbdedges( verts, edges, trineigh, edgemarker, poly, 1 );
end
%
% Set up problem
%
rhsfunc = @(x,y) -4;
dirifunc = @(x,y) x^2 + y^2;
neufunc = @(x,y) 2 * sqrt( x^2 + y^2 );
exactfunc = @(x,y) x^2 + y^2;

%
% Assembly
%
A = fe2_stiff_v( verts, tris );
F = fe2_load_v( verts, tris, quad2_3point(), rhsfunc );
[U,F,dof,nondof] = fe2_boundary( verts, edges, edgemarker, A, F, dirifunc, neufunc );

% Solve
U(dof) = A(dof,dof)\F(dof);

ERR = exacterr(verts, tris, U, exactfunc );
fprintf('Absolute error: %f\n', norm(ERR, 'inf'))

% Plot
trisurf(tris, verts(:,1), verts(:,2), U)
