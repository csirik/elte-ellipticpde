function [ verts, tris, marker ] = mesh_tritri( N )

verts = zeros(N*(N-1)/2, 2); % nodes
tris = zeros((N-1)*(N-1), 3); % triangle indices
marker = zeros(N*(N-1)/2, 1); % node markers

h = 1/(N-1);

num_verts = 0;
num_tris = 0;

%
% build vertex array and mark boundary
%
for j=1:N
    for i=1:N
        if i + j < N+2
            num_verts = num_verts + 1;
            verts(num_verts, :) = [1.0-(i-1)*h, (j-1)*h];
            if( i == 1 || j == 1 || i + j == N+1 )
                marker(num_verts) = 1;
            end
        end
    end
end

%
% build triangles
%
for j=1:N
    for i=1:N
        if i + j < N+1
            num_tris = num_tris + 1;
            tris(num_tris, 1) = i + (j-1)*N - (j-2)*(j-1)/2;
            tris(num_tris, 2) = i+1 +(j-1)*N - (j-2)*(j-1)/2;
            tris(num_tris, 3) = i + j*N - (j-1)*j/2;
            
            if i + j < N
                num_tris = num_tris + 1;
                tris(num_tris, 1) = i+1 +(j-1)*N - (j-2)*(j-1)/2;
                tris(num_tris, 2) = i+1 + j*N - (j-1)*j/2;
                tris(num_tris, 3) = i + j*N - (j-1)*j/2;
            end
        end
    end
end

end

