function [ Dtheta, areas ] = fe22_grad_shape( verts, tris )

nel = size( tris, 1 );

[Dlambda, areas] = fe2_grad_bary( verts, tris );


end

