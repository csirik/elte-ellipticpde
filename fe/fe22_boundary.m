function [U,F,dof,nondof,nodal,nonnodal] = fe22_boundary( verts, edges, boundary, neigh, A, F, dirifunc )

nv = size( verts, 1 ) + size( edges, 1 );

nodal = [1:size(verts,1)]';
nonnodal = [size(verts,1)+1:nv]';

% Dirichlet boundary 
nondof = [find(boundary == 1); ...
    ones( size(find(~neigh(:,2)),1), 1 )*size( verts, 1 ) + find(~neigh(:,2)) ];

nodalnondof = find(boundary == 1);

% Interior and Neumann boundary
dof = [ find(~boundary); ...
    ones( size(find(neigh(:,2)),1), 1 )*size( verts, 1 ) + find(neigh(:,2)) ];

U = zeros( nv, 1 );

% Evaluate the boundary function
Gh = arrayfun( dirifunc, verts( nodalnondof, 1 ), verts( nodalnondof, 2 ) );

% Apply Dirichlet boundary conditions
U(nodalnondof) = Gh;
F = F - A*U;

end

