function [ res_ac, res_jmp ] = fd2_errest( verts, tris, trineigh, uh )

nel = size( tris, 1 );
ned = size( trineigh, 1 );

edges = zeros( nel, 2, 3 );

edges(:,:,1) = verts(tris(:,3),:)-verts(tris(:,2),:);
edges(:,:,2) = verts(tris(:,1),:)-verts(tris(:,3),:);
edges(:,:,3) = verts(tris(:,2),:)-verts(tris(:,1),:);

normals = zeros( nel, 2, 3 );

% rotate edges and normalize to get normal vectors 
normals(:,1,:) = -squeeze(edges(:,2,:));
normals(:,2,:) = squeeze(edges(:,1,:));

for i=1:3
    normalnorm = sqrt( normals(:,1,i).*normals(:,1,i) + normals(:,2,i).*normals(:,2,i) );
    normals(:,1,i) = normals(:,1,i) ./ normalnorm;
    normals(:,2,i) = normals(:,2,i) ./ normalnorm;
end

Duh = fe2_grad( verts, tris, uh );
divDuh = Duh(:,1) + 


Duh_jump = zeros( size( trineigh, 1 ), 1 );

%Duh_jump = 

res_ac = 0;
res_jmp = 0;

end

