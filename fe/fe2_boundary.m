function [U,F,dof,nondof] = fe2_boundary( verts, edges, edgemarker, A, F, dirifunc, neufunc )
% Apply mixed boundary conditions to the load vector. Solve with U(dof) = A(dof,dof)\F(dof); or similar.
%
% INPUT
% verts       - Nx2 array of points, i.e. nodes of the triangle mesh
% edges       - Mx2 array ov edges
% edgemarker  - Mx1 edge markers: 0=interior, 1=Dirichlet, 2=Neumann
% A           - Stiffness matrix
% F           - Load vector
% dirifunc    - Dirichlet boundary data (function handle)
% neufunc     - Neumann boundary data (function handle)
%
% OUTPUT
% U           - Solution vector filled with Dirichlet bounadary
% F           - Load vector
% dof         - Index vector of dofs
% nondof      - Index vector of nondofs
%
% AUTHORS
% Csirik Mihaly, ELTE
%
% Based on the iFEM code by Long Chen.
%

global DEBUG

% Number of vertices
nv = size( verts, 1 );

% Find Dirichlet edges
diriedges = find(edgemarker == 1);
% Find Neumann edges
neuedges = find(edgemarker == 2);

if ~size(diriedges, 1) 
    fprintf( 'Pure Neumann not supported.\n' );
    return 
end

%
% Prepare Dirichlet boundary conditions
%

% Dirichlet vertices indicator
dirivertmask = zeros( nv, 1 );
dirivertmask(edges(edgemarker == 1, 1)) = 1;
dirivertmask(edges(edgemarker == 1, 2)) = 1;

% Dirichlet vertices are non-dofs
nondof = find( dirivertmask );
% Other points are dofs
dof = find( ~dirivertmask );

% Initialize (full) solution vector
U = zeros( nv, 1 );

% Evaluate the Dirichlet boundary data
Gh = arrayfun( dirifunc, verts( nondof, 1 ), verts( nondof, 2 ) );

%
% Apply Dirichlet boundary conditions
%
U(nondof) = Gh;
F = F - A*U;

%
% Prepare Neumann boundary conditions
%

% Neumann edge vectors
neuedgevecs = verts(edges(neuedges,1), :) - verts(edges(neuedges,2), :);
% Neumann edge lengths
neuedgelens = sqrt( dot( neuedgevecs, neuedgevecs, 2 ) );
% Neumann edge midpoints
neuedgemid = 0.5*( verts(edges(neuedges,1), :) + verts(edges(neuedges,2), :) );
% Neumann boundary data evaluated at edge midpoints
neufuncmid = arrayfun( neufunc, neuedgemid(:,1), neuedgemid(:,2) );
% Neumann dofs
neuverts = edges(neuedges,:);

%
% Apply Neumann boundary conditions using the midpoint rule:
%     0.5*|e1-e2|*neufunc((e1+e2)/2)
%
F = F + accumarray( [neuverts(:) ones( 2 * size( neuverts, 1 ), 1 )], repmat( 0.5 * neuedgelens .* neufuncmid, 2, 1 ), [nv 1] );

%
% Print debug info
%
if DEBUG == 1
    fprintf( '=== fe2_boundary ===\n' );
    fprintf( 'nv=%d, edges=%d, diriedges=%d, neuedges=%d\n', nv, size(edges, 1), size(diriedges, 1), size(neuedges, 1) );
    fprintf( 'dofs=%d, nondofs=%d\n', size(dof, 1), size(nondof, 1) );
    fprintf( '====================\n' );
end

end

