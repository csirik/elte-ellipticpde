function U = bdext( verts, marker, U0, g )

U = zeros(size(verts,1), 1);
for v=1:size(verts,1)
    if marker(v) == 1
        U(v) = g( verts(v,1), verts(v,2) );
    end
end

U(~marker) = U0;

end

