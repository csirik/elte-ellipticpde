function [ verts_fine, tris_fine, interp ] = fe2_unirefine( verts_coarse, tris_coarse, levels )

verts = cell( levels, 1 );
tris = cell( levels, 1 );
edges = cell( levels, 1 );
triedges = cell( levels, 1 );
interp = cell( levels, 1 );

% interpolation operator for the finest level is the identity
interp{levels} = 1;

verts{1} = verts_coarse;
tris{1} = tris_coarse;

num_verts = size( verts{1}, 1 );
num_tris = size( tris{1}, 1 );

% nonunique edges
edges{1} = [ tris{1}(:,[1,2]); tris{1}(:,[2,3]); tris{1}(:,[3,1]) ];
[edges{1},j,j] = unique( sort( edges{1}, 2 ), 'rows' );
% unique edges
trisi = 1:num_tris;
triedges{1} = [j(trisi), j(trisi+num_tris), j(trisi+2*num_tris)];

for i=1:levels-1
    fprintf( '>> Refining level %d...', i ); tic
    num_verts = size( verts{i}, 1 );
    num_tris = size( tris{i}, 1 );
    trisi = 1:num_tris;

    % append edge midpoints to vertex list
    mids = 0.5*( verts{i}(edges{i}(:,1),:) + verts{i}(edges{i}(:,2),:) );
    verts{i+1} = [ verts{i}; mids ];
    
    % subdivide triangles
    tris{i+1} = zeros( 4*num_tris, 3 );
    tris{i+1}(trisi,:)              = [tris{i}(trisi,1) num_verts+triedges{i}(trisi,1) num_verts+triedges{i}(trisi,3)];
    tris{i+1}(trisi+num_tris,:)     = [num_verts+triedges{i}(trisi,1) tris{i}(trisi,2) num_verts+triedges{i}(trisi,2)];
    tris{i+1}(trisi+2*num_tris,:)   = [num_verts+triedges{i}(trisi,3) num_verts+triedges{i}(trisi,2) tris{i}(trisi,3)];
    tris{i+1}(trisi+3*num_tris,:)   = [num_verts+triedges{i}(trisi,1) num_verts+triedges{i}(trisi,2) num_verts+triedges{i}(trisi,3)];

    % determine unique edges and triangle edges
    edges{i+1} = [ tris{i+1}(:,[1,2]); tris{i+1}(:,[2,3]); tris{i+1}(:,[3,1]) ];
    [edges{i+1},j,j] = unique( sort( edges{i+1}, 2 ), 'rows' );
    trisi = 1:4*num_tris;
    triedges{i+1} = [j(trisi), j(trisi+4*num_tris), j(trisi+8*num_tris)];
    
    % assemble the interpolation operator
    I_coarse_coarse = sparse( 1:num_verts, 1:num_verts, ones( num_verts, 1), size(verts{i+1},1), num_verts );
    fine_coarse_edges = edges{i+1}(edges{i+1}(:,1) <= num_verts | edges{i+1}(:,2) <= num_verts, :);
    onesv = ones( size(fine_coarse_edges, 1), 1 );
    I_coarse_fine = sparse( fine_coarse_edges(:,2), ...
                            fine_coarse_edges(:,1), ...
                            0.5*onesv, ...
                            size(verts{i+1},1), num_verts );
    interp{i} = I_coarse_coarse + I_coarse_fine;

    fprintf( 'took %fs; num_verts: %d, num_tris: %d, num_edges=%d\n', toc, size(verts{i+1},1), size(tris{i+1},1), size(edges{i+1},1) );
end

verts_fine = verts{levels};
tris_fine = tris{levels};

end

