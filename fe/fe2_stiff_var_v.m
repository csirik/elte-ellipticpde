%
% fe2_stiff_var_v - Fast, vectorized sparse assemble of variable coefficient first-order finite element 
%               stiffness matrix
%
% INPUT
% verts       - Nx2 array of points, i.e. nodes of the triangle mesh
% tris        - Mx3 array of indices into verts specifying the triangles
%
% OUTPUT
%             - The assembled extended stiffness matrix
%             To select the actual A, use masking such as
%             A(dof,dof) with dof obtained from fe2_boundary
%
% AUTHORS
% Csirik Mihaly, ELTE
%
% Based on the iFEM code by Long Chen.
%
function A = fe2_stiff_var_v( verts, tris, XW, coeff )

% Number of elements
nel = size( tris, 1 );
% Number of vertices (nodes)
nv = size( verts, 1 );
% Output matrix
A = sparse( nv, nv );

% 
% Precompute edges and areas
%
edges = zeros( nel, 2, 3 );

edges(:,:,1) = verts(tris(:,3),:)-verts(tris(:,2),:);
edges(:,:,2) = verts(tris(:,1),:)-verts(tris(:,3),:);
edges(:,:,3) = verts(tris(:,2),:)-verts(tris(:,1),:);

areas = 0.5*abs(edges(:,2,3).*edges(:,1,2)-edges(:,1,3).*edges(:,2,2));

%
% Precompute element integrals
%
nc = size( XW, 1 );
XX = zeros( nel, nc, 2 );
coeffints = zeros( nel, 1 );

for k=1:nc
    % Physical cubature points for all triangles
    for d=1:2
        % XW is given on the reference triangle on which the barycentric
        % coordinates are easily evaluated.
        XX(:,k,d) = XW(k,1) * verts( tris( :, 1 ), d ) ...
                  + XW(k,2) * verts( tris( :, 2 ), d ) ...
                  + (1.0 - XW(k,1) - XW(k,2)) * verts( tris( :, 3 ), d );
    end
end

% Element integral p via cubature 
for k=1:nc
    % Substitute cubature points into coeff
    coeffXX = arrayfun( coeff, XX(:,k,1), XX(:,k,2) );
    % Accumulated cubature point contributions
    coeffints = coeffints + 2 * XW(k,3) * areas .* coeffXX;
end

%
% Assemble stiffness matrix
%
for i=1:3
    for j=1:3
        Aij = coeffints .* dot(edges(:,:,i), edges(:,:,j), 2) ./ ( 4*areas.*areas );
        A = A + sparse(tris(:,i),tris(:,j),Aij,nv,nv);
    end
end
