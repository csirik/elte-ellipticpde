function [verts, tris, marker] = mesh_unitri( N )

h = 1/(N-1);

verts = zeros( N*N, 2 );
tris = zeros( 2*(N-2)*(N-2), 3 );
marker = zeros( N*N, 1 );

num_verts = 0;
num_tris = 0;

for j=1:N
    for i=1:N
        num_verts = num_verts + 1;
        verts(num_verts, :) = [(i-1)*h, (j-1)*h];
        if( i == 1 || j == 1 || i == N || j == N )
            marker(num_verts) = 1;
        end
    end
end


for j=1:N-1
    for i=1:N-1
        num_tris = num_tris + 1;
        tris(num_tris, 1) = i + (j-1) * N;
        tris(num_tris, 2) = i+1 + (j-1) * N;
        tris(num_tris, 3) = i + j*N;
        
        num_tris = num_tris + 1;
        tris(num_tris, 1) = i+1 + (j-1) * N;
        tris(num_tris, 2) = i+1 + j*N;
        tris(num_tris, 3) = i + j*N;
    end
end


