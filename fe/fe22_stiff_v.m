%
% fe22_stiff_v -second-order finite element stiffness matrix
%
% INPUT
% verts       - Nx2 array of points, i.e. nodes of the triangle mesh
% tris        - Mx3 array of indices into verts specifying the triangles
% edges       - Px2 array of indices into vers specifying the edges
% triedges    - Mx3 array of indices into edges specifying the triangle
%               edges
%
% OUTPUT
%             - A the assembled extended stiffness matrix
%             To select the actual A use A(~marker,~marker)
%
% AUTHORS
% Csirik Mihaly, ELTE
%
% Based on the iFem code by Long Chen. 
%
function [A, el2dof] = fe22_stiff_v( verts, tris, edges, triedges )

% Number of elements
nel = size( tris, 1 );
% Number of vertices
nv = size( verts, 1 );

% Number of degrees of freedom
ndof = size( verts, 1 ) + size( edges, 1 );

% Elements to dofs
el2dof = [ tris(:,:) ones(nel,3)*nv + triedges(:,:) ];

% Sparse pattern
ii = zeros( 21*nel, 1 ); 
jj = zeros( 21*nel, 1 ); 
index = 0;
for i = 1:6
    for j = i:6
        ii(index+1:index+nel) = double(el2dof(:,i)); 
        jj(index+1:index+nel) = double(el2dof(:,j));  
        index = index + nel;
    end
end

%
% Construct the gradient of the basis
%
[Dlambda,areas] = fe2_grad_bary( verts, tris );

XW = quad2_3point();
lambda = quad2_to_bary( XW );
nxw = size( XW, 1 );

sA = zeros( 21*nel, nxw );
Dtheta = zeros( nel, 2, 6 );

for k=1:nxw
    Dtheta(:,:,1) = ( 4*lambda(k,1) - 1 ) .* Dlambda(:,:,1);            
    Dtheta(:,:,2) = ( 4*lambda(k,2) - 1 ) .* Dlambda(:,:,2);            
    Dtheta(:,:,3) = ( 4*lambda(k,3) - 1 ) .* Dlambda(:,:,3);
    Dtheta(:,:,4) = 4*( lambda(k,1)*Dlambda(:,:,2) + lambda(k,2)*Dlambda(:,:,1) );
    Dtheta(:,:,5) = 4*( lambda(k,2)*Dlambda(:,:,3) + lambda(k,3)*Dlambda(:,:,2) );
    Dtheta(:,:,6) = 4*( lambda(k,3)*Dlambda(:,:,1) + lambda(k,1)*Dlambda(:,:,3) );
    
    index = 0;
    for i = 1:6
        for j = i:6
            Aij = 2*XW(k,3)*dot( Dtheta(:,:,i), Dtheta(:,:,j) , 2);
            Aij = Aij.*areas;
            sA(index+1:index+nel, k) = Aij;
            index = index + nel;
        end
    end
end

sA = sum(sA,2);

diagindx = (ii == jj);   
upperindx = ~diagindx;
A = sparse( ii(diagindx), jj(diagindx), sA(diagindx), ndof, ndof );
AU = sparse( ii(upperindx), jj(upperindx), sA(upperindx), ndof, ndof );
A = A + AU + AU';
end

