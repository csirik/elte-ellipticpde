function [ verts_smooth, tris_smooth, U_smooth ] = fe22_smooth( verts, tris, edges, triedges, U, nodal, nonnodal, refinements )

% Number of elements
nel = size( tris, 1 );
% Number of vertices (nodes)
nv = size( verts, 1 );
% Elements to dofs
el2dof = [ tris(:,:) ones(nel,3)*nv + triedges(:,:) ];

% Number of subtriangles
nsubel = 4^refinements;
% Number of subvertices on a side
nsidev = 2^refinements + 1;
% Number of subvertices (subnodes)
nsubv = nsidev*(nsidev+1)/2;
% subvertices indexing
subindx = zeros( nsidev, nsidev );
index = 0;
for k=1:nsidev
    for l=1:nsidev
        if k+l < nsidev+2
            index = index + 1;
            subindex(k,l) = index;
        end
    end
end


% barycentric coordinates of the subvertices
lambdasub = zeros( nsubv, 3 );
index = 0;
for k=1:nsidev
    for l=1:nsidev
        if k+l < nsidev + 2
            lambdasub(subindex(k,l), :) = [ 1-(k-1)/(nsidev-1)-(l-1)/(nsidev-1), (k-1)/(nsidev-1), (l-1)/(nsidev-1) ];
        end
    end
end


% Shape functions in the refined triangle
thetasub(:,1) = lambdasub(:,1) .* ( 2*lambdasub(:,1) - 1 );
thetasub(:,2) = lambdasub(:,2) .* ( 2*lambdasub(:,2) - 1 );
thetasub(:,3) = lambdasub(:,3) .* ( 2*lambdasub(:,3) - 1 );
thetasub(:,4) = 4*lambdasub(:,1) .* lambdasub(:,2);
thetasub(:,5) = 4*lambdasub(:,2) .* lambdasub(:,3);
thetasub(:,6) = 4*lambdasub(:,3) .* lambdasub(:,1);


verts_smooth = zeros( nsubv*nel, 2 );
tris_smooth = zeros( nsubel*nel, 3 );
U_smooth = zeros( nsubv*nel, 1 );

vindex = 0;
elindex = 0;
for el=1:nel
    
    for k=1:nsidev
        for l=1:nsidev
            if k+l < nsidev + 2
                verts_smooth(vindex+subindex(k,l), :) = lambdasub(subindex(k,l), 1)*verts( tris(el, 1), :) + ...
                    lambdasub(subindex(k,l), 2)*verts( tris(el, 2), :) +  lambdasub(subindex(k,l), 3)*verts( tris(el, 3), :);
                U_smooth(vindex+subindex(k,l)) = 0;
                for m=1:6
                    U_smooth(vindex+subindex(k,l)) = U_smooth(vindex+subindex(k,l)) + thetasub(subindex(k,l),m) * U(el2dof(el,m));
                end
            end
        end
    end
    
    for k=1:nsidev-1
        for l=1:nsidev-1
            if k+l<nsidev+1
                elindex = elindex + 1;
                tris_smooth(elindex, :) = vindex + [ subindex(k,l) subindex(k+1,l) subindex(k,l+1) ];

                if k+l<nsidev
                    elindex = elindex + 1;
                    tris_smooth(elindex, :) = vindex + [ subindex(k+1,l) subindex(k+1,l+1) subindex(k,l+1) ];
                end
            end
        end
    end
    
    
    vindex = vindex + nsubv;
end


end

