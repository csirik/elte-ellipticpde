function Bv = jacobi( v )
global smoothers

Bv = smoothers{end}*v;

end

