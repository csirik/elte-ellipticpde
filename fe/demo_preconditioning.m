clear all

addpath('mesh2d')
addpath('util')

dtheta = pi/10;
theta  = (-pi:dtheta:pi-dtheta)';
node   = [cos(theta) sin(theta)];
    
hdata = [];
hdata.hmax = 0.03;
options = [];
options.output = false;
[verts,tris] = mesh2d(node, [], hdata, options);
[edges, triedges, trineigh, boundary] = connectivity(verts, tris);

edgemarker = [];
edgemarker = markbdedges( verts, edges, trineigh, edgemarker, [], [], 1 );

f = @(x1,x2) sqrt(x1*x1+x2*x2)<0.03;
g = @(x1,x2) 0;
h = @(x1,x2) 0;
exact = @(x1,x2) -(1/(2*pi))*log(sqrt(x1*x1+x2*x2));

Ah = fe2_stiff_v( verts, tris );
XW = [ 2./3. 1./6. 1./6.;
       1./6. 2./3. 1./6.;
       1./6. 1./6. 1./6.];
Fh = fe2_load_v( verts, tris, XW, f );

[Uh,Fh,dof,nondof] = fe2_boundary( verts, edges, edgemarker, Ah, Fh, g, h );

A = Ah(dof,dof);
b = Fh(dof);

MAXIT = 100;
TOL = 1e-8;

[x0,fl0,rr0,it0,rv0] = pcg( A, b, TOL, MAXIT );

L = ichol( A );
[x1,fl1,rr1,it1,rv1] = pcg( A, b, TOL, MAXIT, L, L' );

L = ichol( A, struct('michol','on') );
[x2,fl2,rr2,it2,rv2] = pcg( A, b, TOL, MAXIT, L, L' );

semilogy(0:it0,rv0/norm(b),'b.');
hold on;
semilogy(0:it1,rv1/norm(b),'r.');
semilogy(0:it2,rv2/norm(b),'g.');
legend('No Preconditioner','IC(0)','MIC(0)');
xlabel('iteration number');
ylabel('relative residual');
hold off;
 
% 
% Uh(dof) = A\b;
% 
% max(abs(exacterr(verts, tris, Uh, exact )))
% figure(1)
% trisurf(tris, verts(:,1), verts(:,2), Uh)
% figure(2)
% trisurf(tris, verts(:,1), verts(:,2), arrayfun( exact, verts(:,1), verts(:,2) ) )
