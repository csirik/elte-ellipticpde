function test_fd22_rect
%
% Second order 2D finit element demo
%
% Pure Dirichlet problem for the Poisson equation with exact solution 
% sine
%

clear all
close all

addpath('mesh2d')
addpath('util')
addpath('../atp')

global DEBUG
DEBUG = 1;

a = 1;
b = 1;

ATP = 'eigen';

%
% Generate mesh: rectangle [0,a]x[0,b]
%
node = [
     0   0
     a   0
     a   b
     0   b
         ];
hdata = [];
hdata.hmax = 0.2;
options = [];
options.output = false;

[verts,tris] = mesh2d(node, [], hdata, options);
[edges, triedges, trineigh, boundary] = connectivity(verts, tris);

% Mark all boundary edges Dirichlet
edgemarker = [];
edgemarker = markbdedges( verts, edges, trineigh, edgemarker, [], 1 );

%
% Set up problem
%
switch ATP
    case 'bubble'
        p = 1;
        [ rhsfunc, dirifunc, neufunc, exactfunc ] = atp2_lapl_rect_bubble( 0, a, 0, b, p );
    case 'eigen' 
        [ rhsfunc, dirifunc, neufunc, exactfunc ] = atp2_lapl_rect_eigen( 1, 1, [1 1 1 1], 1, 1 );
end
%
% Assembly
%
A = fe22_stiff_v( verts, tris, edges, triedges );
%Ad = fe22_stiff_d( verts, tris, edges, triedges );
fprintf('Stiffness matrix relative nnz: %f\n', nnz(A)/(size(A,1)^2));
%norm(A-Ad,2)

F = fe22_load_v( verts, tris, edges, triedges, quad2_3point(), rhsfunc );
%Fd = fe22_load_d( verts, tris, edges, triedges, quad2_3point(), rhsfunc );

%norm(F-Fd,2)

[U,F,dof,nondof,nodal,nonnodal] = fe22_boundary( verts, edges, boundary, trineigh, A, F, dirifunc );
U(dof) = A(dof,dof)\F(dof);

% 
% % Solve
% 
% 
% ERR = exacterr(verts, tris, U, exactfunc );
% norm(ERR, 'inf')
% 
% Plot

figure(2)
trisurf(tris, verts(:,1), verts(:,2), U(nodal) )
% 
% figure(2)
% [ verts_fine, tris_fine, U_fine ] = fe22_refine( verts, tris, U, nodal, nonnodal );
% trisurf(tris_fine, verts_fine(:,1), verts_fine(:,2), U_fine )

figure(3)
hold on

material dull 
camlight left
shading interp
colormap hsv


%U = zeros( length(F) );
%U(tris(1,2)) = 1;
%U(triedges(50,1))=1;
[ verts_smooth, tris_smooth, U_smooth ] = fe22_smooth( verts, tris, edges, triedges, U, nodal, nonnodal, 4 );
%plot( verts_smooth(:,1),  verts_smooth(:,2), '*')
trisurf(tris_smooth, verts_smooth(:,1), verts_smooth(:,2), U_smooth, 'FaceLighting','phong','FaceColor','interp',...
      'AmbientStrength',0.5, 'EdgeColor', 'none')
  
trisurf( tris, verts(:,1), verts(:,2), zeros( length(U(nodal)),1 ), 'LineWidth', 2.0, 'FaceColor', 'none')

end

