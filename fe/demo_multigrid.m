function demo_multigrid

clear all


addpath('mesh2d')
addpath('util')

levels = 5;
hdata = [];
cnect = [];
if 0
node = [
     0   0
     1   0
     1   1
     0   1
         ];
elseif 1   
node = [
     0   0
     pi  0
     pi  pi
     0  pi
         ];
     
dtheta = pi/150;
theta  = (-pi:dtheta:pi-dtheta)';
node   = [cos(theta) sin(theta)];
elseif 0
         node  = [0 0
                 3 0
                 3 3
                 0 3
                 0.1 1
                 0.11 1
                 0.11 2
                 0.1 2
                 ];
        cnect = [1 2
                 2 3
                 3 4
                 4 1
                 5 6
                 6 7
                 7 8
                 8 5
                 ];
elseif 0
    theta = 0:pi/100:(2*pi-pi/100);
x     = cos(theta)/2;
y     = sin(theta)/2;

node = [ x'  y'
        -5  -10
        25  -10
        25   10
        -5   10 
       ];

n = size(node,1)-4;

cnect = [(1:n-1)' (2:n)'
          n        1
          n+1      n+2
          n+2      n+3
          n+3      n+4
          n+4      n+1
        ]; 

hdata = [];
hdata.fun = @hfun2;
end

global interp
global Ahcoarsest
global smoothers

% Generate coarse mesh
hdata.hmax = 1;
options = [];
options.output = false;
fprintf('Generating coarsest level mesh...');
tic
[verts_coarse,tris_coarse] = mesh2d( node, cnect, hdata, options );
fprintf('took %fs; num_verts: %d, num_tris: %d\n', toc, length(verts_coarse), length(tris_coarse));

[verts_fine,tris_fine,interp] = fe2_unirefine( verts_coarse, tris_coarse, levels );

% 
% A1 = fe2_stiff_d( verts_coarse, tris_coarse );
% A2 = fe2_stiff_d( verts_fine, tris_fine );
% 
% 
% 
% figure(1)
% hold on
% axis equal 
% 
% trimesh(tris_coarse, verts_coarse(:,1), verts_coarse(:,2));
% plotvertstris( verts_coarse, tris_coarse );
% %plotbdedges( verts, edges, trineigh, edgemarker )
% 
% hold off
% 
% 
% figure(2)
% hold on
% axis equal 
% 
% trimesh(tris_fine, verts_fine(:,1), verts_fine(:,2));
% plotvertstris( verts_fine, tris_fine );
% %plotbdedges( verts, edges, trineigh, edgemarker )
% 
% hold off
% 
% 
% return
% 
% 
% % Generate coarse mesh
% hdata = [];
% hdata.hmax = 1;
% options = [];
% options.output = false;
% fprintf('Generating  level 1...');
% tic
% [fine_verts,fine_tris] = mesh2d( node, [], hdata, options );
% 
% [fine_edges, fine_triedges, fine_trineigh, fine_boundary] = connectivity( fine_verts, fine_tris );
% 
% edgemarker = [];
% edgemarker = markbdedges( fine_verts, fine_edges, fine_trineigh, edgemarker, [], [], 1 );
% fine_dof = fe2_dofindx( length(fine_verts), fine_edges, edgemarker );
% fprintf('%f s; num_verts: %d, num_tris: %d\n', toc, length(fine_verts), length(fine_tris));
% 
% global iops
% global sms
% global Ahs
% % The k'th interpolation operator iops{k} transfers from the kth grid
% % to the (k+1)th grid
% % not stored.
% iops = cell( 1, levels );
% sms = cell( 1, levels );
% Ahs = cell( 1, levels );
% Ahhs = cell( 1, levels );
% dofs = cell( 1, levels );
% 
% Ahhs{1} = fe2_stiff_d( fine_verts, fine_tris );
% dofs{1} = fine_dof;
% 
% % Refine
% for i=2:levels
%     fprintf('Refining on level %d...', i);
%     tic
%     coarse_verts = fine_verts;
%     coarse_tris = fine_tris;
%     coarse_edges = fine_edges;
%     coarse_triedges = fine_triedges;
%     coarse_trineigh = fine_trineigh;
%     coarse_boundary = fine_boundary;
%     coarse_dof = fine_dof;
%     
%     [fine_verts,fine_tris] = refine( coarse_verts, coarse_tris );
%     [fine_edges, fine_triedges, fine_trineigh, fine_boundary] = connectivity( fine_verts, fine_tris );
%     
%     edgemarker = [];
%     edgemarker = markbdedges( fine_verts, fine_edges, fine_trineigh, edgemarker, [], [], 1 );
%     fine_dof = fe2_dofindx( size(fine_verts,1), fine_edges, edgemarker );
% 
%     iops{i-1} = interp( fine_edges, length(fine_verts), length(coarse_verts) );
%     %iops{i-1} = iops{i-1}(fine_dof, coarse_dof);
%     
%     Ahhs{i} = fe2_stiff_d( fine_verts, fine_tris );
%     
%     dofs{i} = fine_dof;
%     
%     fprintf('%f s; num_verts: %d, num_tris: %d\n', toc, length(fine_verts), length(fine_tris));
% end
% 
% Ahs{levels} = fe2_stiff_d( fine_verts, fine_tris );
% %Ahs{levels} = Ahs{levels}(dofs{levels},dofs{levels});
% 
% for i=levels:-1:2
%     Ahs{i-1} = iops{i-1}'*Ahs{i}*iops{i-1};
% end
% 
% 
% 
% % 
% % for i=1:levels
% %     Ahs{i} = Ahs{i}(dofs{i},dofs{i});
% % end
% % 
% % for i=2:levels
% %     iops{i-1} = iops{i-1}(dofs{i},dofs{i-1});
% % end
% 
% iops{levels} = 1;
% 
% i = 9;
% 
% figure(1)
% plotbasis( coarse_verts, coarse_tris, i );
% figure(2)
% plotbasis( fine_verts, fine_tris, iops{2}(:,i) );
% 
% return
% 
% %figure(i)
% %spy(iops{i-1})
% %full(I{i})
% 
% %return

verts = verts_fine;
tris = tris_fine;
%edges = edges_fine;
% edges = fine_edges;
% triedges = fine_triedges;
% trineigh = fine_trineigh;
% boundary = fine_boundary;


% RHS and boundary values
f = @(x1,x2) -4;
g = @(x1,x2) 0;
h = @(x1,x2) 0;

% Assembly
Ah = fe2_stiff_v( verts, tris );
XW = [ 2./3. 1./6. 1./6.;
       1./6. 2./3. 1./6.;
       1./6. 1./6. 1./6.];
Fh = fe2_load_v( verts, tris, XW, f );

[edges, triedges, trineigh, boundary] = connectivity( verts, tris );
edgemarker = [];
edgemarker = markbdedges( verts, edges, trineigh, edgemarker, [], [], 1 );
[Uh,Fh,dof,nondof] = fe2_boundary( verts, edges, edgemarker, Ah, Fh, g, h );


dofs = cell( levels, 1 );
dofs{levels} = dof;

for i=1:levels-1
    dofs{i} = dofs{levels}( dofs{levels} <= size(interp{i},2) );
end

Ahcoarsest = fe2_stiff_v( verts_coarse, tris_coarse );
Ahcoarsest = Ahcoarsest(dofs{1},dofs{1});

smoothers{levels} = spdiags(1./spdiags( Ah, [0] ), [0], size(Ah,1), size(Ah,2));

for i=levels-1:-1:1
    smoothers{i} = interp{i}'*smoothers{i+1}*interp{i};
end

for i=1:levels-1
    interp{i} = interp{i}(dofs{i+1}, dofs{i});
    smoothers{i} = smoothers{i}(dofs{i}, dofs{i});
end

smoothers{levels} = smoothers{levels}(dofs{levels},dofs{levels});


MAXIT = 1000;
TOL = 1e-8;


if 0
fprintf('Solving with NO preconditioner...');
tic
[Uh(dof),iter] = pre_cg( Ah(dof,dof), Fh(dof), TOL, MAXIT );
fprintf('took %fs, # of iterations: %d\n', toc, iter);

fprintf('Solving with JACOBI preconditioner...');
tic
[Uh(dof),iter] = pre_cg( Ah(dof,dof), Fh(dof), TOL, MAXIT, @jacobi );
fprintf('took %fs, # of iterations: %d\n', toc, iter);

fprintf('Solving with BPX preconditioner...');
tic
[Uh(dof),iter] = pre_cg( Ah(dof,dof), Fh(dof), TOL, MAXIT, @bpx );
fprintf('took %fs, # of iterations: %d\n', toc, iter);
else
fprintf('Solving with NO preconditioner...');
tic
[Uh(dof),fl0,rr0,it0,rv0] = pcg( Ah(dof,dof), Fh(dof), TOL, MAXIT);
fprintf('took %fs, # of iterations: %d\n', toc, it0);

fprintf('Solving with JACOBI preconditioner...');
tic
[Uh(dof),fl0,rr0,it0,rv0] = pcg( Ah(dof,dof), Fh(dof), TOL, MAXIT, @jacobi );
fprintf('took %fs, # of iterations: %d\n', toc, it0);

fprintf('Solving with BPX preconditioner...');
tic
[Uh(dof),fl0,rr0,it0,rv0] = pcg( Ah(dof,dof), Fh(dof), TOL, MAXIT, @bpx );
fprintf('took %fs, # of iterations: %d\n', toc, it0);
end

return

hold on
axis equal 

trimesh(tris, verts(:,1), verts(:,2));
%plotvertstris( verts, tris );
%plotbdedges( verts, edges, trineigh, edgemarker )

trisurf(tris, verts(:,1), verts(:,2), Uh)

hold off


function h = hfun2(x,y)

% User defined size function for cylinder

h1 = inf*ones(size(x));
in = (x>=0)&(x<=25)&(y>=-3)&(y<=3);
h1(in) = 0.2;

r = sqrt(x.^2+y.^2);
h2 = inf*h1;
h2(r<=3) = 0.02 + 0.05*r(r<=3);

h = min(h1,h2);

end      % hfun2()

end