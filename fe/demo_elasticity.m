clear all
close all

addpath('mesh2d')
addpath('util')

% [verts, tris, boundary] = mesh_unitri( 10 );
% verts = verts .* 2;
% trimesh(tris, verts(:,1), verts(:,2));

%  node = [
%      0   0
%      1  0
%      1  1
%      0  1
%          ];
%      
% hdata = [];
% hdata.hmax = 0.05;
% options = [];
% options.output = false;
% [verts,tris] = mesh2d(node, [], hdata, options);
% [edges, triedges, trineigh, boundary] = connectivity(verts, tris);

%
% Generate mesh: annulus B(0,2)\B(0,1)
%
dtheta = pi/20;
theta  = (-pi:dtheta:pi)';
node   = [cos(theta) sin(theta); 2*cos(theta) 2*sin(theta)];

hdata = [];
hdata.hmax = 0.1;
options = [];
options.output = false;
[verts,tris] = mesh2d(node, [], hdata, options);
[edges, triedges, trineigh, boundary] = connectivity(verts, tris);

% Uniform gravity
rhsfunc = {@(x,y) 0, @(x,y) -10};

% Mark all boundary edges Dirichlet
edgemarker = [];
edgemarker = markbdedges( verts, edges, trineigh, edgemarker, [], [], 1 );

nv = length( verts );
% Dirichlet vertices indicator
dirivertmask = zeros( nv, 1 );
dirivertmask(edges(edgemarker == 1, 1)) = 1;
dirivertmask(edges(edgemarker == 1, 2)) = 1;

% Dirichlet vertices are non-dofs
nondof = find( dirivertmask );
% Other points are dofs
dof = find( ~dirivertmask );

lambda = 1;
mu = 2;

B1 = [2*mu+lambda 0;
      0  mu];
B2 = [mu 0;
      0 2*mu+lambda];
B3 = [0 mu;
      lambda 0];
  
A11 = fe2_stiff_aniso_v( verts, tris, B1 );
A22 = fe2_stiff_aniso_v( verts, tris, B2 );
A12 = fe2_stiff_aniso_v( verts, tris, B3 );

A = [A11(dof,dof) A12(dof,dof);
     A12(dof,dof)' A22(dof,dof)];

F1 = fe2_load_v( verts, tris, quad2_3point(), rhsfunc{1} );
F2 = fe2_load_v( verts, tris, quad2_3point(), rhsfunc{2} );

F = [F1(dof); F2(dof)];

U = zeros( 2*nv, 1 );
U([dof nv+dof]) = A\F; % Solve
U([nondof nv+nondof]) = zeros(1, 2*length(nondof));

%A([5 14], [5 14])

hold on
trimesh(tris, verts(:,1), verts(:,2));
hold off
figure(2)
hold on
%quiver( verts(:,1), verts(:,2), U(1:nv), U(nv+1:2*nv) )
trimesh(tris, verts(:,1)+U(1:nv), verts(:,2)+U(nv+1:2*nv) );

hold off
