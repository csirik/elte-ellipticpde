%
% First order 2D finit element demo
%
% Pure Dirichlet problem for the Poisson equation with exact solution 
% sine
%

clear all
close all

addpath('mesh2d')
addpath('util')
addpath('../atp')

global DEBUG
DEBUG = 1;

a = 1;
b = 1;

ATP = 'eigen';

%
% Generate mesh: rectangle [0,a]x[0,b]
%
node = [
     0   0
     a   0
     a   b
     0   b
         ];
hdata = [];
hdata.hmax = 0.2;
options = [];
options.output = false;

[verts,tris] = mesh2d(node, [], hdata, options);
[edges, triedges, trineigh, boundary] = connectivity(verts, tris);

% Mark all boundary edges Dirichlet
edgemarker = [];
edgemarker = markbdedges( verts, edges, trineigh, edgemarker, [], [], 1 );

%
% Set up problem
%
switch ATP
    case 'bubble'
        p = 5;
        [ rhsfunc, dirifunc, neufunc, exactfunc ] = atp2_lapl_rect_bubble( 0, a, 0, b, p );
    case 'eigen' 
        [ rhsfunc, dirifunc, neufunc, exactfunc ] = atp2_lapl_rect_eigen( 1, 1, [1 1 1 1], 2, 2 );
end
%
% Assembly
%
A = fe2_stiff_v( verts, tris );
F = fe2_load_v( verts, tris, quad2_3point(), rhsfunc );
[U,F,dof,nondof] = fe2_boundary( verts, edges, edgemarker, A, F, dirifunc, neufunc );

% Solve
U(dof) = A(dof,dof)\F(dof);

ERR = exacterr(verts, tris, U, exactfunc );
norm(ERR, 'inf')

% Plot
trisurf(tris, verts(:,1), verts(:,2), U)
