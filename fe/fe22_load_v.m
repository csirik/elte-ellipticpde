%
% fe2_load_v  - Fast, vectorized assembly of the first-order finite element 
%               load vector
%
% INPUT
% verts       - Nx2 array of points, i.e. nodes of the triangle mesh
% tris        - Mx3 array of indices into verts specifying the triangles
% XW          - Cubature rule: first 2 columns specify the cubature
%               points on the reference triangle, last column the weight.
% f           - A function on the domain
%
% OUTPUT
% F           - The assembled extended load vector
%             To select the actual F, use F(dof) with dof obtained from
%             fe2_boundary.
% AUTHORS
% Csirik Mihaly, ELTE
%
%
function RHS = fe22_load_v( verts, tris, edges, triedges, XW, rhsfunc )

% Number of elements
nel = size( tris, 1 );
% Number of vertices (nodes)
nv = size( verts, 1 );

% Number of degrees of freedom
ndof = size( verts, 1 ) + size( edges, 1 );

% Elements to dofs
el2dof = [ tris(:,:) ones(nel,3)*nv + triedges(:,:) ];

% 
% Precompute edges and jacobians
%
edges = zeros( nel, 2, 1 );

edges(:,:,1) = verts(tris(:,3),:)-verts(tris(:,2),:);
edges(:,:,2) = verts(tris(:,1),:)-verts(tris(:,3),:);
edges(:,:,3) = verts(tris(:,2),:)-verts(tris(:,1),:);

areas = abs(edges(:,2,3).*edges(:,1,2)-edges(:,1,3).*edges(:,2,2));

%
% Assemble load vector
%

nc = size( XW, 1 );
XX = zeros( nel, nc, 2 );
rhsfuncints = zeros( nel, 6 );

% Linear shape functions in the reference triangle
lambda = quad2_to_bary( XW );

% Shape functions in the reference triangle
theta(:,1) = lambda(:,1) .* ( 2*lambda(:,1) - 1 );
theta(:,2) = lambda(:,2) .* ( 2*lambda(:,2) - 1 );
theta(:,3) = lambda(:,3) .* ( 2*lambda(:,3) - 1 );
theta(:,4) = 4*lambda(:,1) .* lambda(:,2);
theta(:,5) = 4*lambda(:,2) .* lambda(:,3);
theta(:,6) = 4*lambda(:,3) .* lambda(:,1);

% Loop over cubature points
for k=1:nc
    % Physical cubature points for all triangles
    XX(:,k,:) = lambda(k,1) * verts( tris( :, 1 ), : ) ...
              + lambda(k,2) * verts( tris( :, 2 ), : ) ...
              + lambda(k,3) * verts( tris( :, 3 ), : );
    % Evaluate function at cubature points      
    rhsfuncXX = arrayfun( rhsfunc, XX(:,k,1), XX(:,k,2) );
    
   	for j = 1:6
        rhsfuncints(:,j) = rhsfuncints(:,j) + XW(k,3) * theta(k, j) * rhsfuncXX;
    end
end

% Multiply by jacobian
rhsfuncints = rhsfuncints .* repmat( areas, 1, 6 );
% Accumulate RHS vector
RHS = accumarray( el2dof(:), rhsfuncints(:), [ndof 1] ); 

