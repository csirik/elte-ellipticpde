function [ dof ] = fe2_dofindx( nv, edges, edgemarker )

diriedges = find(edgemarker == 1);

if ~size(diriedges, 1) 
    fprintf( 'Pure Neumann not supported.\n' );
    return 
end

dirivertmask = zeros( nv, 1 );

dirivertmask(edges(edgemarker == 1, 1)) = 1;
dirivertmask(edges(edgemarker == 1, 2)) = 1;

dof = find( ~dirivertmask );

end

