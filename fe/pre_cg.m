function [ x, k ] = pre_cg( A, b, TOL, MAXIT, pre )

if nargin == 4
    pre = @(x) x;
end

k = 1; 
x = zeros(size(b));
r = b - A*x;
nb = norm(b);
err = zeros(MAXIT,2);

err(1,:) = norm(r);

while (max(err(k,:)) > TOL) && (k <= MAXIT)
    Br = pre(r);
    rho = Br'*r;
    
    if k == 1
        p = Br;
    else
        beta = rho/rho_old;
        p = Br + beta*p;
    end
    
    Ap = A*p;
    alpha = rho/(Ap'*p);
    r = r - alpha*Ap;
    x = x + alpha*p;
    rho_old = rho;
    k = k + 1;
    
    % compute err for the stopping criterion
    %     err(k,1) = alpha*sqrt(p'*Ap/(x'*A*x)); % increamental error in energy norm
    err(k,1) = sqrt(abs(rho/(x'*b))); % approximate relative error in energy norm
    err(k,2) = norm(r)/nb; % relative error of the residual in L2-norm
end

end

