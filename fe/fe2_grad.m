function [ Du ] = fe2_grad( verts, tris, u )

% Number of elements
nel = size( tris, 1 );

Dlambda = fe2_grad_bary( verts, tris );

Du = zeros( nel, 2 );

for i=1:2
    for j=1:3
        Du(:,i) = Du(:,i) + u( tris(:,j) ) .* Dlambda(:, i, j);
    end
end

end

