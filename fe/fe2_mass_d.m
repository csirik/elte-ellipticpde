%
% fe2_mass_d - Dense-assemble first-order finite element mass matrix
%               SLOW -- USE THE VECTORIZED VERSION!
%
% INPUT
% verts       - Nx2 array of points, i.e. nodes of the triangle mesh
% tris        - Mx3 array of indices into verts specifying the triangles
%
% OUTPUT
% A           - The assembled extended mass matrix
%
% AUTHORS
% Csirik Mihaly, ELTE
%
function A = fe2_mass_d( verts, tris )

% Number of elements
nel = size( tris, 1 );
% Number of vertices (nodes)
nv = size( verts, 1 );
% Output matrix
A = zeros( nv, nv );

% Derivatives of local shape functions on the reference element
lambdalambda = (1/12) * [ 2 1 1;
                          1 2 1;
                          1 1 2];

%
% Assemble element-wise
%
for el=1:nel
    p = verts( tris( el, : ), : ); % vertices
    Aff = [ p( 2, : ) - p( 1, : ); p( 3, : ) - p( 1, :) ]; % affine map matrix
    jac = abs( det( Aff ) );
    
    for vi=1:3
        for vj=1:3
           A(tris( el, vi ),tris( el, vj )) = A(tris( el, vi ),tris( el, vj )) + ...
               0.5 * jac * lambdalambda(vi) *  lambdalambda(vj);
        end
    end
end

