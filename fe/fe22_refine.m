function [ verts_fine, tris_fine, U_fine ] = fe22_refine( verts, tris, U, nodal, nonnodal )

% Number of elements
nel = size( tris, 1 );
% Number of vertices (nodes)
nv = size( verts, 1 );

% Refine mesh one level
[ verts_fine, tris_fine ] = fe2_unirefine( verts, tris, 2 );

nvf = size(verts_fine, 1);
U_fine = zeros( nvf, 1 );

U_fine( 1:nv ) = U( nodal );
U_fine( nv+1:nvf ) = U( nonnodal );

end

