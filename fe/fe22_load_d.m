%
% fe2_load2   - Dense assemble the second-order finite element load vector for the 
%               Poisson equation with Dirichlet boundary values on 
%               a polyhedral domain.
%
%               SLOW -- USE THE VECTORIZED VERSION!
%
% INPUT
% verts       - Nx2 array of points, i.e. nodes of the triangle mesh
% tris        - Mx3 array of indices into verts specifying the triangles
% marker      - Nx1 array of boundary flags: 0=interior, 1=Dirichlet
% XW          - Cubature rule: first 2 columns specify the cubature
%               points on the reference triangle, last column the weight.
% f           - A function on the domain
% g           - A function on the boundary of the domain
%
% OUTPUT
%             - F the assembled extended load vector
%
% AUTHORS
% Csirik Mihaly, ELTE
%
function F = fe22_load_d( verts, tris, edges, triedges, XW, f )


        
% Number of elements        
nel = size( tris, 1 );
% Number of nodes
nv = size( verts, 1 ) + size( edges, 1 );
% Output vector
F = zeros( nv, 1 );

INDX = [tris(:,:) triedges(:,:)+ones(nel,3)*size( verts, 1 ) ];

% Linear shape functions in the reference triangle
lambda = quad2_to_bary( XW );

% Shape functions in the reference triangle
theta(:,1) = lambda(:,1) .* ( 2*lambda(:,1) - 1 );
theta(:,2) = lambda(:,2) .* ( 2*lambda(:,2) - 1 );
theta(:,3) = lambda(:,3) .* ( 2*lambda(:,3) - 1 );
theta(:,4) = 4*lambda(:,1) .* lambda(:,2);
theta(:,5) = 4*lambda(:,2) .* lambda(:,3);
theta(:,6) = 4*lambda(:,3) .* lambda(:,1);

for el=1:nel
    p = verts( tris( el, : ), : );
    Aff = [ p( 2, : ) - p( 1, : ); p( 3, : ) - p( 1, :) ];
    jac = abs( det( Aff ) );

    for vi=1:6
        for k=1:size(XW, 1)    
            X = lambda(k,1)*p(1,:) + lambda(k,2)*p(2,:) + lambda(k,3)*p(3,:);
            F(INDX( el, vi )) = F(INDX( el, vi )) + jac*XW(k,3) * f( X(1), X(2) ) * theta(k,vi);
        end
    end
end
