function [ Dlambda, areas ] = fe2_grad_bary( verts, tris )

nel = size( tris, 1 );

edges = zeros( nel, 2, 3 );

edges(:,:,1) = verts(tris(:,3),:)-verts(tris(:,2),:);
edges(:,:,2) = verts(tris(:,1),:)-verts(tris(:,3),:);
edges(:,:,3) = verts(tris(:,2),:)-verts(tris(:,1),:);

areas = 0.5*abs(edges(:,2,3).*edges(:,1,2)-edges(:,1,3).*edges(:,2,2));

areasrep = repmat( areas, 1, 3 );

Dlambda = zeros( nel, 2, 3 );

% rotate edges and normalize to get side normal vectors and divide by 2*area
Dlambda(:,1,:) = -squeeze(edges(:,2,:)) ./ (2*areasrep);
Dlambda(:,2,:) = squeeze(edges(:,1,:)) ./ (2*areasrep);



end

