%
% fe2_load_v  - Fast, vectorized assembly of the first-order finite element 
%               load vector
%
% INPUT
% verts       - Nx2 array of points, i.e. nodes of the triangle mesh
% tris        - Mx3 array of indices into verts specifying the triangles
% XW          - Cubature rule: first 2 columns specify the cubature
%               points on the reference triangle, last column the weight.
% f           - A function on the domain
%
% OUTPUT
% F           - The assembled extended load vector
%             To select the actual F, use F(dof) with dof obtained from
%             fe2_boundary.
% AUTHORS
% Csirik Mihaly, ELTE
%
%
function F = fe2_load_v( verts, tris, XW, rhsfunc )

% Number of elements
nel = size( tris, 1 );
% Number of vertices (nodes)
nv = size( verts, 1 );

% 
% Precompute edges and jacobians
%
edges = zeros( nel, 2, 1 );

edges(:,:,1) = verts(tris(:,3),:)-verts(tris(:,2),:);
edges(:,:,2) = verts(tris(:,1),:)-verts(tris(:,3),:);
edges(:,:,3) = verts(tris(:,2),:)-verts(tris(:,1),:);

areas = abs(edges(:,2,3).*edges(:,1,2)-edges(:,1,3).*edges(:,2,2));

%
% Assemble load vector
%

nc = size( XW, 1 );
XX = zeros( nel, nc, 2 );
rhsfuncints = zeros( nel, 3 );

for k=1:nc
    % Physical cubature points for all triangles
    for d=1:2
        % XW is given on the reference triangle on which the barycentric
        % coordinates are easily evaluated.
        XX(:,k,d) = XW(k,1) * verts( tris( :, 1 ), d ) ...
                  + XW(k,2) * verts( tris( :, 2 ), d ) ...
                  + (1.0 - XW(k,1) - XW(k,2)) * verts( tris( :, 3 ), d );
    end
end

% RHS cubature contributions
for k=1:nc
    % Substitute cubature points into f
    rhsfuncXX = arrayfun( rhsfunc, XX(:,k,1), XX(:,k,2) );
    % Accumulate local contributions for each shape function
    rhsfuncints(:,1) = rhsfuncints(:,1) + XW(k,3) * rhsfuncXX .* areas * (1-XW(k,1)-XW(k,2));
    rhsfuncints(:,2) = rhsfuncints(:,2) + XW(k,3) * rhsfuncXX .* areas * XW(k,1);
    rhsfuncints(:,3) = rhsfuncints(:,3) + XW(k,3) * rhsfuncXX .* areas * XW(k,2);
end

F = accumarray( tris(:), rhsfuncints(:), [nv 1] );


