%
% fe2_load_d  - Dense assemble the first-order finite element load vector for the 
%               Poisson equation with Dirichlet boundary values on 
%               a polyhedral domain.
%
%               SLOW -- USE THE VECTORIZED VERSION!
%
% INPUT
% verts       - Nx2 array of points, i.e. nodes of the triangle mesh
% tris        - Mx3 array of indices into verts specifying the triangles
% marker      - Nx1 array of boundary flags: 0=interior, 1=Dirichlet
% XW          - Cubature rule: first 2 columns specify the cubature
%               points on the reference triangle, last column the weight.
% f           - A function on the domain
% g           - A function on the boundary of the domain
%
% OUTPUT
% F           - The assembled extended load vector
%             To select the actual F, use F(dof) with dof obtained from
%             fe2_boundary.
% AUTHORS
% Csirik Mihaly, ELTE
%
function F = fe2_load_d( verts, tris, XW, f )


        
% Number of elements        
nel = size( tris, 1 );
% Number of vertices (nodes)
nv = size( verts, 1 );
% Output vector
F = zeros( nv, 1 );


for el=1:nel
    p = verts( tris( el, : ), : );
    Aff = [ p( 2, : ) - p( 1, : ); p( 3, : ) - p( 1, :) ];
    jac = abs( det( Aff ) );
    
    for vi=1:3
        lambdaX = [ ones(size(XW, 1),1) - XW(:,1) - XW(:,2), XW(:,1), XW(:,2) ];
        for k=1:size(XW, 1)    
            X = XW(k,1)*p(1,:) + XW(k,2)*p(2,:) + (1-XW(k,1)-XW(k,2))*p(3,:);
            F(tris( el, vi )) = F(tris( el, vi )) + jac*XW(k,3) * f( X(1), X(2) ) * lambdaX(k,vi);
        end
    end
end
