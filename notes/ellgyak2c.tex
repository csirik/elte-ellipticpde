
\section{Lineáris algebra}
A valós $n\times m$-es mátrixok vektorterét $\Real^{n\times m}$-mel jelöljük. Az $A\in\Real^{n\times m}$ és $B\in\Real^{m\times p}$ mátrixok szorzata $C=AB\in\Real^{n\times p}$,
\[
c_{ij}=\sum_{k=1}^m a_{ik}b_{kj}\;\;(i=1,\ldots n,\;\;j=1,\ldots,p).
\]
Egy $A\in\Real^{n\times m}$ mátrix \textbf{elemenként pozitív}, ha $a_{ij}>0$ ($i=1,\ldots,n$, $j=1,\ldots,m$), jelben $A>0$.\footnote{Figyelem! Az $A>0$
szimbólumot szokás használni a pozitív definitség jelölésére is, de az egy független fogalom.}
A mátrixszorzás definíciója alapján világos, hogy ha $A>0$ és $B>0$, akkor $AB>0$.

Azt mondjuk, hogy egy $A\in\Real^{n\times n}$ négyzetes mátrix \textbf{diagonálisan domináns}, ha 
\begin{equation}\label{diagdom}
\sum_{\substack{j=1\\j\neq i}}^n |a_{ij}|<|a_{ii}| \quad (i=1,\ldots,n).
\end{equation}
Azonnal kövektezik, hogy ha $A$ diagonálisan domináns, akkor $\|A\|_\infty< 2\|\diag A\|_\infty$, ahol $\diag A$ az $A$ főátlójából alkotott vektor.
Valóban,
\[
\|A\|_\infty=\max_{i=1,\ldots,n} \sum_{j=1}^n |a_{ij}|<2\max_{i=1,\ldots,n}|a_{ii}|=2\|\diag A\|_\infty.
\]

A diagonális dominancia megértéséhez idézzük fel a következő, jól ismert tételt.
\begin{gershgorin}
Egy $A\in\Real^{n\times n}$ mátrix minden sajátértéke a komplex síkon a 
\[
D_i=\Bigg\{ z\in\Cmplx \;:\; |z-a_{ii}|\le\sum_{\substack{j=1\\j\neq i}}^n |a_{ij}| \Bigg\}
\]
Gershgorin-körlapok valamelyikében helyezkedik el.
\end{gershgorin}
\begin{proof}
Közvetlen számolással adódik.
\end{proof}
Ebből adódik a következő, Levy--Desplanques-tételnek nevezett állítás.
\begin{bprop}\label{diagdomreg}
Minden diagonálisan domináns mátrix reguláris.
\end{bprop}
\begin{proof}
Legyen $A\in\Real^{n\times n}$ diagonálisan domináns, és indirekt tegyük fel, hogy $A$ nem invertálható.
Ekkor $0\in D_{i_0}$ valamely $1\le i_0\le n$ indexre, azaz 
\[
|a_{i_0,i_0}|\le\sum_{\substack{j=1\\j\neq i_0}}^n |a_{i_0j}|,
\]
ami ellentmond $A$ diagonális dominanciájának.
\end{proof}

\begin{prop}
Minden szimmetrikus, diagonálisan domináns mátrix, amely főátlója pozitív, pozitív definit.
\end{prop}
\begin{proof}
A Gershgorin-tétel szerint érvényes
\[
|\lambda_i-a_{ii}|\le \sum_{\substack{j=1\\j\neq i}}^n |a_{ij}|<a_{ii},
\]
minden $i=1,\ldots,n$ esetén. 
\end{proof}

Érdekességképpen megemlítjük Ostrowski tételét: ha $A\in\Real^{n\times n}$ diagonálisan domináns, akkor
\[
\det A\ge \prod_{i=1}^n \Big(|a_{ii}|-\sum_{\substack{j=1\\j\neq i_0}}^n|a_{ij}|\Big).
\]

Szükségünk lesz még a mértani sor lineáris operátorokra vett általánosítására. Ezt már Funkcionálanalízisből tanultuk.
Emlékszik az olvasó az egyszerű bizonyításra?
\begin{neuseries}
Legyen $X$ Banach-tér. Ha egy $\Map{A}{X}{X}$ lineáris operátorra $\|A\|<1$ teljesül, akkor
$I-A$ invertálható és
\[
(I-A)^{-1}=\sum_{k=0}^\infty A^k \;\; (\text{az $\|\cdot\|$ operátornormában})
\]
\end{neuseries}

\textbf{Rayleigh-hányados.} Előadáson néhányszor felhasználjuk a Rayleigh-hányadosokra vonatkozó eredményeket. Legyen $A\in\Real^{n\times n}$ \textbf{szimmetrikus}, \textbf{pozitív definit}, ekkor
tudjuk, hogy $A$ sajátértékei valósak és pozitívak. Rayleigh kezdte alkalmazni az
\[
\forall x\neq 0 \;:\; R(x)=\frac{Ax\cdot x}{|x|^2}
\]
függvényt az $A$ sajátértékeinek vizsgálatához és a következőkre összefüggésekre jutott\footnote{Lásd pl. Lax P. \emph{Lineáris algebra és alkalmazásai.}}
\begin{align*}
\lambda_{\max}&=\max\Big\{\frac{Ax\cdot x}{|x|^2} \;:\; x\neq 0 \Big\}\\
\lambda_{\min}&=\min\Big\{\frac{Ax\cdot x}{|x|^2} \;:\; x\neq 0 \Big\}
\end{align*}
ahol $\lambda_{\min}$ és $\lambda_{\max}$ jelöli $A$ legkisebb illetve legnagyobb sajátértékét. Továbbá az is igaz, hogy 
az vektor, amelyen a fenti maximum (minimum) felvétetik, egy $\lambda_{\max}$-hoz tartozó sajátvektor. Hasonló állítás érvényes a minimumra is.
A további sajátértékekre a Courant--Fischer-féle minmax tétel vonatkozik.

Az előadáson a fentiekből adódó
\[
\lambda_{\min}|x|^2\le Ax\cdot x\le \lambda_{\max}|x|^2.
\]
egyenlőtlenséget használjuk, amit gyakran szokás úgy is kifejezni, hogy $A$ spektrálisan ekvivalens az $I$ egységmátrixszal.

\section{M-mátrixok}

Az elődáson az $A\in\Real^{N\times N}$ mátrixot \textbf{M-mátrixnak} neveztük, ha $a_{ij}\le 0$ ($i\neq j$) és
létezik olyan $g\in\Real^N$, $g>0$ vektor amelyre $Ag>0$. Egy $A\in\Real^{N\times N}$ mátrix \textbf{megengedett előjeleloszlású}, ha
$a_{ij}\le 0$ ($i\neq j$) és $a_{ii}>0$. Nyilván egy M-mátrix megengedett előjeleloszlású.
A következő alaptétel az M-mátrixok ekvivalens definícióit adja meg.\footnote{vö. G. D. Poole and T. Boullion, \emph{A Survey on M-Matrices.}}
\begin{theorem}
Legyen $A\in\Real^{N\times N}$ megengedett előjeloszlású. Ekkor $A$ pontosan akkor M-mátrix, ha az alábbi ekvivalens feltételek egyike teljesül.
\begin{enumerate}[(1)]\indented
\item Létezik egy $B$ nemnegatív mátrix, és $\alpha>\lambda_{\max}(B)$, amellyel $A=\alpha I - B$.
\item $A$ minden sajátértékének valós része pozitív.
\item $A$ minden főminorja pozitív.
\item $A$ invertálható és $A^{-1}$ nemnegatív.
\item Ha $B$ megengedett előjeleloszlású és $B\ge A$, akkor $B$ invertálható.
\end{enumerate}
\end{theorem}

Most lássuk be az előadáson nem bizonyított állításokat.
\begin{prop}
Egy $A\in\Real^{N\times N}$ megengedett előjeleloszlású mátrix pontosan akkor diagonálisan domináns, ha M-mátrix a $g:=e$ vektor mellett, ahol e a csupa 1-esből álló
oszlopvektor.
\end{prop}
\begin{proof}
Ha $A$ diagonálisan domináns, akkor \az{\eqref{diagdom}} reláció alapján most
\[
-\sum_{i\neq j} a_{ij}< a_{ii}\quad (i=1,\ldots,N),
\]
amiből átrendezéssel
\[
0<\sum_{j=1}^N a_{ij}=(Ae)_i \quad (i=1,\ldots,N),
\]
azaz $Ae>0$, tehát $A$ M-mátrix.

Megfordítva, legyen $A$ M-mátrix a $g=e$ vektor mellett, ekkor 
\[
\sum_{j=1}^N a_{ij}=a_{ii}+\sum_{j\neq i} a_{ij}>0 \quad (i=1,\ldots,N).
\]
Mivel $A$ megengedett előjeleloszlású, ezért $a_{ii}=|a_{ii}|$ és $-a_{ij}=|a_{ij}|$, amiből következik a diagonális dominancia.
\end{proof}

\begin{prop}
Ha az $A$ megengedett előjeleloszlású mátrix diagonálisan domináns, akkor $A$ reguláris és $A^{-1}$ nemnegatív.
\end{prop}
\begin{proofnoqed}
Az állítás első felét már beláttuk \az{\ref{diagdomreg}}. Állításban. 
Az állítás második részéhez a Neumann-sort fogjuk felhasználni:
\[
A^{-1}=\frac{1}{\alpha}\big(I-(I -\alpha A)\big)^{-1}=\frac{1}{\alpha}\sum_{k=0}^\infty (I-\alpha A)^k.
\]
A bizonyítás kész, ha meg tudunk adni egy olyan $\alpha>0$ paramétert, amellyel $\|I-\alpha A\|_\infty<1$ és $I-\alpha A\ge 0$, amelyhez elég
\[
|1-\alpha a_{ii}| + \alpha \sum_{j\neq i} |a_{ij}|<1,\;\;\text{és} \quad 1-\alpha a_{ii}>0 \quad (i=1,\ldots,N).
\]
Az utóbbi feltétel kielégíthető az $\alpha=\frac{1}{\max a_{jj}}$ választással, és ekkor
\[
1-\frac{a_{ii}}{\max a_{jj}}- \frac{1}{\max a_{jj}}\sum_{j\neq i}a_{ij}= 1 - \frac{1}{\max a_{kk}}\sum_{j=1}^Na_{ij}<1\rqedhere
\]
\end{proofnoqed}

\begin{prop}
Ha az $A$ M-mátrix, akkor $A$ reguláris és $A^{-1}$ nemnegatív.
\end{prop}
\begin{proof}
Legyen $G=\diag(g)$, ahol $g>0$ az $A$ M-mátrix tulajdonságából származó vektor. Ekkor $AG$ diagonálisan domináns, ugyanis
\[
\sum_{j\neq i}|(AG)_{ij}|=\sum_{j\neq i}|a_{ij}|g_j=a_{ii}g_i-\sum_{j=1}^N a_{ij}g_j<a_{ii}g_i=|a_{ii}g_i|=|(AG)_{ii}|.
\]
\Az{\ref{diagdomreg}}. Állítást alkalmazva kapjuk a regularitást és az előző állításból $A^{-1}$ nemnegativitását.
\end{proof}

A $B$ mátrix \textbf{monoton}, ha megőrzi a pontonként rendezést, azaz ha $x\le y$, akkor $Bx\le By$.
Ezzel ekvivalens, hogy $Bx\ge 0$ minden $x\ge 0$ esetén. Az előző állítás alapján tehát egy M-mátrix inverze monoton.
A következő állítás lényeges lesz stabilitásbecslések bizonyításánál.
\begin{prop}
Legyen $A$ M-mátrix egy $g>0$ vektorral. Ekkor $\|A^{-1}\|_\infty\le \frac{\max g_i}{\min (Ag)_i}$, azaz minden $b\in\Real^N$ esetén
az $Ax=b$ egyenlet $x\in\Real^N$ megoldására
\[
\|x\|_\infty \le \frac{\max g_i}{\min (Ag)_i} \|b\|_\infty.
\]
\end{prop}
\begin{proof}
Minden $b\in\Real^N$ vektorra érvényes a triviális
\[
\pm b \le \frac{Ag}{\min (Ag)_i} \|b\|_\infty
\] 
koordinátánkénti egyenlőtlenség. Az $A^{-1}$ monotonitását felhasználva adódik
\[
\pm x\le \frac{g}{\min (Ag)_i} \|b\|_\infty,
\]
amiből következik az állítás.
\end{proof}

\section{Egy híres tridiagonális mátrix*}

Ugyan nem tartozik szorosan a félév anyagához, érdemes ismerni a sokszor látott (pl. Numerikus analízisen, vagy rugós rendszereknél)
\[
T=\begin{pmatrix}
a & b &  & &  \\
b & a & b & &  \\
 & \ddots & \ddots & \ddots & \\
 & & b & a & b \\
 & & & b & a
\end{pmatrix}
\]
tridiagonális mátrix sajátértékeit és sajátvektorait. 

Ehhez írjuk a $T\in\Real^{n\times n}$ mátrixot 
\[
T=aI + bG
\]
alakba, ahol
\[
G=\begin{pmatrix}
0 & 1 &  & &  \\
1 & 0 & 1 & &  \\
 & \ddots & \ddots & \ddots & \\
 & & 1 & 0 & 1 \\
 & & & 1 & 0
\end{pmatrix}.
\]
A feladatot visszavezetjük $G$ sajátértékfeladatára. 

\textbf{A segédfeladat megoldása.} Minthogy $G$ szimmetrikus, minden $\lambda$ sajátértéke valós.
Határozzuk meg egy $\lambda$ sajátértékhez tartozó $v$ sajátvektorát. Tudjuk, hogy létezik $\{v^\ell\}_{\ell=1}^n$ ortogonális sajátvektorrendszer
és $\{\lambda_\ell\}_{\ell=1}^n$ sajátértékek. A sajátértékfeladat
\[
(G-\lambda I)v=0.
\]
Írjuk ki expliciten az egyenletrendszert,
\[
\left.
\begin{aligned}
v_2 - \lambda v_1&=0 \\
v_1 - \lambda v_2 + v_3&=0\\
\vdots&\\
v_{k-1} - \lambda v_k + v_{k+1}&=0\\
\vdots&\\
v_{n-1}-\lambda v_n&=0
\end{aligned}
\right\}
\]
Ha most bevezetjük a $v_0=0$ és $v_{n+1}=0$ jelöléseket, akkor azt mondhatjuk, hogy a 
\begin{equation}\label{linrecur}
v_{k-1} - \lambda v_k + v_{k+1}=0 \;\; (k=1,\ldots,n)
\end{equation}
lineáris rekurziót kell megoldanunk. Valamilyen $r\in\Real$ mellett tegyük fel, hogy a megoldás $v_k=r^k$ alakú.
A rekurzióba helyettesítve kapjuk az
\[
r^2 -\lambda r + 1=0
\]
karakterisztikus egyenletet. Ennek megoldásai
\[
r_{\pm}=\frac{\lambda\pm\sqrt{\lambda^2 - 4}}{2}.
\]
A Vieta-formulák szerint érvényesek a
\[
r_+r_-=1, \quad r_++r_-=\lambda
\]
azonosságok. Két esetet különböztetünk meg: $\lambda\neq\pm 2$ vagy $\lambda=\pm 2$. 

Ha $\lambda\neq\pm 2$, akkor a gyökök különbözőek és lineáris rekurzióknál tanultak alapján \az{\eqref{linrecur}} feladat általános megoldása 
\[
v_k=C_+r^k_+ + C_-r^k_-= C_+r^k + C_-r^{-k} \;\; (k=0,\ldots,n+1),
\]
ahol $r=r_+$. A $C_+$, $C_-$ konstansokat a ,,peremfeltételekből'' lehet meghatározni; a $v_0=C_++C_-=0$
peremfeltételből $C:=C_+=-C_-$, így a 
\begin{equation}\label{vkgen}
v_k=C(r^k-r^{-k})
\end{equation}
általános megoldást kapjuk. Mivel $v$ sajátvektor, $C\neq 0$ és a második peremfeltételből kapjuk az $r^{n+1}-r^{-n-1}=0$, azaz
\begin{equation}\label{r1}
r^{2n+2}=1.
\end{equation}
Ebből adódik $|r|=1$, amiből
$|\lambda|=\Big|r+\frac{1}{r}\Big|\le 2$, sőt a jelen esetben $|\lambda|<2$.

Ha $\lambda=\pm 2$, akkor csak egy gyök van: $r=\lambda/2$ és ezért a lineáris rekurzió általanos megoldása
\[
v_k=(C_1+C_2k)r^k.
\]
Az első peremfeltételből $C_1=0$ adódík, míg a másodikből $C_2(n+1)r^{n+1}=0$, ezért $C_2=0$. Ezért a $\lambda=\pm 2$ esetben nem kapunk sajátvektort.

Most már meg tudjuk adni a sajátértékfeladat általános megoldását. Legyen ehhez 
\[
r=e^{i\theta}.
\]
Ekkor
\[
\lambda=r+\frac{1}{r}=e^{i\theta}+e^{-i\theta}=2\cos\theta
\]
az Euler-formula alapján. \Az{\eqref{r1}} egyenletből kapjuk az
\[
e^{i(2n+2)\theta}=1=e^{2\pi i \ell}
\]
relációt tetszőleges $\ell=1,\ldots,n$ esetén; ebből
\[
\theta=\theta_\ell=\frac{\pi \ell}{n+1}.
\]
Itt az $\ell=0$ és az $\ell=n+1$ esetek ki vannak zárva a $\lambda\neq\pm 2$ miatt. Mármost \az{\eqref{vkgen}} megoldásba ezeket beírva,
\[
v_k=C(e^{i\frac{\pi k\ell}{n+1}} - e^{-i\frac{\pi k\ell}{n+1}}),
\]
ezért a $C=\frac{1}{2i}$ választással valós megoldást kapunk; éspedig
\[
v_k=\sin \frac{\pi k\ell}{n+1}.
\]

\textbf{Az általános feladat megoldása.} Összefoglalva, a $Gv=\lambda v$ sajátértékfeladat megoldása:
\[
\lambda_\ell=2\cos\frac{\pi \ell}{n+1}, \quad v^\ell=\Big(\sin \frac{\pi \ell}{n+1},\ldots,\sin \frac{\pi k\ell}{n+1},\ldots,\sin \frac{\pi kn}{n+1}\Big)
\]
minden $\ell=1,\ldots,n$ esetén. A $T=aI+bG$ mátrix sajátértékei egyszerű affin transzformációval kaphatóak:
\[
\mu_\ell=a+2b\cos\frac{\pi \ell}{n+1},
\]
ezekre ugyanis $Tv^\ell=\mu_\ell v^\ell$. Ezekből speciálisan az
\[
T=\begin{pmatrix}
2 & -1 &  & &  \\
-1 & 2 & -1 & &  \\
 & \ddots & \ddots & \ddots & \\
 & & -1& 2 & -1 \\
 & & & -1 & 2
\end{pmatrix}
\]
mátrix sajátértékei
\[
\lambda_\ell=2-2\cos\frac{\pi \ell}{n+1}=4\sin^2 \frac{\pi\ell}{2n+2}
\]
ahol $\ell=1,\ldots,n$. Legyen $f(x)=4\sin^2 x$, ahol $x\in[\frac{\pi}{2n+2},\frac{\pi n}{2n+2}]$. Az $f$ függvény
minimuma az $x=0$, maximuma az $x=\frac{\pi}{2}$ pontban van, ezért 
\[
\lambda_1=4\sin^2 \frac{\pi}{2n+2}
\]
a minimális és 
\[
\lambda_n=4\sin^2 \frac{\pi n}{2n+2}
\]
a maximális sajátértéke a $T$ mátrixnak.

\Exercises
\begin{Exercise}
Határozzuk meg az
\[
\begin{pmatrix}
1 & -1 &  & & &  \\
 & 1 & -1 &  &  &  \\
 &  & \ddots & \ddots & &  \\
 &  &  & \ddots & \ddots  &  \\
 &  &  &  & 1 & -1 \\
&  &  &  &  & 1
\end{pmatrix}
\]
$n\times n$-es mátrix inverzét.
\end{Exercise}

\begin{Exercise}
Mutassuk meg, hogy $R(x)\le\|A\|$, ha $x\neq 0$.
\end{Exercise}


\begin{Exercise}
Lássuk be, hogy ha $Av=\lambda v$, akkor tetszőleges $p$ polinomra $p(A)v=\lambda(\lambda)v$.
\end{Exercise}

\begin{Exercise}[difficulty=1]
Rekurzió felírásával lássuk be, hogy a $D(\lambda)=\det(G-\lambda I)$ determinánsra
$|\lambda|<1$ esetén érvényes a
\[
D(\lambda)=\frac{\sin(n+1)\theta}{\sin\theta}
\]
kifejezés, ahol $\theta\in(0,\pi)$ és $\cos\theta=-\frac{\lambda}{2}$. Ennélfogva
\[
V_n(\cos\theta)=D(\lambda),
\]
ahol $V_n$ az elsőfajú Chebyshev-polinomot jelöli.
\end{Exercise}