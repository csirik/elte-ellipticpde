
\section{A végeselem általános definíciója}
Az elsőrendű végeselem-módszer tárgyalásakor nem ellenőriztük az előadáson elhangzott definíciókat (lásd 48. o.). 
Idézzük fel, hogy milyen tulajdonságokat követeltünk meg a $V_h$ approximáló altértől.

``A $V_h$ altér szakaszonként polinomokból áll, melyek folytonosok az egész tartományon,
azaz
\[
V_h\subset \{u\in C(\overline{\Omega}) \;:\; \restr{u}{T_k}\in P^{\ell_k}(T_k)\;\forall k=1,\ldots,M\},
\]
ahol $P^{\ell_k}(T_k)$ jelöli a legfeljebb $\ell_k$-adfokú polinomok $T_k$-ra való leszűkítésének halmazát.
Általában az alábbi tulajdonságú altereket szokás használni:
\begin{enumerate}[(1)]
\item A $T\in\mathcal{T}_h$ halmazok azonos típusúak, vagyis egy adott trianguláció csupa
(zárt) háromszögből/ tetraéderből vagy csupa (zárt) négyszögből/téglatestből áll.
\item $\ell_k\equiv\ell$, vagyis minden résztartományon azonos fokú polinomokat tekintünk.
\item Előfordulhat, hogy $V_h$-ban $\restr{u}{T_k}$ nem az összes legfeljebb $\ell_k$-adfokú 
(ill. $\ell$-edfokú) polinomot veheti fel, ennek pontosítására használjuk a
\[
P(T_k):=\{\restr{u}{T_k} \;:\; u\in V_h\}
\]
jelölést.
\item Az $\ell$-edfokú polinomokat $T_k$-ban kijelölt \emph{csomóponti értékek} határozzák meg.
Ezek függvény-értékek, és lehetnek még deriváltértékek is. Amikor a csomóponti értékek csak függvény-értékek,
akkor a $\{\phi_i\}\subset P(T_k)$ \emph{bázis} olyan polinomokból áll, melyek egy adott csomópontban 1-et, a többiben 0-t vesznek fel,
azaz ha $x_1,\ldots,x_r$ jelöli a csomópontokat, akkor
\begin{equation}\label{phidelta}
\phi_i(x_j)=\delta_{ij}.\text{''}
\end{equation}
\end{enumerate}

A (4)-es tulajdonság ellenőrzése igényli a legtöbb munkát, foglalkozzunk részletesebben tehát ezzel. 
A fenti definíciót konkrétabbá téve egy 
\[
\Sigma=\{\sigma_1,\ldots,\sigma_r\}\subset P(T)^*
\] bázist \textbf{csomóponti változóknak} nevezzük. Például a $T_3$ Courant-elem esetében a 
\[
T=\{\fie{a}_1,\fie{a}_2,\fie{a}_3\}^{co}
\]
háromszögön a
\[
\Sigma=(\delta_{\fie{a}_1},\delta_{\fie{a}_2},\delta_{\fie{a}_3})
\]
csomóponti bázist használtuk\footnote{Ahol $\delta_a(f)=f(a)$ ($f\in C$) a Dirac-$\delta$ funkcionál.} és $P(T)$ a lineáris polinomokból állt. 

Mármost $\Sigma$ egy bázis $P(T)$ duális terében, így $P(T)$ véges dimenziós volta miatt $\Sigma$ kanonikus módon indukál egy 
\begin{equation}\label{phibase}
\Phi=\{\phi_1,\ldots,\phi_r\}\subset P(T)
\end{equation}
bázist, amelyet \textbf{alakfüggvényeknek} nevezünk és amelyre \az{\eqref{phidelta}} megfelelő általánosítása, egy $\delta$-tulajdonság teljesül:
\[
\sigma_i(\phi_j)=\delta_{ij}.
\]
Ebben az esetben a $\Sigma$-t \textbf{csomóponti bázisnak} nevezzük.
A $T_3$ Courant-elemek esetén az alakfüggvények a baricentrikus koordináták voltak: $\phi_i(\fie{x})=\lambda_i(\fie{x})$ ($i=1,2,3$),
ezek a konstrukciójuk miatt teljesítették a $\delta$-tulajdonságot.

A következő absztrakt lemma megmutatja, hogy miért is nevezik ezt a tulajdonságot néha \emph{egyértelmű megoldhatóságnak}:
egy adott $v\in P(T)$ esetén a $\sigma_i(v)$ helyettesítési értékek (ahol $i=1,\ldots,r$) egyértelműen meghatározzák $v$-t.
\begin{blemm}
Legyen $P$ véges dimenziós vektortér, és legyen $\Sigma=\{\sigma_1,\ldots,\sigma_r\}\subset P^*$. Ekkor az alábbi állítások ekvivalensek.
\begin{enumerate}[(i)]\indented
\item $\Sigma=(\sigma_1,\ldots,\sigma_r)\subset P^*$ bázis.
\item Ha $v\in P$ és $\sigma_i(v)=0$ minden $i=1,\ldots,r$-re, akkor $v=0$.
\item Ha $v_1,v_2\in P$ és $\sigma_i(v_1)=\sigma_i(v_2)$ minden $i=1,\ldots,r$-re, akkor $v_1=v_2$.
\end{enumerate}
\end{blemm}
\begin{proof}
Mivel $r=\dim P=\dim P^*$, a $\{\sigma_1,\ldots,\sigma_r\}$ halmaz pontosan akkor bázis $P^*$-ban, 
ha bármely $\ell\in P^*\iso P$ lineáris funkcionál esetén egyértelműen léteznek $c_1,\ldots,c_r\in\Real$ számok, hogy
\[
\ell=c_1\sigma_1+\ldots+c_r\sigma_r,
\]
Legyen most $\{\phi_1,\ldots,\phi_r\}\subset P$ tetszőleges bázis. Ekkor az iménti egyenlőség pontosan akkor teljesül, ha a $\{\phi_i\}$ bázison teljesül,
azaz
\[
\ell(\phi_i)=c_1\sigma_1(\phi_i)+\ldots+c_r\sigma_r(\phi_i)=\sum_{j=1}^r \sigma_j(\phi_i)c_j,
\]
minden $i=1,\ldots,r$ esetén. Azt kaptuk tehát, hogy
a $\{\sigma_1,\ldots,\sigma_r\}$ halmaz pontosan akkor bázis $P^*$-ban, 
ha bármely $\ell\in P^*\iso P$ lineáris funkcionál esetén egyértelműen létezik $c\in\Real^r$ úgy, hogy ha $\{\phi_1,\ldots,\phi_r\}\subset P$ tetszőleges
bázis, $B=(\sigma_j(\phi_i))_{i,j=1}^r$ és $y=(\ell(\phi_i))_{i=1}^r$, akkor $Bc=y$ és $B$ invertálható. 

Ha $v\in P$ tetszőleges, akkor ehhez egyértelműen létezik $a\in\Real^r$ úgy, hogy $v=\sum a_i\phi_i$. Tegyük fel, hogy hacsak $\sigma_j(v)=0$ minden $j=1,\ldots,r$-re, 
azaz 
\[
\sum_{i=1}^r \sigma_j(\phi_i)a_i=0
\] minden $j=1,\ldots,r$-re, akkor ebből szükségképpen $a=0$ következik. Más szóval, $B^\trans a=0\;\impli\;a=0$, ami ekvivalens azzal, hogy 
$B^\trans$ invertálható, ami viszont azzal ekvivalens, hogy $B$ invertálható.
\end{proof}
Ezzel tehát eljutottunk a végeselem, mint a végeselem-módszer lokális építőkövének általános definíciójához.
\begin{bdefn}
Legyen $T\subset\Rn$ egy korlátos zárt halmaz, amelynek nemüres a belseje és szakszonként sima a pereme.
Legyen továbbá $P(T)$ egy véges dimenziós $T$-n értelmezett függvényekből álló vektortér. Végül legyen
$\Sigma=\{\sigma_1,\ldots,\sigma_r\}\subset P(T)^*$ egy bázis. Ekkor az mondjuk, hogy a $(T,P(T),\Sigma)$ hármas egy \textbf{végeselem}.
\end{bdefn}
Abban a speciális esetben nevezzük $\Sigma$-t \textbf{csomóponti bázisnak}, ha teljesül a $\delta$-tulajdonság.

Definiáljuk a $(T,P(T),\Sigma)$ végeselemhez a $\Map{\Pi_T}{V(T)}{P(T)}$ \textbf{lokális interpolációs operátort},
\[
\Pi_Tv=\sum_i \sigma_i(v)\phi_i,
\]
ahol $v$ olyan függvény, amelyen minden $\sigma_i$ értelmezve van. 
\begin{bprop}
A $\Map{\Pi_T}{V(T)}{P(T)}$ lokális interpolációs operátorra érvényesek a következő tulajdonságok.
\begin{enumerate}[(a)]\indented
\item $\Pi_T$ lineáris,
\item $\sigma_i(\Pi_Tv)=\sigma_i$ minden $i=1,\ldots,r$-re,
\item $\Pi_Tv=v$ minden $v\in P(T)$ esetén,
\item $\Pi^2_T=\Id_{P(T)}$.
\end{enumerate}
\end{bprop}
Az egyszerű bizonyítást az olvasóra bízzuk.

A $\Pi_Tv$ lokális interpolánsokból össze tudunk rakni az egész $\mathcal{T}_h$ trianguláción értelmes 
$\Pi_h$ \textbf{globális interpolációs operátort}, amelyre
\[
\forall T\in\mathcal{T}_h \;:\; \restr{\Pi_hv}{T}=\Pi_Tv.
\]
Ha a szakaszonként összeállított $\Pi_hv$ függvény $C^m(\overline{\Omega})$-osztályú, akkor a $\Pi_h$ interpolációs
operátort $m$\emph{-edrendűnek} nevezzük és a generáló végeselemet $C^m$\emph{-elemnek} hívjuk.
Ez azt jelenti, hogy az elemek határán nem csupán folytonosan csatlakoznak a lokális interpolánsok, hanem
magasabb rendű simaságot is garantálnunk kell.

\section{A másodfokú $T_6$ Lagrange-elem}
\begin{center}
\includegraphics[scale=1.0]{fig/t6.pdf}
\end{center}

A $T_6$ Lagrange-elem estén a $P^2(T)$ másodfokú polinomok terében interpolálunk. A $T=\{\fie{a}_1,\fie{a}_2,\fie{a}_3\}^{co}$ háromszögön a
csomóponti bázis
\[
\Sigma=\{\delta_{\fie{a}_1},\delta_{\fie{a}_2},\delta_{\fie{a}_3},\delta_{\fie{a}_{12}},\delta_{\fie{a}_{23}},\delta_{\fie{a}_{13}}\},
\]
ahol $\fie{a}_{ij}=(\fie{a}_i+\fie{a}_j)/2$ jelöli az oldalfelező pontokat. Azt állítjuk, hogy a
\[
\Theta=\{\theta_1,\theta_{2},\theta_{3},\theta_{12},\theta_{23},\theta_{13}\},\quad \theta_i=\lambda_i(2\lambda_i-1), \quad \theta_{ij}=4\lambda_i\lambda_j
\]
alakfüggvényekkel \textbf{végeselemet} kapunk, ahol $\{\lambda_1,\lambda_2,\lambda_3\}$ baricentrikus koordináták. A $\delta$-tulajdonság teljesen nyilvánvaló a definíciókból.
Legyen most $v\in P^2(T)$ olyan, hogy $v(a_i)=v(a_{ij})=0$. Ebből le akarjuk vezetni, hogy $v=0$. A következő általános eredményt fogjuk használni.
\begin{blemm}\label{lagrlem}
Legyen $p\in P^d$, ahol $d\ge 1$ és tételezzük fel, hogy $p\equiv 0$ valamely
\[
L=\{\fie{x}\;:\;\ell(\fie{x})=0\}
\]
 hipersíkon, ahol $0\neq \ell\in P^1$. Ekkor
létezik $q\in P^{d-1}$ úgy, hogy $p=\ell q$.
\end{blemm}

Visszatérve a $T_6$ elem egyértelmű megoldhatóságára: az 
\[
L_1=\{\fie{a}_1+t(\fie{a}_2-\fie{a}_1)\;:\;t\in\Real\}
\]
oldalegyenesen $\restr{v}{L_1}$ egyváltozós másodfokú polinom, amely három pontban eltűnik. Ebből $\restr{v}{L_1}\equiv 0$.
A lemma szerint $v=\ell_1 q_1$, ahol $q_1$ elsőfokú. Felhasználva, hogy 
\[
\left.
\begin{aligned}
0&=v(\fie{a}_3)=\ell_1(\fie{a}_3)q_1(\fie{a}_3)\\
0&=v(\fie{a}_{23})=\ell_1(\fie{a}_{23})q_1(\fie{a}_{23})\\
0&=v(\fie{a}_{13})=\ell_1(\fie{a}_{13})q_1(\fie{a}_{13})
\end{aligned}
\right\}
\]
kapjuk, hogy a lineáris $q_1$ három, nem kollineáris pontban eltűnik, ezért $q_1=0$ és így $v=0$.

\begin{rmrk}
A $\Phi$ alakfüggvények segítségével megadható a $\Pi_T$ interpolációs operátor konkrét alakja:
\[
\Pi_Tv=\sum_{i=1}^3 v(\fie{a}_i)\lambda_i(2\lambda_i-1) + 4\sum_{\substack{i,j=1\\i<j}}^3 v(\fie{a}_{ij}) \lambda_i\lambda_j.
\]
Vegyük észre, hogy a $2\lambda_i(2\lambda_i-1)$ polinom negatív értékeket is felvesz, így a $v\ge 0$ ($T$-n) relációból nem következik $\Pi_Tv\ge 0$ ($T$-n).
Ha tehát $v$ valamilyen \emph{a priori} nemnegatív mennyiséget ír le, pl. koncentrációt vagy abszolút hőmérsékletet, akkor ügyelnünk
kell arra, hogy a $\Pi_Tv$ interpoláns megsértheti ezt a követelményt.
\end{rmrk}

\subsection{Összefűzés} 
A merevségi mátrix összefűzéséhez ki kell értékelnünk $a(\ph,\ph)$-t $\Theta\times\Theta$-n. Világos, hogy
\[
D\theta_i=D\lambda_i(4\lambda_i-1), \quad D\theta_{ij}=4(\lambda_jD\lambda_i+\lambda_iD\lambda_j).
\]
Mivel az $R$ referenciaelemen
\[
\lambda_1(x_1,x_2)=1-x_1-x_2,\quad\lambda_2(x_1,x_2)=x_1,\quad\lambda_3(x_1,x_2)=x_2,
\]
így szintén a referenciaelemen az alakfüggvények konkrétan
\[
\left.
\begin{aligned}
\theta_1(x_1,x_2)&=(1-x_1-x_2)(1-2x_1-2x_2)=1-3x_1-3x_2+4x_1x_2+2x_1^2+2x_2^2\\
\theta_2(x_1,x_2)&=x_1(2x_1-1)=2x_1^2-x_1\\
\theta_3(x_1,x_2)&=x_2(2x_2-1)=2x_2^2-x_2\\
\theta_{12}(x_1,x_2)&=4(1-x_1-x_2)x_1=4(x_1-x_1^2-x_1x_2)\\
\theta_{23}(x_1,x_2)&=4x_1x_2\\
\theta_{13}(x_1,x_2)&=4(1-x_1-x_2)x_2=4(x_2-x_2^2-x_1x_2)
\end{aligned}
\right\}
\]
amelyek deriváljai
\[
D\theta_1(x_1,x_2)=\begin{pmatrix}
-3 + 4x_1 + 4x_2\\
-3 + 4x_1 + 4x_2
\end{pmatrix},\;
D\theta_2(x_1,x_2)=\begin{pmatrix}
4x_1-1\\
0
\end{pmatrix},\;
D\theta_3(x_1,x_2)=\begin{pmatrix}
0\\
4x_2-1
\end{pmatrix}
\]
és
\[
D\theta_{12}(x_1,x_2)=4\begin{pmatrix}
1-2x_1-x_2\\
-x_1
\end{pmatrix},\;
D\theta_{23}(x_1,x_2)=4\begin{pmatrix}
x_2\\
x_1
\end{pmatrix},\;
D\theta_{13}(x_1,x_2)=4\begin{pmatrix}
-x_2\\
1-2x_2-x_1
\end{pmatrix}
\]
\textbf{Figyelem!} Az összefűzés során a bázisfüggvények \emph{sorrendje} rögzített!

Veszessük be a $\phi_{T,x}$ alakfüggvényeket, amelyek a $T$ háromszöghöz tartozó $T_6$ alakfüggvényeket jelölik, ám $x$ most nem csak a csúcsokat futja be,
hanem az oldalfelező pontokat is (tekinthetőek ``virtuális pontoknak''). Most gondoljunk bele, hogyan tudjuk gyorsan kiszámolni az
\begin{equation}\label{aphiphi}
\begin{aligned}
a(\theta_{T,\fie{x}},\theta_{T,\fie{y}})&=|\det Z|\int_R Z^{-1}D\theta_{R,\bm{\zeta}^{-1}(\fie{x})}\cdot Z^{-1}D\theta_{R,\bm{\zeta}^{-1}(\fie{y})}\\
&=|\det Z|\int_R \sum_{m=1}^2\sum_{i=1}^2\sum_{j=1}^2 z^{mi} z^{mj}\frac{\partial \theta_k}{\partial x_i}\frac{\partial \theta_\ell}{\partial x_j}
\end{aligned}
\end{equation}
integrálokat, ahol $Z^{-1}=(z^{mi})_{m,i=1}^2$. Most nem mondhatjuk azt, hogy az integrandus konstans. Viszont a mátrix-vektor szorzás definíciója és a skalárszorzat, illetve az integrál linearitása
miatt elegendő előre meghatározni az
\begin{equation}\label{dphidphi}
I_{i,j,k,\ell}=\int_R \frac{\partial \theta_k}{\partial x_i}\frac{\partial \theta_\ell}{\partial x_j}
\end{equation}
mennyiségeket. Ekkor ugyanis \az{\eqref{aphiphi}} egyenlet jobb oldala
\begin{equation}\label{aasum}
|\det Z|\sum_{i=1}^2\sum_{j=1}^2 I_{i,j,k,\ell}\sum_{m=1}^2z^{mi}z^{mj}.
\end{equation}
Az $I_{i,j,k,\ell}$ meghatározása könnyű, bár megterhelő munka. Például
\begin{align*}
I_{1,1,1,1}=\int_R &\frac{\partial \theta_1}{\partial x_1}\frac{\partial \theta_1}{\partial x_1}\,dx_1\,dx_2=\int_R(-3 + 4x_1 + 4x_2)^2\,dx_1dx_2\\
&=\frac{1}{6}\Big[\Big(-3+4\cdot\frac{1}{2}+4\cdot 0\Big)^2 + \Big(-3+4\cdot\frac{1}{2}+4\frac{1}{2}\Big)^2 + \Big(-3+4\cdot 0 +4\cdot\frac{1}{2}\Big)^2\Big]\\
&=\frac{1}{6}\Big[(-1)^2 + 1^2 + (-1)^2\Big]=\frac{1}{2}
\end{align*}
ahol az előző gyakorlaton tanult másodrendű kubatúraformulát alkalmaztuk (mivel másodfokú az integrandus). Másik példa:
\begin{align*}
I_{2,2,3,4}=\int_R &\frac{\partial \theta_3}{\partial x_1}\frac{\partial \theta_4}{\partial x_2}\,dx_1\,dx_2=\int_R (4x_2-1)(-4x_1)\,dx_1dx_2\\
&=\frac{1}{6}(0 + (-2) + 2)=0
\end{align*}

Könnyen látható, hogy \az{\eqref{dphidphi}} formulában $2\times 2\times 6\times 6=144$ számot kell előre meghatároznunk (mínusz a szimmetriák). 
Ezeket a számításokat nem szokás papíron elvégezni (ha hibát vétünk, teljesen elromlik a diszkretizáció), inkább írunk rá egy kis programot. Szimbolikus programcsomagok is alkalmazhatóak ilyen
táblázatok generálására (pl. Maple, Mathematica, vagy a MATLAB Symbolic Math Toolbox-ja). De még jobb, ha kubatúrát alkalmazunk, hiszen lehet tudni, hogy hanyadfokú
polinomok lesznek az integrandusok és így alkalmas kubatúraformulával pontos eredmény kaphatunk.


\begin{center}
\includegraphics[scale=1.0]{fig/t6ex.pdf}
\end{center}

\begin{exam} Határozzuk meg a $(0,1)^2$ tartomány 
\[
T_1=\{(0,0),(1,0),(0,1)\}^{co},\quad\text{és}\quad T_2=\{(0,1),(1,1),(0,1)\}^{co}
\]
háromszögekkel vett triangulációjanak merevségi mátrixát homogén Dirichlet-peremfeltétel mellett.
Egyetlen szabadsági fok van: az $(1/2,1/2)$ oldalfelező pontnak megfelelő, amelyekhez a szokásos körüljárás szerint a 
$\phi_{T_1,23}$ és $\phi_{T_2,13}$ alakfüggvények tartoznak. A $T_1$ éppen az $R$ referenciaelem, a $T_2$-n pedig
könnyű meghatározni a szükséges alakfüggvényt. Azt kapjuk, hogy
\[
\phi_{T_1,23}(x_1,x_2)=4x_1x_2, \quad \phi_{T_2,13}(x_1,x_2)=4(1-x_1)(1-x_2),
\]
így
\[
D\phi_{T_1,23}(x_1,x_2)=4\begin{pmatrix}
x_2\\
x_1
\end{pmatrix},
\quad
D\phi_{T_2,13}(x_1,x_2)=4\begin{pmatrix}
x_2-1\\
x_1-1
\end{pmatrix}
\]
Ezek alapján meghatározható a kívánt $A^h$ merevségi mátrix (ami most csak egy skalár), amely az alábbi két, elementkénti járuléknak az összege:
\begin{align*}
a(\phi_{T_1,23},\phi_{T_1,23})&=\int_{T_1} 16(x_2^2+x_1^2)\,dx_1dx_2=\frac{8}{3}\\
a(\phi_{T_2,13},\phi_{T_2,13})&=\int_{T_2} 16((x_2-1)^2+(x_1-1)^2)\,dx_1dx_2=\frac{8}{3}
\end{align*}
Ezzel $A^h=16/3\approx 5.33$. Ilyesfajta triviális, papíron is könnyen kiszámítható eseteket szokás a programok helyességének ellenőrzésére használni.
\end{exam}

\subsection{Implementáció} 
A szakasz lezárásaképpen gondoljuk meg, hogy hogyan kell összefűzni a $T_6$ elemeket. 
Korábban megállapodtunk, hogy elemenkénti összefűzést végzünk. A $C^0$-csatlakozáshoz
az oldalfelező pontoknak egyazon szabadsági foknak kell tekintenünk. Technikailag a trianguláció \emph{éllistájára}
van szükség, ezt a rácsgeneráló programok külön kérésre visszaadják, így ennek meghatározásával nem kell vesződnünk (lásd a \lstinline|mesh2d| csomag \lstinline|connectivity| programja).
A globális indexelés az \lstinline|fe22_stiff_d| programban a ,,sakktábla'' elrendezéshez hasonló: először a csúcsokhoz tartozó
szabadsági fokok jönnek, aztán az oldalfelező pontokhoz tartozóak. Az elemenkéni járulékok meghatározása
\az{\eqref{aasum}} képlet szerint történik.

\begin{rmrk}
A másodfokú elemekből álló merevségi mátrix összefűzését vektorizálva végzi el a \lstinline|fe22_stiff_v| program.
A terhelési mátrix összefűzésnek implementációja a \lstinline|fe22_load_v| programban található.
\end{rmrk}

\section{A harmadfokú $T_{10}$ Lagrange-elem}

\section{A harmadfokú $H_{10}$ Hermite-elem}
\begin{center}
\includegraphics[scale=1.0]{fig/h10.pdf}
\end{center}
A tíz szabadsági fokú $H_{10}$ Hermite-elemen a $P^3(T)$ térben interpolálunk. Az ábrán a $\circ$ jelölés az adott pontban vett gradiens
kiértékelését jelenti. Ezzel a csomóponti bázis
\[
\Sigma=\{\delta_{\fie{a}_1},\delta_{\fie{a}_2},\delta_{\fie{a}_3},\delta_{\fie{a}_4},D\delta_{\fie{a}_1},D\delta_{\fie{a}_2},D\delta_{\fie{a}_3}\},
\]
ahol $\fie{a}_4=(\fie{a}_1+\fie{a}_2+\fie{a}_3)/3$. Világos, hogy ez $1+1+1+1+2+2+2=10$ szabadsági fok. Az alakfüggvények
\begin{align*}
\phi_i&=-2\lambda_i^3 + 3\lambda_i^2-7\lambda_i\sum_{\substack{j<k\\j\neq i\\k\neq i}} \lambda_j\lambda_k,\quad\phi_4=27\sum_{i<j<k}\lambda_i\lambda_j\lambda_k\\
\psi_i&=\sum_{\substack{j=1\\i\neq j}}^3 \lambda_i\lambda_j(2\lambda_i+\lambda_j-1)(\fie{a}_j-\fie{a}_i)
\end{align*}

\section{Az $R_4$ és $R_9$ tégla-elemek}
Emlékeztetünk, hogy az $f$ és $g$ egyváltozós függvények \emph{tenzorszorzatán} az
\[
(f\otimes g)(x_1,x_2)=f(x_1)g(x_2)
\]
kétváltozós függvényt értjük. Tenzorszorzás útján fogjuk megkonstruálni a tégla-elemeket az egydimenziós esetből.

\begin{center}
\includegraphics[scale=1.0]{fig/1d.pdf}
\end{center}
Az egydimenziós végeselemeket külön nem tárgyaljuk, mert triviálisak és a gyakorlatban nagyon ritkán használtak. 
Legyen $R=[0,1]$ a ``referenciaintervallum'', ezen $\Sigma=\{\delta_0,\delta_1\}$ és $\Phi=\{\lambda_1,\lambda_2\}$,
ahol 
\[
\lambda_1(x)=1-x,\;\lambda_2(x)=x
\]
a ``baricentrikus koordináták''. A másodrendű elemek esetén
$\Sigma=\{\delta_0,\delta_{1/2},\delta_2\}$ és 
\[
\phi_1=\lambda_1(2\lambda_1-1),\;\phi_2=2\lambda_1\lambda_2,\;\phi_3=\lambda_2(2\lambda_2-1)
\]
Az egydimenziós esetben az alakfüggvények deriváltjai $\lambda_1'=-1$ és $\lambda_2'=1$, illetve $\phi_1'(x)=4x-3$,
$\phi_2'(x)=2-4x$ és $\phi_3'(x)=4x-1$. Ezek alapján pedig kiértékelhető az $a(\ph,\ph)$ bilineáris forma.

Az $f\otimes g$ tenzorszorzat-alakban előálló függvények meglehetősen speciálisak. Először is,
egy ilyen szorzat integrálja a tényezők integráljainak szorzata. Másrészt, ha a tényezök rendelkeznek
valamilyen $\delta$-tulajdonsággal, akkor a tenzorszorzat is. Pontosabban, ha $\phi^1_k(x_j^1)=\delta_{kj}$ és
$\phi^2_m(x_n^2)=\delta_{mn}$, akkor $(\phi^1_k\otimes\phi_m^2)(x_j^1,x_n^2)=\delta_{kj}\delta_{mn}$.
Ez a két egyszerű észrevétel az alapja a tenzorszorzat-elemek konstrukciójának.

\begin{center}
\includegraphics[scale=0.6]{fig/r4r8.pdf}
\end{center}

Az $\pmb{R_4}$\textbf{-elemek} esetében az alakfüggvények
\begin{align*}
\phi_{11}(x_1,x_2)&=\lambda_1(x_1)\lambda_1(x_2)=(1-x_1)(1-x_2)\\
\phi_{21}(x_1,x_2)&=\lambda_2(x_1)\lambda_1(x_2)=x_1(1-x_2)\\
\phi_{22}(x_1,x_2)&=\lambda_2(x_1)\lambda_2(x_2)=x_1x_2\\
\phi_{12}(x_1,x_2)&=\lambda_1(x_1)\lambda_2(x_2)=(1-x_1)x_2
\end{align*}
Az $\pmb{R_9}$\textbf{-elemek} elemek teljesen hasonlóan konstruálhatóak meg.


\Exercises

\begin{Exercise}
Bizonyítsuk be, hogy \az{\eqref{phibase}} egyenletben szereplő $\{\phi_i\}$ bázis létezik. [Segítség: azonosítsuk $P(T)$-t $\Real^m$-mel.]
\end{Exercise}

\begin{Exercise}
Határozzuk meg a $\Pi_R v$ függvényt az $R$ Courant-referenciaelemen, ahol $v(x_1,x_2)=e^{x_1x_2}$.
[Segítség: $\Pi_Rv(x_1,x_2)=1$.]
\end{Exercise}


\begin{exam}[difficulty=2]
Bizonyítsuk be \az{\ref{lagrlem}}. Lemmát. [Segítség: Brenner--Scott, (3.1.10) Lemma]
\end{exam}

\begin{Exercise}
Bizonyítsuk be \az{\ref{lagrlem}}. Lemma segítségével, hogy a már korábban kezelt \emph{lineáris} Courant-elem egyértelműen megoldható.
[Segítség: Ha a $v$ elsőfokú polinom mindhárom csúcsban eltűnik, akkor $v$ mindhárom \emph{oldalon} azonosan nulla.
Az egyik oldalegyenesre, mint $L$ ``hipersíkra'' alkalmazva a lemmát kapjuk, hogy $v=Lc$, ahol $c\in\Real$. Ebből viszont $c=0$ következik behelyettesítéssel.]
\end{Exercise}

\begin{Exercise}
Nézzük meg az \verb!fe2_gentabs! programot, amely a példaprogramokban használt mindenféle táblázatok generálására íródott.
\end{Exercise}

\begin{Exercise}
Ez a feladat a Lagrange-interpoláció alaptulajdonságairól szól.\footnote{ld. V. Komornik. \emph{Valós analízis előadások I.} 172. o.}
Legyen $X_n=\{x_1,\ldots,x_n\}\subset[a,b]\subset\Real$, amelyben $x_i\neq x_j$ ha $i\neq j$, és $f\in C[a,b]$. Az $f$ függvény $X_n$ csomópontokhoz tartozó \textbf{Lagrange-felé
interpolációs polinomja}
\begin{align*}
Lf(x)=\sum_{k=1}^n f(x_k)\phi_k(x),\;\text{ahol}\quad\quad\phi_k(x)=\prod_{\substack{1\le j\le n\\ k\neq j}}\frac{x-x_j}{x_k-x_j}
\end{align*}
jelöli a Lagrange-féle alappolinomokat. Bizonyítsuk be az alábbiakat.
\begin{enumerate}[(i)]
\item $\phi_k(x_j)=\delta_{kj}$
\item $Lf\in P^{n-1}$
\item Azt mondjuk, hogy a $p$ polinom interpolálja az $f$ függvényt az $X_n$ csomópontokban, ha $p(x_k)=f(x_k)$ minden $k=1,\ldots,n$ esetén.
Az $Lf$ Lagrange-féle interpolációs polinom az egyetlen olyan $P^{n-1}$-beli polinom, amely interpolálja $f$-et az $X_n$ csomópontokban.
[Segítség: legyen $\Lambda=(\delta_{x_1},\ldots,\delta_{x_n})\;:\;P^{n-1}\to \Rn$. Mutassuk meg, hogy $\ker\Lambda=\{0\}$.]
\item Ha $p\in P^{n-1}$, akkor $Lp=p$, más szóval $\Map{L}{C[a,b]}{P^{n-1}}$ projekció.
\end{enumerate}
\end{Exercise}
