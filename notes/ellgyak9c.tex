\section{Függvényegyütthatós egyenletek}

Fizikai alkalmazásokban gyakran előfordul, hogy bizonyos anyagtulajdonságok térben változnak. Az elliptikus feladatokban ezt 
több helyen megjelenhet, először nézzük azt az esetet, amikor a Poisson-egyenletben szereplő Laplace-operátort módosítjuk 
\[
-\text{div}( p(x) Du)=f
\]
alakú feladatra, ahol $\Map{p}{\Omega}{\Real}$ korlátos. Parcdiffből az ilyen feladatokról tanultunk, és azt is tudjuk, hogy a gyenge 
alakban szereplő bilineáris forma
\[
a(u,v)=\int_\Omega p(x) Du\cdot Dv\,dx,
\]
amely tehát koercív. Regularitáselméletből ismert, hogy szép tartomány és sima $f$ esetén az $u$ gyenge megoldás esetén a $p(x)Du$ mennyiség
folytonos [???]. Emiatt $p(x)$ szakadásainál $Du$-nak is szakadnia kell, kivéve, ha $Du$ ott zérus.

Minthogy még a legegyszerűbb Courant-elemeknél sem biztos, hogy konstans lesz az integrandus a $p(x)$ tényező miatt,
már a merevségi mátrix összefűzésénél is valamilyen kubaturát kell alkalmazni, ennek az approximációja hibájára gyakorolt hatásával a jegyzet 3.6.3. pontja foglalkozik részletesen.
Az összefűzésnél tehát az
\begin{align*}
a(\lambda_{T,\fie{x}},\lambda_{T,\fie{y}})&=|\det\fie{Z}|\fie{Z}^{-1}D\lambda_{R,\zeta^{-1}(\fie{x})}\cdot \fie{Z}^{-1}D\lambda_{R,\zeta^{-1}(\fie{y})} \int_R p\circ\zeta\\
&=\fie{Z}^{-1}D\lambda_{R,\zeta^{-1}(\fie{x})}\cdot \fie{Z}^{-1}D\lambda_{R,\zeta^{-1}(\fie{y})} \int_T p
\end{align*}
mennyiségeket kell meghatározni. Ehhez érdemes egy előfeldolgozási lépésben kiszámolni az
\[
\int_T p
\]
integrálokat minden $T\in\mathcal{T}_h$ esetén.
\begin{rmrk}
\Az{\ref{stiffvec}}. Megjegyzésben látottakat itt alkalmazhatjuk. Az ottani jelölésekkel most az
\[
\int_T p(x)D\lambda_k\cdot D\lambda_\ell=\frac{1}{4|T|^2} \bm{\eta}_k\cdot\bm{\eta}_\ell \int_T p
\]
mennyiséget kell meghatározni minden $T\in\mathcal{T}_h$ és $k,\ell=1,2,3$ esetén.
\end{rmrk}

A térben változó anyagtulajdonságok leírásához néha többindexes mennyiségek szükségesek (ún. \textbf{anizotróp anyagmodell}). Ekkor a Poisson-egyenletet
\[
-\text{div}( \textbf{B}(x) Du)=f
\]
alakban szokás felírni, ahol $\Map{\textbf{B}}{\Omega}{\Real^{n\times n}}$ egy (szimmetrikus-) mátrixértékű függvény, amely egyenletesen elliptikus:
\[
\exists C>0\,\forall x\in\Omega\;\forall \xi\in\Rn \;:\; \textbf{B}(x)\xi\cdot \xi\ge C|\xi|^2.
\]
A Dirichlet-peremfeltételt ugyanúgy kell kezelni, 
a Neumann-peremfeltételben szereplő $D_\nu u$ normális irányú deriváltat viszont felváltja a $\textbf{B}(x)Du\cdot \nu$ ún. \textbf{konormális irányú derivált}\footnote{Lásd Simon László és E. A. Baderko.
\emph{Másodrendű lineáris parciális differenciálegyenletek.} (1983)}.
A gyenge alakban szereplő bilineáris forma
\[
a(u,v)=\int_\Omega \textbf{B}(x)Du\cdot Dv.
\]
Ha most $\textbf{B}(x)\equiv \textbf{B}\in\Real^{n\times n}$ konstans, akkor a $T_3$ Courant elemek merevségi mátrix összefűzése nem sokkal bonyolultabb, mint a 6. gyakorlaton tárgyalt esetben,
mindössze be kell írnunk a konstans $\textbf{B}$ mátrixot a megfelelő helyre:
\[
a(\lambda_{T,\fie{x}},\lambda_{T,\fie{y}})=\frac{1}{2}|\det \fie{Z}| \fie{Z}^{-1}\textbf{B}D\lambda_{R,\zeta^{-1}(\fie{x})}\cdot \fie{Z}^{-1}D\lambda_{R,\zeta^{-1}(\fie{y})}
\]
Az anizotróp, konstans merevségi mátrix összefűzésének naív, nagy \lstinline|for|-ciklusokat tartalmazó implementációja az \lstinline|fe2_stiff_aniso_d| program.
\begin{rmrk}
\Az{\ref{stiffvec}}. Megjegyzésben láttuk, hogy az \lstinline|fe2_stiff_v| vektorizált implementáció ,,lelke'' \az{\eqref{dlambdaintedge}} formula volt, amely lehetővé tette, hogy a baricentrikus
függvények deriváltjainak szorzatintegrálját a megfelelő \emph{élek} skalárszorzatával és a háromszög területével fejezzük ki. Most viszont
az
\[
\int_T \fie{B}D\lambda_k\cdot D\lambda_\ell=\frac{|T|}{d_kd_\ell} \fie{B}\fie{n}_k\cdot\fie{n}_\ell
\]
kifejezést kapjuk, ahol a korábbi jelöléseket használtuk.
Ekkor a korábbi levezetés szóról-szóra megismételhető, és azt kapjuk, hogy
\[
\frac{1}{4|T|} \fie{R}^\trans\fie{B}\fie{R}\bm{\eta}_k\cdot\bm{\eta}_\ell,
\]
ahol $\fie{R}$ a $90^\circ$-os forgatás mátrixa. Az \lstinline|fe2_stiff_aniso_v| függvény implementálja a mondottakat.
\end{rmrk}

\begin{exam} A programok kipróbálására konstruáljunk egy egzaktan megoldható feladatot. Legyen
\[
\textbf{B}=\begin{pmatrix}
1 & 0\\
0 & 2
\end{pmatrix}
\]
\begin{enumerate}[(1)]
\item Tekintsük az $\Omega''=(0,\pi)^2$ tartományon a homogén Dirichlet-peremfeltétellel ellátott
\[
\left.
\begin{aligned}
-\text{div}Dv&=2v\;\;\text{($\Omega''$-n)}\\
v&=0\;\;\;\;\text{($\partial\Omega''$-n)}
\end{aligned}
\right\}
\]
sajátértékfeladatot. Ennek megoldása 
\[
v(x_1,x_2)=\sin x_1\sin x_2.
\]
\item Könnyen látható, hogy a
\[
\left.
\begin{aligned}
-\text{div}(\textbf{B}Dw)&=2w\;\;\text{($\Omega'$-n)}\\
w&=0\;\;\;\; \text{($\partial\Omega'$-n)}
\end{aligned}
\right\}
\]
feladat megoldása az $\Omega'=(0,\pi)\times(0,\sqrt{2}\pi)$ átskálázott tartományon
\[
w(x_1,x_2)=\sin x_1\sin \frac{x_2}{\sqrt{2}}.
\]
% \item Végül legyen $u(x)=w(R^\trans x)$, azaz 
% \[
% u(x_1,x_2)=w\Big(\frac{x_1+x_2}{\sqrt{2}},\frac{x_2-x_1}{\sqrt{2}}\Big)=\sin \frac{x_1+x_2}{\sqrt{2}}\sin \frac{x_2-x_1}{2}.
% \]
% Erre már
% \[
% \left.
% \begin{aligned}
% -\text{div}(BDu)&=2u\;\;\text{($\Omega$-n)}\\
% u&=0\;\;\;\;\text{($\partial\Omega$-n)}
% \end{aligned}
% \right\}
% \]
% teljesül az
% \[
% \Omega=\{(0,0),(a,a),(0,b),(-a,a)\}^{co}
% \]
% tartományon, ahol $a=\pi/\sqrt{2}$ és $b=\sqrt{2}\pi$.
\end{enumerate}
\end{exam}
\begin{exam} Egy másik példához legyen
\[
\textbf{B}=\begin{pmatrix}
1 & 1 \\
1 & 3
\end{pmatrix},
\]
illetve $\Omega=B(0,1)\subset\Real^2$ és tekintsük a
\[
\left.
\begin{aligned}
-\text{div}(\textbf{B}Du)&=f\;\;\text{($\Omega$-n)}\\
u&=1\;\;\;\;\text{($\partial\Omega$-n)}
\end{aligned}
\right\}
\]
peremérték-feladatot, ahol
\[
f(x)=-\text{div}(\textbf{B}D|x|^2)=-8.
\]
Ekkor tehát az $u(x)=|x|^2$ a feladat megoldása.
\end{exam}

\section{Nulladrendű tagot tartalmazó feladatok. A tömegmátrix}

Tekintsük most a 
\[
-\lapl u + c(x)u=f
\]
\textbf{Helmholtz-egyenletet} (vagy \textbf{redukált hullámegyenletet}) valamilyen peremfeltétellel, ahol $\Map{c}{\Omega}{\Real}$ korlátos  és $c\ge 0$. Vegyük észre, hogy ha $f=0$ és $c(x)=c$, akkor
a Laplace-operátor sajátértékegyenletét kapjuk. Ha tehát $c=\lambda$ éppen egy sajátérték (vagy közel van hozzá), akkor a $-\lapl u + \lambda u=f$ feladatnak csak az $f=0$
esetben lehet megoldása. Az inverz feladat, a sajátértékfeladat megoldása, azaz $\lambda$ és $u$ meghatározására itt nem térünk ki.

\begin{exam}
A szokásos trükkel tudunk konstruálni egy példafeladatot a programok tetszelésére. Legyen
$\Omega=B(0,1)\subset\Real^2$ és tekintsük a
\[
\left.
\begin{aligned}
-\lapl u + u&=f\;\;\text{($\Omega$-n)}\\
u&=1\;\;\;\;\text{($\partial\Omega$-n)}
\end{aligned}
\right\}
\]
peremérték-feladatot, ahol $f(x)=-\lapl(|x|^2) + |x|^2=|x|^2-4$. Ekkor a feldat pontos megoldása $u(x)=|x|^2$
\end{exam}

A parcdiffes tanuljmányaink során láttuk, hogy a Helmholtz-egyenlethez tartozó gyenge alak bilineáris formája
\[
a(u,v)=\int_\Omega Du\cdot Dv + \int_\Omega c(x)uv.
\]
A végeselemes lineáris egyenletrendszer összeépítésekor tehát második integrált is figyelembe kell venni.
Egy rögzített $\{\phi_i\}$ bázis esetén a második integrál alkotta mátrixot \textbf{tömegmátrixnak} nevezzük, azaz
\[
(M^h)_{ij}=\int_\Omega c(x)\phi_i(x)\phi_j(x)\,dx.
\]
Ha most az erősebb, $c(x)\ge c_0>0$ feltételt tekintjük, akkor megállapíthatjuk, hogy $M^h$ szimmetrikus pozitív definit, ugyanis $M^h$ a lineárisan független $\{\phi_i\}$ rendszer Gram-mátrixa
az $c(x)$-szel súlyozott $L^2(\Omega;c)$ skalárszorzatva nézve. 

Az $M^h$ tömegmátrix összefűzése teljesen hasonlóan történik, mint az $A^h$ merevségi mátrixé. A legegyszerűbb eset nyilván a $c\equiv 1$, 
a szakasz hátralevő részében ezzel foglalkozunk.
Az implementációval kapcsolatban csak annyit jegyzünk meg, hogy a szokott módon, egy tetszőleges $T$ háromszögön 
vett szorzatintegrált az $\Map{\alpha}{R}{T}$ affin leképezéssel ``visszahúzzuk'' az $R$ referenciaelemre:
\[
\int_T \lambda_{T,x}\lambda_{T,x'}=|\det A|\int_R \lambda_{R,\alpha^{-1}(x)}\lambda_{R,\alpha^{-1}(x)}.
\]

\Az{\ref{1dmass}}. feladat alapján azt sejtjük, hogy a tömegmátrix \emph{jól kondicionált}, azaz a $h$ rácsmérettől független a kondiciószáma.

Most vizsgáljuk meg az $M^h$ tömegmátrix kondíciószámát a kétdimenziós Courant-elemek esetén. Ehhez először emlékeztetünk a \textbf{rácsfüggő skalárszorzatra} és az általa indukált
\textbf{diszkrét $\pmb{L^2}$-normára},
\[
\dua{u}{v}_{0,h}=h^2\sum_{x\in\Omega_h}u(x)v(x), \quad \|v\|_{0,h}^2 = h^2 \sum_{x\in\Omega_h} v(x)^2,
\]
ahol $h=\max_{T\in\mathcal{T}_h}\diam T$, és arra, hogy \az{\ref{meshequ}}. Állítás alapján, ha a $\{\mathcal{T}_h\}$ triangulációcsalád reguláris, akkor
\[
C\|u\|_{L^2(\Omega)}\le \|u\|_{0,h}\le C'\|u\|_{L^2(\Omega)}.
\]
minden $u\in V_h$ esetén, alkalmas $C,C'>0$ rácsfüggetlen konstansokkal.

\begin{bprop}
Legyen $\Omega_h\subset\Real^2$ sokszög, $\mathcal{T}_h$ reguláris trianguláció és $V_h$
a $\mathcal{T}_h$-n vett Courant-elemek által generált approximáló altér. Ekkor
\[
\kappa(M^h)\le C,
\]
ahol a $C$ konstans nem függ a $h$ rácsmérettől.
\end{bprop}
\begin{proof}
Legyen $M^hv^h=\lambda v^h$, $\|v^h\|=1$, ahol $\lambda>0$ elvégre $M^h$ szimmetrikus és pozitív definit.
\Az{\ref{quadrmass}}. feladatot felhasználva, erre
\[
\|v\|_{L^2(\Omega)}^2= M^hv^h\cdot v^h=\lambda
\]
teljesül. Az iménti normaekvivalencia miatt viszont
\[
Ch^2=C\|v\|_{0,h}^2\le \lambda \le C' \|v\|_{0,h}^2=C'h^2,
\]
úgyhogy $\kappa(M^h)\le C'/C$.
\end{proof}

\section{Egy rugalmasságtani feladat*}

\footnote{A szakasz Mark S. Gockenbach. \emph{Understanding And Implementing the Finite Element Method.} p. 8 alapján íródott.}Most elliptikus parciális differenciálegyenlet\emph{-rendszerekkel} fogunk foglalkozni,
egy konkrét rugalmasságtani feladat bemutatása útján. Legyen $\Omega\subset\Real^2$ korlátos tartomány.
Az $\Omega$ síkbeli membrán \textbf{elmozdulását} egy 
\[
\Map{\mathbf{u}}{\Omega}{\Real^2}
\]
 függvénnyel fogjuk jellemezni (mértékegysége: \textsf{m})\footnote{Ebben, és következő szakaszban a vektorértékű 
függvényeket vastagon szedve jelöljük, ezt írásban aláhúzzással szokás írni. A tenzormezőket (mátrixértékű függvényeket) görög betűkkel jelöljük, kivéve $\pmb{\nu}$-t,
amely az egység hosszú normálvektormezőt jelöli.}:
az egyensúlyi állapotban az $x\in\Omega$ pont az $x+\mathbf{u}(x)\in\Real^2$ pontba kerül.
A rugalmasságtanban $\mathbf{u}$ Taylor-sorának,
\[
\mathbf{u}(x+h)=\mathbf{u}(x)+D\mathbf{u}(x)h + \ldots
\]
magasabbrendű tagjait elhanyagolják, és csupán $D\mathbf{u}(x)\in \Real^{2\times 2}$ szimmetrikus részét\footnote{Emlékeztetünk, 
hogy minden $M\in\Real^{n\times n}$ mátrix egyértelműen felbontható $M=S+A$ alakban, ahol $S=(A+A^\trans)/2$ szimmetrikus és $A=(A-A^\trans)/2$ antiszimmetrikus.}
\[
\epsilon_{ij}=\epsilon(\mathbf{u})_{ij}=\frac{1}{2}\big(\partial_iu_j+\partial_ju_i\big), \quad 
\text{azaz} \quad \epsilon(\mathbf{u})=\begin{pmatrix}
\partial_1u_1 & \frac{1}{2}(\partial_1u_2+\partial_2u_1) \\
\frac{1}{2}(\partial_1u_2+\partial_2u_1)  & \partial_2u_2
\end{pmatrix}
\]
\textbf{deformációs tenzort} (más néven fajlagos nyúlás, mértékegység nélküli mennyiség) használják a közeg lokális torzulásának jellemzésére. 
$D\mathbf{u}(x)$ antiszimmetrikus része,
\[
\omega_{ij}=\omega(\mathbf{u})_{ij}=\frac{1}{2}\big(\partial_ju_i-\partial_iu_j\big), \quad 
\text{azaz} \quad \omega(\mathbf{u})=\begin{pmatrix}
0 & \frac{1}{2}(\partial_2u_1-\partial_1u_2) \\
\frac{1}{2}(\partial_1u_2-\partial_2u_1)  & 0
\end{pmatrix}
\]
merev forgást ír le, a membrán pontjainak egymáshoz viszonyított megváltozására érzéketlen.

Egy szemléleletes felírása az $\epsilon$ deformációs tenzornak a következő. Legyen
\begin{align*}
\epsilon(\mathbf{u})&= \underbrace{\frac{1}{2}(\text{div}\mathbf{u}) I}_{\displaystyle{D(\mathbf{u})}} + \underbrace{\Big(\epsilon(\mathbf{u})-\frac{1}{2}(\text{div}\mathbf{u}) I\Big)}_{\displaystyle{S(\mathbf{u})}}
\end{align*}
Ekkor $D(\mathbf{u})$ az izotróp térfogatváltozás mértékét adja meg, míg $S(\textbf{u})$ a koordinátatengelyek irányában bekövetkező \emph{nyírást} írja le, amely
során nem megy végbe térfogatváltozás.
Világos, hogy $\Tr\epsilon(\mathbf{u})=\text{div}\mathbf{u}$ és $\Tr S(\mathbf{u})=0$.

A $\Map{\sigma}{\Omega}{\Real^{2\times 2}}$ \textbf{feszültségtenzort} (mértékegysége \textsf{Pa}, azaz $\textsf{N}/\textsf{m}^2$) a $\sigma=C\epsilon$ Hooke-féle anyagtörvénynek megfelelően \emph{definiáljuk},
ahol $C\in\Real^{2\times 2\times 2\times 2}$ merevségi tenzor. Izotróp anyagmodell esetén ez
\[
\boxed{\sigma=2\mu\epsilon + \lambda\Tr(\epsilon) I}
\]
ahol $\mu$ és $\lambda$ konstansok az ún. \textbf{Lamé-állandó} (mértékegységük: \textsf{Pa}), amelyek méréssel meghatározhatóak, ezért mérnöki táblázatokban megtalálhatunk konkrét értékeket.
A $\lambda+\mu$ mennyiség neve \textbf{bulk modulusz}, a $\mu$ neve \textbf{nyírási modulusz}. A bulk modulusz az anyag egyenletes kitágulással szembeni 
ellenállóképességét jellemzi, pontosabban ha a bulk modulusz nagy, akkor nehéz kitágítani az anyagot. A nyírási modulusz ezzel szemben a nyírásra adott válaszát írja le,
amelyek során nem történik térfogatváltozás. Ha $\lambda$ nagyon nagy $\mu$-höz képest, akkor az anyag gyakorlatilag összenyomhatatlan. 

Az egyensúlyi állapotot $\Map{\fie{f}}{\Omega}{\Real^2}$ külső terfogati erősűrűség mellett (mértékegysége: $\textsf{N}/\textsf{m}^3$) leíró 
egyenlet (amely általános $\sigma$ feszültségtenzorra is érvényes)
\[
\boxed{-\text{Div}\, \sigma=\fie{f}} \tag*{$(*)$}
\]
Itt $\text{Div}$ a \textbf{tenzordivergenciát jelöli}:
\[
(\text{Div}\,\sigma)_i=\sum_{j=1}^2 \partial_j\sigma_{ij}, \quad\text{azaz}\quad 
\text{Div}\,\sigma=\begin{pmatrix}
\partial_1\sigma_{11} + \partial_2\sigma_{12}\\
\partial_1\sigma_{21} + \partial_2\sigma_{22}
\end{pmatrix}
\]

\subsection{Erős alak}
Mármost a $(*)$ egyenlet egy elliptikus parciális differenciálegyenlet-rendszer $\textbf{u}$-ra. Valóban,
koordinátákban felírva
\[
\partial_j\sigma_{ij}=2\mu\partial_j\epsilon_{ij} + \lambda\partial_j(\epsilon_{11}+\epsilon_{22})\delta_{ij},
\]
és ezzel
\[
(\text{Div}\,\sigma)_i=\mu \sum_{j=1}^2 \big(\partial_j\partial_iu_j+\partial_j\partial_ju_i\big) + 
\lambda\big(\partial_i\partial_1u_1+\partial_i\partial_2u_2\big),
\]
amiből
\[
\left.
\begin{aligned}
-\mu\partial_{11}u_1 -\mu \partial_{11}u_1 - \mu \partial_{21}u_2 -\mu\partial_{22} u_1 
- \lambda \partial_{11}u_1 - \lambda\partial_{12} u_2&=f_1\\
-\mu\partial_{12}u_1 - \mu\partial_{11}u_2 - \mu\partial_{22}u_2 - \mu\partial_{22}u_2 - \lambda\partial_{21}u_1 -\lambda\partial_{22}u_2&=f_2
\end{aligned}
\right\}
\]
azaz
\[
\left.
\begin{aligned}
-(2\mu+\lambda)\partial_{11} u_1 - \mu\partial_{22}u_1 - (\mu+\lambda) \partial_{12}u_2 &= f_1\\
- \mu\partial_{11}u_2 -(2\mu+\lambda)\partial_{22} u_2  - (\mu+\lambda) \partial_{12}u_1 &= f_2
\end{aligned}
\right\}\tag*{$(**)$}
\]
Vegyük észre, hogy az egyenleteket két ,,anizotróp'' Laplace-operátor segítségével is fel lehet írni,
\[
\left.
\begin{aligned}
-\text{div}(\fie{B}_1Du_1) - (\mu+\lambda) \partial_{12}u_2 &= f_1\\
-\text{div}(\fie{B}_2Du_2)  - (\mu+\lambda) \partial_{12}u_1 &= f_2
\end{aligned}
\right\}
\]
ahol
\[
\fie{B}_1=\begin{pmatrix}
2\mu+\lambda & 0 \\
0 & \mu
\end{pmatrix}, \quad
\fie{B}_2=\begin{pmatrix}
\mu & 0 \\
0 & 2\mu +\lambda
\end{pmatrix}
\]
\textbf{Peremfeltételek.} A következő peremfeltételek jöhetnek szóba.
\begin{itemize}
\item \textbf{Dirichlet.} Egy $\Gamma_D\subset\partial\Omega$ halmazon a membrán le van rögzítve, az elmozdulás adott: $\mathbf{u}=\mathbf{g}$ ($\Gamma_D$-n),
ahol $g$ adott vektormező (mértékegysége: \textsf{m}). Ez egy kinematikai feltétel, ami előírja a membrán elmozdulását.
\item \textbf{Neumann.} Egy másik $\Gamma_N=\partial\Omega\setdif\Gamma_D$ halmazon külső mechanikai terhelés hat: $\sigma\pmb{\nu}=\mathbf{t}$ ($\Gamma_N$-n), ahol
$\textbf{t}$ adott vektormező (mértékegysége: $\textsf{N}/\textsf{m}$). Ez egy dinamikai feltétel és a $\sigma\pmb{\nu}$ mennyiség neve normális feszültség.
\end{itemize}

\subsection{Gyenge alak} 
Tekintsük most a következő  homogén Dirichlet-peremértékfeladatot:
\[
\left.\begin{aligned}
-\text{Div}\,\sigma &= \fie{f}\quad \text{($\Omega$-n)}\\
\mathbf{u}&=0\quad \text{($\Gamma_D$-n)}\\
\sigma(\mathbf{u})\pmb{\nu}&=\mathbf{t}\quad \text{($\Gamma_N$-en)}
\end{aligned}\right\}
\]
Szorozzuk meg a $\mathbf{v}\in H^1_D(\Omega;\Real^2)$ 
tesztfüggvénnyel az első egyenletet és integráljunk. Ekkor a bal oldalon a
\[
-\int_\Omega \text{Div}\,\sigma(\mathbf{u})\cdot \mathbf{v}\,dx =-\int_\Omega 2\mu \text{Div}\,\epsilon(\mathbf{u})\cdot\mathbf{v}
+ \lambda \text{Div}\big(\Tr(\epsilon(\mathbf{u})) I\big)\cdot \mathbf{v} \,dx \tag*{(A)}
\]
kifejezés szerepel. A Laplace-operátorral kapcsolatos gyenge alakok levezetéséhez hasonlóan, itt is ``át szeretnénk dobni a tesztfüggvényre a deriválást''.
Ehhez a Green-formula megfelelő általánosítására van szükség, lásd \az{\ref{gaussexer}}. Feladatot.

Az iménti Green-formulát alkalmazva (A) bal oldalának első tagjára, kapjuk, hogy
\begin{align*}
\int_\Omega 2\mu \epsilon(\mathbf{u}) : D\mathbf{v} - \int_{\Gamma_N} 2\mu\epsilon(\mathbf{u})\nu\cdot\mathbf{v}=\int_\Omega 2\mu \epsilon(\mathbf{u}) : \epsilon(\mathbf{v})
- \int_{\Gamma_N} 2\mu\epsilon(\mathbf{u})\bm{\nu}\cdot\mathbf{v},
\end{align*}
míg (A) második tagjára,
\begin{align*}
\int_\Omega \lambda \Tr(\epsilon(\mathbf{u}))I : D\mathbf{v} - \int_{\Gamma_N} \lambda\Tr(\epsilon(\mathbf{u}))\pmb{\nu} \cdot \mathbf{v}=
\int_\Omega \lambda \text{div}\,\mathbf{u}\, \text{div}\,\mathbf{v} - \int_{\Gamma_N} \lambda\Tr(\epsilon(\mathbf{u}))\pmb{\nu}\cdot \mathbf{v}
\end{align*}
Az első tag átalakításánal felhasználtuk \az{\ref{abtrace}}. Feladatot.

Összefoglalva, a
\[
\left.\begin{aligned}
-\text{Div}\,\sigma &= \fie{f}\quad \text{($\Omega$-n)}\\
\mathbf{u}&=0\quad \text{($\Gamma_D$-n)}\\
\sigma(\mathbf{u})\pmb{\nu}&=\mathbf{t}\quad \text{($\Gamma_N$-en)}
\end{aligned}\right\}
\]
feladat gyenge alakja
\[
\forall \mathbf{v}\in H^1_D(\Omega;\Real^2) \;:\; \underbrace{\int_\Omega 2\mu \epsilon(\mathbf{u}):\epsilon(\mathbf{v}) + \lambda \text{div}\,\mathbf{u}\,
\text{div}\,\mathbf{v}}_{\displaystyle{=:a(\mathbf{u},\mathbf{v})}}
=\underbrace{\int_\Omega \mathbf{f}\cdot \mathbf{v} + \int_{\Gamma_N}\mathbf{t}\cdot \mathbf{v} }_{\displaystyle{=:\ell(\mathbf{v})}},
\]
ahol $\mathbf{u}\in H^1_D(\Omega;\Real^2)$. Az $\mathbf{u}=\fie{g}$ ($\Gamma_D$-n) inhomogén Dirichlet-peremfeltétel kezelése a szokásos homogenizálási eljárrassal történik.
A $\fie{g}$ megfelelő kiterjesztésével készítsük el azt a $\widetilde{\fie{g}}\in H^1(\Omega;\Real^2)$ függvényt, amelyre $\fie{g}=\widetilde{\fie{g}}$  ($\Gamma_D$-n).
Ekkor az inhomogén feladat megoldása $\fie{u}=\fie{u}_0+\widetilde{\fie{g}}$, ahol $\fie{u}_0\in H^1_D(\Omega;\Real^2)$ a
\[
\forall \fie{v}\in H^1_D(\Omega;\Real^2) \;:\; a(\fie{u}_0,\fie{v})=\ell(\fie{v})-a(\widetilde{\fie{g}},\fie{v}).
\]
módosított jobb oldalú feladat megoldása. 

\subsection{A Korn-egyenlőtlenség} 
Emlékeztetünk, hogy a Lax--Milgram-tétel alkalmazásához (amelyet absztrakt Hilbert téren mondtunk ki,
így közvetlenül alkalmazható a vektorértékű terekre is) szükséges az $a(\ph,\ph)$ bilineáris forma koercivitása. Ehhez tehát
az
\[
a(\mathbf{v},\mathbf{v})=\int_\Omega 2\mu \|\epsilon(\mathbf{v})\|^2 + \lambda (\text{div}\,\mathbf{v})^2\ge C\|\mathbf{v}\|_{H^1(\Omega;\Real^2)}^2
\]
egyenlőtlenséget kell bizonyítani. Emlékeztetünk, hogy a $H^1(\Omega;\Real^2)$ teret az
\[
\dua{\mathbf{u}}{\mathbf{v}}_{H^1(\Omega;\Real^2)}=\dua{\mathbf{u}}{\mathbf{v}}_{L^2(\Omega;\Real^2)} + \dua{D\mathbf{u}}{D\mathbf{v}}_{L^2(\Omega;\Real^{2\times 2})}
\]
skalárszorzattal láttuk el. A $\Gamma_N=\emptyset$ esetben a koercivitási egyenlőtlenség bármely $\mathbf{v}\in H^1_D(\Omega;\Real^2)$ mellett teljesül,
ez az ún. Korn-egyenlőtlenség. Ennek bizonyítása hosszadalmas, ezért erre itt nem térünk ki
\footnote{Susanne C. Brenner, Ridgway Scott. \emph{The Mathematical Theory of Finite Element Methods}, 2008. pp. 313--318}.
A $|\Gamma_D|>0$, $\Gamma_N\neq\emptyset$ vegyes eset homogenizálással visszavezethető a homogén Dirichlet-peremfeltétel esetére. 

A $\Gamma_N=\partial\Omega$ tiszta Neumann-peremfeltétel esetében viszont az alaptér $H^1(\Omega;\Real^2)$, és ezen nemtriviális a magtere az
\[
\Map{\epsilon}{H^1(\Omega;\Real^2)}{L^2(\Omega;\Real^{2\times 2})}
\]
lineáris leképezésnek\footnote{lásd a szakasz elején szereplő feladatot. Viszont, ha $\Map{\epsilon}{H^1_D(\Omega;\Real^2)}{L^2(\Omega;\Real^{2\times 2})}$, akkor $\ker\epsilon=\{0\}$.},
így hasonlóan a skalár esethez (lásd 8. gyakorlat), egy kompatibilitási feltétel teljesülése szükséges (és elégséges) egy megoldás létezéséhez.


\section{A rugalmasságtani feladat diszkretizációja*}
\footnote{A szakasz Mark S. Gockenbach. \emph{Understanding And Implementing the Finite Element Method.} pp. 213--218 alapján íródott.}A fentieknek megfelelően
 a $\mathbb{V}_h\subset H^1_D(\Omega;\Real^2)$ végeselemes approximáló altér
is vektorértékű függvényekből fog állni, mégpedig
\[
\mathbb{V}_h=V_h\oplus V_h=\{(v_1,v_2)\in V_h\times V_h \;:\; v_1,v_2\in V_h\},
\]
és itt $V_h$ most legyen az elsőfokú Courant-elemekből kapott altér, amelynek elemei eltűnnek $\Gamma_D$-n.
A $\mathbb{V}_h$ tér örökli a $V_h$ bázisát a következő értelemben: ha 
\[
V_h=\Span\{\phi_1,\ldots,\phi_m\},
\]
ahol $\{\phi_i\}$ a csomóponti bázis, akkor
\[
\mathbb{V}_h=\Span\{\pmb{\phi}_1,\ldots,\pmb{\phi}_m,\pmb{\phi}_{m+1},\ldots,\pmb{\phi}_{2m}\},
\]
ahol
\[
\pmb{\phi}_i=\begin{bmatrix}
\phi_i\\
0
\end{bmatrix}\;\;(i=1,\ldots,m),\quad
\pmb{\phi}_i=\begin{bmatrix}
0\\
\phi_i
\end{bmatrix}
\;\;(i=i+1,\ldots,2m)
\]
Nyilván a $\{\pmb{\phi}_i\}$ bázis sorrendjét szabadon megválaszthatjuk, de most állapodjunk meg a fentiben.
A lináris algebrai egyenletrendszer a következő blokkmátrix alakban írható:
\[
\underbrace{\begin{bmatrix}
A^h_{11} & A^h_{12} \\
A^h_{21} & A^h_{22}
\end{bmatrix}}_{\displaystyle{A^h}}
\underbrace{\begin{bmatrix}
U^h_1 \\
U^h_2
\end{bmatrix}}_{\displaystyle{U^h}}=
\underbrace{\begin{bmatrix}
F^h_1\\
F^h_2
\end{bmatrix}}_{\displaystyle{F^h}}
\]
Itt
\begin{align*}
\big(A^h_{11}\big)_{ij}&=a\Bigg(\begin{bmatrix}\phi_j\\ 0\end{bmatrix}, \begin{bmatrix} \phi_i\\ 0\end{bmatrix}\Bigg), \quad
 \big(A^h_{22}\big)_{ij}=a\Bigg(\begin{bmatrix} 0 \\ \phi_j \end{bmatrix}, \begin{bmatrix} 0\\ \phi_i\end{bmatrix}\Bigg)\\
\big(A^h_{12}\big)_{ij}&=a\Bigg(\begin{bmatrix} \phi_j \\ 0 \end{bmatrix}, \begin{bmatrix} 0\\ \phi_i\end{bmatrix}\Bigg), \quad A^h_{21}=\big(A^h_{12}\big)^\trans\\
\big(F^h_1\big)_i&=\ell\Bigg(\begin{bmatrix}\phi_i\\ 0\end{bmatrix}\Bigg), \quad \big(F^h_2\big)_i=\ell\Bigg(\begin{bmatrix}0 \\ \phi_i\end{bmatrix}\Bigg)
\end{align*}
Az $a(\ph,\ph)$ bilineáris forma kiértékelése most kissé komplikáltabb. Némi számolással belátható, hogy
\begin{align*}
a\Bigg(\begin{bmatrix}\phi_j\\ 0\end{bmatrix}, \begin{bmatrix} \phi_i\\ 0\end{bmatrix}\Bigg)&=
\int_\Omega (2\mu + \lambda) \partial_1\phi_j\partial_1\phi_i +\mu\partial_2\phi_j\partial_2\phi_i \,dx=\int_\Omega \fie{B}_1D\phi_j\cdot D\phi_i\\
a\Bigg(\begin{bmatrix} 0 \\ \phi_j \end{bmatrix}, \begin{bmatrix} 0\\ \phi_i\end{bmatrix}\Bigg)&=
\int_\Omega \mu \partial_1\phi_j\partial_1\phi_i +(2\mu+\lambda)\partial_2\phi_j\partial_2\phi_i \,dx=\int_\Omega \fie{B}_2D\phi_j\cdot D\phi_i\\
a\Bigg(\begin{bmatrix} \phi_j \\ 0 \end{bmatrix}, \begin{bmatrix} 0\\ \phi_i\end{bmatrix}\Bigg)&=
\int_\Omega \mu \partial_2\phi_j\partial_1\phi_i +\lambda\partial_1\phi_j\partial_2\phi_i \,dx=\int_\Omega \fie{B}_3D\phi_j\cdot D\phi_i
\end{align*}
ahol
\[
\fie{B}_1=\begin{bmatrix}
2\mu+\lambda & 0\\
0 & \mu
\end{bmatrix},\quad
\fie{B}_2=\begin{bmatrix}
\mu & 0 \\
0 & 2\mu+\lambda
\end{bmatrix},\quad
\fie{B}_3=\begin{bmatrix}
0 & \mu \\
\lambda & 0
\end{bmatrix}
\]
illetve
\[
\big(F^h_1\big)_i=\int_\Omega f_1\phi_i\,dx,\quad \big(F^h_2\big)_i=\int_\Omega f_2\phi_i\,dx
\]
Vegyük észre, hogy ezzel teljes egészében megadtuk az $A^h$ merevségi mátrix és $F^h$ terhelési vektor összefűzésének módját
homogén Dirichlet-peremfeltétel mellett.

\begin{exam}
Számoljuk ki egy egyszerű esetben az $A^h$ mátrixot! Legyen ismét $\Omega=(0,2)^2$, homogén Dirichlet-peremfeltétel és legyen
$\mathcal{T}_h$ az alábbi trianguláció:
\begin{center}
\includegraphics[scale=1.4]{fig/1dof.pdf}
\end{center}
Ekkor tehát \emph{két} szabadsági fok van és 
\[
\mathbb{V}_h=\Span\Bigg\{\begin{bmatrix}\phi_x\\0\end{bmatrix},\begin{bmatrix}0\\\phi_x\end{bmatrix}\Bigg\},
\]
ahol $\phi_x$ az $x$ pontnak megfelelő kalapfüggvény. Elemenkénti összefűzést végzünk, bár sok számolást már elvégeztünk erre a 6. gyakorlaton. Emlékeztetőül:
\begin{align*}
\lambda_{T_2,x}(\xi_1,\xi_2)=\xi_1+\xi_2-1,\quad \lambda_{T_4,x}(\xi_1,\xi_2)=2-\xi_1,\quad \lambda_{T_4,x}(\xi_1,\xi_2)=2-\xi_2\\
\lambda_{T_3,x}(\xi_1,\xi_2)=\xi_2,\quad \lambda_{T_5,x}(\xi_1,\xi_2)=\xi_1,\quad \lambda_{T_7,x}(\xi_1,\xi_2)=3-\xi_1-\xi_2
\end{align*}
és
\begin{align*}
D\lambda_{T_2,x}=\begin{bmatrix}1\\ 1\end{bmatrix},\quad D\lambda_{T_4,x}=\begin{bmatrix}-1\\ 0\end{bmatrix},\quad D\lambda_{T_6,x}=\begin{bmatrix}0\\ -1\end{bmatrix} \\
D\lambda_{T_3,x}=\begin{bmatrix}0\\ 1\end{bmatrix},\quad D\lambda_{T_5,x}=\begin{bmatrix}1\\ 0\end{bmatrix},\quad D\lambda_{T_7,x}=\begin{bmatrix}-1\\ -1\end{bmatrix}
\end{align*}
Ezek alapján
\begin{align*}
a\Bigg(\begin{bmatrix}\phi_x\\ 0\end{bmatrix}, \begin{bmatrix} \phi_x\\ 0\end{bmatrix}\Bigg)&=\sum_{k\in\{2,\ldots,7\}}\int_{T_k} B_1D\lambda_{T_k,x}\cdot D\lambda_{T_k,x}\\
&=\frac{1}{2}\Big((3\mu+\lambda) + \mu + (2\mu+\lambda) + (2\mu+\lambda) + \mu + (3\mu+\lambda)\Big)=6\mu + 2\lambda\\
a\Bigg(\begin{bmatrix}0 \\\phi_x\end{bmatrix}, \begin{bmatrix} 0 \\ \phi_x\end{bmatrix}\Bigg)&=\sum_{k\in\{2,\ldots,7\}}\int_{T_k} B_2D\lambda_{T_k,x}\cdot D\lambda_{T_k,x}\\
&=\frac{1}{2}\Big((3\mu+\lambda) + (2\mu+\lambda) + \mu + \mu + (2\mu+\lambda) + (3\mu+\lambda)\Big)=6\mu + 2\lambda\\
a\Bigg(\begin{bmatrix} \phi_x\\ 0\end{bmatrix}, \begin{bmatrix} 0 \\ \phi_x\end{bmatrix}\Bigg)&=\sum_{k\in\{2,\ldots,7\}}\int_{T_k} B_3D\lambda_{T_k,x}\cdot D\lambda_{T_k,x}\\
&=\frac{1}{2}\Big((\mu+\lambda) + 0 + 0 + 0 + 0 + (\mu+\lambda)\Big)=\mu+\lambda
\end{align*}
azaz
\[
A^h=\begin{bmatrix}
6\mu + 2\lambda & \mu+\lambda \\
\mu+\lambda & 6\mu + 2\lambda
\end{bmatrix}
\]
\end{exam}

\subsection{Peremfeltételek}
A Dirichlet-peremfeltételeket teljesen hasonlóan kell érvényesíteni, mint a skalár esetben. A Neumann-peremfeltétel alkalmazásához
ismét a skalár-esethez hasonlóan az
\[
\int_{e}\mathbf{t}\cdot \fie{\phi}_{\fie{x}}
\]
felszíni integrált kell kiértékelni minden $e=[\fie{x},\fie{y}]\subset\Gamma_N$ Neumann-típusú peremélre, amit az ott elmondottakhoz hasonlóan,
a közpponti szabállyal lehet közelíteni.


\subsection{Implementáció}
A rugalmasságtani feladat implementálásához érdemes felhasználni a korábban megírt programokat, hiszen a szakasz során lényegében
visszavezettük a skalár esetre a problémát.

\subsection{Rácsbefagyás}
Ha az anyag lényegében összenyomhatatlan, azaz ha $\lambda$ nagyon nagy $\mu$-höz képest, akkor azt érezzük
\[
a(\fie{u},\fie{v})=\int_\Omega 2\mu \epsilon(\mathbf{u}):\epsilon(\mathbf{v}) + \lambda \text{div}\,\mathbf{u}\,\text{div}\,\mathbf{v}
\]
bilineáris formában az $\dive\fie{u}$ nagyon kicsi lesz (ez belátható precízen is). Azonban a Courant-elemekkel kapott $\mathbb{V}_h$
vektoriális approximáló altérben általában csak az azonosan nulla függvény divergenciamentes. 

\Exercises

\begin{Exercise}
Lássuk be, hogy ha $B=\text{diag}\,(b_1,\ldots,b_n)$, és $-\lapl v=f$ akkor az
\[
u(x_1,\ldots,x_n)=v\Big(\frac{x_1}{\sqrt{b_1}},\ldots,\frac{x_n}{\sqrt{b_n}}\Big)
\]
függvényre $-\Tr(BD^2u)=f$. Milyen tartományokon vannak értelmezve a függvények?
\end{Exercise}

\begin{Exercise}[label=quadrmass]
Bizonyítsuk be, hogy bármely $u,v\in V_h$ esetén $\dua{u}{v}_{L^2(\Omega;c)}=M^hu^h\cdot v^h$, ahol $u=\sum_i (u^h)_i\phi_i$ és $v=\sum_i (v^h)_i\phi_i$. [Segítség: Karátson János. 
\emph{Numerikus funkcionálanalízis.}, 19.1 Állítás bizonyítása.]
\end{Exercise}

\begin{Exercise}[label=1dmass]
Tekintsük az egydimenziós elsőfokú elemek esetét a $h=1/(N+1)$ egyenletes rácson.
\begin{enumerate}[(1)]
\item Lássuk be, hogy
\[
M^h=\frac{h}{6}\text{tridiag}(1,4,1).
\]
\item Mutassuk meg, hogy $M^h$ $\{\lambda_k\}$ sajátértékeire
\[
\lambda_k=\frac{h}{3}\big(2+\cos(\pi h k)\big).
\]
\item Vonjuk le azt a következtetést, hogy $\lambda_{\max}/\lambda_{\min}\le C$, ahol $C>0$ nem függ a rácsmérettől.\footnote{Itt az ún. spektrális kondíciószámra kaptunk becslést, de
szimmetrikus mátrixok esetén ez megegyezik a szokásos $\kappa(A)=\|A\|\|A^{-1}\|$ kondíciószámmal, lásd egy későbbi feladat.}
\end{enumerate}
\end{Exercise}

\begin{Exercise}
Határozzuk meg az $R$ referenciaelemen a 
\[
\int_R \lambda_i\lambda_j \, dx
\]
mennyiségeket ($i,j\in\{1,2,3\}$).
\end{Exercise}
% \begin{align*}
% \int_R \lambda_1\lambda_1&=\int_R (1-x_1-x_2)^2\,dx_1dx_2=\frac{1}{6}\Big(\frac{1}{4} + \frac{1}{4}\Big)=\frac{1}{12}\\
% \int_R \lambda_1\lambda_2&=\int_R (1-x_1-x_2)x_1\,dx_1dx_2=\frac{1}{6}\cdot\frac{1}{4}=\frac{1}{24}\\
% \int_R \lambda_1\lambda_3&=\int_R (1-x_1-x_2)x_2\,dx_1dx_2=\frac{1}{6}\cdot\frac{1}{4}=\frac{1}{24}\\
% \int_R \lambda_2\lambda_2&=\int_R x_1^2\,dx_1dx_2=\frac{1}{6}\Big(\frac{1}{4}+\frac{1}{4}\Big)=\frac{1}{12}\\
% \int_R \lambda_2\lambda_3&=\int_R x_1x_2\,dx_1dx_2=\frac{1}{6}\cdot\frac{1}{4}=\frac{1}{24}\\
% \int_R \lambda_3\lambda_3&=\int_R x_2^2\,dx_1dx_2=\frac{1}{6}\Big(\frac{1}{4}+\frac{1}{4}\Big)=\frac{1}{12}
% \end{align*}

\begin{Exercise}
Lássuk be, hogy ha $A$ szimmetrikus, akkor az $\kappa(A)=\|A\|\|A^{-1}\|$ kondíciószámra $\kappa(A)=\lambda_{\max}(A)/\lambda_{\min}(A)$ teljesül.
\end{Exercise}

\begin{Exercise}
Bizonyítsuk be, hogy ha $\mathbf{u}(x)=a+bRx$, ahol $a\in\Real^2$ és $b\in\Real$, és
\[
R=\begin{pmatrix}
0 & -1 \\
1 & 0
\end{pmatrix}=\begin{pmatrix}
\cos \pi/2 & -\sin \pi/2 \\
\sin \pi/2 & \cos \pi/2
\end{pmatrix},
\]
akkor $D\mathbf{u}=R$ és így $\epsilon(\mathbf{u})=0$, illetve $\omega(\mathbf{u})=R$.\footnote{Ha most $a=0$, akkor $\mathbf{u}$-t \textbf{infinitezimális forgatásnak} nevezzük, ugyanis az antiszimmetrikus
$R$ mátrix az $SO(2)$ Lie-csoport generátora:
\[
\forall \theta\in\Real/2\pi\mathbb{Z}\;:\;\begin{pmatrix}
\cos\theta & -\sin\theta\\
\sin\theta & \cos\theta
\end{pmatrix}
=e^{\theta R}.
\]}
Ám a megfordítás is igaz: ha a deformációs tenzort egy $\Map{\epsilon}{C^1(\Omega;\Real^2)}{C(\Omega;\Real^{2\times 2})}$ lineáris leképezésnek tekintjük, akkor
\[
\ker\epsilon=\Span\Bigg\{\begin{pmatrix}
1 \\
0
\end{pmatrix},\;
\begin{pmatrix}
0 \\
1
\end{pmatrix},\;
\begin{pmatrix}
-x_2 \\
x_1
\end{pmatrix}
\Bigg\}.
\]
\end{Exercise}

\begin{Exercise}[difficulty=1]
Legyen $\Omega=(0,1)^2\subset\Real^2$, és tekintsük a 
\[
\left.
\begin{aligned}
\text{Div}\,\sigma&=0\;\; \text{($\Omega$-n)}\\
\sigma\nu&=\mathbf{h}\;\; \text{($\partial\Omega$-n)}
\end{aligned}
\right\}
\]
feladatot, ahol $\mathbf{h}=(-0.5,0)$ ($\{0\}\times(0,1)$-en), $\mathbf{h}=(0.5,0)$ ($\{1\}\times(0,1)$-en) egyébként pedig zérus. Igazoljuk, hogy az
\[
\mathbf{u}(x_1,x_2)=\begin{bmatrix}
\dfrac{1}{2E}x_1\\
-\dfrac{\nu}{E}x_2
\end{bmatrix}
\]
függvény megoldása a feladatnak, ahol
\[
E=\frac{4\mu(\mu+\lambda)}{2\mu+\lambda},\quad \nu=\frac{\lambda}{2\mu+\lambda}
\]
rendre az ún. Young-modulusz és Poisson-szám.
\end{Exercise}

\begin{Exercise}[label=gaussexer]
Igazoljuk a Green-formula tenzormezőkre vonatkozó általánosítását:
\[
\int_\Omega \text{Div}\,\tau \cdot \mathbf{v}=\int_{\partial\Omega} \tau\pmb{\nu}\cdot \mathbf{v}-\int_\Omega \tau : D\mathbf{v},
\]
ahol $\Map{\tau}{\Omega}{\Real^{n\times n}}$, $\Map{\mathbf{v}}{\Omega}{\Real^n}$ és $\Map{\pmb{\nu}}{\partial\Omega}{S(0,1)\subset\Rn}$ a kifelé mutató normálvektor-mező. Vezessük le ebből
a Gauss-tétel tenzormezőkre vonatkozó alakját:
\[
\int_\Omega \text{Div}\,\tau=\int_{\partial\Omega} \tau\pmb{\nu}.
\]
[Segítség: legyen $\tau=(\tau_1,\ldots,\tau_n)^\trans$, ekkor $\text{Div}\,\tau=(\text{div}\tau_1,\ldots,\text{div}\tau_n)^\trans$. Alkalmazzuk a bal oldali,
$(\text{div}\tau_k)v_k$ tagokból álló összeg minden egyes tagjára a Gauss-tétel következő alakját:
\[
\int_\Omega u\,\text{div}\, \mathbf{F}=\int_{\partial\Omega} \mathbf{F}\cdot\pmb{\nu}\,u - \int_\Omega Du\cdot \mathbf{F},
\]
ahol $\Map{u}{\Omega}{\Real}$ és $\Map{\mathbf{F}}{\Omega}{\Rn}$.]
\end{Exercise}

\begin{Exercise}[label=abtrace]
Az első tag átalakításánal felhasználtuk a következőt triviális tényt: ha $\fie{A},\fie{B}\in\Real^{n\times n}$ és $\fie{A}=\fie{A}^\trans$, akkor $\fie{A}:\fie{B}=\fie{A}:\frac{1}{2}(\fie{B}+\fie{B}^\trans))$.
[Segítség: $\fie{A}:\fie{B}=\Tr(\fie{A}\fie{B}^\trans)$]
\end{Exercise}
