
\section{Nem-ekvidisztáns rácsok: 1D}\label{nonequi1d}

Gyakran előfordulhat (lásd pl. \az{\ref{solveplot}}. szakasz példáját), hogy az ekvidisztáns rácsfelosztás nem megfelelően ,,mintavételezi'' a megoldást:
bizonyos helyeken (például ahol megoldás gyorsan változik, azaz oszcillál) túl ritka, máshol fölöslegesen sűrű (ahol a megoldás nagyjából konstans).
Ezért igen gazdaságos ún. nem-ekvidisztáns rácsokat alkalmazni, amelynek az rácstávolságát szabadon besűrűsíthetjük, illetve megritkíthatjuk.

Jelölje $h_{1,2},h_{2,3},\ldots,h_{N-1,N}$ az 
\[
\Omega_h=\{x_1,x_2,x_3,\ldots,x_{N-1},x_N\}\subset\Real
\]
rácspontok közötti távolságot és legyen $h$ a maximális $h_{i,i+1}$ rácstávolság. Írjuk fel tetszőleges $x_i$ pontban ($i=2,\ldots,N-1$) az előre-, ill. a hátramenő Taylor-sort:
\begin{align*}
u_{i+1}&=u_i + h_{i,i+1} u'(x_i)+ \frac{h_{i,i+1}^2}{2} u''(x_i)  + \frac{h_{i,i+1}^3}{6} u^{(3)}(x_i) + \mathcal{O}(h^4) \\
u_{i-1}&=u_i - h_{i-1,i} u'(x_i) + \frac{h_{i-1,i}^2}{2} u''(x_i)  - \frac{h_{i-1,i}^3}{6} u^{(3)}(x_i) + \mathcal{O}(h^4)
\end{align*}
valamely $\xi\in (x_i,x_{i+1})$ és $\zeta\in(x_{i-1},x_i)$ pontokra. Ezekből a szokásos \emph{forward-}, ill. \emph{backward-differenciasémákat} kapjuk:
\begin{align*}
\frac{u_{i+1}-u_i}{h_{i,i+1}}&= u'(x_i) + \frac{h_{i,i+1}}{2} u''(x_i)  + \frac{h_{i,i+1}^2}{6} u^{(3)}(x_i) + \mathcal{O}(h^3) \\
\frac{u_i-u_{i-1}}{h_{i-1,i}}&= u'(x_i) - \frac{h_{i-1,i}}{2} u''(x_i)  + \frac{h_{i-1,i}^2}{6} u^{(3)}(x_i) - \mathcal{O}(h^3)
\end{align*}
Kivonással kapjuk az
\[
\frac{u_{i+1}-u_i}{h_{i,i+1}}-\frac{u_i-u_{i-1}}{h_{i-1,i}}=\frac{h_{i,i+1}+h_{i-1,i}}{2} u''(x_i) + \frac{h_{i,i+1}^2-h_{i-1,i}^2}{6} u^{(3)}(x_i)+\mathcal{O}(h^3)
\]
egyenletet. Ebből
\[
\frac{2}{h_{i,i+1}+h_{i-1,i}} \Big(\frac{u_{i+1}-u_i}{h_{i,i+1}}-\frac{u_i-u_{i-1}}{h_{i-1,i}}\Big)= u''(x_i) + \frac{h_{i,i+1}-h_{i-1,i}}{3}u^{(3)}(x_i) + \mathcal{O}(h^2)
\]
A Taylor-soros módszerrel csak akkor tudunk arra következetetni, hogy a bal oldalon szereplő séma másodrendű, ha 
a rács ekvidisztáns (ekkor ugyanis a jobb oldal második tagja eltűnik). Hosszadalmas számolással megmutatták\footnote{Thomas A. Manteuffel, Andrew B. White, Jr. \emph{The Numerical Solution of Second-Order
Boundary Value Problems on Nonuniform Meshes.} (1986)}, hogy az imént levezetett séma másodrendű, azaz
\begin{equation}\label{nulapl}
\boxed{u''(x_i)=\frac{2}{h_{i,i+1}+h_{i-1,i}} \Big(\frac{u_{i+1}-u_i}{h_{i,i+1}}-\frac{u_i-u_{i-1}}{h_{i-1,i}}\Big) + \mathcal{O}(h^2),\quad i=2,\ldots,N-1.}
\end{equation}

\begin{exam} Adjuk meg a $-u''(x)=f(x)$ ($x\in(0,1)$), $u(0)=u(1)=0$ véges differenciás diszkretizációját az $\Omega_h=\{0, \tfrac{1}{4}, \tfrac{1}{2}, 1\}$ rácson 
($h_{1,2}=\tfrac{1}{4}$, $h_{2,3}=\tfrac{1}{4}$, $h_{3,4}=\tfrac{1}{2}$). Legyen $u_1=u_4=0$. Az $u_2$-re vonatkozó egyenlet:
\[
64u_2-32u_3=f(x_2).
\]
Az $u_3$-ra vonatkozó egyenlet:
\[
-8u_2 + \frac{64}{3} u_3=f(x_3).
\]
Elmondhatjuk, hogy a nem-ekvidisztáns rácson, a \emph{csomópontokban} felírt véges differenciás diszkretizáció mátrixa általában \emph{nem szimmetrikus}, ugyanis itt
\[
A_h=\begin{pmatrix}
64 & -32 \\
-8 & \tfrac{64}{3}
\end{pmatrix}.
\]
\end{exam}

\begin{figure}
\begin{center}
\includegraphics[scale=6.5]{fig/centernode1d.pdf}
\end{center}
\caption{}
\label{centernode}
\end{figure}

\begin{rmrk} Ha az $(x_{i-1},x_i)$ cellák $c_i=\tfrac{1}{2}(x_{i-1}+x_i)$ középpontjaiban értelmezzük az $u_i$ megoldást (lásd \ref{centernode}. Ábra), akkor belátható, hogy az egyenletrendszer szimmetrikus lesz.
Az irodalomban és a gyakorlatban gyakran találkozhat az olvasó olyan véges differenciás diszktretizációval, amely a cellák középponjában tárolja a közelítő megoldás értékeit --
ennek oka, hogy általában szimmetrikus lesz a kapott egyenletrendszer. Mi mégis a csomóponti megközelítést alkalmazzuk, mivel ennek implementációja sokkal egyszerűbb (2D-ben) és
ugyanolyan pontosságot lehet vele elérni.
\end{rmrk}

A peremfeltételeknek megfelelő diszkretizációs mátrixot hasonlóan tudjuk megkonstruálni, mint a függvényegyütthatós esetben. \Az{\eqref{nulapl}} sémát a $-u''=f$ Poisson-egyenletre, kapjuk a
\[
\frac{2}{h_{i,i+1}+h_{i-1,i}}\Big[-\frac{1}{h_{i-1,i}} u_{i-1} +  \Big(\frac{1}{h_{i-1,i}}+\frac{1}{h_{i,i+1}}\Big)u_i -\frac{1}{h_{i,i+1}} u_{i+1}\Big]=f(x_i),
\]
$i=2,\ldots,N-1$ egyenleteket. Gondolatban ellenőrizzuk le, hogy visszakapjuk az ekvidisztáns esetet! A képletek egyszerűsítése végett vezessük be az
\[
\eta_i = \frac{2}{h_{i,i+1}+h_{i-1,i}}
\]
jelölést, ahol $i=2,\ldots,N-1$.
 Az $u(x_1)=\alpha$, $u(x_N)=\beta$ \textbf{tiszta Dirichlet-peremfeltétel} kezelése teljesen ugyanúgy történik, ahogyan az ekvidisztáns esetben. Az kapott egyenletrendszer mátrixos alakban
\begin{align*}
D
\begin{pmatrix}
\frac{1}{h_{1,2}}+\frac{1}{h_{2,3}} & -\frac{1}{h_{2,3}} & \\
-\frac{1}{h_{2,3}} & \frac{1}{h_{2,3}}+\frac{1}{h_{3,4}} & -\frac{1}{h_{3,4}}\\
 &\ddots &\ddots &\ddots \\
& & -\frac{1}{h_{N-2,N-1}} & \frac{1}{h_{N-2,N-1}}+\frac{1}{h_{N-1,N}}
\end{pmatrix}
\begin{pmatrix}
u_2 \\
u_3 \\
u_4 \\
\vdots \\
u_{N-2}\\
u_{N-1}
\end{pmatrix}
=\begin{pmatrix}
f(x_2) + \eta_2\frac{1}{h_{1,2}}\alpha\\
f(x_3) \\
f(x_4) \\
\vdots \\
f(x_{N-2})\\
f(x_{N-1}) + \eta_{N-1}\frac{1}{h_{N-1,N}}\beta
\end{pmatrix}
\end{align*}
ahol $D=\diag(\eta_2,\ldots,\eta_{N-1})$.

 A bal végpontban lévő $-u'(x_1)=\alpha$ \textbf{Neumann-peremfeltétel} kezeléséhez a
$u_0$ ghost-változót az $x_0=-h_{1,2}$ pontba helyezzük. Ekkor a
\[
\left.
\begin{aligned}
-\frac{1}{h_{1,2}^2} u_{0} +  \frac{2}{h_{1,2}^2}u_1 -\frac{1}{h_{1,2}^2} u_{2}&=f(x_1)\\
\frac{u_0-u_1}{2h_{1,2}}&=\alpha
\end{aligned}\right\}
\]
egyenletrendszert tudjuk felírni az $x_1$ pontban. Visszahelyettesítéssel kapjuk az
\[
\frac{1}{h_{1,2}^2}u_1 -\frac{1}{h_{1,2}^2} u_{2}=f(x_1)+\frac{2}{h_{1,2}}\alpha
\]
egyenletet. A szimmetrizálással most nem kell törődnünk.

\begin{figure}
\begin{center}
\includegraphics[scale=0.5]{fig/fd1neq.pdf}
\includegraphics[scale=0.5]{fig/fd1eq.pdf}
\end{center}
\caption{\Az{\eqref{atantest}} feladat diszkretizációjának véges differenciás közelítése egy adaptív ráccsal (kék, bal oldal),
ekvidisztáns ráccsal (kék, jobb oldal) és a pontos megoldás (piros) az $L=1000$ esetben, $N=101$ rácsponttal. 
Látható, hogy az adaptív rácspontok besűrűsödnek az ugrás körül, és megritkulnak, ahol konstans a megoldás.}
\label{fd1neqsol}
\end{figure}

Hogyan építsük össze gyorsan a rendszermátrixot MATLAB/Octave-ban? Az $\{x_i\}$ rácspontokat egy \lstinline|X| nevű $N$-dimenziós vektorban tárolva 
$\{h_{i,i+1}\}$ rácstávolságokat a \lstinline|H=X(2:N)-X(1:N-1)|  utasítással kapjuk, ami egy $(N-1)$-dimenziós vektor. 
A tiszta Dirichlet-peremfeltétel esetében a diszkretizációs mátrix összeépítése ekkor az
\begin{lstlisting}
H = (X(2:N) - X(1:N-1))'; 
Hinv = 1 ./ H(1:N-1);
D = spdiags( 2./( H(2:N-1) + H(1:N-2) ), 0, N-2, N-2);
\end{lstlisting}
segédmennyiségek kiszámítása után az
\begin{lstlisting}
A = D * spdiags( [ -Hinv(2:end) Hinv(1:end-1)+Hinv(2:end) -Hinv(1:end-1) ], -1:1, N-2, N-2 );
\end{lstlisting}
utasítással történhet. A teljes implementációt az \lstinline|fd1_lapl_neq| függvény tartalmazza.

Az $\{x_i\}$ rácspontok megválasztásában teljes szabadságunk van, csupán arra kell ügyelnünk, hogy az első és az utolsó elemek
az $\Omega$ intervallum végpontjai legyenek. Az \lstinline|test_fd1_neq| szkript néhány egyszerű rácsot generál \az{\eqref{atantest}}
feladat diszkretizációjához. \Az{\ref{fd1neq}}. táblázatban olyan nem-ekvidisztáns rácsot használunk, amelyet a problémához adaptáltunk, lásd \ref{fd1neqsol}. ábra.
A táblázatból világosan látszik az \textbf{adaptív rácsok} hasznossága. 

\begin{table}                                                                                                                       
\centering                                                                                                                               
\begin{tabular}{|c|c|c|c|c|}                                                                                                                 
\hline                                                                                                                                   
$N$ & $h$ & $h_{eff}$ & Ekvidisztáns & Nem-ekvidisztáns \\                                                                                           
\hline                                                                                                                                   
11 & 1.0000e-01 & 4.0000e-03 & 1.2291e+00 & 4.5665e-01 \\                                                                                                           
\hline                                                                                                                                   
51 & 2.0000e-02 & 3.2000e-05 & 1.3449e+00 & 6.6245e-02 \\                                                                                                          
\hline                                                                                                                                   
101 & 1.0000e-02 & 4.0000e-06 & 1.2099e+00 & 1.6675e-02 \\                                                                                                          
\hline                                                                                                                                   
501 & 2.0000e-03 & 3.2000e-08 & 3.1760e-01 & 6.6078e-04 \\                                                                                                          
\hline                                                                                                                                   
1001 & 1.0000e-03 & 4.0000e-09 & 3.0340e-02 & 1.6515e-04 \\                                                                                                         
\hline                                                                                                                                   
5001 & 2.0000e-04 & 3.2000e-11 & 2.1805e-03 & 6.6070e-06 \\                                                                                                         
\hline                                                                                                                                   
\end{tabular}                                                                                                                            
\caption{\Az{\eqref{atantest}} feladat diszkretizációjának abszolút hibája az $L=1000$ esetben. A $h_{eff}$ a nem-ekvidisztáns rács effektív rácsmérete, azaz a minimális rácstávolság. 
(Generálta: \lstinline|test_fd1_neq.m|)}
\label{fd1neq}                                                                                                               
\end{table} 

Végül alkalmazzuk az iménti eljárást a 
\[
\left.\begin{aligned}
-\epsilon u'' + u'&=1\\
u(0)&=0\\
u(1)&=1
\end{aligned}\right\}
\]
\textbf{konvekció-dominált} feladat közelítésére, lásd előadásjegyzet 90. o. A feladat egzakt megoldása könnyen láthatóan
\[
u(x)=x-\frac{e^{x/\epsilon}-1}{e^{1/\epsilon}-1}.
\]
Ennek deriváltja
\[
u'(x)=1-\frac{1}{\epsilon}\frac{e^{x/\epsilon}}{e^{1/\epsilon}-1},
\]
amely $0$-hoz közeli $x$-ekre $u'(x)\approx 1$, ám 1-hez közeledve óriási lesz (negatív). Ennek az résztartománynak a neve \textbf{határréteg}, és a rácsot itt nagyon finomnak fogjuk
választani.

Az adaptív rácsfinomitási algoritmusokban (\textbf{adaptive mesh refinement} -- AMR) egy \emph{a posteriori} hibabecslő jelöli meg azokat a helyeket a rácson,
amelyeket finomításával remélhetőleg csökkenthető a (szintén becsült) hiba. Az AMR eljárások a gyakorlati feladatokban nagyon hasznosak és széles körben használtak a kereskedelmi programcsomagokban,
mivel a rácsgenerálást teljesen automatizálják -- ezt a lépést korábban kézzel kellett elvégezni (2-, vagy 3-dimenzióban!), amely az egész modellezési/szimulációs 
munkát lelassította.
