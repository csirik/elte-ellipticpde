
\section{Baricentrikus koordináták}

Ahogy egy körlapon a polárkoordinátákat, egy háromszögön a baricentrikus koordinátákat tekintjük ``természetes koordinátáknak. 
Mivel a baricentrikus koordinátákat $n$-dimenzióban kényelmesebb tárgyalni, emlékeztetünk, hogy a ``háromszög'' 
megfelelő $n$-dimenziós általánosítása az $n$-szimplex, amely egyszerűen $n+1$ darab $\Rn$-beli \emph{általános helyzetű}\footnote{Azaz nem esnek mind egy valódi hipersíkra} pont
konvex burka. Jelekkel, $T\subset\Rn$ $\pmb{n}$\textbf{-szimplex}, ha létezik
\[
\{\fie{a}_0,\fie{a}_1,\ldots,\fie{a}_n\}\subset\Rn
\]
általános helyzetű ponthalmaz úgy, hogy $T=\{\fie{a}_0,\fie{a}_1,\ldots,\fie{a}_n\}^{co}$.\footnote{így jelöljük a konvex burkot} Az egydimenziós esetben egy zárt $[a_0,a_1]\subset\Real$ intervallumot kapunk,
kétdimenzióban zárt háromszöglapot, és háromdimenzióban tetraédert. Egy technikai megjegyzés: a ponthalmazt (\textbf{csúcslistáját})
rögzített sorrendűnek tekintjük, bár nyilván a konvex burok képzése érzéketlen az átrendezésre.

Rögzített $T\subset\Rn$ szimplex esetén a $\Map{\bm{\lambda}}{\Rn}{\Real^{n+1}}$ függvényt \textbf{baricentrikus leképezésnek} nevezzük, a 
\[
\bm{\lambda}(\fie{x})=(\lambda_0(\fie{x}),\lambda_1(\fie{x}),\ldots,\lambda_n(\fie{x}))
\]
által meghatározott $\Map{\lambda_k}{\Rn}{\Real}$ koordinátafüggvényeket \textbf{baricentrikus koordinátáknak} nevezzük, ha
\begin{enumerate}[(B1)]\indented
\item \textbf{Egységosztás:} 
\[
\forall \fie{x}\in\Rn \;:\; \sum_{k=0}^n \lambda_k(\fie{x})=1
\]
\item \textbf{Baricentrikus tulajdonságú:}
\[
\forall \fie{x}\in\Rn \;:\; \sum_{k=0}^n \lambda_k(\fie{x})\fie{a}_k=\fie{x}
\]
\end{enumerate}
Ez a $\bm{\lambda}$ baricentrikus leképezés egyértelműen meghatározott, ugyanis az (B1)--(B2) tulajdonságok ekvivalensek az alábbi lineáris algebrai egyenletrendszerrel
\begin{equation}\label{Blambda}
\underbrace{\begin{pmatrix}
1 & 1 & \cdots & 1\\
a_{01} & a_{11} & \cdots & a_{n1}\\
\vdots & \vdots & \ddots & \vdots \\
a_{0n} & a_{1n} & \cdots & a_{nn}
\end{pmatrix}}_{\displaystyle{B}}
\begin{pmatrix}
\lambda_0 \\
\lambda_1 \\
\vdots \\
\lambda_n
\end{pmatrix}
=
\begin{pmatrix}
1 \\
x_1 \\
\vdots \\
x_n
\end{pmatrix}
\end{equation}
ahol $\fie{x}=(x_1,\ldots,x_n)$. Minthogy $\{\fie{a}_0,\fie{a}_1,\ldots,\fie{a}_n\}$ általános helyzetű ponthalmaz, $\det B\neq 0$ (miért?), így az egyenletrendszernek minden $\fie{x}\in\Rn$ esetén
egyértelműen létezik $\bm{\lambda}=\bm{\lambda}(\fie{x})\in\Real^{n+1}$ megoldása. Ennélfogva bármely $\fie{x}\in\Rn$ pont ``reprezentálható'' egy $\bm{\lambda}(\fie{x})\in\Real^{n+1}$ baricentrikus ponttal
a
\begin{equation}\label{bariinv}
\bm{\lambda}(\fie{x}) = B^{-1}\begin{pmatrix}
1 \\
\fie{x}
\end{pmatrix}
\end{equation}
képlet szerint. Ha a $T$ szimplex rögzített, egy programban a $B$ mátrixot csak egyszer kell invertálni és a baricentrikus koordináták meghatározása egy mátrix-vektor szorzást igényel.

Most nézzük meg, hogy festenek a $\lambda_k(\fie{x})$ függvények! A fenti \eqref{bariinv} képlet alapján
\begin{equation}\label{lambdahyp}
\lambda_k(x)= \fie{e}_{k+1}^\trans B^{-1} \begin{pmatrix}
1 \\
\fie{x}
\end{pmatrix}
=\mu_k + \bm{\nu}_k\cdot \fie{x},
\end{equation}
ahol $\fie{e}_{k+1}$ a $(k+1)$-edik standard egységvektor és $(\mu_k\,\bm{\nu}_k)=\fie{e}_k^\trans B^{-1}$, $\mu_k\in\Real$, $\bm{\nu}_k\in\Rn$. Így
\[
H_k=\{\fie{x}\in\Rn \;:\; \lambda_k(\fie{x})=0\}
\]
egy $(n-1)$ dimenziós (affin) hipersík amelyre az $\fie{a}_k$-t kivéve az összes csúcs illeszkedik. Ez utóbbi geometriai tény könnyen látható az alábbi, ún. $\pmb{\delta}$\textbf{-tulajdonságból}.
\begin{bprop}
Legyen $T\subset\Rn$ tetszőleges szimplex, amely rendezett csúcslistája $A=\{\fie{a}_0,\fie{a}_1,\ldots,\fie{a}_n\}$. Ekkor a 
$\bm{\lambda}(\fie{x})=(\lambda_0(\fie{x}),\lambda_1(\fie{x}),\ldots,\lambda_n(\fie{x}))$ baricentrikus leképezésre
\[
\forall k,\ell\in\{0,1,\ldots,n\} \;:\; \lambda_k(\fie{a}_\ell)=\delta_{k\ell}.
\]
\end{bprop}
\begin{proof}
Azonnal következik a baricentrikus koordináták (B2) tulajdonságából az $\fie{x}=\fie{a}_\ell$ választással.
\end{proof}

\begin{exam}
Legyen $n=2$ és $\fie{a}_0=(0\, 0)$, $\fie{a}_1=(1\,0)$, $\fie{a}_2=(0\,1)$. Ekkor 
\[
B=\begin{pmatrix}
1 & 1 & 1 \\
0 & 1 & 0 \\
0 & 0 & 1
\end{pmatrix},\quad
B^{-1}=\begin{pmatrix}
1 & -1 & -1 \\
0 & 1 & 0 \\
0 & 0 & 1
\end{pmatrix}
\]
és
\[
\lambda_0(\fie{x})= 1 - x_1 - x_2, \quad \lambda_1(\fie{x})=x_1, \quad \lambda_2(\fie{x})=x_2.
\]
\end{exam}

\begin{exam}
Legyen $n=3$ és $\fie{a}_0=(0\, 0\,0)$, $\fie{a}_1=(1\,0\,0)$, $\fie{a}_2=(0\,1\,0)$, $\fie{a}_3=(0\,0\,1)$. Ekkor 
\[
B=\begin{pmatrix}
1 & 1 & 1 & 1\\
0 & 1 & 0 & 0\\
0 & 0 & 1 & 0\\
0 & 0 & 0 & 1
\end{pmatrix},\quad
B^{-1}=\begin{pmatrix}
1 & -1 & -1 & -1\\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{pmatrix}
\]
és
\[
\lambda_0(x)= 1 - x_1 - x_2 - x_3, \quad \lambda_1(x)=x_1, \quad \lambda_2(x)=x_2, \quad \lambda_3(x)=x_3.
\]
\end{exam}

Az egyszerűség kedvéért az $n=2$ esetben rögtön megállapíthatjuk, hogy
\begin{itemize}
\item $\{\lambda_0,\lambda_1,\lambda_2\}\subset P^1$ bázis.\footnote{$P^1$ jelöli az $n$-valtozós elsőfokú polinomok vektorterét.}
\item $\forall k,\ell\in\{0,1,2\} \;:\; \lambda_k(\fie{a}_\ell)=\delta_{k\ell}$ 
\item $\forall k \in \{0,1,2\} \;:\; 0\le \lambda_k(\fie{x})\le 1\;\iff\; \fie{x}\in\Sigma$.
\item $D\lambda_0=(-1\;-1)$, $D\lambda_1=(1\;0)$, és $D\lambda_2=(0\;1)$.
\end{itemize}

A fenti, mintkét esetben $\Sigma$-val jelölt szimplexet \textbf{standard-} vagy \textbf{referenciaszimplexnek} nevezzük és általában
\[
R=\{0,\fie{e}_1,\ldots,\fie{e}_n\}^{co},
\]
ahol $\{\fie{e}_k\}$ a standard egységvektorokat jelöli.

A fenti tulajdonságok megfelelő módosítással általános $T$ szimplexen is igazak.
\begin{bprop}\label{simpbary}
Legyen $T\subset\Rn$ tetszőleges szimplex. Ekkor
\begin{enumerate}[(1)]\indented
\item $\{\lambda_k\}\subset P^1$ bázis.
\item $\forall k\in\{0,1,\ldots,n\} \;:\; 0\le \lambda_k(\fie{x})\le 1\;\iff\;\fie{x}\in T$.
\item $\forall k\in\{0,1,\ldots,n\} \;:\; \displaystyle{D\lambda_k=-\frac{1}{d_k}\fie{n}_k}$, ahol $d_k=d(\fie{a}_k,H_k)$ és $\fie{n}_k\in\Rn$ a $H_k$ kifelé mutató egység hosszú normálvektora.
\end{enumerate}
\end{bprop}
\begin{proof}
\textbf{(1)} Azt állítjuk, hogy tetszőleges $u\in P^1$ polinomra
\[
u(\fie{x}) = \sum_{k=0}^n u(\fie{a}_k)\lambda_k(\fie{x}).
\]
Valóban, a $\delta$-tulajdonság miatt a jobb oldal az $n+1$ darab $\{\fie{a}_k\}$ pontban megegyezik $u$ megfelelő helyettesítési értékeivel.
A két oldal tehát egyenlő, mivel $u$ grafikonja egy hipersík ($\Real^{n+1}$-ben) és azt egyértelműen meghatározza a rajta fekvő $n+1$ pont ismerete.

\textbf{(2)} Legyen először $0\le \lambda_k(\fie{x})\le 1$ ($k\in\{0,1,\ldots,n\}$), ekkor a (B1) és (B2) tulajdonságok alapján
\[
\sum_{k=0}^n \lambda_k(\fie{x})\fie{a}_k=\fie{x}\in\Delta,
\]
ugyanis a bal oldalon egy konvex kombináció áll. Megfordítva, tegyük fel, hogy $\fie{x}\in T$. \Az{\eqref{Blambda}} egyenletrendszere alkalmazva a Cramer-szabályt, kapjuk
\[
\forall k\in\{0,\ldots,n\} \;:\;\lambda_k(\fie{x})=\frac{\det(B_k)}{\det(B)}=\frac{n!|T_k|}{n!|T|}=\frac{|T_k|}{|T|},
\]
ahol $B_k$-t a $B$-ből kapjuk a $k$-adik oszlop $(1\;\fie{x})=(1\; x_1\; \ldots\; x_n)$-re való cserélésével és
\[
T_k=\{\fie{a}_0,\ldots,\fie{a}_{k-1},\fie{x},\fie{a}_k,\ldots,\fie{a}_n\}^{co}.
\]
Mármost, $\fie{x}\in T$ miatt $T_k\subset T$ mivel $T_k$ minden csúcsa $T$-ben fekszik. Ebből $|T_k|\le |T|$, amiből pedig $0\le \lambda_k(\fie{x})\le 1$ következik.

\textbf{(3)} \Az{\eqref{lambdahyp}} felírás alapján $D\lambda_k=\bm{\nu}_k$. A kívánt eredmény következik a pont és hipersík távolságára vonatkozó képletből.
\end{proof}

\section{A $T_3$ Courant-elem}
Először emlékeztetünk az előadáson bevezetett jelölésre. Az $\Omega\subset\Rn$ tartomány (itt $n=2,3$) $\mathcal{T}_h$ (konform) \textbf{triangulációja} egy 
zárt poliéderekkel (most szimplexekkel: háromszögekkel vagy tetraéderekkel) való felbontása úgy, hogy $T_1\cap T_2$ csak csúcs, vagy teljes lap lehet ($T_1,T_2\in\mathcal{T}_h$).

Most legyen $\mathcal{T}_h$ egy szimplexekből álló trianguláció és álljon a $V_h$ véges dimenziós \textbf{approximáló altér} 
a résztartományonként lineáris, folytonos függvényekből. Jelekkel:
\[
V_h=\Big\{u\in C(\overline{\Omega}) \;:\; \restr{u}{T}\in P^1(T) \;\;(\forall T\in\mathcal{T}_h)\Big\}.
\]
A végeselem-módszer alkalmazásához ebben egy bázist kell megadni. Legyen
\[
\{\phi_{\fie{x}}\}_{\fie{x}\in\overline{\Omega}_h}\subset V_h,
\]
ahol $\overline{\Omega}_h$ a $\mathcal{T}_h$ trianguláció csúcsait jelöli, és követeljük meg a $\pmb{\delta}$\textbf{-tulajdonságot}
\[
\forall \fie{x},\fie{y}\in\overline{\Omega}_h \;:\; \phi_{\fie{x}}(\fie{y})=\delta(\fie{x}-\fie{y}),
\]
ahol $\delta(0)=1$, és $\delta(\fie{z})=0$ egyébként.

Az alábbiakban belátjuk, hogy ilyen $\phi_{\fie{x}}$ ún. \textbf{kalap-}, vagy másképpen \textbf{sátorfüggvényekből} álló bázis egyértelműen létezik.
Ehhez jelölje 
\[
\Star(\fie{x})\subset\mathcal{T}_h
\]
az $\fie{x}$ pont szomszédos szimplexeiből álló ``környezetét'', azaz azokat a $T\in\mathcal{T}_h$ szimplexeket
amelyeknek csúcsa $\fie{x}$. Legyen most $\phi_{\fie{x}}$ egy ilyen $T\in\mathcal{T}_h$ szimplexen egyenlő azzal a $\lambda_{T,\fie{x}}$ baricentrikus függvénnyel, 
amelyre $\lambda_{T,\fie{x}}(\fie{x})=1$; a környezeten kívül pedig legyen azonosan nulla.
\footnote{Figyeljük meg, hogy most $\lambda_T$-t a $T$ csúcsai szerint indexeljük.} Jelekkel:
\[
\phi_{\fie{x}}(\bm{\xi})=\begin{cases}
\,\lambda_{T,\fie{x}}(\bm{\xi}), \;&\text{ha}\; \bm{\xi}\in T\in \Star(\fie{x})\\
\,0, \;&\text{egyébként}
\end{cases}
\]
Világos, hogy $\phi_{\fie{x}}$ folytonos, teljesíti $\delta$-tulajdonságot, és $\{\phi_{\fie{x}}\}$ elemszáma $|\overline{\Omega}_h|$, sőt a korábbiak alapján $0\le\phi_{\fie{x}}\le 1$ is teljesül.
\begin{center}
\includegraphics[scale=1.1]{fig/phix.pdf}
\end{center}

\begin{bprop}
A fenti  választással $\{\phi_{\fie{x}}\}\subset V_h$ bázis, és nincs más bázis, ami kielégíti a $\delta$-tulajdonságot.
\end{bprop}
\begin{proof}
Lássuk be, hogy egy tetszőleges $u\in V_h$ függvényre
\begin{equation}\label{uuphi}
u=\sum_{\fie{y}\in\overline{\Omega}_h} u(\fie{y})\phi_{\fie{y}}.
\end{equation}
Egyrészt korábbiakhoz hasonlóan a $\phi_{\fie{x}}$-ek $\delta$-tulajdonsága miatt a csúcsokban megegyeznek a két oldal helyettesítési értékei.
Másrészt a $\phi_{\fie{x}}$-ek definícióját alkalmazva egy rögzített $T\in\mathcal{T}_h$ esetén
\[
\restr{u}{T}=\sum_{\fie{y}} u(\fie{y})\lambda_{T,\fie{y}},
\]
(ahol az összegzés a $T$ csúcsain értendő), amiből $\{\lambda_{T,\fie{x}}\}\subset P^1(T)$ bázis-tulajdonsága miatt egyértelmű az iménti kifejtés.
Végül jegyezzük meg, hogy \eqref{uuphi} jobb oldala folytonos függvény, elvégre folytonos függvények lineáris kombinációja.
Világos, hogy $\{\phi_{\fie{x}}\}$ lineárisan független; ezzel beláttuk, hogy $\{\phi_{\fie{x}}\}$ bázis a $V_h$ altérben.
\end{proof}

\section{A referenciaelem-technika}
A végeselem-módszer mátrix-összefűzésekor az alábbi kényelmi eszközt használják. 

\subsection{Affin eset}
Koncentráljunk arra az esetre, amikor minden szimplex megkapható egy adott szimplex affin transzformáltjaiként.
Nevezzük az $R=\Sigma\subset\Rn$  standard szimplexet \textbf{referenciaelemnek}. 
Így minden $T\in\mathcal{T}_h$ egy $\Map{\bm{\zeta}}{R}{T}$ affin transzformációval kapunk meg. 

Legyen $n=2$ és $T=\{\fie{a}_0,\fie{a}_1,\fie{a}_2\}^{co}$. Ekkor
\[
\bm{\zeta}(\fie{x})=\underbrace{\begin{pmatrix}
(\fie{a}_1-\fie{a}_0)^\trans \\
(\fie{a}_2-\fie{a}_0)^\trans
\end{pmatrix}}_{\displaystyle{Z}}
\fie{x}
+\underbrace{\fie{a}_0}_{\displaystyle{\fie{b}}}
\]
a megfelelő affin transzformáció, ugyanis $R=\{0,\fie{e}_1,\fie{e}_2\}^{co}$. A $Z$ mátrix koordinátákkal kifejezve:
\[
Z=\begin{pmatrix}
a_{11}-a_{01} & a_{21}-a_{01} \\
a_{12}-a_{02} & a_{22}-a_{02}
\end{pmatrix}.
\]

Emlékeztetünk az integráltranszformációs formulára. Legyen $\Map{f}{T}{\Real}$ egy $T$-n értelmezett skalárfüggvény. Ekkor
\[
\int_{T} f = |\det Z|\int_R f\circ\bm{\zeta},
\]
ugyanis $T=\bm{\zeta}(R)$ és $\bm{\zeta}'=Z$. Ennek alapján az
\[
a(u,v)=\int_T Du\cdot Dv\,dx
\]
bilineáris forma kifejezhető $R$-n vett integrállal. Ehhez legyen $\Map{\widetilde{u}=u\circ\bm{\zeta}}{R}{\Real}$ és vegyük figyelembe, hogy
\[
D\widetilde{u}=D(u\circ\bm{\zeta})=\bm{\zeta}'Du\circ\bm{\zeta}=ZDu\circ\bm{\zeta},
\]
így
\[
Du\circ\bm{\zeta}=Z^{-1}D(u\circ\bm{\zeta})=Z^{-1}D\widetilde{u}.
\]
Tehát
\begin{align*}
a(u,v)&=|\det Z|\int_R (Du\circ\bm{\zeta}) \cdot (Dv\circ\bm{\zeta})=|\det Z|\int_R Z^{-1}D(u\circ\bm{\zeta})\cdot Z^{-1}D(v\circ\bm{\zeta})\\
&=|\det Z|\int_R  Z^{-1}D\widetilde{u} \cdot Z^{-1}D\widetilde{v}
\end{align*}

\Exercises

\begin{Exercise}
Bizonyítsuk be a $\delta$-tulajdonság alapján, hogy $\{\lambda_k\}$ lineárisan független.
\end{Exercise}

\begin{Exercise}
Az $n=2$ esetben adjuk meg \az{\ref{simpbary}}. Állítás bizonyításban szereplő $\lambda_k(x)=|T_k|/|T|$ formula geometriai magyarázatát.
\end{Exercise}

\begin{Exercise}
Legyen $n=2$ és jelölje
\[
\bm{\eta}_0=\fie{a}_1-\fie{a}_0,\; \bm{\eta}_1=\fie{a}_2-\fie{a}_1, \; \bm{\eta}_2=\fie{a}_0-\fie{a}_2
\]
a $T=\{\fie{a}_0,\fie{a}_1,\fie{a}_2\}^{co}$ éleit. Bizonyítsuk be, hogy
\[
\int_T D\lambda_k\cdot D\lambda_\ell=\frac{1}{4|T|} \bm{\eta}_k\cdot \bm{\eta}_\ell
\]
\end{Exercise}

\begin{Exercise}[difficulty=1]
Legyen $n=2$, és jelölje $\theta_k$ a $T$ háromszög $a_k$ csúcsához tartozó belső szöget. Bizonyítsuk be, hogy
\[
\int_T D\lambda_k\cdot D\lambda_\ell\,dx = -\frac{1}{2} \cot \theta_m,
\]
ha $k$, $\ell$ és $m$ különbözőek.
\end{Exercise}

\begin{Exercise}[difficulty=1]
Bizonyítsuk be, hogy
\[
\int_0^1 x^\alpha(1-x)^\beta\,dx = \frac{\alpha!\beta!}{(\alpha+\beta+1)!},
\]
illetve, hogy
\[
\int_0^1\int_0^{1-x} x^\alpha y^\beta(1-x-y)^\gamma\,dy\,dx=\frac{\alpha!\beta!\gamma!}{(\alpha+\beta+\gamma+1)!}
\]
\end{Exercise}


\begin{Exercise}
Az integráltranszformációs formula segítségével lássuk be, hogy tetszőleges $\Delta\subset\Rn$ szimplex esetén $|\Delta|=\frac{1}{n!} |\det Z|=\frac{1}{n!} |\det B|$.
\end{Exercise}