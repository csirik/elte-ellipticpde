\section{Rácsfinomítási stratégiák}
Az adaptív rácsfinomítási algoritmusok (\textbf{AMR} -- Adaptive Mesh Refinement) roppant népszerűek a gyakorlati alkalmazásokban. Láttuk
a véges differencia-módszernél, hogy érdemes a rácsot bizonyos helyeken finomabbnak, bizonyos helyeket durvábbnak venni.
Ott nem foglalkoztunk azzal az egyáltalán nem triviális kérdéssel, hogy \emph{miként} döntsük el, hogy hol kell finomítani és hol kell sűrűsíteni.
Ennek alapját a következő szakaszban bemutatott \textbf{hibabecslő eljárás} adják. Először az adaptív algoritmusok algoritmussémáját, és a
rács geometriai finomításának lehetséges módjait fogjuk diszkutálni.

\section{A reziduális}
Legyen $V_h$ szakaszonként legfeljebb $k$-adfokú polinomokból álló approximáló altér.
Legyen $u_h\in V_h$ a Galjorkin-approximáns, azaz amelyre
\[
a(u_h,v)=\ell(v)
\]
minden $v\in V_h$ esetén. Ha $u$ a pontos variációs megoldás, azaz
\[
a(u,v)=\ell(v)
\]
minden $v\in V$ esetén, akkor érvényes a Galjorkin-ortogonalitás:
\[
a(u-u_h, v)=0,
\]
minden $v\in V_h$ esetén (itt kihasználtuk, hogy $V_h\subset V$!). Legyen
\[
e_h=u-u_h\in V
\]
a \textbf{hiba}. Ekkor a Galjorkin-ortogonalitást úgy tudjuk egyszerűen kifejezni, hogy 
\emph{az $e_h$ hiba $a$-ortogonális a $V_h$ approximáló altérre},
\[
\boxed{\forall v\in V_h \;:\; a(e_h,v)=0}
\]
Mármost az $e_h$ is kielégít egy variációs feladatot \emph{az egész} $V$-n is:
\[
\boxed{\forall v\in V\;:\; a(e_h,v)=\ell(v)-a(u_h,v)=:R(v)}
\]
Nevezzük ezt \emph{reziduális feladatnak}, az $R\in V^*$ lineáris funkcionál az ún. \emph{reziduális}. Világos, hogy erre
\[
\boxed{\forall v\in V_h \;:\; R(v)=0}
\]
érvényes. A reziduális hibabecslésben betöltött szerepe a következő. Ha rendelkezésünkre áll egy $|R(v)|\le C\mathcal{R} \|v\|_V$
alakú becslés, ahol $\mathcal{R}$ lehetőleg valamilyen könnyen kiszámítható mennyiség, akkor az $a(\ph,\ph)$ bilineáris forma $V$-koercivitásának felhasználásával
egy
\[
C_{koerc}\|e_h\|_V\le a(e_h,e_h)=R(e_h)\le C\mathcal{R}
\]
becslést kapunk az $e_h$ hibára.

\vspace{1em}
Az iménti absztrakt gondolatok alkalmazásához tekintsünk egy konkrét feladatot. A szokásos jelöléseket követve legyen $V=H^1_D(\Omega)$ és
\[
a(u,v)=\int_\Omega \fie{B}(x)Du\cdot Dv,\quad \ell(v)=\int_\Omega fv + \int_{\Gamma_N} hv,
\]
ahol $\fie{B}\in L^\infty(\Omega)^{2\times 2}$ szimmetrikus, egyenletesen pozitív definit:
\[
\exists M,m>0 \; \forall x\in\Omega\; \forall \bm{\xi}\in\Real^2 \;:\; m|\bm{\xi}|^2\le \fie{B}(x)\bm{\xi}\cdot\bm{\xi}\quad\text{és}\quad |\fie{B}(x)|\le M,
\]
és $f\in L^2(\Omega)$. Az
\[
\forall v\in H^1_D(\Omega) \;:\; a(u,v)=\ell(v)
\]
variációs feladat persze a 
\[
\left.\begin{aligned}
-\dive \fie{B}(x)Du &= f \quad(\Omega\text{-n})\\
u&=0 \quad(\Gamma_D\text{-n})\\
\fie{B}(x)Du\cdot \nu&=h \quad(\Gamma_N\text{-n})
\end{aligned}\right\}
\]
feladat gyenge alakja. 

A hibára vonatkozó reziduális feladat a következőképpen írható:
\begin{align*}
a(e_h&,v)=\sum_{T\in\mathcal{T}_h} \Bigg[ \int_T fv + \int_{\Gamma_N} hv - \int_T \fie{B}(x)Du_h\cdot Dv \Bigg]\\
&=\sum_{T\in\mathcal{T}_h} \Bigg[ \int_T \big(f+\dive \fie{B}(x)Du_h\big) v - \int_{\partial T} \big(\fie{B}(x)Du_h\cdot\nu\big) v  + \int_{\Gamma_N} hv\Bigg]\\
&=\sum_{T\in\mathcal{T}_h} \Bigg[ \int_T \big(f+\dive \fie{B}(x)Du_h\big) v - \int_{\partial T\setminus\Gamma_N} \big(\fie{B}(x)Du_h\cdot\nu\big) v +\int_{\partial T \cap \Gamma_N} \big(h -\fie{B}(x)Du_h\cdot\nu\big)v\Bigg].
\end{align*}
Az első integrandus első tényezője az ún. \emph{abszolút folytonos reziduális},
\[
\restr{R_{AC}(u_h)}{T}=\restr{(f+\dive \fie{B}(x)Du_h)}{T}.
\]
Az elnevezést azért használják, mert többi integrál az élekre vonatkozik, így az élekre koncentrált $\delta$-mérték szerint kell integrálni,
ami nyilván nem abszolút folytonos a 2-dimenziós Lebesgue-mértékre nézve (hanem szinguláris). Lineáris elemek esetén ($k=1$), rögtön látszik, hogy
 mivel $\restr{u_h}{T}$ lineáris polinom, ezért $\restr{Du_h}{\inter T}$ konstans, így ha $\fie{B}(x)\equiv I$, akkor $\restr{\dive Du_h}{\inter T}\equiv 0$.
Világos tehát, hogy lineáris elemek esetén általában akkor kicsi $R_{AC}(u_h)$, ha a finom a $\mathcal{T}_h$ rácsozás.

Az reziduális szinguláris (,,ugró'') részét a következőképpen tudjuk egyszerűbb alakra hozni.
Jelölje $\mathcal{E}_h$ a $\mathcal{T}_h$ éleit, illetve $\mathcal{E}_h^0$ a belső éleit. Ha $e\subset \Gamma_D$, akkor $\int_e p(x)D_\nu u_h v=0$.
Rögzített $e\in\mathcal{E}_h^0$ esetén $T_{e,1}$ és $T_{e,2}$ az $e$ két oldalán fekvő háromszögeket. Ekkor
\begin{align*}
\sum_{T\in\mathcal{T}_h}\int_{\partial T\setminus\Gamma_N} \big(\fie{B}(x)Du_h\cdot\nu\big) v=
\sum_{e\in\mathcal{E}_h^0} \int_e\underbrace{\Bigg[\fie{B}(x)D\restr{u_h}{T_{e,1}}\cdot\nu - \fie{B}(x)D\restr{u_h}{T_{e,2}}\cdot\nu  \Bigg]}_{\displaystyle{=:\big\llbracket \fie{B}(x)Du_h\cdot\nu\big\rrbracket_e}}v
\end{align*}
ahol bevezettük $e$ élen vett \emph{ugrást}, ami a feloldali határértékek különbsége. Pontosabban, egy $g$ függvény $x$ pontban
vett ugrását a $\fie{n}$ irányban a
\[
\llbracket g\rrbracket_{\fie{n}}(x)=\lim_{\epsilon\to 0} g(x+\epsilon\fie{n})-g(x-\epsilon\fie{n})
\]
relációval szokás definiálni. Az $e$ élen való ugrás pedig egyszerűen az él egyik $\pm\nu$ normálvektorának irányában vett ugrás az iménti értelemben.
Nyilván a normálvektor adott választása csak egy előjelbeli eltérést okoz a másikhoz képest, és könnyen látható, hogy
az iménti
\[
\big\llbracket \fie{B}(x)Du_h\cdot\nu\big\rrbracket_e
\]
ugrás nem függ a normálvektor irányának megválasztásától. Ha már bevezethetjük az \emph{ugró reziduálist} az
\[
R_J(v)=\sum_{T\in\mathcal{T}_h}\int_{\partial T \cap \Gamma_N} (h - \fie{B}(x)Du_h\cdot\nu\big)v + \sum_{e\in\mathcal{E}_h^0} \int_e\big\llbracket \fie{B}(x)D u_h\cdot\nu \big\rrbracket_ev
\]
definícióval, vagy még egyszerűbben az
\[
R_J(v)=\sum_{e\in\mathcal{E}_h} \int_e \rho_J v
\]
definícióval, ahol
\[
\restr{\rho_J}{e}(u_h)=\begin{cases}
0, & e\subset\Gamma_D\\
h - \fie{B}(x)Du_h\cdot\nu, & e\subset\Gamma_N\\
\big\llbracket \fie{B}(x)Du_h\cdot\nu\big\rrbracket_e, & e\in\mathcal{E}_h^0
\end{cases}
\]

Összességben tehát a reziduális feladatunkat a
\[
\boxed{\forall v\in V \;:\; a(e_h,v)=R(v)}
\]
alakba tudtuk írni, ahol a \emph{reziduális}
\[
\boxed{R(v)=\sum_{T\in\mathcal{T}_h} \int_T R_{AC}(u_h)v + \sum_{e\in\mathcal{E}_h} \int_e \rho_J(u_h) v}
\]
és ebben két, alapvetően másképpen kezelendő mennyiséget tudtunk elkülöníteni.

\section{Egy egyszerű explicit hibabecslő}
Az reziduálison alapuló ,,explicit'' hibabecslő konstrukciójához szükségünk van két, az interpolációs operátor hibájára vonatkozó becslésre:
\begin{align*}
\forall T\in\mathcal{T}_h \;:\; \|v-\Pi v\|_{L^2(T)}&\le C h_T \|v\|_{H^1(\Patch T)},\\
\forall e\in\mathcal{E}_h \;:\; \|v-\Pi v\|_{L^2(e)}&\le C \sqrt{h_e} \|v\|_{H^1(\Patch e)},
\end{align*}
ahol $\Patch(T)$ és $\Patch(e)$ a $T$ és $e$ ,,foltja'':
\begin{align*}
\Patch(T)&=\{S\in\mathcal{T}_h \;:\; T\cap S\neq\emptyset\},\\
\Patch(e)&=\{S\in\mathcal{T}_h \;:\; e\cap S\neq\emptyset\}.\\
\end{align*}
\begin{center}
\includegraphics[scale=0.9]{fig/patch.pdf}
\end{center}
Itt megfelelő a $h_T\approx \sqrt{|T|}$, és a $h_e=|e|$ választás.
Ennél általánosább állítás található Ainsworth és Oden monográfiájának 1.7. Tételében\footnote{Mark Ainsworth, J. Tinsley Oden. \emph{A Posteriori Error Estimation in Finite Element Analysis}, 2011}.

A reziduális-alapú hibabecslők konstrukciójának alapja a következő.
\begin{prop}
Legyen
\[
\mathcal{R}(u_h)=\Bigg[ \sum_{T\in\mathcal{T}_h} h_T^2\|R_{AC}(u_h)\|_{L^2(T)}^2 + \sum_{e\in\mathcal{E}_h} h_e \|\rho_J(u_h)\|_{L^2(e)}^2\Bigg]^{1/2}.
\]
Ekkor $\|e_h\|_{H^1(\Omega)}\le C\mathcal{R}(u_h)$.
\end{prop}
\begin{proof}
A Galjorkin-ortogonalitás miatt $a(e_h,v)=a(e_h,v-\Pi v)$, ezért alkalmazhatjuk az iménti becsléseket:
\begin{align*}
a(e_h,v)&=\sum_{T\in\mathcal{T}_h} \int_T R_{AC}(u_h)(v-\Pi v) + \sum_{e\in\mathcal{E}_h} \int_e \rho_J(u_h) (v-\Pi v)\\
&\le \sum_{T\in\mathcal{T}_h} \|R_{AC}(u_h)\|_{L^2(T)}\|v-\Pi v\|_{L^2(T)} + \sum_{e\in\mathcal{E}_h} \|\rho_J(u_h)\|_{L^2(e)}\|v-\Pi v\|_{L^2(e)}\\
&\le C\sum_{T\in\mathcal{T}_h} h_T\|R_{AC}(u_h)\|_{L^2(T)} \|v\|_{H^1(\Patch T)} + C\sum_{e\in\mathcal{E}_h} \sqrt{h_e}\|\rho_J(u_h)\|_{L^2(e)}\|v\|_{H^1(\Patch e)}\\
&\le C\underbrace{\Bigg[ \sum_{T\in\mathcal{T}_h} h_T^2\|R_{AC}(u_h)\|_{L^2(T)}^2 + \sum_{e\in\mathcal{E}_h} h_e\|\rho_J(u_h)\|_{L^2(e)}^2\Bigg]^{1/2}}_{\displaystyle{\mathcal{R}(u_h)}} \\
&\quad\times\Bigg[\sum_{T\in\mathcal{T}_h} |v|_{H^1(\Patch T)}^2 + \sum_{e\in\mathcal{E}_h} \|v\|_{H^1(\Patch e)}^2\Bigg]^{1/2}\\
&\le C\mathcal{R}(u_h) \|v\|_{H^1(\Omega)}.
\end{align*}
Az $a(\ph,\ph)$ bilineáris forma koercivitását felhasználva kapjuk a kívánt egyenlőtlenséget.
\end{proof}
A globális $\mathcal{R}$ mennyiséget kétféleképpen meg lehet adni ,,lokális hibabecslők'' segítségével. Egyrészt az ,,élenkénti'' lokális hibabecslőkön,
\[
\boxed{\epsilon_e(u_h)^2=\sum_{T\in\Patch(e)} h^2_T \|R_{AC}(u_h)\|^2_{L^2(T)} + h_e\|\rho_J(u_h)\|_{L^2(e)}^2}
\]
kereszül
\[
\|e_h\|_{H^1(\Omega)}\le C\mathcal{R}(u_h)\le C\Bigg[\sum_{e\in\mathcal{E}_h} \epsilon_e(u_h)^2\Bigg]^{1/2},
\]
mivel többször számoltuk bizonyos háromszögekek járulékait. Másrészt az ,,elementkénti'' lokális hibabecslőkre,
\[
\boxed{\epsilon_T(u_h)^2=  h^2_T \|R_{AC}(u_h)\|^2_{L^2(T)} + \sum_{e\subset \partial T}h_e\|\rho_J(u_h)\|_{L^2(e)}^2}
\]
is hasonló becslés érvényes.

\vspace{1em}
Most belátunk egy fordított irányú \emph{globális} egyenlőtlenséget az élenkénti becslő segítségével. A számítások egyszerűsítése végett tegyük fel, hogy
tiszta Dirichlet-peremfeltétel van előírva, azaz $\Gamma_N=\emptyset$.
\begin{prop}
Tegyük fel, hogy $\fie{B}$ és $f$ szakaszonként polinomok a $\mathcal{T}_h$ rácson, amelyek fokszámai legfeljebb $r-k+2$ és $r$. Ekkor
\[
\Bigg[\sum_{e\in\mathcal{E}_h} \epsilon_e(u_h)^2\Bigg]^{1/2}\le C|e_h|_{H^1(\Omega)}
\]
\end{prop}
Az iménti globális becslést természetesen lokális becslések összerakásával fogjuk megmutatni.
A $\fie{B}$ és $f$ adatokra vonatkozó regularitási feltevés oka és haszna a következő:
az  $R_{AC}(u_h)$ abszolút folytonos reziduális konkrét alakjára visszaemlékezve,
\[
\restr{R_{AC}}{T}(u_h)=\restr{(f+\dive \fie{B}(x)Du_h)}{T}
\]
kapjuk, hogy $\restr{R_{AC}(u_h)}{T}$ legfeljebb $r$-edfokú polinom.

Most figyeljük meg, hogy minden $T\in\mathcal{T}_h$ háromszögön 
\[
\forall v\in H^1_0(T) \;:\; a(e_h,v)=\int_T R_{AC}(u_h)v
\]
más szóval a reziduálisnak csak az abszolút folytonos része számít, ha az elemek peremén eltűnő függvényeket tekintünk.
Ezért
\[
\Big|\int_T R_{AC}(u_h)v\Big|\le M |e_h|_{H^1(T)}|v|_{H^1(T)},
\]
amiből
\[
\tag{$*$}\sup_{v\in H^1_0(T)} \frac{\Big|\int_T R_{AC}(u_h)v\Big|}{|v|_{H^1(T)}} \le M|e_h|_{H^1(T)}.
\]

Az $R_J(u_h)$ ,,ugrórészre'' analóg egyenlőtlenségek érvényesek. Ehhez legyen az $e\in\mathcal{E}_h^0$ belső él rögzített, és tekintsük az $T_e^+\in\mathcal{T}_h$ és $T_e^-\in\mathcal{T}_h$
szomszédait, illetve legyen $T_e=T_e^+\cup T_e^-$. 
\begin{center}
\includegraphics[scale=0.9]{fig/te.pdf}
\end{center}
Most definiáljuk a
\[
V_e=\big\{v\in H^1_0(T_e) \;:\; \dua{v}{p}_{L^2(T_e^+)}=\dua{v}{p}_{L^2(T_e^-)}=0 \; (p\in P^r)\big\}
\]
függvényteret. Ekkor
\[
\forall v\in V_e \;:\; a(e_h,v)=\int_e \big\llbracket \fie{B}(x)Du_h\cdot\nu\big\rrbracket_e v,
\]
hiszen $R_{AC}(u_h)$ legfeljebb $r$-edfokú polinom a $T_e^+$-on és $T_e^-$-on és ezért itt ortogonális $v$-re a $V_e$ definíciója miatt.
Ebből
\[
\Big|\int_e \big\llbracket \fie{B}(x)Du_h\cdot\nu\big\rrbracket_e v\Big|\le M |e_h|_{H^1(T_e)}|v|_{H^1(T_e)},
\]
és
\[
\tag{$**$} \sup_{v\in V_e} \frac{ \Big|\int_e \big\llbracket \fie{B}(x)Du_h\cdot\nu\big\rrbracket_e v\Big|}{|v|_{H^1(T_e)}}\le M|e_h|_{H^1(T_e)}.
\]

A lokális becsléshez szükségünk van egy technikai lemmára.
\begin{lemm}
Legyen $R\subset\Real^2$ a refernciaháromszög.
\begin{enumerate}[(1)]
\item Ekkor létezik $C_r>0$ úgy, hogy bármely $p\in P^r(R)$ polinom esetén
\[
C_r\|p\|_{L^2(R)}\le \sup_{v\in H^1_0(R)} \frac{\int_R pv}{|v|_{H^1(R)}}.
\]
\item Legyen $e\subset R$ egy él, és $W_e=\{v\in H^1(R)\;:\; \restr{v}{\partial R\setminus e}=0,\; \dua{v}{p}_{L^2(R)}=0 \; (p\in P^r(R))\}$. Ekkor létezik
$C_r>0$ úgy, hogy bármely $p\in P^r(T)$ esetén
\[
C_r\|p\|_{L^2(R)}\le \sup_{v\in W_e} \frac{\int_e pv}{|v|_{H^1(R)}}.
\]
\end{enumerate}
\end{lemm}
\begin{proof}

\end{proof}

\begin{prop}
Tegyük fel, hogy $\fie{B}$ és $f$ szakaszonként polinomok a $\mathcal{T}_h$ rácson, amelyek fokszámai legfeljebb $r-k+2$ és $r$. Ekkor
\[
\epsilon_e(u_h)\le C|e_h|_{H^1(T_e)}
\]
minden $e\in\mathcal{E}_h$ esetén.
\end{prop}
\begin{proof}
A $(*)$ egyenlőtlenség és a lemma (1) része alapján
\[
h_T\Big[\int_T R_{AC}(u_h)^2\Big]^{1/2}\le C|e_h|_{H^1(T)}.
\]
Hasonlóan, a $(**)$ egyenlőtlenség és a lemma (2) része alapján (és felhasználva, hogy $\big\llbracket \fie{B}(x)Du_h\cdot\nu\big\rrbracket_e$ egy legfeljebb $(r+1)$-edfokú polinom)
\[
\sqrt{h_e}\big\|\big\llbracket \fie{B}(x)Du_h\cdot\nu\big\rrbracket_e\big\|_{L^2(e)}\le C|e_h|_{H^1(T_e)}
\]
A két egyenlőtlenséget összerakva következik az állítás.
\end{proof}

\section{Egy boborékfüggvény-alapú implicit hibabecslő}

\section{Implementáció}
A hibabecslők használhatóságát természetesen a gyakorlat dönti el. Mielőtt komoly kísérletezésbe és összehasonlításba kezdenénk 
(ami egyébként bőven meghaladja a jegyzet kereteit), gondolkozzunk el, hogy hogyan számítsuk ki az $\epsilon_e(u_h)^2$ vagy $\epsilon_T(u_h)^2$ 
lokális mennyiségeket. Emlékeztetünk, hogy a $Du_h$ deriváltat már kiszámítottuk lineáris elemek esetén ($k=1$), ezt az
\lstinline|fe2_grad| függvény elvégzi. Egyrészt az
\[
\|R_{AC}(u_h)\|_{L^2(T)}=\big\|\restr{(f+\dive \fie{B}(x)Du_h)}{T}\big\|_{L^2(T)}
\]
mennyiség meghatározásához egy legfeljebb $2r$-edfokú polinom integrálját meghatározni, valamilyen kubatúrával.
Másrészt, a
\[
\|\rho_J(u_h)\|_{L^2(e)}=\big\|\big\llbracket \fie{B}(x)Du_h\cdot\nu\big\rrbracket_e\big\|_{L^2(e)}
\]
mennyiség meghatározásához ki kell számolnunk a $Du_h$ ugrását minden $e$ élen.