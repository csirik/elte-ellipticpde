\section{Függvényegyütthatós operátorok: 1D}

Tekintsük az $\Omega=(0,a)$ intervallumon a
\[
-(k(x)u')' =f
\]
egyenletet, ahol $k\in C^1(\overline{\Omega})$ az ún. \textbf{diffúziós} (vagy \textbf{diszperziós}) \textbf{együttható}, amelyre $k(x)\ge\alpha>0$.
A diffúziós együttható az $\Omega$ tartományt kitöltő közeg inhomogenitását írja le. A stacionárius hővezetési feladatban
$k$ a hővezetési együttható. Ha elektrosztatikai feladatunk van, akkor $k$ a dielektrikum villamos permittivitása.

Először az $(k(x)u')'$ differenciáloperátor diszkretizációjával foglalkozunk. \emph{Nézzünk meg egy naív ötletet, amely nem működik!}
Vezessük be a $k_i=k(x_i)$ jelölést. Az első ötletünk az, hogy $u'$-t a backward differenciahányadossal közelítjük az $x_i$ pontban, majd kapott mennyiséget megszorozzuk
$k_i$-vel és újra alkalmazzuk a forward differenciasémát. Azt kapjuk, hogy
\[
(ku')'(x_i)\approx \frac{k_{i+1}(u_{i+1}-u_{i}) - k_i(u_i-u_{i-1})}{h^2}=\frac{k_{i+1}u_{i+1} - (k_i+k_{i+1})u_i + k_{i}u_{i-1}}{h^2}.
\]
Ez a $k\equiv 1$ esetben visszaadja a szokásos másodrendű sémát. Intuitíve, a probléma azzal lesz, hogy $k_{i}u_{i-1}$ szorzatban $k_i$ az $x_i$ pontban
van értelmezve, viszont $u_{i-1}$ az $x_{i-1}$ pontban. Ezt a tényt numerikusan is kipróbálhatjuk.

A kiút ebből a problémából a következő. A $k(x)$ függvényt egy $\frac{h}{2}$-vel eltolt rácson adjuk meg:
\[
k_{0,1}=k(x_{0,1}),\;k_{1,2}=k(x_{1,2}),\; k_{2,3}=k(x_{2,3}),\; \ldots,\; k_{N-1,N}=k(x_{N-1,N}),\; k_{N,N+1}=k(x_{N,N+1}),
\]
ahol
\[
x_{i,i+1}=x_i+\frac{h}{2}, \quad i=0,\ldots,N.
\]
Azt állítjuk, hogy érvényes a 
\begin{align*}
(pu')'(x_i)&=\frac{k_{i,i+1}(u_{i+1}-u_{i}) - k_{i-1,i}(u_i-u_{i-1})}{h^2}+\mathcal{O}(h^2)\\
&=\frac{k_{i-1,i}u_{i-1} - (k_{i-1,i}+k_{i,i+1})u_i + k_{i,i+1}u_{i+1}}{h^2}+\mathcal{O}(h^2)=:(\lapl_h^ku)_i+\mathcal{O}(h^2)
\end{align*}
reláció.
Ez a képlet ,,lokális'' és már nem szenved a naív séma hibáitól. Először emlékeztetünk a centrális differenciasémára: írjuk fel az $x_{i+1/2}$ pont körül a következő Taylor-sorokat:
\begin{align*}
u(x_i+h)&=u(x_i+\tfrac{h}{2}) + \frac{h}{2} u'(x_i+\tfrac{h}{2}) + \frac{h^2}{8} u''(x_i+\tfrac{h}{2}) + \mathcal{O}(h^3),\\
u(x_i)&=u(x_i+\tfrac{h}{2}) - \frac{h}{2} u'(x_i+\tfrac{h}{2}) + \frac{h^2}{8} u''(x_i+\tfrac{h}{2}) + \mathcal{O}(h^3),
\end{align*}
bármely $i=1,\ldots,N-1$ esetén. Ezekből kivonással és $h$-val való osztással kapjuk
\begin{equation}\label{forwcent}
\frac{u_{i+1}-u_i}{h}= u'(x_{i,i+1}) + \mathcal{O}(h^2)
\end{equation}
Teljesen hasonlóan kapható az
\begin{equation}\label{backcent}
\frac{u_i-u_{i-1}}{h}= u'(x_{i-1,i}) + \mathcal{O}(h^2)
\end{equation}
formula.

Az új séma elemzéséhez írjuk fel $ku'$ Taylor sorát az $x_{i}$ pont körül:
\begin{align*}
(ku')(x_i+\tfrac{h}{2})&=(ku')(x_i) + \frac{h}{2} (ku')'(x_i) + \frac{h^2}{8} (ku')''(x_i) + \mathcal{O}(h^3)\\
(ku')(x_i-\tfrac{h}{2})&=(ku')(x_i) - \frac{h}{2} (ku')'(x_i) + \frac{h^2}{8} (ku')''(x_i)) + \mathcal{O}(h^3)
\end{align*}
Ezekből kivonással és $h$-val való osztással kapjuk a
\[
\frac{(ku')(x_i+\tfrac{h}{2})-(ku')(x_i-\tfrac{h}{2})}{h}= (ku')'(x_i) +  \mathcal{O}(h^2)
\]
kifejezést. Most pedig írjuk be az $u'$ helyére a fenti \eqref{forwcent} és \eqref{backcent} képleteket. Azt kapjuk összességében, hogy 
\[
(\lapl_h^ku)_i=(ku')'(x_i) + \mathcal{O}(h^2).
\]
\begin{rmrk}
Gondoljuk meg, milyen simasági feltevést kell tennünk $k$-re és $u$-ra, hogy az iménti gondolatmenet működjön! Mivel
\[
(ku')'''=(k'u' + ku'')''=(k''u' + 2k'u'' + k'u''')'=k'''u' + 3k''u'' + 2k'u''' + k''u''' + k'u'''',
\]
ezért $k\in C^3$ és $u\in C^4$ elégséges.
\end{rmrk}

Előfordulhat, hogy a $k(x)$ függvényértékek csak az $x_i$ rácspontokban ismertek és az $x_{i,i+1}$-ben nem. Ekkor használhatjuk a $k_{i,i+1}=(k_i+k_{i+1})/2$
formulát a köztes értékek meghatározására.

A $-(k(x)u')'=f$ egydimenziós Poisson-egyenlet véges differenciás diszkretizációját tehát belső pontokban a 
\[
\boxed{\frac{-k_{i-1,i}u_{i-1} + (k_{i-1,i}+k_{i,i+1})u_i -k_{i,i+1}u_{i+1}}{h^2}=f(x_i)+\mathcal{O}(h^2), \quad i=2,\ldots,N-1}
\]
egyenletek adják. 

\textbf{Dirichlet-peremfeltétel.} Világos, hogy $u(0)=\alpha$, $u(a)=\beta$ Dirichlet-peremfeltételek esetén az
\begin{align*}
\frac{1}{h^2}\begin{pmatrix}
 k_{1,2}+k_{2,3} & -k_{2,3} & & & \\
-k_{2,3} & k_{2,3}+k_{3,4} & -k_{3,4} & & \\
 & \ddots & \ddots & \ddots & \\
& &  -k_{N-2,N-1} & k_{N-2,N-1}+k_{N-1,N}
\end{pmatrix}
\begin{pmatrix}
u_2\\
u_3\\
\vdots\\
u_{N-1}
\end{pmatrix}
=\begin{pmatrix}
f(x_2)+\frac{k_{1,2}\alpha}{h^2}\\
f(x_3)\\
\vdots\\
f(x_{N-1})+\frac{k_{N-1,N}\beta}{h^2}
\end{pmatrix}
\end{align*}
egyenletrendszert kapjuk. MATLAB/Octave-ban a iménti $A_h$ diszkretizációs mátrixot a
\begin{lstlisting}
hinv = (N-1)/a; hinv2 = hinv*hinv;
coeff = arrayfun( coefffunc, (0.5+(-1:N-1))/(N-1) )';
A = hinv2 * spdiags( [-coeff(3:N) coeff(3:N)+coeff(2:N-1) -coeff(2:N-1)], -1:1, N-2, N-2 );
\end{lstlisting}
utasításokkal kapjuk, ahol a \lstinline|coefffunc| egy egyváltozós függvény, amely a $k$ diffúziós állandónak felel meg.
Jegyezzük meg, hogy a \lstinline|coeff| vektor elemei 
\[
(k_{0,1},k_{1,2},k_{2,3},k_{3,4},\ldots,k_{N-2,N-1},k_{N-1,N},k_{N,N+1}),
\]
ezért a $k_{i,j}$ mennyiségeket a MATLAB/Octave-ban a $j$ indexet tartjuk számon. 

\textbf{Neumann-Dirichlet-peremfeltétel.} Tegyük fel, hogy a bal oldali perempontban $-u'(0)=\alpha$ 
Neumann-peremfeltétel van előírva, és $u(a)=\beta$. Ekkor az $x_1=0$ rácspontban a következő egyenletrendszert kapjuk
\[
\left.
\begin{aligned}
\frac{-k_{0,1}u_{0} + (k_{0,1}+k_{1,2})u_1 -k_{1,2}u_{2}}{h^2}=f(x_1)\\
\frac{u_0-u_2}{2h}=\alpha
\end{aligned}
\right\}
\]
Az utolsó egyenletből kifejezve a ghost-változót: $u_0=u_2+2h\alpha$ és behelyettesítve az elsőbe, kapjuk
\[
\frac{(k_{0,1}+k_{1,2})(u_{1}-u_{2})}{h^2}=f(x_1)+\frac{2k_{0,1}}{h}\alpha.
\]
A szimmetria megőrzéséhez a 
\[
\frac{k_{1,2}(u_{1}-u_{2})}{h^2}=\frac{k_{1,2}}{k_{0,1}+k_{1,2}}f(x_1)+\frac{2k_{0,1}k_{1,2}}{k_{0,1}+k_{1,2}}\frac{\alpha}{h}.
\]
átskálázást kell elvégezni. Összességében az
\begin{align*}
\frac{1}{h^2}\begin{pmatrix}
 k_{1,2} & -k_{1,2} & & & \\
-k_{1,2} & k_{1,2}+k_{2,3} & -k_{2,3} & & \\
 & \ddots & \ddots & \ddots & \\
 & &  -k_{N-2,N-1} & k_{N-2,N-1}+k_{N-1,N}
\end{pmatrix}
\begin{pmatrix}
u_1\\
u_2\\
\vdots\\
u_{N-1}
\end{pmatrix}
=\begin{pmatrix}
\frac{k_{1,2}}{k_{0,1}+k_{1,2}}f(x_1)+\frac{2k_{0,1}k_{1,2}}{k_{0,1}+k_{1,2}}\frac{\alpha}{h}\\
f(x_2)\\
\vdots\\
f(x_{N-1})+\frac{k_{N-1,N}\beta}{h^2}
\end{pmatrix}
\end{align*}
egyenletrendszert kapjuk. A Laplace-operátor esetét szépen visszakapjuk a $k\equiv 1$ választással. MATLAB/Octave-ban a tiszta Dirichlet-esetben generált \lstinline|A|
mátrixot a következő utasításokkal tudjuk ,,kibővíteni'' a Neumann--Dirichlet-esetre:
\begin{lstlisting}
A = [hinv2*coeff(2) -hinv2*coeff(2) zeros(1,N-3);
     [-hinv2*coeff(2); zeros(N-3,1)]  A ];
\end{lstlisting}

\textbf{Dirichlet-Neumann-peremfeltétel.} Tegyük fel, hogy $u(0)=\alpha$ és $u'(a)=\beta$ peremfeltételek vannak előírva.
Ekkor az $x_N=a$ rácspontban a következő egyenletrendszert kapjuk
\[
\left.
\begin{aligned}
\frac{-k_{N-1,N}u_{N-1} + (k_{N-1,N}+k_{N,N+1})u_{N} -k_{N,N+1}u_{N+1}}{h^2}=f(x_N)\\
\frac{u_{N+1}-u_{N-1}}{2h}=\beta
\end{aligned}
\right\}
\]
Az utolsó egyenletből kifejezve a ghost-változót: $u_{N+1}=u_{N-1}+2h\beta$ és behelyettesítve az elsőbe, kapjuk
\[
\frac{(k_{N,N+1}+k_{N-1,N})(u_{N}-u_{N-1})}{h^2}=f(x_{N-1}) + \frac{2k_{N,N+1}}{h}\beta.
\]
Szimmetrizálással
\[
\frac{k_{N-1,N}(u_{N-1}-u_{N-2})}{h^2}=\frac{k_{N-1,N}}{k_{N-1,N}+k_{N,N+1}}f(x_{N-1}) + \frac{2k_{N-1,N} k_{N,N+1}}{k_{N-1,N}+k_{N,N+1}} \frac{\beta}{h}.
\]
Összességében az
\begin{align*}
\frac{1}{h^2}\begin{pmatrix}
 k_{1,2}+k_{2,3} & -k_{2,3} & & & \\
 \ddots & \ddots & \ddots &  \\
 &  -k_{N-2,N-1} & k_{N-2,N-1}+k_{N-1,N} &-k_{N-1,N}\\
 & &  -k_{N-1,N} & k_{N-1,N}
\end{pmatrix}
\begin{pmatrix}
u_2\\
\vdots\\
u_{N-1}\\
u_{N}
\end{pmatrix}
=\\\begin{pmatrix}
f(x_2)+\frac{k_{1,2}\alpha}{h^2}\\
\vdots\\
f(x_{N-1})\\
\frac{k_{N-1,N}}{k_{N-1,N}+k_{N,N+1}}f(x_{N-1}) + \frac{2k_{N-1,N} k_{N,N+1}}{k_{N-1,N}+k_{N,N+1}} \frac{\beta}{h}
\end{pmatrix}
\end{align*}
egyenletrendszert kapjuk.  MATLAB/Octave-ban a tiszta Dirichlet-esetben generált \lstinline|A|
mátrixot a következő utasításokkal tudjuk ,,kibővíteni'' a Dirichlet--Neumann-esetre:
\begin{lstlisting}
A = [ A [zeros(N-3,1); -hinv2*coeff(N)]; ...
zeros(1,N-3) -hinv2*coeff(N) hinv2*coeff(N) ];
\end{lstlisting}

A fennmaradó \textbf{Neumann--Neumann-eset} meggondolását az olvasóra bízzuk. A diszkretizációs mátrixot és a jobb oldali vektort az \lstinline|fd1_lapl_var|
 függvénnyel tudjük összeépíteni. \Az{\ref{fig:fd1var}}. ábra egy tesztfeladat futási eredményeit tartalmazza.

\begin{figure}
\centering
\subfigure{\includegraphics[scale=0.45]{fig/fd1_var.pdf}}\hspace{3ex} 
\subfigure{
\begin{tabular}[b]{|c|c|c|}                                                                                                                                                                                
\hline                                                                                                                                                                                                  
$N$ & $h$ & $\|u_h-\overline{u}_h\|_\infty$ \\                                                                                                                                                          
\hline                                                                                                                                                                                                  
4 & 0.333333 & 3.9389e-02 \\                                                                                                                                                                            
\hline                                                                                                                                                                                                  
8 & 0.142857 & 7.3179e-03 \\                                                                                                                                                                            
\hline                                                                                                                                                                                                  
16 & 0.066667 & 1.5969e-03 \\                                                                                                                                                                           
\hline                                                                                                                                                                                                  
32 & 0.032258 & 3.7404e-04 \\                                                                                                                                                                           
\hline                                                                                                                                                                                                  
64 & 0.015873 & 9.0574e-05 \\                                                                                                                                                                           
\hline                                                                                                                                                                                                  
128 & 0.007874 & 2.2289e-05 \\                                                                                                                                                                          
\hline                                                                                                                                                                                                  
256 & 0.003922 & 5.5286e-06 \\                                                                                                                                                                          
\hline                                                                                                                                                                                                  
512 & 0.001957 & 1.3767e-06 \\                                                                                                                                                                          
\hline                                                                                                                                                                                                  
1024 & 0.000978 & 3.4351e-07 \\                                                                                                                                                                         
\hline                                                                                                                                                                                                  
2048 & 0.000489 & 8.5784e-08 \\                                                                                                                                                                         
\hline                                                                                                                                                                                                  
\end{tabular}}                                                                                                                                                                                           
\caption{A $-((1+x^2)u')'=-e^x(1+x)^2$, $-u'(0)=-1$, $u(1)=e$ Neumann-Dirichlet-peremértékfeladat diszkretizációjának abszolút hibája (Pontos megoldás: $e^x$). 
A rendbecslés eredménye $\alpha=1.9967\approx 2$. (Generálta: \lstinline|test_fd1_var.m|)}
\label{fig:fd1var}                                                                                                                                                                                    
\end{figure} 