
\section{Nem-ekvidisztáns rácsok: Quadtree*}

Említettük, hogy a véges differencia-módszer gyakorlati alkalmazásakor általában valamilyen nem-ekvidisztáns rácsot alkalmaznak: a tartomány bizonyos részein nagyobb felbontású
rácsra van szükség (pl. ott nagy a hiba), míg máshol elég egy durvább rács is (pl. ott tudjuk, hogy nagyjából konstans lesz a megoldás).
\Az{\ref{nonequi1d}}. szakaszban az egydimenziós esetet már kezeltük, és itt egy lehetséges módszert mutatunk be a kétdimenziós esetre.
Egy kétdimenziós nem-ekvidisztáns rács olyan, amit nem feltétlenül lehet megkapni egydimenziós rácsok Descartes-szorzataként. 
Világos, hogy a rács megválasztásában rejlő szabadságunkkal egyúttal felmerül a rács \emph{generálásának} problémája is.

A legegyszerűbb stratégia a rácsgenerálásra az ún. \emph{quadtree}. Ez egy olyan $k$-fa (a bináris fa általánosítása), amelynek \emph{levelei} a cellák. 
A quadtree reguláris, ha egy adott cella oldala legfeljebb 2 cellával lehet szomszédos.
Ilyen rácsot generál az \lstinline{fd2_gen_qt} program. 

Hogyan lehet a deriváltakat közelíteni nem-ekvidisztáns rácson? Itt a Min et al.\footnote{Chohong Min, Frédéric Gibou, Hector D. Ceniceros. \emph{A supra-convergent finite difference scheme for the variable coefficient Poisson equation on non-graded grids.} (2006)} által kijelölt módszert fogjuk tárgyalni és implementálni.
\begin{figure}
\begin{center}
\includegraphics[scale=3.7]{fig/quadlapl.pdf}
\end{center}
\caption{A $p_1$ pontban felírt differenciacsillag.}
\label{quadlapl}
\end{figure}
\Az{\ref{quadlapl}}. ábrán látható elrendezésben fogjuk meghatározni a $p_1$ ponthoz tartozó $u_1$
változóra vonatkozó differenciacsillagot. Az $u_{56}$ mennyiséget lineáris interpolációval közelítjuk a
\[
p_{56}=\frac{t_6p_5 + t_5p_6}{t_{56}}
\]
pontban az
\begin{equation}\label{u56}
u_{56}=\frac{t_6u_5 + t_5u_6}{t_{56}}
\end{equation}
képlet szerint, ahol $t_{56}=t_5+t_6$ jelölést használtuk. Először határozzuk meg az $u_{56}$ közelítés hibáját.
Ehhez írjuk fel az $p_1$ pontból az $p_5$, $p_6$ és $p_{56}$ pontokhoz tartozó Taylor-sorokat:
\begin{align*}
u_5&=u(p_1) - t_{56} u_x(p_1) + t_5u_y(p_1) + \frac{t_{56}^2}{2} u_{xx}(p_1) - t_{56}t_5u_{xy}(p_1) + \frac{t_5^2}{2} u_{yy}(p_1) + \mathcal{O}(h^3)\\
u_6&=u(p_1) - t_{56} u_x(p_1) - t_6u_y(p_1) + \frac{t_{56}^2}{2} u_{xx}(p_1) + t_{56}t_6u_{xy}(p_1) + \frac{t_6^2}{2} u_{yy}(p_1) + \mathcal{O}(h^3)\\
u(x_{56})&=u(x_1) - t_{56} u_x(p_1) + \frac{t_{56}^2}{2} u_{xx}(p_1) + \mathcal{O}(h^3)
\end{align*}
Képezve az első két egyenlet konvex kombinációját \az{\eqref{u56}} képlet szerint és levonva ezt a harmadik egyenletből, 
\begin{equation}\label{u56err}
u(p_{56})=u_{56} - \frac{t_5t_6}{2} u_{yy}(p_1) + \mathcal{O}(h^2)
\end{equation}
A vízszintes tengely irányában, az $p_{56}$, $p_1$, $p_2$ pontokban felírva az egydimenziós nem-ekvidisztáns \eqref{nulapl} formulát és beleírva \az{\eqref{u56err}}
formulát,
\[
\frac{2}{t_{56}+t_2}\Big( \frac{u_{56}-u_1}{t_{56}} - \frac{u_1-u_2}{t_2} \Big)=u_{xx}(p_1) + \frac{t_5t_6}{t_{56}(t_{56}+t_2)}u_{yy}(p_1) + \mathcal{O}(h^2)
\]
A függőleges tengely irányában az $p_3$, $p_1$, $p_6$ pontokban \az{\eqref{nulapl}} formula az
\[
\frac{2}{t_3+t_4}\Big( \frac{u_3-u_1}{t_3} - \frac{u_1-u_6}{t_4} \Big)=u_{xx}(p_1) + \mathcal{O}(h^2)
\]
eredményt adja. Ezzel az 
\begin{align*}
\lapl u(p_1)=\frac{2}{t_{56}+t_2}\Big( \frac{u_{56}-u_1}{t_{56}} - \frac{u_1-u_2}{t_2} \Big)+\alpha\frac{2}{t_3+t_4}\Big( \frac{u_3-u_1}{t_3} - \frac{u_1-u_6}{t_4} \Big) +  \mathcal{O}(h^2)
\end{align*}
eredményt kapjuk, ahol
\[
\alpha=1-\frac{t_5t_6}{t_{56}(t_{56}+t_2)}.
\]
Az $\alpha$-val való szorzás nem rontja le a nagyságrendet, mivel $\alpha=\mathcal{O}(1)$.

\Az{\ref{quadlapl}}. ábrán látható elrendezés $90^\circ$-os elforgatásával kapjuk egy másik konfigurációt, amelyben az $x$-tengely mentén kell az interpolációt végrehajtani.
A quadtree kontrukciója miatt az nem fordulhat elő, hogy mindkét tengely irányában interpolálni kelljen, hiszen ekkor ,,lógna a levegőben'' a csúcs, azaz csak két szomszédja lenne.

\begin{figure}
\begin{center}
\includegraphics[scale=0.5]{fig/quadtree.eps}
\end{center}
\caption{Egy, az \lstinline{fd2_genqt} program által generált reguláris quadtree. Itt \texttt{depthfun}-t a $\{0.1<|x|<0.25\}$ körgyűrűlap karakterisztikus függvényének választottuk.}
\end{figure}

\textbf{Implementáció.}