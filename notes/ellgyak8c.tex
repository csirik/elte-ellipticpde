
\section{Háromszögrácsok generálása}

A végeselem-módszer gyakorlati hatékonysága erősen függ a $\mathcal{T}_h$ háromszögrács geometriai tulajdonságaitól.
\begin{enumerate}[(i)]
\item Láttuk, hogy $A^h$ elemeinek kiszámításához szükség van a háromszögek területeire. Ezért az $A^h$ merevségi mátrix
egy kis háromszöghöz tartozó elemei nagyon kicsik lehetnek, ami numerikus instabilitáshoz vezethet. 
Ha a rácsgeneráló program sok ``vékony'' háromszöget generál, akkor azt mondjuk, hogy a rács rossz minőségű. Az ilyen
rácsok tulajdonképpen használhatatlanok.
\item A \textbf{diszkrét maximumelv} teljesülése egy alapvető elvárás egy $u$ közelítő megoldástól: ha $f\le 0$ m.m. $\Omega$-n,
akkor 
\[
\max_{\overline{\Omega}} u=\max_{\partial\Omega} u^+.
\]
Érdekes módon a diszkrét maximumelv teljesülése csak speciális rácsokon és elsőfokú elemek esetén várható. A rácsra tett feltevés
az ún. \textbf{nemtompaszög-feltétel}: minden háromszög belső szöge legfeljebb $\pi/2$ lehet. Ha tehát szeretnénk,
hogy a megoldás teljesítse a diszkrét maximumelvet, akkor gondoskodnunk kell a háromszögek megfelelő felosztásáról.
\footnote{Negatív eredményekért lásd: Andrei Draganescu, Todd F. Dupont and L. Ridgway Scott, \emph{Failure of the discrete maximum principle for an elliptic finite element problem.}}
\item Az ún. \textbf{\emph{a posteriori} hibabecslők} a közelítő megoldás számítása után megoldanak egy segédfeladatot, amely
eredményéből lehet következtetni a lokális hiba mértékére. Egy \textbf{adaptív eljárás} ezután a nagy hibájú helyeken finomítja (pl. negyedeli a háromszögeket) a rácsot,
és ezen oldja meg az eredeti feladatot. Az algoritmus ismétlődik a kívánt hibanagyságrend eléréséig. 
Egy ``rossz minőségű'' háromszög a finomítások során még rosszabb lesz, és a megnövekedett számítási igény ellenére rosszabb eredményt is kaphatunk.
Egy jól finomítható rácsnak egyenletesen  ``kövér'' elemeket kell tartalmaznia. Emlékeztetünk, hogy hasonló feltétel már szerepelt az előadáson, lásd 3.32 Állítás a jegyzetben,
ahol megköveteltük a $\mathcal{T}_h$ rácstól, hogy
\[
\forall T_k\in\mathcal{T}_h \;:\; \frac{h_k^2}{|T_k|}\le C,
\]
ahol $h_k=\diam(T_k)$ és a $C>0$ konstans nem függ $k$-től. Ezt a feltételt kihasználtuk a globális interpolációs operátor hibájának becsléséhez.
\end{enumerate}


Bizonyos egyszerű geometriák esetében mi magunk is tudunk háromszögrácsot generálni, lásd pl. a 
\lstinline|mesh_unitri| és \lstinline|mesh_tritri| amely az alábbi rácsokat generálja.
\begin{center}
\includegraphics[scale=0.4]{fig/unitri.pdf}
\includegraphics[scale=0.4]{fig/tritri.pdf}
\end{center}

Jópár szabad háromszög-, és tetraéderrács-generáló programcsomag található az interneten. 
Mi a \lstinline|mesh2d| MATLAB csomagot fogjuk itt bemutatni,
amelyet Darren Engwirda írt. A csomagot az \lstinline|addpath('mesh2d')| (ahol az idézőjelek között a csomag elérési 
útvonala szerepel) utasítással tudjuk betölteni. Ennek fő programja
\begin{lstlisting}
[p,t] = mesh2d(node,edge,hdata,options);
\end{lstlisting}
Ez a program a \lstinline|node| $N\times 2$-es mátrix által meghatározott pontok konvex burkának készíti el a triangulációját, ha \lstinline|edge=[]| egyébként
pedig az \lstinline|edge| egy $M\times 2$-es éllistája egy tetszőleges sokszögnek, amely a \lstinline|node| soraira mutat. A \lstinline|hdata|
struktúrában szereplő \lstinline|hdata.hmax| segítségével megadható a legnagyobb elemméret. A kimeneti \lstinline|p| és \lstinline|t| változók a háromszögrácsot tartalmazzák.
Egyszerű példa gyanánt tekintsük az egységnégyzet rácsozását:
\begin{lstlisting}
node       = [ 0 0; 1 0; 1 1; 0 1 ];
hdata      = [];
hdata.hmax = 0.1;
[verts,tris] = mesh2d(node, [], hdata);
\end{lstlisting}
\begin{center}
\includegraphics[scale=0.6]{fig/012.pdf}
\includegraphics[scale=0.6]{fig/b01.pdf}
\end{center}
További példa az egységkörlap:
\begin{lstlisting}
dtheta     = pi/10;
theta      = (-pi:dtheta:(pi-dtheta))';
node       = [cos(theta) sin(theta)];
hdata      = [];
hdata.hmax = 0.5;
[verts,tris] = mesh2d(node, [], hdata);
\end{lstlisting}

Elsőfokú esetben a merevségi mátrix és a terhelési vektor összefűzéséhez elégséges a \verb!p!
 és \verb!t!
tömb -- ezeket mi a kódban rendre \verb!verts!-nek és \verb!tris!-nek neveztük.
A később tárgyalt másodfokú $T_6$ elemeken azonban az \emph{éleken} is vannak szabadsági fokok. Az éllistát, és
más topológiai információkat a 
\[
\verb![edges, triedges, neigh, boundary] = connectivity(p, t);!
\]
programmal lehet lekérdezni. Ezek pontos jelentését a megfelelő \verb!.m! fájlban találjuk.

\section{Plottolás}
Az $u$ megoldás háromszögrács-felületként való kirajzolását a 
\begin{lstlisting}
trisurf(tris, verts(:,1), verts(:,2), U)
\end{lstlisting}
utasítással lehet elvégezni. Gyakran hasznosabb az ún. kontúr-plot, amelyet a 
\begin{lstlisting}
tricontour(verts, tris, U, 20)
\end{lstlisting}
külső függvénnyel lehet kérni. Az alábbi két ábra mutatja az $u(x,y)=(x-0.5)^2+(y-0.5)^2$
függvény különböző plotjait az egységnégyzeten.
\begin{center}
\includegraphics[scale=1.4]{fig/trisurf.pdf}
\includegraphics[scale=0.6]{fig/cont.pdf}
\end{center}


\section{Vegyes peremfeltétel}\label{mixedfem}

Vegyes peremfeltételről akkor beszélünk amikor a $\partial\Omega$ perem két diszjunkt, pozitív $(n-1)$-dimenziós Lebesgue-mértékű
$\Gamma_D$ és $\Gamma_N$ relatív nyílt részhalmazán rendre Dirichlet-, és Neumann-peremfeltétel van előírva. Érdekes módon a \emph{tiszta}
Neumann-peremfeltétel esete (azaz $\Gamma_N=\partial\Omega$) külön tárgyalást igényel, lásd a következő szakaszt.

Vezessük be $H^1(\Omega)$ azon alterét, amelyben azok a függvények találhatóak, amelyek a $\Gamma_D$-n (nyom értelemben) eltűnnek:
\[
H^1_D(\Omega)=\Big\{u\in H^1(\Omega) \;:\; \restr{u}{\Gamma_D}=0\Big\}.
\]
Ekkor, a 
\[
\left.
\begin{aligned}
-\lapl u &= f\;\;\text{($\Omega$-n)}\\
u &= 0\;\;\;\text{($\Gamma_D$-n)}\\
D_\nu u &= h\;\;\;\text{($\Gamma_N$-en)}
\end{aligned}
\right\}
\]
\emph{homogén Dirichlet-peremfeltételű} peremértékfeladat gyenge alakja könnyen láthatóan
\[
\forall v\in H^1_D(\Omega) \;:\; \underbrace{\int_\Omega Du\cdot Dv}_{\displaystyle{a(u,v)}}=\underbrace{\int_\Omega fv + \int_{\Gamma_N}hv,}_{\displaystyle{\ell(v)}}
\]
ahol $u\in H^1_D(\Omega)$. Az $\ell$ lineáris funkcionál folytonos, elvégre
\begin{align*}
|\ell(v)|&\le \|f\|_{L^1(\Omega)}\|v\|_{L^2(\Omega)}+\|h\|_{L^2(\Gamma_N)}\|v\|_{L^2(\partial\Omega)}\\
&\le C_\Omega\|f\|_{L^1(\Omega)}\|v\|_{H^1(\Omega)}+C_\Omega\|h\|_{L^2(\Gamma_N)}\|v\|_{H^1(\Omega)}\\
&\le C\|v\|_{H^1(\Omega)}
\end{align*}
a Szoboljev-, és a nyom-egyenlőtlenségek miatt. Ha $u=g$ ($\Gamma_D$-n) \emph{inhomogén Dirichlet-peremfeltételt} írunk elő, akkor
a már megismert homogenizálási eljárással visszavezetjük a problémát a homogén peremfeltétel esetére.

\subsection{A peremintegrál kiértékelése} 
Most koncentráljunk a gyenge alak jobb oldalán álló
\[
\int_{\Gamma_N} h(\bm{\xi})v(\bm{\xi})\,d\bm{\xi}
\]
peremintegrálra. Legyen $\fie{x}\in \Omega_h^\partial$, azaz $\fie{x}$ egy peremen lévő csúcs. Ekkor lineáris Courant-háromszögek esetén az
\[
\int_{\mathcal{E}(\fie{x})\cap\partial\Omega} h(\bm{\xi})\phi_{\fie{x}}(\bm{\xi})\,d\bm{\xi}
\]
integrált kell meghatároznunk, ahol $\mathcal{E}(\fie{x})$ jelöli az $\fie{x}$-re illeszkedő \emph{perem}élek halmazát. Nyilván $\mathcal{E}(\fie{x})$ vagy két élből áll, vagy egyből
(izolált pontra nem lehet előírni peremfeltételt -- lásd a szakasz eleji feltételeket). Elegendő azzal az esettel foglalkozni amikor csak egy élen kell integrálni,
elvégre két él esetében a két járulékot csak össze kell adni. Legyen tehát $e=[\fie{y},\fie{x}]$ a szóban forgó él, ahol $\fie{y}\in \Omega_h^\partial$. Ekkor
a $\bm{\xi}=\fie{y}+t(\fie{x}-\fie{y})$, $d\bm{\xi}=|\fie{x}-\fie{y}|\,dt$ helyettesítést alkalmazva
\[
\int_e h(\bm{\xi})\phi_{\fie{x}}(\bm{\xi})\,d\bm{\xi}=|\fie{x}-\fie{y}|\int_0^1 h(\fie{y}+t(\fie{x}-\fie{y}))\phi_{\fie{x}}(\fie{y}+t(\fie{x}-\fie{y}))\,dt.
\]
Az utóbbi integrál közelítésére pedig valamilyen kvadradtúraformulát alkalmazunk, pl. a \textbf{középponti szabállyal}
\[
\approx \frac{1}{2}|\fie{x}-\fie{y}| h\Big(\frac{\fie{x}+\fie{y}}{2}\Big),
\]
ahol az $1/2$ szorzótényező a $\phi_{\fie{x}}((\fie{x}+\fie{y})/2)=1/2$ triviális egyenlőségből származik.


\subsection{Implementáció}
 A vegyes peremfeltétel megvalósítása néhány technikai jellegű problémát vet fel. \Az{\ref{purediriimpl}}. szakaszban az éleket az \verb!edgemarker!
tömbbel tudjuk ``jelöltük meg'' (marker) -- \verb!0! belső élet jelent, \verb!1! Dirichlet-, és \verb!2! Neumann-peremfeltételt --
ott azonban minden peremél Dirichlet-típusú volt.

Az alábbi ábrán vastagon kiemelve láthatóak a peremélek és azok típusai a $B(0,2)\setdif \overline{B}(0,1)$ tartomány egy háromszögesítésén.
\begin{center}
\includegraphics[scale=0.7]{fig/bdmark.pdf}
\end{center}
Ezt az elrendezést a \lstinline|markbdedges| függvény ismételt alkalmazásával tudjuk megvalósítani:
\begin{lstlisting}
edgemarker = [];
% Minden peremelet megjelolunk Neumann-nak:
edgemarker = markbdedges( verts, edges, trineigh, edgemarker, [], [], 2 );
% Aztan atjeloljuk a [-1,1]x[-1,1] negyzetben levoket Dirichlet-nek    
xv = [-1  1 1 -1 -1];
yv = [-1 -1 1  1 -1];   
edgemarker = markbdedges( verts, edges, trineigh, edgemarker, xv, yv, 1 );
\end{lstlisting}
Természetesen bonyolultabb geometriák peremének szelektív megjelöléséhez kifinomultabb eljárásra van szükségünk. 
Ipari programcsomagok (AutoCAD, COMSOL, ANSYS, stb.) a grafikus felületükön teszik lehetővé a peremfeltételek megadását.
A MATLAB PDE Toolkit-ben a \lstinline|pdetool| hasonló (bár igen kezdetleges) felületet biztosít.

\Az{\ref{purediriimpl}}. szakaszban kilistázott \lstinline|fe2_boundary_diri_only| programot fogjuk kiegészíteni a következő kódrészlettel:
\begin{lstlisting}
% Neumann-tipusu elek es csucsok
neuedges = find(edgemarker == 2);
neuverts = edges(neuedges,:);
% Elek mint vektorok
neuedgevecs = verts(edges(neuedges,1), :) - verts(edges(neuedges,2), :);
% Elek hosszai
neuedgelens = sqrt( dot( neuedgevecs, neuedgevecs, 2 ) );
% Elek felezopontjai
neuedgemid = 0.5*( verts(edges(neuedges,1), :) + verts(edges(neuedges,2), :) );
% Az Neumann-adatok kiertekelese az elfelezokben
neufuncmid = arrayfun( neufunc, neuedgemid(:,1), neuedgemid(:,2) );
% A terhelesi vektor frissitese csúcsonkénti összefűzéssel
F = F + accumarray( ...
    [neuverts(:) ones( 2 * size( neuverts, 1 ), 1 )], ...
    repmat( 0.5 * neuedgelens .* neufuncmid, 2, 1 ), ...
    [nv 1] );
\end{lstlisting}
A program utolsó utasítása a lényeges. A \lstinline|repmat| függvény megismétli az első argumentumában megadott oszlopvektort, így egy kétszer akkora dimenziójú vektort kapunk.


\section{Tiszta Neumann-peremfeltétel*}
A véges differenciáknál szőnyeg alá söpörtük azt a tényt, hogy az $A^h$ mátrix nem invertálható, ha tiszta Neumann-peremfeltételt írunk elő a Poisson-feladathoz:
\[
\left.
\begin{aligned}
-\lapl u &= f\;\;\text{($\Omega$-n)}\\
D_\nu u &= h\;\;\;\text{($\partial\Omega$-n)}
\end{aligned}
\right\}
\]
elvégre a konstans rácsfüggvényeket $A^h$ nullába viszi.
A végeselem-módszer elegáns kontextust biztosít a Galjorkin-diszkretizációval kapott szinguláris lineáris egyenletrendszer vizsgálatához és megoldásához.
Meglepő, hogy mennyire nem triviális a tiszta Neumann-peremfeltétel kezelése.

Most az alaptér legyen 
\[
\mathring{H}^1(\Omega)=\Bigg\{u\in H^1(\Omega) \;:\; \int_\Omega u=0\Bigg\},
\] és a gyenge alak
\[
\forall v\in \mathring{H}^1(\Omega) \;:\; \underbrace{\int_\Omega Du\cdot Dv}_{\displaystyle{a(u,v)}}=\underbrace{\int_\Omega fv + \int_{\partial\Omega} hv}_{\displaystyle{\ell(v)}}
\]
ahol $u\in \mathring{H}^1(\Omega)$. Láttuk azt is, hogy a Green-formulát alkalmazva egy klasszikus $u\in C^2(\Omega)\cap C^1(\partial\Omega)$ megoldásra,
\emph{szükséges}, hogy az $f$ ``bemenő adatokra''
\[
\int_\Omega f=-\int_{\partial\Omega}h
\]
teljesüljön. A Poincaré-egyenlőtlenség alapján világos, hogy $a(\ph,\ph)$ \emph{koercív} $\mathring{H}^1(\Omega)$-n, és az előző szakaszban láttuk, hogy $\ell$ folytonos,
ezért a Lax--Milgram-lemmát alkalmazva kapjuk, hogy egyértelműen létezik megoldása a variációs feladatnak.

Rögtön a végeselem-recept alkalmazásának első lépése nem világos: milyen bázist válasszunk $\mathring{H}^1(\Omega)$ egy véges dimenziós alterében?
Most nem ezt az utat fogjuk követni, hanem a variációs feladatot fogjuk átértelmezni és visszatérni a $H^1(\Omega)$ térre.
Világos, hogy most $1\in H^1(\Omega)$ és $a(1,1)=0$, így $a(\ph,\ph)$ \emph{nem koercív} $H^1(\Omega)$-n.

A kiindulópontunk az előadáson már látott energiaminimalizálási feladat:
\[
\min_{v\in H^1(\Omega)} E(v)=E(u),
\]
ahol $u\in H^1(\Omega)$,
\[
E(v)=\frac{1}{2}a(v,v)-\ell(v)
\]
az energiafunkcionál. Láttuk, hogy a minimalizáló $u$-t karakterizálja a
\[
\forall v\in H^1(\Omega) \;:\; a(u,v)=\ell(v)
\]
gyenge alak. Az $E(v)$ funkcionál helyett tekintsük az
\[
E_\rho(v)=E(v) + \frac{\rho}{2}\Big(\int_\Omega v\Big)^2
\]
\textbf{relaxált energiafunkcionált}. A $\rho>0$ szám neve \textbf{relaxációs-}, vagy \textbf{büntetőparaméter}.
Heurisztikusan, ha úgy oldjuk meg a $\min E_\rho(v)$ feladatsorozatot, hogy közben $\rho\to\infty$, akkor a minimalizáló $u$-ra
a kívánt $\int_\Omega u=0$ kényszer is teljesülni fog.

Világos, hogy ha éppenséggel $\int_\Omega u=0$, akkor $a_\rho(u,v)=a(u,v)$ minden $v\in H^1(\Omega)$. Nem nehéz azt sem belátni, hogy a
\[
\forall v\in H^1(\Omega) \;:\; a_\rho(u,v)=\ell(v)\tag*{$(*)$}
\]
feladatnak egyértelműen létezik megoldása, ami abból következik, hogy $a_\rho(\ph,\ph)$ már koercív $H^1(\Omega)$-ban.


Most készítsük el a $(*)$ feladat végeselem-diszkretizációját $T_3$ Courant-elemek segítségével. Ha
szokásos módon
\[
V_h=\Span\{\phi_i\}_{i=1}^m\subset H^1(\Omega),
\]
akkor a $(*)$ merevségi mátrixa
\[
(A^{h,\rho})_{ij}=a_\rho(\phi_j,\phi_i)=a(\phi_j,\phi_i) + \rho\Big(\int_\Omega \phi_i\Big)\Big(\int_\Omega \phi_j\Big),
\]
azaz, bevezetve az $w\in \Real^m$ jelölést,
\[
w_i=\int_\Omega \phi_i,
\]
azt írhatjuk, hogy
\[
A^{h,\rho}=A^h+ww^\trans.
\]
Mármost $A^{h,\rho}$ szimmetrikus és pozitív definit, elvégre az általa generált $(\xi,\zeta)\mapsto (A^{h,\rho}\xi,\zeta)$ bilineáris forma szimmetrikus és koercív $a_\rho(\ph,\ph)$ bilineáris forma
 $V_h\times V_h\subset H^1(\Omega)\times H^1(\Omega)$ altérre való megszorítása.

\Exercises

\begin{Exercise}
Bizonyítsuk be, hogy pontosan akkor teljesül a nemtompaszög-feltétel egy $T$ háromszögön, ha $D\lambda_x\cdot D\lambda_{x'}\le 0$ minden $x\neq x'$ esetén.
\end{Exercise}

\begin{Exercise}
Legyen $\Omega=(0,\pi)^2$, $\Gamma_D=\{0,\pi\}\times(0,\pi)$ és $\Gamma_N=(0,\pi)\times \{0,\pi\}$. Oldjuk meg a 
\[
\left.
\begin{aligned}
-\lapl u &=\lambda u\;\;\text{($\Omega$-n)}\\
u &= 0\;\;\;\text{($\Gamma_D$-n)}\\
D_\nu u &= 0\;\;\;\text{($\Gamma_N$-en)}
\end{aligned}
\right\}
\]
sajátértékfeladatot. [Segítség: azt kell kapnunk, hogy
\[
\forall k_1,k_2\in\Nat \;:\; \lambda_{k_1,k_2}=k_1^2+k_2^2, \quad u_{k_1,k_2}(x_1,x_2)=\sin(k_1x_1)\cos(k_2x_2).]
\]
\end{Exercise}

\begin{Exercise}
Legyen $\Omega=B(0,r_2)\setdif \overline{B}(0,r_1)$, ahol $r_1<r_2$, és oldjuk meg a 
\[
\left.
\begin{aligned}
\lapl u &=0\;\;\;\;\text{($\Omega$-n)}\\
u &= c_1\;\;\;\text{($\partial B(0,r_1)$-n)}\\
D_\nu u &= c_2\;\;\;\text{($\partial B(0,r_2)$-en)}
\end{aligned}
\right\}
\]
peremértékfeladatot. [Segítség: Használjuk a Laplace-operátor polárkoordinákban felírt alakját. Azt kell kapnunk, hogy
\[
u(r)=c_2r_2\log r + c_1-c_2r_2\log r_1.]
\]
\end{Exercise}

\begin{Exercise}[difficulty=1]
Lássuk be, hogy $E_\rho$ Gateaux-deriváltja egy $u\in H^1(\Omega)$ pontban
\[
\forall v\in H^1(\Omega) \;:\; D_vE_\rho(u)=\underbrace{a(u,v)+\rho\Big(\int_\Omega u\Big)\Big(\int_\Omega v\Big)}_{\displaystyle{a_\rho(u,v)}}-\ell(v).
\]
\end{Exercise}

\begin{Exercise}[difficulty=2]
Bizonyítsuk be, hogy $a_\rho(\ph,\ph)$ koercív $H^1(\Omega)$-ban.
\end{Exercise}

\begin{Exercise}[difficulty=1]
Gondoljunk bele, hogy a $W=ww^\trans$ 1-rangú (vagy tenzorszorzat) mátrixszal való szorzás nagyon gyorsan elvégezhető, még ha a dimenzió nagy is. Továbbá
\begin{enumerate}[(i)]\indented
\item Ha $\|w\|=1$, akkor $W$ ortogonális projekció a $\Span\{w\}$ 1-dimenziós altérre.
\item $W$ pozitív definit.
\end{enumerate}
\end{Exercise}

\begin{Exercise}
Lássuk be, hogy ha $a(\ph,\ph)$ koercív $X$-en, akkor tetszőleges, folytonosan beágyazott $Y\subset X$ altéren is.
\end{Exercise}
