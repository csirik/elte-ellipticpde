\documentclass[a4paper,12pt]{book}

\usepackage[a4paper, margin=1in]{geometry}
\usepackage{subfigure}

\usepackage{hyperref}
\usepackage{eltefont}
\usepackage{eltehun}
\usepackage{eltesymb}
\usepackage{eltethmhun}

\usepackage{ascii}

%%
%% Listings
%% 
\usepackage[framed,autolinebreaks]{mcode}
\lstdefinestyle{myCustomMatlabStyle}{
  language=Matlab,
  aboveskip=10pt,
  tabsize=4,
  showspaces=false,
  showstringspaces=false
}
\lstset{basicstyle=\normalsize\ttfamily,literate={~} {$\sim$}{1},style=myCustomMatlabStyle}



%%
%% Debug
%% 
\usepackage[yyyymmdd,hhmmss]{datetime}
\usepackage[all]{background}

\SetBgContents{\texttt{\today\ \currenttime}}% Set contents
\SetBgPosition{-0.6cm,-6.0cm}% Select location
\SetBgOpacity{1.0}% Select opacity
\SetBgAngle{90.0}% Select rotation of logo
\SetBgScale{2.0}% Select scale factor of logo

\usepackage{showframe}
\overfullrule=1mm
\usepackage{lineno}
\linenumbers

%%
%% END Debug
%% 

\usepackage{algorithmic}
\renewcommand\algorithmiccomment[1]{%
  \hfill\#\ \eqparbox{COMMENT}{#1}%
}
\newcommand{\op}[1]{\mathcal{#1}}
\newcommand{\iop}{\mathcal{\altI}}


\hypersetup{
    bookmarks=true,         % show bookmarks bar?
    unicode=false,          % non-Latin characters in Acrobat’s bookmarks
    pdftoolbar=true,        % show Acrobat’s toolbar?
    pdfmenubar=true,        % show Acrobat’s menu?
    pdffitwindow=false,     % window fit to page when opened
    pdfstartview={FitH},    % fits the width of the page to the window
    pdftitle={Elliptikus parci\'alis differenci\'alegyenletek numerikus m\'odszerei \'es alkalmaz\'asai},    % title
    pdfauthor={Csirik Mih\'aly},     % author
    pdfsubject={},   % subject of the document
    pdfcreator={Csirik Mih\'aly},   % creator of the document
    pdfproducer={pdfLaTeX}, % producer of the document
    pdfkeywords={pde} {analysis}, % list of keywords
    pdfnewwindow=true,      % links in new window
    colorlinks=true,       % false: boxed links; true: colored links
    linkcolor=blue,          % color of internal links
    citecolor=red,        % color of links to bibliography
    filecolor=magenta,      % color of file links
    urlcolor=cyan           % color of external links
}

\begin{document}

\pagestyle{empty}
\pagenumbering{gobble}

\cleardoublepage

\begin{center}
\vspace*{4cm}
\hrule
\vspace{1cm}
{\Large\textbf{\textsc{Elliptikus parciális differenciálegyenletek}}}\\
\vspace{0.3cm}
{\Large\textbf{\textsc{numerikus módszerei és alkalmazásai}}}\\
\vspace{0.8cm}
{\Large\textsc{Gyakorlati jegyzet}}
\vspace{1cm}
\hrule
\vspace{1.8cm}
{\large\textsc{Csirik Mih\'aly}}
\vfill
{\Large\textsc{ELTE}}\\
\vspace{0.1cm}
{\Large\textsc{2014--2015}}
\end{center}
\cleardoublepage

\tableofcontents

\cleardoublepage
\pagestyle{plain}
\pagenumbering{arabic}

\chapter*{CC0}
\textbf{To the extent possible under law, the author has waived all copyright and related or neighboring rights to this file.}
\vspace{1cm}

\textbf{Én, a szerző, ezennel minden copyright-jogomról lemondok és jelen művet közkinccsé nyilvánítom.}

\vspace*{4cm}
\hrule
\vspace*{1cm}
A PDF-fájlt az alábbi GhostScript utasítással tudjuk szürkeárnyalatossá alakítani: 
\begin{lstlisting}
gs \
 -sOutputFile=ellgyak_grey.pdf \
 -sDEVICE=pdfwrite \
 -sColorConversionStrategy=Gray \
 -dProcessColorModel=/DeviceGray \
 -dCompatibilityLevel=1.4 \
 -dNOPAUSE \
 -dBATCH \
 ellgyak.pdf
\end{lstlisting}

\chapter*{Előszó}
\addcontentsline{toc}{chapter}{Előszó}

Ebben a bevezető kurzusban megtanulunk numerikusan megoldani elliptikus parciális differenciálegyenleteket. Száz évvel ezelőtt egy ilyen témájú kurzusban nem 
MATLAB-ot használtunk volna, hanem papírt, ceruzát, logarlécet és függvénytáblázatot. Sokféle feladatot meg tudtunk volna oldani, de
eléggé átfogó alkalmazott matematikai ismeretekre lett volna szükségünk (komplex függvénytan, speciális függvények, Fourier-sorok). 
Továbbá jelentős fizikai intuícióra és feladatmegoldási tapasztalatra is szükségünk volna. Az sem mellékes, hogy órákon át csak numerikus számolásokat végeztünk
volna papíron, esetleg (elektro-)mechanikus kalkulátoron -- ezek napjainkban pillanatok alatt végrehajthatóak akármelyik számítógépen.

Most úgy tűnhet, hogy a számítógép mindent elintéz helyettünk -- kiadjuk az utasítást és némi várakozás után kapunk táblázatokat, színes grafikonokat és egyéb ,,látványosságokat''.
Azt állítjuk, hogy az előző bekezdésben felsorolt ismeretekre változatlanul szükség van\footnote{kivéve a logarléc használatára};
 sőt méginkább ,,észnél kell lennünk'', amikor a számítógép által adott eredményeket értékeljük.
A számítógépben ugyanis semmiféle varázslat nem történik, pusztán a mi utasításainkat képes villámgyorsan végrehajtani. Ezért mindenképpen ,,kapunk'' valamit, de kérdés, hogy az helyes-e?
\footnote{Az ún. ,,garbage in, garbage out'' paradigma.}
Minden program fejlesztése során szükség van a hibakeresésre (debugging), mivel minden programban vannak hibák. Egy hasznos tanács, amit jelen sorok írója nagyon lassan tanult meg:
\begin{center}
\textbf{numerikus program írása során mindig legyen kéznél papír és ceruza!}
\end{center}
A katasztrófák\footnote{1991-ben leomlott a Sleipner A tengeri olajfúrótorony mivel egy hibás számítógépes
szimuláció eredményeképpen alultervezték a tartóoszlopok vastagságát!} elkerülése érdekében addig ne kezdjünk neki programozásnak (és hibakeresésnek), amíg nem győződünk meg arról, hogy értjük az algoritmust:
számoljunk végig egy konkrét példát!

 A hibakeresés lényegi része most a kapott közelítő megoldás kvalitatív vagy kvantitatív kiértékelése. Ezért továbbra is szükség van
\emph{a megoldandó feladat elméleti hátterének alapos ismeretére}, illetve \emph{konkrét megoldások} papíron való levezetésére. Ezek ugyanis a próbái
a számítógép által adott eredményeknek. (A valódi próba persze a konkrét fizikai kísérlet elvégzése lenne.)
 Ha az (abszolút) hőmérsékletre, koncentrációra vagy tömegre negatív értéket kapunk, akkor valami nagy baj van; hasonlóan,
a megoldásfüggvény \emph{alakja} sem lehet akarmilyen; ha minden adatot nullának választunk, akkor általában azonosan zérus megoldást kell kapunk. És így tovább,
számtalan módon ,,kipróbálhatjuk'' a kapott numerikus megoldásunkat. Ezzel meggyőzhetjük magunkat (és a főnökünket), hogy a numerikus megoldásunknak
igenis köze van a valósághoz. Igen fontos, hogy meg tudjuk becsülni a numerikus megoldás hibáját -- elvégre a természettudományokban nincs értelme mérési adatokról beszélni
hibaelemzés nélkül. A számítógépes szimulációk tekinthetők egyfajta ,,virtuális mérésnek'', amelyeket igen olcsó elvégezni (pl. egy híd megépítéséhez képes)
és igen megbízhatóak.

Úgy gondoljuk, hogy a parciális differenciálegyenletek megismeréséhez már elengedhetetlen a számítógép használata. Egyrészt megnyugtató, ha numerikusan is alátámasztást
nyernek az elméleti eredményeink, másrészt sejtéseket is meg tudunk fogalmazni segítségével. Ezért a kurzust a pusztán elmélet iránt érdeklődő hallgatóknak is
jó szívvel ajánljuk.

A félév anyagának kidolgozása közben a legnagyobb kihívás a gyakorlatban (fizikában, kémiában, mérnöki tudományban, vagyis az ,,iparban'') is releváns feladatok megtalálása volt.
Mégsem foglalkozhatunk egy egész féléven kereszül a $\lapl u=0$ Laplace-egyenlettel! Bár ez utóbbi feladat numerikus megoldási módszereinek tárgyalása önmagában akár \emph{két} félévet is felemésztene,
mi mégis úgy döntöttünk, hogy általánosabb feladatokra is sort kerítünk. Például a függvényegyütthatós $-\dive \textbf{A}(x)Du =f$, a nulladrendű tagot tartalmazó
$-\lapl u + c(x)u=f$ és a konvekciós tagot tartalmazó legáltalánosabb $-\dive\textbf{A}(x)Du + \textbf{b}\cdot Du + c(x)u=f$ egyenlettel is foglalkozni fogunk.
Sőt, a félév vége fele a legegyszerűbb, de a gyakorlatban is érdekes rugalmasságtani feladatot is kezelni fogunk. 

Feltűnhet az olvasónak, hogy érdemben csak kétdimenziós feladatokkal foglalkozunk. Ennek oka főképpen megjelenítés -- sokkal nehezebb ugyanis egy $\Real^3\to\Real$ függvényt 
áttekinthetően kirajzolni. Az \emph{adatvizualizáció} az informatika egy különálló területe, és komoly programozási tapasztalatokat igényel a művelése. Természetesen
megjegyzések erejéig kitérünk a tárgyalt numerikus módszerek háromdimenziós verzióinak megvalósítására, amely többnyire egyébként triviális általánosítása a kétdimenziós esetnek.
Az egydimenziós speciális esetek kidolgozását feladatokként fogjuk feladni.

\textbf{Programok.} Komoly figyelmet fordítottunk a programok hatékonyságának javítására. Első lépés persze az, hogy megértsük, hogy a programok mit csinálnak, hogyan működnek és így, vagy úgy
leprogramozzuk azokat. Nem szeretnénk viszont, hogy ez legyen az utolsó szó: \emph{hatékony} (gyors, kevés memóriát használó) programok írásával is foglalkoznunk kell.
Világos, érthető és jelöléseiben következetes kódot kell írnunk; olyat, amelyet évek múlva is meg tudunk érteni.
A félév legvégén olyan algoritmusokat is tanulunk, amelyek a jelenleg ismert leggyorsabb módját alkotják a lineáris elliptikus parciális differenciálegyenletek
numerikus megoldásának. Ezek mintegy ,,szubrutinként'' használják a korábbi programjaikat, így nem árt, ha azokat jól megírtuk. Végül megemlítem, hogy grafikus felhasználói felülettel rendelkező
programokat is írtunk (GUI), ezekben lehet interaktívan kísérletezni. Sajnos egy adott verziójú MATLAB-ban írt GUI-k nem biztos, hogy működnek egy másikban;
mi a 2014a verziót használtuk.

\textbf{Forráskód.} A félév során tárgyalt MATLAB/Octave programok és a jelen jegyzet forráskódja a
\begin{center}
\url{https://bitbucket.org/csirik/elte-ellipticpde/src}
\end{center}
Bitbucket repository-ban érhető el. Ezeket közkinccsé (public domain) nyilvánítom, szerzői jogomról lemondok.

\textbf{Jelölések.} A jegyzet nagyrészt követi a 
\begin{center}
Horváth R., Izsák F., Karátson J., \emph{Parciális differenciálegyenletek numerikus módszerei számítógépes alkalmazásokkal.} (2013)
\end{center}
I. részében használt jelöléseket. Számtalanszor fogunk hivatkozni az ott szereplő definíciókra és tételekre.

Feltűnhet a jelen jegyzetben tárgyalt anyag óriási mennyisége: a * (csillag) jelet
opcionális szakaszokra és feladatokra használjuk. Mélyen egyetértünk Richard Feynman tanácsával, amit a közvetlenül halála előtt hagyott fent a tábláján: \emph{,,Tanulj meg minden feladatot
megoldani, amit már megoldottak.''}

\textbf{Köszönetnyilvánítás.} Köszönettel tartozom kollégámnak, Karátson Jánosnak a jelen munka lektorálásáért, illetve hasznos tanácsaiért.

\begin{flushright}
Cs. M.\\
Budapest, 2015. augusztus
\end{flushright}

\chapter{Alapismeretek}
\input{ellgyak1c.tex}

\chapter{Lineáris algebra és M-mátrixok}
\input{ellgyak2c.tex}

\chapter{A véges differencia-módszer}
\input{ellgyak3c.tex}

\chapter{Fejezetek a véges differencia-módszer alkalmazásaiból}
\emph{Ezen a gyakorlaton az első szakasz tárgyalása után 2-3 szabadon válaszott szakasszal foglalkozunk. A fennmaradó szakaszok az első beadandó program témáját képezhetik.}

\input{ellgyak4_fd1_lapl_var.tex}
\input{ellgyak4_fd2_mix.tex}
\input{ellgyak4_fd2_lapl_var.tex}
\input{ellgyak4_fd1_lapl_4.tex}
\input{ellgyak4_fd2_lapl_4.tex}
\input{ellgyak4_fd1_lapl_per.tex}
\input{ellgyak4_fd2_lapl_pol.tex}
\input{ellgyak4_fd1_lapl_neq.tex}
\input{ellgyak4_fd2_lapl_qt.tex}

\chapter{Elsőfokú végeselem-módszer I.}
\input{ellgyak5c.tex}

\chapter{Elsőfokú végeselem-módszer II.}
\input{ellgyak6c.tex}

\chapter{A végeselem-módszer implementációja}
\input{ellgyak8c.tex}

\chapter{Magasabbfokú FEM}
\input{ellgyak7c.tex}

\chapter{Fejezetek a végeselem-módszer alkalmazásaiból}
\input{ellgyak9c.tex}

\chapter{További fejezetek a végeselem-módszer alkalmazásaiból}
\input{ellgyak10c.tex}

\chapter{Adaptív végeselem-módszerek}
\input{ellgyak12c.tex}

\chapter{Multigrid prekondicionálók*}
\input{ellgyak11c.tex}

\appendix
\chapter{Az \texttt{elte-ellipticpde} programkönyvtár}

\chapter{Tesztfeladatok}
\input{atp.tex}

\end{document}
