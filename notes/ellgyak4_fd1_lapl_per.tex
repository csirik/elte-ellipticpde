
\section{Periodikus peremfeltételek*}\label{periosec}

Periodikus peremfeltételeket kell alkalmaznunk például akkor, ha a tartományunk két oldala ,,össze van ragasztva''; mondjuk a körív vagy a tórusz megkapható ilyen módon.
Egy egydimenziós, periodikus peremfeltételt tartalmazó feladat az $\Omega=(0,1)$ tarományon értelmezett
\[
\left.
\begin{aligned}
-u''&=f\\
u(0)&=u(1)\\
u'(0)&=u'(1)
\end{aligned}
\right\}
\]
Világos, hogy a tiszta Neumann-peremfeltételnél látottakhoz hasonlóan is sem lesz egyértelműség, hiszen ha $u$ megoldás, akkor $u+c$ is megoldás bármely $c\in\Real$ esetén. A megoldhatósághoz
szükséges az
\begin{equation}\label{fintzero}
\int_0^1 f=-\int_0^1 u''=u'(0)-u'(1)=0
\end{equation}
feltétel. Például az $f(x)=4\pi^2\sin 2\pi x$ esetben az $u(x)=\sin 2\pi x$ függvény megoldás lesz. 

\textbf{Fontos!} A szakasz hátralevő részében a következő, periodikus esetben általánosan használt indexezést fogjuk követni. A vektorok koordinátáit 0-tól indexeljük, pl.
$v=(v_0,v_1,\ldots,v_{N-1})$ egy $N$-dimenziós vektor. Továbbá az indexeket \emph{modulo} $N$ értjük, azaz $v_{k+\ell N}=v_k$ minden $k=0,\pm 1,\ldots,\pm(N-1)$ és $\ell\in\Int$ esetén.

Hogy fest a periodikus feladat véges differenciás diszkretizációja? Az $A_h$ mátrix \emph{majdnem} a szokásos $(-1,2,-1)$ tridiagonális mátrix lesz. Ha
figyelembe vesszük az $u_0=u_N$ peremfeltételt, azt kapjuk, hogy a szabadsági fokokra vonatkozó egyenletrendszer
\[
\underbrace{\frac{1}{h^2}\begin{pmatrix}
2 & -1 &  & & -1 \\
-1 & 2 & -1 & &  \\
 & \ddots & \ddots & \ddots & \\
 & & -1 & 2 & -1 \\
-1 & & & -1 & 2
\end{pmatrix}}_{\displaystyle{A_h}}
\begin{pmatrix}
u_0\\
u_1\\
\vdots\\
u_{N-1}
\end{pmatrix}
=
\begin{pmatrix}
f_0\\
f_1\\
\vdots\\
f_{N-1}
\end{pmatrix}.
\]
Itt $f_k=f(\tfrac{k}{N})$. Az $u'(0)=u'(1)$ peremfeltétel implicite benne van az egyenletrendszerben.
Ha $f=0$, akkor az $(1,\ldots,1)$ vektor megoldás, tehát $A_h$ nem invertálható. 

\subsection{Diszkrét Fourier-transzformáció} 
Érdekes módon az $A_h$ mátrix szerkezete könnyen felderíthető. Ehhez írjuk $A_h$-t
\[
A_h=\frac{1}{h^2}(2I - P - P^\trans)
\]
alakba, ahol
\[
P=\begin{pmatrix}
0 &  &  & & 1 \\
1 & 0 &  & &  \\
 & \ddots & \ddots &  & \\
 & & 1 & 0 &  \\
 & & & 1 & 0
\end{pmatrix}
\]
a \emph{ciklikus permutáció}, vagy \emph{jobbraforgatás} mátrixa. 

Jelölje $e_k$ a $k$-adik $N$-dimenziós standard bázisvektort: $(e_k)_j=\delta_{kj}$ ($k,j=0,1,\ldots,N-1$). Ekkor
$Pe_k=e_{k+1}$, ha $k=0,1,\ldots,N-1$. Adódik a $P^N=I$ azonosság, amiből azonnal kapjuk, hogy $P$ minden $x$ sajátértékére érvényes
$x^N=1$. Ennek az egyenletnek a megoldásai az $N$-edik komplex egységgyökök: $\epsilon^{k}$ ($k=0,1,\ldots, N-1$), ahol 
\[
\epsilon=e^{2\pi i/N}
\]
egy primitív $N$-edik egységgyök. Azt állítjuk, hogy minden $\ell=0,1,\ldots,N-1$ esetén az
\[
u_\ell=\sum_{k=0}^{N-1} \epsilon^{k\ell}e_k
\]
vektor a $\lambda_\ell=\epsilon^{-\ell}$ sajátértékhez tartozó sajátvektor. Ugyanis
\begin{equation}\label{cyceig}
\begin{aligned}
Pu_\ell&=\sum_{k=0}^{N-1} \epsilon^{k\ell}Pe_k=\sum_{k=0}^{N-2} \epsilon^{k\ell}e_{k+1}+e_0=\epsilon^{-\ell}\Big(\sum_{k=0}^{N-2} \epsilon^{(k+1)\ell}e_{k+1}+\epsilon^{\ell}e_0\Big)\\
&=\epsilon^{-\ell}\sum_{k=0}^{N-1} \epsilon^{(k+1)\ell}e_{k+1}=\lambda_\ell u_\ell.
\end{aligned}
\end{equation}
Belátjuk, hogy $\{u_\ell\}$ ortogonális rendszer:
\begin{align*}
u_\ell \cdot u_m &= \Big(\sum_{k=0}^{N-1} \epsilon^{k\ell}e_k\Big)\cdot \overline{\Big(\sum_{n=0}^{N-1} \epsilon^{nm}e_n\Big)}=\sum_{k=0}^{N-1}\sum_{n=0}^{N-1} \epsilon^{k\ell}\epsilon^{-nm}
\underbrace{e_k\cdot e_n}_{\displaystyle{\delta_{kn}}}\\
&=\sum_{k=0}^{N-1}\epsilon^{k\ell}\epsilon^{-km}=\sum_{k=0}^{N-1}\epsilon^{k(\ell-m)}=\begin{cases}
N, & \text{ha} \;\; \ell=m\\
0, & \text{egyébként}
\end{cases}
\end{align*}
Ezért az 
\[
w_\ell:=\frac{1}{\sqrt{N}} u_\ell=\frac{1}{\sqrt{N}}\begin{pmatrix}
1\\
\epsilon^\ell\\
\vdots\\
\epsilon^{\ell(N-1)}
\end{pmatrix}
\]
rendszer \emph{ortonormált} $\Cmplx^N$-ben.

Elemi Hilbert-teres ismereteinkre emlékezve világos, hogy tetszőleges 
\[
v=(v_0,v_1\ldots,v_{N-1})\in\Cmplx^N
\]
komplex vektor egyérteműen felírható a $\{w_\ell\}$ rendszer segítségével
\[
v=\sum_{\ell=0}^{N-1} \widehat{v}_\ell w_\ell
\]
alakban. A $\widehat{v}_\ell\in\Cmplx$ ún. \textbf{Fourier-együtthatókat} a
\begin{equation}\label{dft}
\widehat{v}_\ell=v\cdot w_\ell=\frac{1}{\sqrt{N}}\sum_{k=0}^{N-1} \epsilon^{-\ell k}v_k
\end{equation}
képlet adja meg. A $v\mapsto\widehat{v}$ lineáris leképezés neve \emph{diszkrét Fourier-transzformáció}. Ennek a lineáris leképezésnek a mátrixa az aktuálisan használt bázisokban
\begin{align*}
U&:=\frac{1}{\sqrt{N}}F_N:=\frac{1}{\sqrt{N}}(\epsilon^{-\ell k})_{\ell,k=0}^{N-1}=\frac{1}{\sqrt{N}} \begin{pmatrix}
1 & 1 & 1 & 1 & \cdots & 1\\
1 & \epsilon^{-1} & \epsilon^{-2} & \epsilon^{-3} & \cdots & \epsilon^{-(N-1)}\\
1 & \epsilon^{-2} & \epsilon^{-4} & \epsilon^{-6} & \cdots & \epsilon^{-2(N-1)}\\
1 & \epsilon^{-3} & \epsilon^{-6} & \epsilon^{-9} & \cdots & \epsilon^{-2(N-1)}\\
\vdots & \vdots  & \vdots & \vdots & \ddots & \vdots   \\
1 & \epsilon^{-(N-1)} & \epsilon^{-2(N-1)} & \epsilon^{-3(N-1)} & \cdots & \epsilon^{-(N-1)(N-1)}
\end{pmatrix}\\
&=\begin{pmatrix}
w_0 & w_1 & \dots & w_{N-1}
\end{pmatrix},
\end{align*}
ahol az $F_N$ mátrix neve \emph{Fourier-mátrix}.
\begin{exam}
Néhány Fourier-mátrix:
\[
F_2=\begin{pmatrix}
1 & 1\\
1 & -1
\end{pmatrix}, \quad
F_4=\begin{pmatrix}
1 & 1 & 1 & 1\\
1 & i^{-1} & i^{-2} & i^{-3} \\
1 & i^{-2} & i^{-4} & i^{-6} \\
1 & i^{-3} & i^{-6} & i^{-9}
\end{pmatrix}=\begin{pmatrix}
1 & 1 & 1 & 1\\
1 & -i & -1 & i \\
1 & -1 & 1 & -i \\
1 & i & -i & i
\end{pmatrix}
\]
\end{exam}

Ezzel tehát $\widehat{v}=Uv$. A $\{w_k\}$ rendszer ortonormáltsága miatt $UU^*=I$ azaz $U$ unitér. A diszkrét Fourier-transzformáció tehát egy
\emph{forgatás} $\Cmplx^N$-ben. Vegyük észre, hogy az inverz diszkrét Fourier-transzformációt az $U^{-1}=U^*$ képlet adja meg. Az inverziós formula a következőképpen fest:
\[
(U^{-1}w)_\ell=\frac{1}{\sqrt{N}}\sum_{k=0}^{N-1} \epsilon^{\ell k}w_k=\frac{1}{\sqrt{N}}\sum_{k=0}^{N-1} \epsilon^{-\ell(N-k)}w_k =\frac{1}{\sqrt{N}}\sum_{m=1}^N \epsilon^{-\ell m}w_{N-m}=(URw)_\ell,
\]
ahol az $R$ operátor megfordítja $w$ komponenseit; $R$ mátrixa az ,,antidiagonális'' egységmátrix. Erre nyilván $R^2=I$, ezért
\[
U^2=U(U^{*})^{-1}=UU^*R=R,\;\;\text{és így}\;\;U^4=I.
\]
Érdemes megfigyelni, hogy valós $v\in\Real^N$ vektor $\widehat{v}\in\Cmplx^N$ diszkrét Fourier-transzformáltjára érvényes $\widehat{v}=R\widehat{v}^*$,
amely jól használható a programok ellenőrzésére.

A folytonos Fourier-transzformáció számos jellegzetes tulajdonsága érvényes a diszkrét Fourier-traszformációra is. Az $U$ unitérsége miatt világos, hogy 
tetszőleges $v,w\in\Cmplx^N$ vektorok esetén érvényes a \textbf{Parseval-azonosság}: $v\cdot w=\widehat{v}\cdot\widehat{w}$ ennélfogva $|v|^2=|\widehat{v}|^2$.
Az intuícióval összhangban, a folytonos eset származtatható a diszkrét Fourier-transzformációból a Riemann-közelítőösszegen keresztül:
\[
\widehat{f}_\ell=\int_0^{1} e^{-2\pi i \ell x}f(x)\,dx=\lim_{N\to\infty}\frac{1}{N}\sum_{k=0}^{N-1} e^{-2\pi i \ell \frac{k}{N}}f(\tfrac{k}{N})=\lim_{N\to\infty}\frac{1}{N}\sum_{k=0}^{N-1} \epsilon^{-\ell k}f(\tfrac{k}{N}),
\]
ahol $\Map{f}{\Real}{\Cmplx}$ Riemann-integrálható és periodikus $1$ periódussal. 

\subsection{A gyors Fourier-transzformáció} 
\Az{\eqref{dft}} képletek alapján meghatározva a diszkrét Fourier-transzformáltat egy $\mathcal{O}(N^2)$ időkomplexitású algoritmust kapunk. 
J. W. Cooley és J. Tukey 1965-ben felfedezte a Fast Fourier Transform algoritmust (FFT), amely időkomplexitása $\mathcal{O}(N\log N)$. Az eredeti algoritmus alapja a következő ötlet.
Tegyük fel, hogy $N$ kettőhatvány: $N=2^M$, valamely $M$-re. \Az{\eqref{dft}} transzformációs formulában először a páros indexű $v_0,v_2,\ldots,v_{N-2}$ tagok járulékait határozzuk meg,
majd a $v_1,v_3,\ldots,v_{N-1}$ páratlan indexűeket és a kettőt összekombináljuk. Az algoritmus leírásához célszerű átrendezni az $(e_0,e_1,\ldots,e_{N-1})$ standard bázist az
$(e_0,e_2,\ldots,e_{N-2},e_1,e_3,\ldots,e_{N-1})$ alakra, ekkor a $v$ ,,inputvektor'' és a $\widehat{v}$ ,,outputvektor'' a
\begin{equation}\label{fftsh}
v=\begin{pmatrix}
v_{\text{ps}}\\
v_{\text{ptl}}
\end{pmatrix}
=\begin{pmatrix}
v_0\\
v_2\\
\vdots\\
v_{N-2}\\
v_1\\
v_3\\
\vdots\\
v_{N-1}
\end{pmatrix},
\quad
\widehat{v}=\begin{pmatrix}
\widehat{v}_{\text{ps}}\\
\widehat{v}_{\text{ptl}}
\end{pmatrix}
=\begin{pmatrix}
\widehat{v}_0\\
\widehat{v}_2\\
\vdots\\
\widehat{v}_{N-2}\\
\widehat{v}_1\\
\widehat{v}_3\\
\vdots\\
\widehat{v}_{N-1}
\end{pmatrix}
\end{equation}
alakú lesz. 

\begin{prop}
Az $F_N$ Fourier-mátrix kifejezhető az $F_{N/2}$-vel az
\[
F_N=\begin{pmatrix}
F^{00} & F^{01} \\
F^{10} & F^{11}
\end{pmatrix}Q=\begin{pmatrix}
I & D \\
I & -D
\end{pmatrix}
\begin{pmatrix}
F_{N/2} & 0 \\
0 & F_{N/2}
\end{pmatrix}
Q
\]
alakban, ahol $D=\diag(1,\epsilon^{-1},\epsilon^{-2},\ldots,\epsilon^{-(\frac{N}{2}-1)})$ és a $Q$ permutációmátrix elvégzi a bázis átrendezését -- más szóval $F_N$ oszlopait permutálja.
\end{prop}
\begin{proof}
Legyen $\eta=e^{4\pi i/N}$ egy $\frac{N}{2}$-edik primitív egységgyök és vegyük figyelembe, hogy $N$ páros, így $\epsilon^{N/2}=-1$, valamint hogy $\eta=\epsilon^2$.
\begin{enumerate}[(1)]\indented
\item Az $F^{00}_{\ell k}=\epsilon^{-2\ell k}=\eta^{-\ell k}$ blokk megegyezik az $(F_{N/2})_{\ell k}=\eta^{-\ell k}$ Fourier-mátrixszal,
ahol $\ell,k=0,1,\ldots,\frac{N}{2}-1$.
\item  Az $F^{10}_{\ell k}=\epsilon^{-2(\ell+\frac{N}{2}) k}=\epsilon^{-N k}\eta^{-\ell k}=\eta^{-\ell k}$ blokk megegyezik az $(F_{N/2})_{\ell k}=\eta^{-\ell k}$ Fourier-mátrixszal,
ahol $\ell,k=0,1,\ldots,\frac{N}{2}-1$.
\item Az $F^{01}_{\ell k}=\epsilon^{-\ell(2k+1)}=\epsilon^{-\ell}\epsilon^{-2\ell k}=\epsilon^{-\ell}\eta^{-\ell k}$ blokk megegyezik a $(DF_{N/2})_{\ell k}=\epsilon^{-\ell}\eta^{-\ell k}$
mátrixszal, ahol $\ell,k=0,1,\ldots,\frac{N}{2}-1$.
\item Az $F^{11}_{\ell k}=\epsilon^{-(\ell+\frac{N}{2})(2k+1)}=\epsilon^{-\frac{N}{2}}\epsilon^{-\ell}\epsilon^{-2\ell k}\epsilon^{-Nk}=-\epsilon^{-\ell}\eta^{-\ell k}$ 
blokk megegyezik a $(-DF_{N/2})_{\ell k}=-\epsilon^{-\ell}\eta^{-\ell k}$ mátrixszal, ahol $\ell,k=0,1,\ldots,\frac{N}{2}-1$.
\end{enumerate}
\end{proof}
A diszkrét Fourier-transzformáltat az FFT algoritmusok más normalizáló konstanssal számítják ki: az $U$ definíciójában szereplő $N^{-1/2}$ helyett $N^{-1}$-t írnak.
A MATLAB/Octave is $N^{-1}$ normalizálást használ (mindkettő az FFTW könyvtárat használja), erre ügyelnünk kell. Ezzel a konvencióval
\begin{equation}\label{fnmul}
\begin{pmatrix}
\widehat{v}_{\text{ps}}\\
\widehat{v}_{\text{ptl}}
\end{pmatrix}=\frac{1}{N} \begin{pmatrix}
F_{N/2} & DF_{N/2} \\
F_{N/2} & -DF_{N/2}
\end{pmatrix}\begin{pmatrix}
v_{\text{ps}}\\
v_{\text{ptl}}
\end{pmatrix}.
\end{equation}
Az iménti állítást rekurzíve alkalmazva most az $F_{N/2}$ Fourier-mátrixra, és a folyamatot egészen az $F_2$ mátrixig ismételve kapjuk az FFT algoritmust.
Most belátjuk, hogy ennek az eljárásnak az időkomplexitása $\mathcal{O}(N\log N)$. Jelölje $\mu(N)$ \az{\eqref{fnmul}} mátrix-vektor szorzáshoz szükséges
szorzások számát. Erre érvényes a $\mu(N)=2\mu(N/2) + 2N$ rekurzió. Mivel feltevésünk szerint $N=2^M$,
\begin{align*}
\mu(N)&=2\mu(2^{M-1}) + 2^{M+1}\\
&=2(2\mu(2^{M-2})+2\cdot 2^{M-1}) + 2^{M+1}=2^2\mu(2^{M-2}) + 2\cdot 2^{M+1}\\
&=2^2(2\mu(2^{M-3}) + 2\cdot 2^{M-2}) + 2\cdot 2^{M+1} =2^3\mu(2^{M-3}) + 3\cdot 2^{M+1}\\
&=2^3(2\mu(2^{M-4}) + 2\cdot 2^{M-3}) + 3\cdot 2^{M+1}=2^4\mu(2^{M-4}) + 4\cdot 2^{M+1} \\
&\ldots\\
&=2^{M-1}\underbrace{\mu(2^1)}_{2} + (M-1)\cdot 2^{M+1}=2\cdot 2^MM=2N\log_2N.
\end{align*}
Az FFT algoritmus hatékony implementációja persze nem a mi dolgunk; itt pusztán a lényeges elemeit ismertettük. A MATLAB-ban az \verb!fft! utasítással
lehet végrehajtani a diszkrét Fourier-transzformációt. Az \verb!fft! függvény \az{\eqref{fftsh}} szerinti elrendezésben adja vissza a diszkrét
Fourier-együtthatókat, azt a szokásos sorrendbe az \verb!fftshift! függvénnyel tudjuk visszarendezni.

\subsection{A periodikus peremfelételű feladat megoldása DFT-vel} 
\Az{\eqref{cyceig}} egyenlethez hasonlóan belátjuk, hogy a $P^\trans$ \emph{balraforgatásra}
érvényes a $P^\trans w_\ell=\lambda_\ell^* w_\ell$,
\begin{align*}
P^\trans u_\ell=\sum_{k=2}^{N} \epsilon^{k\ell}e_{k-1} + \epsilon^\ell e_N=\epsilon^{\ell}\Big(\sum_{k=2}^{N} \epsilon^{(k-1)\ell}e_{k-1} + e_N\Big)=\lambda_\ell^* u_\ell.
\end{align*}

 Ha kiszámítjuk az $v$ vektor $\widehat{v}$ diszkrét Fourier-transzformáltját, akkor
a fentiek alapján
\begin{equation}\label{ahfour}
A_hv=A_h\Big(\sum_{\ell=1}^N \widehat{v}_\ell w_\ell\Big)=\frac{1}{h^2}(2I-P-P^\trans)\sum_{\ell=1}^N \widehat{v}_\ell w_\ell=\frac{1}{h^2}\sum_{\ell=1}^N (2-\lambda_\ell-\lambda_\ell^*)\widehat{v}_\ell w_\ell.
\end{equation}
Ez az eredmény nem meglepő: korábbi tanulmányaink során megfigyelhettünk, hogy a Fourier-transzformáció \emph{diagonalizálja a Laplace-operátort},
abban az értelemben, hogy
\[
\mathcal{F}(\lapl u)(\xi)=|\xi|^2(\mathcal{F}u)(\xi),
\]
ahol $\xi\in\Cmplx^N$ és $\Map{u}{\Rn}{\Real}$. \Az{\eqref{ahfour}} egyenlet tehát azt mutatja, hogy a diszkrét Laplace-operátort diagonalizálja
a diszkrét Fourier-transzformáció -- periodikus peremfeltételek esetén.

Diagonális egyenletrendszert viszont $\mathcal{O}(N)$ időben meg lehet oldani. Képezzük az $f_h\in \Real^N$ vektor $\widehat{f}_h\in\Cmplx^N$
diszkrét Fourier-transzformáltját. Ezzel az $A_hv=f_h$ egyenlet ekvivalens az
$h^{-2}(2-\lambda_\ell-\lambda_\ell^*)\widehat{v}_\ell=(\widehat{f}_h)_\ell$ egyenletekkel, ahol $\ell=1,\ldots,N$. Az $\ell=0$ esetben kapjuk a $(\widehat{f}_h)_0=0$ feltételt, 
ami ekvivalens \az{\eqref{fintzero}} feltétellel. A többi egyenletet megoldva
\begin{align*}
\widehat{v}_\ell=\frac{h^2(\widehat{f}_h)_\ell}{2-\lambda_\ell-\lambda_\ell^*}=\frac{h^2(\widehat{f}_h)_\ell}{2-2\re\lambda_\ell}=\frac{h^2(\widehat{f}_h)_\ell}{2-2\cos\frac{2\pi\ell}{N}} \;\; (\ell=1,\ldots,N-1)
\end{align*}
Az $\widehat{v}_0=0$ választással éppen a szakasz elején megfigyelt additív konstansfüggvényt rögzítjük le (azonosan zérusnak), hiszen $w_0=(1,\ldots,1)$.
A kapott $\widehat{v}$ vektort visszatranszformálva kapjuk a $v$ megoldást.

%\textbf{2D eset.} A szakaszban tárgyalt diszkrét Fourier-transzformációs trükk kétdimenzióban is működik. Ebben az esetben a 
% \[
% \left.
% \begin{aligned}
% -\lapl u&=f\\
% u(0)&=u(1)\\
% u'(0)&=u'(1)
% \end{aligned}
% \right\}
% \]