\select@language {magyar} \contentsline {chapter}{Előszó}{3}{chapter*.3}
\select@language {magyar} \contentsline {chapter}{\numberline {1}Alapismeretek}{7}{chapter.1}
\select@language {magyar} \contentsline {section}{\numberline {1.1}Gyenge derivált}{7}{section.1.1}
\select@language {magyar} \contentsline {section}{\numberline {1.2}$L^p$-terek}{8}{section.1.2}
\select@language {magyar} \contentsline {section}{\numberline {1.3}$H^1$-terek}{9}{section.1.3}
\select@language {magyar} \contentsline {section}{\numberline {1.4}A Taylor-formula és a ,,nagyordó''-jelölés}{11}{section.1.4}
\select@language {magyar} \contentsline {section}{\numberline {1.5}Elliptikus operátorok és peremértékfeladatok}{13}{section.1.5}
\select@language {magyar} \contentsline {chapter}{\numberline {2}Lineáris algebra és M-mátrixok}{15}{chapter.2}
\select@language {magyar} \contentsline {section}{\numberline {2.1}Lineáris algebra}{15}{section.2.1}
\select@language {magyar} \contentsline {section}{\numberline {2.2}M-mátrixok}{17}{section.2.2}
\select@language {magyar} \contentsline {section}{\numberline {2.3}Egy híres tridiagonális mátrix*}{19}{section.2.3}
\select@language {magyar} \contentsline {chapter}{\numberline {3}A véges differencia-módszer}{23}{chapter.3}
\select@language {magyar} \contentsline {section}{\numberline {3.1}Poisson-egyenlet: 1D}{23}{section.3.1}
\select@language {magyar} \contentsline {section}{\numberline {3.2}Konvekció-diffúzió egyenlet: 1D}{27}{section.3.2}
\select@language {magyar} \contentsline {section}{\numberline {3.3}Harmadfajú peremfeltételek: 1D*}{29}{section.3.3}
\select@language {magyar} \contentsline {section}{\numberline {3.4}Poisson-egyenlet: 2D}{30}{section.3.4}
\select@language {magyar} \contentsline {section}{\numberline {3.5}A diszkretizációs mátrix Kronecker-szorzatos felírása}{37}{section.3.5}
\select@language {magyar} \contentsline {section}{\numberline {3.6}Megoldás és plottolás}{41}{section.3.6}
\select@language {magyar} \contentsline {section}{\numberline {3.7}Numerikus eredmények kiértékelése}{45}{section.3.7}
\select@language {magyar} \contentsline {subsection}{\numberline {3.7.1}Modellfeladatok}{45}{subsection.3.7.1}
\select@language {magyar} \contentsline {subsection}{\numberline {3.7.2}Rendbecslés}{45}{subsection.3.7.2}
\select@language {magyar} \contentsline {subsection}{\numberline {3.7.3}Diszkrét $L^2$-, és $H_0^1$-normák}{46}{subsection.3.7.3}
\select@language {magyar} \contentsline {subsection}{\numberline {3.7.4}Időmérés}{47}{subsection.3.7.4}
\select@language {magyar} \contentsline {subsection}{\numberline {3.7.5}Utófeldolgozás: a megoldás gradiensének kiszámítása}{47}{subsection.3.7.5}
\select@language {magyar} \contentsline {chapter}{\numberline {4}Fejezetek a véges differencia-módszer alkalmazásaiból}{49}{chapter.4}
\select@language {magyar} \contentsline {section}{\numberline {4.1}Függvényegyütthatós operátorok: 1D}{49}{section.4.1}
\select@language {magyar} \contentsline {section}{\numberline {4.2}Vegyes deriváltak közelítése*}{53}{section.4.2}
\select@language {magyar} \contentsline {section}{\numberline {4.3}Függvényegyütthatós operátorok: 2D}{55}{section.4.3}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.1}Divergencia-típusú operátorok}{55}{subsection.4.3.1}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.2}Nem-divergencia-típusú operátorok}{55}{subsection.4.3.2}
\select@language {magyar} \contentsline {section}{\numberline {4.4}Magasabbrendű sémák: 1D*}{55}{section.4.4}
\select@language {magyar} \contentsline {section}{\numberline {4.5}Magasabbrendű sémák: 2D*}{60}{section.4.5}
\select@language {magyar} \contentsline {section}{\numberline {4.6}Periodikus peremfeltételek*}{62}{section.4.6}
\select@language {magyar} \contentsline {subsection}{\numberline {4.6.1}Diszkrét Fourier-transzformáció}{63}{subsection.4.6.1}
\select@language {magyar} \contentsline {subsection}{\numberline {4.6.2}A gyors Fourier-transzformáció}{66}{subsection.4.6.2}
\select@language {magyar} \contentsline {subsection}{\numberline {4.6.3}A periodikus peremfelételű feladat megoldása DFT-vel}{67}{subsection.4.6.3}
\select@language {magyar} \contentsline {section}{\numberline {4.7}Polárkoordináták}{68}{section.4.7}
\select@language {magyar} \contentsline {section}{\numberline {4.8}Nem-ekvidisztáns rácsok: 1D}{69}{section.4.8}
\select@language {magyar} \contentsline {section}{\numberline {4.9}Nem-ekvidisztáns rácsok: Quadtree*}{74}{section.4.9}
\select@language {magyar} \contentsline {chapter}{\numberline {5}Elsőfokú végeselem-módszer I.}{77}{chapter.5}
\select@language {magyar} \contentsline {section}{\numberline {5.1}Baricentrikus koordináták}{77}{section.5.1}
\select@language {magyar} \contentsline {section}{\numberline {5.2}A $T_3$ Courant-elem}{80}{section.5.2}
\select@language {magyar} \contentsline {section}{\numberline {5.3}A referenciaelem-technika}{81}{section.5.3}
\select@language {magyar} \contentsline {subsection}{\numberline {5.3.1}Affin eset}{81}{subsection.5.3.1}
\select@language {magyar} \contentsline {chapter}{\numberline {6}Elsőfokú végeselem-módszer II.}{85}{chapter.6}
\select@language {magyar} \contentsline {section}{\numberline {6.1}Mátrix-összefűzés a $T_3$ elemekhez}{85}{section.6.1}
\select@language {magyar} \contentsline {subsection}{\numberline {6.1.1}Implementáció}{88}{subsection.6.1.1}
\select@language {magyar} \contentsline {section}{\numberline {6.2}Inhomogén Dirichlet-peremfeltétel}{90}{section.6.2}
\select@language {magyar} \contentsline {subsection}{\numberline {6.2.1}Implementáció}{91}{subsection.6.2.1}
\select@language {magyar} \contentsline {section}{\numberline {6.3}Kubatúra háromszögön}{92}{section.6.3}
\select@language {magyar} \contentsline {section}{\numberline {6.4}Rácsfüggő normák}{94}{section.6.4}
\select@language {magyar} \contentsline {chapter}{\numberline {7}A végeselem-módszer implementációja}{97}{chapter.7}
\select@language {magyar} \contentsline {section}{\numberline {7.1}Háromszögrácsok generálása}{97}{section.7.1}
\select@language {magyar} \contentsline {section}{\numberline {7.2}Plottolás}{99}{section.7.2}
\select@language {magyar} \contentsline {section}{\numberline {7.3}Vegyes peremfeltétel}{99}{section.7.3}
\select@language {magyar} \contentsline {subsection}{\numberline {7.3.1}A peremintegrál kiértékelése}{100}{subsection.7.3.1}
\select@language {magyar} \contentsline {subsection}{\numberline {7.3.2}Implementáció}{101}{subsection.7.3.2}
\select@language {magyar} \contentsline {section}{\numberline {7.4}Tiszta Neumann-peremfeltétel*}{102}{section.7.4}
\select@language {magyar} \contentsline {chapter}{\numberline {8}Magasabbfokú FEM}{107}{chapter.8}
\select@language {magyar} \contentsline {section}{\numberline {8.1}A végeselem általános definíciója}{107}{section.8.1}
\select@language {magyar} \contentsline {section}{\numberline {8.2}A másodfokú $T_6$ Lagrange-elem}{109}{section.8.2}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.1}Összefűzés}{110}{subsection.8.2.1}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.2}Implementáció}{113}{subsection.8.2.2}
\select@language {magyar} \contentsline {section}{\numberline {8.3}A harmadfokú $T_{10}$ Lagrange-elem}{113}{section.8.3}
\select@language {magyar} \contentsline {section}{\numberline {8.4}A harmadfokú $H_{10}$ Hermite-elem}{113}{section.8.4}
\select@language {magyar} \contentsline {section}{\numberline {8.5}Az $R_4$ és $R_9$ tégla-elemek}{114}{section.8.5}
\select@language {magyar} \contentsline {chapter}{\numberline {9}Fejezetek a végeselem-módszer alkalmazásaiból}{117}{chapter.9}
\select@language {magyar} \contentsline {section}{\numberline {9.1}Függvényegyütthatós egyenletek}{117}{section.9.1}
\select@language {magyar} \contentsline {section}{\numberline {9.2}Nulladrendű tagot tartalmazó feladatok. A tömegmátrix}{119}{section.9.2}
\select@language {magyar} \contentsline {section}{\numberline {9.3}Egy rugalmasságtani feladat*}{121}{section.9.3}
\select@language {magyar} \contentsline {subsection}{\numberline {9.3.1}Erős alak}{122}{subsection.9.3.1}
\select@language {magyar} \contentsline {subsection}{\numberline {9.3.2}Gyenge alak}{123}{subsection.9.3.2}
\select@language {magyar} \contentsline {subsection}{\numberline {9.3.3}A Korn-egyenlőtlenség}{124}{subsection.9.3.3}
\select@language {magyar} \contentsline {section}{\numberline {9.4}A rugalmasságtani feladat diszkretizációja*}{124}{section.9.4}
\select@language {magyar} \contentsline {subsection}{\numberline {9.4.1}Peremfeltételek}{127}{subsection.9.4.1}
\select@language {magyar} \contentsline {subsection}{\numberline {9.4.2}Implementáció}{127}{subsection.9.4.2}
\select@language {magyar} \contentsline {subsection}{\numberline {9.4.3}Rácsbefagyás}{127}{subsection.9.4.3}
\select@language {magyar} \contentsline {chapter}{\numberline {10}További fejezetek a végeselem-módszer alkalmazásaiból}{131}{chapter.10}
\select@language {magyar} \contentsline {section}{\numberline {10.1}Post-processing}{131}{section.10.1}
\select@language {magyar} \contentsline {subsection}{\numberline {10.1.1}A gradiens kiszámítása}{132}{subsection.10.1.1}
\select@language {magyar} \contentsline {section}{\numberline {10.2}Inverz becslések. A merevségi mátrix rossz kondicionáltsága*}{132}{section.10.2}
\select@language {magyar} \contentsline {section}{\numberline {10.3}Az egyenletrendszer prekondicionálása}{134}{section.10.3}
\select@language {magyar} \contentsline {section}{\numberline {10.4}További egyenlőtlenségek*}{135}{section.10.4}
\select@language {magyar} \contentsline {subsection}{\numberline {10.4.1}Az Aubin--Nitsche-féle dualitási trükk}{135}{subsection.10.4.1}
\select@language {magyar} \contentsline {subsection}{\numberline {10.4.2}Negatív-normás becslések}{135}{subsection.10.4.2}
\select@language {magyar} \contentsline {subsection}{\numberline {10.4.3}Maximumnormás becslések}{135}{subsection.10.4.3}
\select@language {magyar} \contentsline {chapter}{\numberline {11}Adaptív végeselem-módszerek}{137}{chapter.11}
\select@language {magyar} \contentsline {section}{\numberline {11.1}Rácsfinomítási stratégiák}{137}{section.11.1}
\select@language {magyar} \contentsline {section}{\numberline {11.2}A reziduális}{137}{section.11.2}
\select@language {magyar} \contentsline {section}{\numberline {11.3}Egy egyszerű explicit hibabecslő}{140}{section.11.3}
\select@language {magyar} \contentsline {section}{\numberline {11.4}Egy boborékfüggvény-alapú implicit hibabecslő}{143}{section.11.4}
\select@language {magyar} \contentsline {section}{\numberline {11.5}Implementáció}{143}{section.11.5}
\select@language {magyar} \contentsline {chapter}{\numberline {12}Multigrid prekondicionálók*}{145}{chapter.12}
\select@language {magyar} \contentsline {section}{\numberline {12.1}Absztrakt additív Schwarz-prekondicionálók}{145}{section.12.1}
\select@language {magyar} \contentsline {section}{\numberline {12.2}A csomóponti bázis}{148}{section.12.2}
\select@language {magyar} \contentsline {section}{\numberline {12.3}A HB-prekondicionáló}{149}{section.12.3}
\select@language {magyar} \contentsline {section}{\numberline {12.4}A BPX-prekondicionáló}{150}{section.12.4}
\select@language {magyar} \contentsline {section}{\numberline {12.5}Implementáció}{151}{section.12.5}
\select@language {magyar} \contentsline {chapter}{\numberline {A\@gobble }Az \texttt {elte-ellipticpde} programkönyvtár}{153}{appendix.A}
\select@language {magyar} \contentsline {chapter}{\numberline {B\@gobble }Tesztfeladatok}{155}{appendix.B}
\select@language {magyar} \contentsline {section}{\numberline {B.1}Poisson-egyenlet}{155}{section.B.1}
\select@language {magyar} \contentsline {subsection}{\numberline {B.1.1}Intervallum}{155}{subsection.B.1.1}
\select@language {magyar} \contentsline {subsection}{\numberline {B.1.2}Téglalap}{155}{subsection.B.1.2}
\select@language {magyar} \contentsline {subsection}{\numberline {B.1.3}L-alakú tartomány}{155}{subsection.B.1.3}
\select@language {magyar} \contentsline {subsection}{\numberline {B.1.4}Körlap}{155}{subsection.B.1.4}
\select@language {magyar} \contentsline {subsection}{\numberline {B.1.5}Körgyűrű}{155}{subsection.B.1.5}
\select@language {magyar} \contentsline {section}{\numberline {B.2}Helmholtz-egyenlet}{155}{section.B.2}
\select@language {magyar} \contentsline {section}{\numberline {B.3}Konvekció-diffúzió-egyenlet}{155}{section.B.3}
\select@language {magyar} \contentsline {section}{\numberline {B.4}Anizotróp Poisson-egyenlet}{155}{section.B.4}
