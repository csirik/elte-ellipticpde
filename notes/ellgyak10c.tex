
\section{Post-processing}

Post-processing, azaz \textbf{utófeldolgozás} alatt minden olyan tevékenységet értenek, amely az $u_h$ közelítő megoldás meghatározása után történik.
Az esetek többségében ugyanis nem az $u_h$ megoldásra kíváncsiak, hanem az abból meghatározható valamilyen más függvényre. A számítási eredmények
fizikai értelmezéséhez és kiértékeléséhez nagyon fontos a megfelelő utófeldolgozás. Általában ugyanis egzakt megoldás nem ismert,
fizikai intuíciónkkal talán el tudjuk dönteni, hogy a megoldásunk helyes lehet-e. Tehát a megfelelő \textbf{adatvizualizáció} is az utófeldolgozás része.

\begin{enumerate}[(1)]
\item Ha $\theta$ egyensúlyi hőmérséklet jelöl, akkor érdekes lehet annak $D\theta$ gradiense, $\{\theta=c\}$ szinthalmazai.
Egy mérnöki kérdés: hol magasabb a hőmérséklet, mint valamilyen adott $\theta_{\max}$ küszöbérték? Ehhez meg kell határoznunk a $\{\theta>\theta_{\max}\}$ halmazt.

\item A rugalmasságtani feladat esetében az $\mathbf{u}$ elmozdulás kísérletileg mérhehető. Gyakran azonban arra vagyunk kíváncsiak, hogy a test \emph{eltört-e},
\footnote{A törések számítógépes szimulációja igen bonyolult terület, ugyanis az anyag mikroszkopikus tulajdonságai is szerepet játszanak.}
ami a $\sigma$ feszültségtenzorra jelent valamilyen feltételt.

\item  További mérnöki kérdés annak ellenőrzése, hogy az ún. \textbf{folyáshatár} (yield point) alatt
maradt-e a feszültség: a folyáshatár után plaszikusan deformálódik az anyag. Számos erre vonatkozó elmélet és kritérium ismert.
Teljesen hasonló kérdés, hogy egy elektromos szigetésen átüt-e az áram.

\item Stacionárius áramlási feladatok esetén az \textbf{áramvonalak} szinte mindig többet mondanak, mint a vektormező nyilacskás rajza. 
Egy áramvonalnak ugyanis meglehetősen szemléletes jelentése van: ez egy nulla tömegű részecske (porszem) pályája a vektormező, mint külső erőtér hatására.

\item Ha $c$ egységnyi térfogatra vonatkoztatott koncentrációt jelent, akkor nyilván $c\ge 0$ mindenütt. Azonban a végeselemes $c_h$ megoldás
nem biztos, hogy teljesíti a $c_h\ge 0$ egyenlőtlenséget, más szóval a $c_h$ megoldás fizikailag értelmetlen. Az ilyesfajta
nehézségek a \textbf{diszkrét maximumelvvel} kapcsolatosak.
\end{enumerate}

Összefoglalva, egy konkrét gyakorlati feladat megoldásának utolsó lépése az utófeldolgozás, amely gyakran nagyobb erőfeszítést jelent, mint
a végeselem-módszer leprogramozása. 

\subsection{A gradiens kiszámítása}
A véges differencia-módszernél kiszámítottuk a közelítő megoldás gradiensét, és most a lineáris háromszögrácson megadott függvények esetén is
megtesszük ezt. Minthogy minden $u\in V_h$ $T_3$-Courant-elemekből ,,összerakott'' rácsfüggvény $Du$ gradiense háromszögenként konstans,
ezért önkényesen a háromszögek súlypontjaiban fogjuk ezeket a konstans vektorokat ábrázolni. Emlékezzünk vissza, hogy $u$  egyértelműen felírható a $\{\phi_{\fie{x}}\}$
csomóponti bázisban. Mivel $\phi_{\fie{x}}$ egyszerűen felírható rögzített $T\in \mathcal{T}_h$ háromszög esetén a baricentrikus függvényekkel (alakfüggvényekkel), 
ezért
\[
D\lambda_{T,\fie{x}}=-\frac{1}{2|T|} \bm{\eta}_{T,\fie{x}}^\perp,
\]
a $T$ minden $\fie{x}$ csúcsára. Itt $\bm{\eta}_{T,\fie{x}}^\perp$ az $\fie{x}$ csúccsal szemközti oldal $90^\circ$-os elforgatottja.
Ezzel
\[
\restr{Du}{T}=-\frac{1}{2|T|} \sum_{\fie{x}\in\mathcal{V}(T)} u(\fie{x})\bm{\eta}_{T,\fie{x}}^\perp.
\]
Egy lehetséges, vektorizált megvalósítást az \lstinline|fe2_grad| függvény tartalmaz.


\section{Inverz becslések. A merevségi mátrix rossz kondicionáltsága*}

Bár általában a $H^1$ Szoboljev-normát nem lehet kontrollálni az $L^2$-normával (lásd 1. gyakorlat), véges dimenziós altéren ez lehetséges;
az ilyen eredményeket \textbf{inverz becslésnek} nevezzük. Először ennek egy lokális változatát igazoljuk.

\begin{blemm}
Legyen $T\subset\Rn$ korlátos, $\diam T\le h\le 1$. Ha $P\subset H^1(T)$ véges dimenziós, akkor létezik $C=C_{T,P}$ konstans, hogy
\[
\forall v\in P \;:\; \|v\|_{H^1(T)}\le C h^{-1} \|v\|_{L^2(T)}.
\]
\end{blemm}
\begin{proof}
Adott $v\in P$ esetén tekintsük az átskálázott $\widehat{T}=\{x/\diam T \;:\; x\in T\}$ (vegyük észre, hogy $\diam\widehat{T}=1$) tartományon a $\widehat{v}(x)=v((\diam T)x)$ függvényt és
az ezekből kapott $\widehat{P}=\{\widehat{v} \;:\; v\in P\}$ alteret. Mivel $\widehat{P}$ véges dimenziós, a normaekvivalencia miatt
\[
\|\widehat{v}\|_{H^1(\widehat{T})}\le C\|\widehat{v}\|_{L^2(\widehat{T})}.
\]
 Az 1. Gyakorlat egy feladatában meghatároztuk, hogyan viselkedik az $L^2$-norma és a $H^1$-\emph{fél}norma
dilatáció hatására, ennek alapján
\[
\|\widehat{v}\|_{L^2(\widehat{T})}=(\diam T)^{-n/2},\quad\text{és}\quad|\widehat{v}|_{H^1(\widehat{T})}=(\diam T)^{1-n/2}|v|_{H^1(T)}.
\]
Az egyenlőtlenségünk bal oldalát csökketjük, ha $H^1$-normát a $H^1$-félnormára cseréljük, így összeségében azt kapjuk, hogy
\[
|v|_{H^1(T)}\le C h^{-1} \|v\|_{L^2(T)}.
\]
Mivel $h\le 1$, mindkét oldalhoz $\|v\|_{L^2(T)}$-t adva kapjuk az állítást.
\end{proof}

Az elemek ``összefércelésével'' kapjuk a következő, globális inverz becslést.

\begin{btheorem}
Legyen $\{\mathcal{T}_h\;:\;h\in(0,1]\}$ reguláris háromszögrács-sorozat az $\Omega\subset\Real^2$ tartományon, és $P(R)\subset H^1(R)$ véges dimenziós függvénytér amelynek
elemei az $R$ referenciaelemen vannak értelmezve.
Tegyük fel, hogy minden $T\in\mathcal{T}_h$ elem megkapható $R$ alkalmas affin transzformációjával. Jelölje
\[
V_h=\{v \in H^1(\Omega)  \;:\; \restr{v}{T}\in P(T)\;\;(\forall T\in\mathcal{T}_h)\}
\]
a megfelelő végeselemes approximáló alteret. Ekkor
\[
\forall v\in V_h \;:\; \Big[\sum_{T\in\mathcal{T}_h}\|v\|_{H^1(T)}^2\Big]^{1/2}\le C h^{-1} \Big[\sum_{T\in\mathcal{T}_h}\|v\|_{L^2(T)}^2\Big]^{1/2}
\]
\end{btheorem}

Tömören fogalmazva, a tételben szereplő egyenlőtlenség a
\[
\forall v\in V_h \;:\; \|v\|_{H^1(\Omega)}\le C h^{-1} \|v\|_{L^2(\Omega)}
\]
alakba írható.

\begin{bprop}\label{condstiffmass}
Legyen $A^h$  a lineáris végeselem-diszkretizáció merevségi mátrixa. Ekkor
\[
\forall h>0 \;:\; \kappa(A^h)\le C h^{-2},
\]
ahol $C>0$ nem függ a $h$ rácsmérettől.
\end{bprop}
\begin{proof}
Először $\|A^h\|_2$-t becsüljük. Legyen $U\in\Real^N$, és $u\in V_h$ az ennek megfelelő rácsfüggvény. Ekkor
\[
\|A^hU\|_2=\sup_{V\in\Real^N} \frac{A^hU\cdot V}{\|V\|_2}=\sup_{V\in\Real^N} \frac{a(u,v)}{\|V\|_2}\le C\|u\|_{V_h} \sup_{V\in\Real^N} \frac{\|v\|_{V_h}}{\|V\|_2}
\]
Az fenti inverz becslést és \az{\ref{meshequ}}. Állítást alkalmazva,
\[
\|A^hU\|_2\le C h^{-2} \|u\|_{L^2(\Omega)} \sup_{V\in\Real^N} \frac{\|v\|_{L^2(\Omega)}}{\|V\|_2}\le C h^{-2} h\|U\|_2 h\sup_{v\in V_h} \frac{\|v\|_{L^2(\Omega)}}{\|v\|_{0,h}}\le C\|U\|_2,
\]
mivel $\|v\|_{0,h}=h\|V\|_2$. Ebből $\|A^h\|_2\le C$.

Most $\|(A^h)^{-1}\|_2$-t becsüljük. Azt kapjuk, hogy
\begin{align*}
h\|U\|_2&=\|u\|_{0,h}\le C\|u\|_{V_h}\le C\sup_{v\in V} \frac{a(u,v)}{\|v\|_{V_h}} \le C \sup_{v\in V_h}\frac{A^hU\cdot V}{\|v\|_{0,h}}\\
&\le C h^{-1} \sup_{V\in \Real^N}\frac{A^hU\cdot V}{\|v\|_{0,h}}\le C h^{-1} \|A^hU\|_2,
\end{align*}
amiből
\[
\|(A^h)^{-1}\|_2\le Ch^{-2}.
\]

Ezeket összetéve kapjuk a kondiciószám
\[
\kappa(A^h)=\|A^h\|_2\|(A^h)^{-1}\|_2\le Ch^{-2}
\]
becslését.
\end{proof}

\section{Az egyenletrendszer prekondicionálása}

Az előadáson szerepelt konjugált gradiens módszer (CGM vagy KGM) nem tekinthető egy univerzális egyenletrendszer-megoldó algoritmusnak,
a gyakoratban az $Ax=b$ feladatot mindig prekondicionálni kell. Ugyanis, ha a $h$ rácsméretet csökkentjük, 
akkor az $A^h$ merevségi mátrix 
\[
\kappa(A^h)=\mathcal{O}(h^{-2})
\]
\textbf{kondiciószáma} nem marad korlátos, ami azért kellemetlen, mert a CG algoritmus konvergenciahányadosára
\[
q=\frac{\sqrt{\kappa}-1}{\sqrt{\kappa}+1}\to 1
\]
teljesül, ha $\kappa\to+\infty$.

 Ha valahogyan ``meg tudjuk javítani'' az egyenletrendszert (prekondícionálással),
és korlátos marad a kondíciószám a finomítás során, akkor beszélünk \textbf{rácsfüggetlenségről}.
\footnote{Lásd J. Karátson. \emph{Numerikus funkcionálanalízis.}, 19. fejezet.}

Megjegyezzük, hogy amikor a \texttt{x=A\textbackslash b} MATLAB utasítást kiadjuk, akkor is valamilyen prekondicionált
konjugált gradiens módszer (PCG) fut le.

A MATLAB-ban a PCG algoritmust a 
\[
\texttt{[ x, flag, relres, iter ] = pcg( A, b, tol, maxit, M1, M2 )}
\]
függvény implementálja. Ennek bemenő/kimenő paraméterei a következők.
\begin{itemize}
\item \texttt{A} szimmetrikus, pozitív definit mátrix, \texttt{b} jobb oldali vektor. 
\item Akkor tekinjük konvergensnek az iterációt, ha a 
\[
\rho_n=\frac{\|r_n\|}{\|b\|}=\frac{\|b-Ax\|}{\|b\|}
\]
relatív reziduálisra $\rho_n\le\texttt{tol}$ teljesül, ekkor \texttt{flag=0}.
\item Az algoritmus legfeljebb \texttt{maxit} iterációt futtat, ha addig nincs konvergencia, akkor \texttt{flag=1}.
\item $M=\texttt{M1*M2}$ szimmetrikus, pozitív definit prekondicionáló mátrix, amellyel a prekondicionált egyenletrendszer
\[
M^{-1}Ax=M^{-1}b,
\]
amellyel tehát $\kappa(M^{-1}A)\ll\kappa(A)$.
\item $x$ a megoldása az $Ax=b$ egyenletnek.
\item \texttt{relres} a fenti $\rho_n$ mennyiségek vektora, ennek dimenziója az végrehajtott iterációk száma, amelyet az
\texttt{iter} természetes szám ad meg.
\end{itemize}

A MATLAB-ban alapból megtalálható néhány egyszerű prekondicionáló. 
Az előadásjegyzetben említett inkomplett Cholesky faktorizációt a \texttt{ichol} függvény implementálja.
Ha tehát \texttt{L=ichol(A)}, akkor a \texttt{pcg} eljárás prekondició mátrixai \texttt{M1=L} és \texttt{M2=L'}.

\section{További egyenlőtlenségek*}

\subsection{Az Aubin--Nitsche-féle dualitási trükk}

\subsection{Negatív-normás becslések}

\subsection{Maximumnormás becslések}