
\section{Gyenge derivált}

Számos parciális differenciálegyenletekkel, Szoboljev-terekkel vagy numerikus módszerekkel foglalkozó munkában mellőzik a disztribúciók
használatát. Ehelyett a disztribúciós derivált reguláris speciális esetét alkalmazzák, amely már elég a Szoboljev terek definíciójához.

Emlékeztetünk, hogy egy $T\in\mathcal{D}(\Omega)^*$ disztribúció $D^\alpha T$ deriváltja egy $\alpha$ multiindex szerint
szintén disztribúció és
\[
\forall \phi\in\mathcal{D}(\Omega) \;:\; D^\alpha T(\phi)=(-1)^{|\alpha|}T(D^\alpha\phi),
\]
ahol $\mathcal{D}(\Omega)$ a $C^\infty_0(\Omega)$ a szokásos topológiával.
Ha feltesszük, hogy $T$ és $D^\alpha T$ is \textbf{reguláris disztribúciók}, azaz létezik $f\in \Lloc(\Omega)$ és $g\in \Lloc(\Omega)$, hogy
\[
\forall\phi\in\mathcal{D}(\Omega) \;:\; T(\phi)=\int_\Omega f\phi\;\; \text{és}\;\; D^\alpha T(\phi)=\int_\Omega g\phi,
\]
akkor a következő elnevezéssel szokás illetni a disztribúciós deriváltat.
\begin{bdefn}
Legyen $\Omega\subset\Rn$ nyílt, $f\in \Lloc(\Omega)$ és $\alpha$ multiindex. Azt mondjuk, hogy $f$ \textbf{gyenge deriváltja} (az $\alpha$ multiindex szerint)
egy $g\in \Lloc(\Omega)$, ha
\[
\forall \phi\in C_c^\infty(\Omega) \;:\; \int_\Omega fD^\alpha\phi=(-1)^{|\alpha|}\int_\Omega g \phi,
\]
és ekkor a $D^\alpha f=g$ jelöléssel élünk.
\end{bdefn}
Ismert, hogy a $D^\alpha f$ gyenge derivált m.m. egyértelmű. Legyen az egyszerűség kedvéért most $|\alpha|=1$, ekkor
ha $f\in C^1(\Omega)$, akkor a klasszikus értelemben vett derivált megegyezik a gyenge deriválttal.

\begin{rmrk} Felhívjuk a figyelmet, hogy míg az $f(x)=\sgn x$ függvény a 0-t kivéve mindenhol klasszikus értelemben differenciálható
(és a derivált 0), a gyenge deriváltja nem létezik. Tudjuk parcdiffből, hogy a $\sgn$ disztribúciós deriváltja a 0-ra koncentrált
$\delta$-disztribúció kétszerese (jelben $T_{\sgn}'=2\delta_0$), de sajnos az \textbf{nem reguláris disztribúció}. 
Vigyáznunk kell tehát a továbbiakban, hogy szakaszonként konstans függvények gyenge deriváltja általában nem létezik.
\end{rmrk}

\section{$L^p$-terek}
Legyen $\Omega\subset\Rn$ Lebesgue-mérhető halmaz, $1\le p\le\infty$, és emlékezzünk vissza a következő definícióra:
\footnote{Felesleges precízkedés a ``Lebesgue-mérhető'' jelző használata, és a továbbiakban nem is fogjuk használni.
Ennek oka, hogy a \textbf{nem} Lebesgue-mérhető halmazok (függvények) létezését kizárólag nem konstruktív eljárással
lehet bizonyítani -- más szóval senki nem látott még nem Lebesgue-mérhető halmazt és nem is fog. Bár roppant érdekes
az ilyen halmazok vizsgálata, mi a numerikus analízisben nyugodtan feltehetjük egyszer és mindenkorra, hogy minden szóbajövő halmaz (és függvény) Lebesgue-mérhető.}
\[
L^p(\Omega)=\Bigg\{\,\Map{f}{\Omega}{[-\infty,+\infty]}\;\; \text{Lebesgue-mérhető} \;:\; \int_\Omega |f|^p<+\infty\Bigg\}
\]
Ez egy (valós) Banach-tér \textbf{minden} $1\le p\le\infty$ esetén, az
\[
\forall f\in L^p(\Omega) \;:\; \|f\|_p=\begin{cases}
\displaystyle{\Big[\int_\Omega |f|^p\Big]^{1/p}},\;\; &\text{ha}\;\; 1\le p<\infty\\
\text{ess}\sup_\Omega |f|, \;\; &\text{ha}\;\; p=\infty
\end{cases}
\]
normával ellátva. 
A legszebb eset a $p=2$, főképp ebben fogunk dolgozni, ekkor ugyanis $L^2(\Omega)$ (valós) Hilbert-tér az ún. $L^2$-skalárszorzattal
\[
\forall f,g\in L^2(\Omega) \;:\; \dua{f}{g}_{L^2(\Omega)}=\int_\Omega fg.
\]
A félév során korlátos $\Omega$ alaphalmazokkal fogunk foglalkozni, ekkor $|\Omega|<\infty$\footnote{$|\Omega|$ jelöli $\Omega$ $n$-dimenziós Lebesgue-mértékét.} (fordítva nem igaz!)
és ilyenkor az $L^p(\Omega)$-tércsaládra a következő egyszerű tartalmazások teljesülnek.
\begin{bprop}\label{lpincl}
Legyen $|\Omega|<\infty$, és $1\le p\le q\le\infty$. Ekkor $L^q(\Omega)\subset L^q(\Omega)$ altér, sőt az
\[
\Map{I}{L^q(\Omega)}{L^p(\Omega)},\;\; If(x)=f(x) \;\;(\forall f\in L^q(\Omega), \;\forall x\in\Omega)
\]
beágyazás folytonos lineáris leképezés, sőt $\|I\|_{\mathcal{B}(L^q,L^p)}=|\Omega|^{\frac{1}{p}-\frac{1}{q}}$. 
A folytonos beágyazást röviden az $L^q(\Omega)\hookrightarrow L^p(\Omega)$ szimbólummal szokás jelölni.
\end{bprop}

Szavakkal kifejezve az állítás azt mondja ki, hogy \textbf{véges mértékű alaphalmaz esetén adott rendű integrálhatóságból automatikusan következik 
az összes alacsonyabb rendű integrálhatóság}. Speciálisan, ilyenkor a korlátos függvények integrálhatóak. 

A numerikus analízisben használatosak olyan $L^p$-terek is, amelyekben vektorértékű függvények vannak. Az egyszerűség kedvéért ez csak a $p=2$ esetben mondjuk el.
Legyen $d\ge 1$, és 
\[
L^2(\Omega,\Real^d)=\Bigg\{\Map{\fie{f}}{\Omega}{\Real^d} \;:\; \int_\Omega |\fie{f}(x)|\,dx<\infty\Bigg\}.
\]
Legyen $\fie{f}=(f_1,\ldots,f_d)$, ahol $\Map{\fie{f}_\ell}{\Omega}{\Real}$ ($1\le \ell\le d$). Nyilván $\fie{f}\in L^2(\Omega,\Real^d)$ pontosan akkor, ha $f_k\in L^2(\Omega)$ ($1\le \ell\le d$).
Analízisből tanultuk, hogy egy adott (valós) véges dimenziós vektortéren (ez mindig izomorf $\Real^d$-vel valamilyen $d$-re) minden norma ekvivalens\footnote{Lásd pl. Komornik V. \emph{Valós analízis előadások I.}, 3.8 Tétel, 73.o.} 
így ha a definícióban a $2$-normát helyett mást írunk, ekvivalens $L^2$-normát kapunk. 
Az $L^2(\Omega,\Real^d)$ vektortér
Hilbert-tér az
\[
\forall \fie{f},\fie{g}\in L^2(\Omega,\Real^d) \;:\; \dua{\fie{f}}{\fie{g}}_{L^2(\Omega,\Real^d)}=\int_\Omega \fie{f}(x)\cdot \fie{g}(x)\,dx=\sum_{1\le \ell\le d}\int_\Omega f_\ell(x) g_\ell(x)\,dx
\]
skalárszorzatra nézve. Teljesen hasonlóan definiálható $L^2(\Omega,\Real^{d\times d})$, ezen a szokásos a skalárszorzatot a négyzetes
mátrixokon értelmezett $\fie{A}:\fie{B}=\Tr( \fie{A}^\trans \fie{B})$ Frobenius-skalárszorzat indukálja,
\begin{align*}
\forall \fie{F},\fie{G}\in L^2(\Omega,\Real^{d\times d}) \;:\; \dua{\fie{F}}{\fie{G}}_{L^2(\Omega,\Real^{d\times d})}&=\int_\Omega \fie{F}(x):\fie{G}(x)\,dx=\int_\Omega \Tr( \fie{F}(x)^\trans \fie{G}(x))\,dx \\
&=\sum_{1\le \ell,k\le d}\int_\Omega F_{k,\ell}(x) G_{k,\ell}(x)\,dx.
\end{align*}
Az $L^2(\Omega,\Real^d)$ értelmezése világos -- négyzetesen integrálható vektormezőket írnak le az elemei. Az $L^2(\Omega,\Real^{d\times d})$ térben
azonban mátrix-értékű függvények vannak. Ezeket tenzormezőknek szokás nevezni. Fizikában pl. a rugalmasságtanban fordulnak elő:
egy kicsiny térfogatelem deformációjának leírásához ugyanis több paraméter kell -- meg kell adni, hogy mennyire ``nőtt'' a koordinátatengelyek mentén
illetve, hogy hogyan ``nyíródott'' szintén a koordinátatengelyek mentén. A félév során fogunk ilyen feladatokkal is foglalkozni.

\section{$H^1$-terek}
Legyen $\Omega\subset\Rn$ \textbf{korlátos tartomány}.
A gyenge deriváltat az $L^2$-térrel vegyítve, a ``simaságnak'' egy eléggé gyenge fogalmát kapjuk: a $H^1(\Omega)$ függvénytérben
olyan $L^2(\Omega)$-beli (más szóval négyzetesen integrálható) függvények vannak, amelyek $x_k$-szerinti gyenge deriváltjai ($1\le k\le n$), jelben $D_ku$, léteznek, és
szintén $L^2(\Omega)$-ban vannak. Más szóval,
\[
H^1(\Omega)=\{u\in L^2(\Omega) \;:\; \exists D_ku\in L^2(\Omega) \;\;(1\le k\le n)\}.
\]
Ez (valós) \textbf{Hilbert-tér} az
\begin{align*}
\forall u,v\in H^1(\Omega) \;:\; \dua{u}{v}_{H^1(\Omega)}&=\dua{u}{v}_{L^2(\Omega)} + \dua{Du}{Dv}_{L^2(\Omega,\Rn)}\\
&=\int_\Omega u(x)v(x)\,dx + \int_\Omega \sum_{1\le k\le n} D_ku(x)D_kv(x)\,dx
\end{align*}
\textbf{Szoboljev-skalárszorzattal}. A skalárszorzat definíció szerint az
\[
\forall u\in H^1(\Omega) \;:\; \|u\|_{H^1(\Omega)}^2=\|u\|_{L^2(\Omega)}^2+\|Du\|_{L^2(\Omega,\Rn)}^2
\]
\textbf{Szoboljev-normát} indukálja. Ha eldobjuk az $\|u\|_{L^2(\Omega)}$ tagot, a fennmaradó
\[
\forall u\in H^1(\Omega) \;:\; |u|_{H^1(\Omega)}=\|Du\|_{L^2(\Omega,\Rn)}
\]
\textbf{Szoboljev-félnorma} félnorma lesz, elvégre egy konstans függvény Szoboljev-félnormája nulla, míg a függvény maga nemnulla.
A Szoboljev-félnorma a $H^1(\Omega)$ tér bizonyos alterein norma lesz.

Megjegyezzük, hogy az irodalomban használatosak más, ekivalens\footnote{Emlékeztetünk, hogy azt mondjuk, hogy 
az $\|\cdot\|$ és az $\|\!\|\cdot\|\!\|$ normák \textbf{ekvivalensek}, ha léteznek olyan univerzális $C,C'>0$ konstansok úgy,
hogy $C\|u\|\le \|\!\|u\|\!\|\le C'\|u\|$ teljesül minden $u$ esetén. Ebben az esetben $\|\cdot\|$ és az $\|\!\|\cdot\|\!\|$ szerint
ugyanazok a sorozatok konvergensek, más szóval ugyanazt a topológiát indukálják. Egy jól megválaszott, ekvivalens normának óriási jelentősége van
az alkalmazások szempontjából.}  normák is, amelyeket a már említett 
``véges dimenziós normaekvivalenciából'' kapunk.

Milyen függvények vannak a $H^1(\Omega)$ térben? Először is $C^1(\overline{\Omega})\subset H^1(\Omega)$. Viszont egy $H^1(\Omega)$-beli függvény 
\textbf{nem feltétlenül korlátos}, ha $n\ge 2$. 
\begin{exam}
Legyen $\Omega=B(0,1/2)$ és $f(x)=\log|\log\|x\||$. Ekkor $f\in L^2(\Omega)$. Továbbá, $D_kf(x)=x_k/(\|x\|^2\log\|x\|)$ és $D_kf\in L^2(\Omega)$.
Viszont nyilván $f\notin L^\infty(\Omega)$.
\end{exam}
Emlékeztetünk, hogy 
\[
H^1_0(\Omega)=\{u\in H^1(\Omega) \;:\; \restr{u}{\partial\Omega}=0\;\;\text{(nyom)}\}.
\]

Most idézzük fel a Szoboljev-egyenlőtlenséget. Ehhez már extra feltételek kellenek
a tartomány határára vonatkozóan, lásd előadás/jegyzet.
\begin{sobineq}
Legyen $\Omega\subset\Rn$ Lipschitz-tartomány, $n\ge 2$. Bármely $u\in H^1_0(\Omega)$ függvényre
\[
\begin{cases}
\,\|u\|_{L^{q}(\Omega)}\le C_\Omega |u|_{H^1(\Omega)}\;\;(\forall 2\le q<\infty)\quad &\text{ha} \;\; n=2\\
\,\|u\|_{L^{q}(\Omega)}\le C_\Omega |u|_{H^1(\Omega)}\;\;(\forall 2\le q\le 2^*)\quad &\text{ha} \;\; n\ge 3
\end{cases}
\]
ahol $C_\Omega$ csak a tartománytól függ és $2^*$ a \textbf{Szoboljev-kitevő}:
\[
2^*=\frac{2n}{n-2},\;\;\text{vagy}\;\;\frac{1}{2^*}=\frac{1}{2}-\frac{1}{n}.
\]
\end{sobineq}
 Az Szoboljev-egyenlőtlenség megfordítására az ellenpéldát az $u_k(x,y)=\sin kx\sin ky$ függvénysorozat szolgáltatja az $\Omega=(0,\pi)^2$ tartományon.

Mivel $2^*>2$, így következőt mondhatjuk a Szoboljev-egyenlőtlenség alapján: 
\textbf{egy $H^1_0$-beli függvény simaságát beválthatjuk magasabb rendű integrálhatóságra, azonban ezt fordítva nem lehet megtenni, azaz
tetszőlegesen magas integrálhatóságból nem következik semmilyen simaság.}

Vegyük észre, hogy a jegyzetben szereplő 
\[
\forall u\in H^1_0(\Omega) \;:\; \|u\|_{L^2(\Omega)}\le C_\Omega' |u|_{H^1(\Omega)}
\]
\textbf{Poincaré--Friedrichs-egyenlőtlenség} (a jegyzetben az (1.8) egyenlet), azonnal következik a Szoboljev-egyenlőtlenségből.
Felhívjuk a figyelmet, hogy az $n=2$ esetben a $q$ kitevő \emph{nem} lehet $\infty$, és ez éles, lásd a már bemutatott $f(x)=\log|\log\|x\||$ példát.

A Poincaré--Friedrichs egyenlőtlenségből következik, hogy 
\begin{bprop}
A $|\cdot|_{H^1(\Omega)}$ Szoboljev-félnorma \emph{norma} $H^1_0(\Omega)$-n és ez itt ekvivalens a $\|\cdot\|_{H^1(\Omega)}$ normával.
\end{bprop}

A Poincaré--Friedrichs egyenlőtlenség teljesüléséhez a peremen való eltűnés is lényeges, ugyanis ha megengednénk más függvényeket is, pl. a $u\equiv K\in\Real$ konstanst (ami $H^1$-beli),
akkor az abszurd $K\le C_\Omega\cdot 0=0$ egyenlőtlenséget kapnánk tetszőleges $K\in\Real$ esetén. A problémát az okozza, hogy nem megfelelő mennyiséget akarunk
kontrollálni a bal oldalon. A megfelelő általánosítás, amely \emph{minden} $H^1(\Omega)$-beli függvényre érvényes, a következő.
\begin{poineq}
Legyen $\Omega\subset\Rn$ Lipschitz-tartomány. Bármely $u\in H^1(\Omega)$ függvényre
\[
\|u-u_\Omega\|_{L^2(\Omega)}\le C_\Omega |u|_{H^1(\Omega)},
\]
ahol $u_\Omega$ az $u$ átlaga a $\Omega$-n, azaz
\[
u_\Omega=\frac{1}{|\Omega|}\int_\Omega u
\]
\end{poineq}
Ebből könnyen levezethető az alábbi, a jegyzetben szereplő (1.9) egyenlőtlenséggel ekvivalens eredmény.
\begin{poineuineq}
Legyen $\Omega\subset\Rn$ Lipschitz-tartomány. Bármely $u\in H^1(\Omega)$ függvényre
\[
\|u\|_{L^2(\Omega)}\le C_\Omega\Bigg(|u|_{H^1(\Omega)}+\Big|\int_{\Omega}u\Big|\Bigg)
\]
\end{poineuineq}

A Poincaré--Neumann egyenlőtlenség jelentősége a következő. Definiáljuk $H^1(\Omega)$ következő alterét
\[
\mathring{H}^1(\Omega)=\{u\in H^1(\Omega) \;:\; u_\Omega=0\}.
\]
Ebben tehát a nulla átlagú Szoboljev-függvények vannak, amelyek a tiszta Neumann-típusú peremfeltétel esetén jönnek be a játékba.
Ekkor minden $u\in \mathring{H}^1(\Omega)$ függvényre a Poincaré--Friedrichs-egyenlőtlenséggel analóg
\[
\|u\|_{L^2(\Omega)}\le C_\Omega |u|_{H^1(\Omega)}
\]
becslés teljesül. A $H^1_0(\Omega)$ esettel analóg állítás itt is érvényes.
\begin{bprop}
A $|\cdot|_{H^1(\Omega)}$ Szoboljev-félnorma \emph{norma} $\mathring{H}^1(\Omega)$-n és ez itt ekvivalens a $\|\cdot\|_{H^1(\Omega)}$ normával.
\end{bprop}

\section{A Taylor-formula és a ,,nagyordó''-jelölés}

\textbf{Egyváltozós Taylor-formula.} A véges differencia-módszer elemzéséhez Taylor-polinomok komoly manipulációjára van szükség. 
Először idézzük fel az egyváltozós Taylor-formula Lagrange-féle maradéktagos alakját.
\begin{btheorem}
Legyen $\Map{u}{\Real}{\Real}$ $r+1$-szer folytonosan differenciálható az $a\in\Real$ pontban. Ekkor minden $x\in\Real$
ponthoz létezik egy $\xi\in(a,a+x)$ szám, amellyel
\[
u(x)=u(a)+u'(a)(x-a)+\frac{1}{2!}u''(a)(x-a)^2+\ldots+\frac{1}{r!}u^{(r)}(a)(x-a)^r+\frac{1}{(r+1)!}u^{(r+1)}(\xi)(\xi-a)^{r+1}
\]
minden $x\in\Real$ esetén.
\end{btheorem}
Az $x=a+h$ változócserével kapjuk az
\begin{equation}\label{tayexp}
u(a+h)=u(a)+u'(a)h + \frac{1}{2!} u''(a)h^2 +\ldots+\frac{1}{r!}u^{(r)}(a)h^r+\frac{1}{(r+1)!}u^{(r+1)}(\xi)(\xi-a)^{r+1}
\end{equation}
alakot. 

\textbf{A Landau-szimbólum.} Az iménti maradéktagot ritkán szokták ilyen explicit alakban ,,cipelni'' az átalakítások során, mivel a $u^{(r+1)}(\xi)$ helyettesítési érték általában ismeretlen,
mivel $\xi$-ről nem tudunk semmit (csak azt hogy létezik). Ezért bevezetik a következő
roppant hasznos, ún. ,,\textbf{nagyordót}'' vagy \textbf{Landau-szimbólumot.}
\begin{bdefn}
Legyen $\Map{g}{\Real}{\Real}$ tetszőleges függvény. Valamilyen $a\in\Real$ véges határérték esetén legyen
\[
\mathcal{O}(g)=\{\Map{f}{\Real}{\Real} \;:\; |f(x)|\le C |g(x)|,\;\;\text{ha}\;\; |x-a|<\delta \;\;(\exists\delta,C>0)\}
\]
$+\infty$ határérték esetén,
\[
\mathcal{O}(g)=\{\Map{f}{\Real}{\Real} \;:\; |f(x)|\le C |g(x)|,\;\;\text{ha}\;\; x\ge M \;\;(\exists M,C>0)\},
\]
és hasonlóan a $-\infty$ esetében. 
\end{bdefn}
A határértéket mindig fel kell tűntetni az $\mathcal{O}$ jelölés használata közben, vagy világosnak kell lennie a kontextusból.
A $\mathcal{O}$-jelöléssel kapcsolatban a fő zavart a következő jelölésbeli abúzus adja. Az $f\in\mathcal{O}(g)$ tartalmazásra
az ,,$f=\mathcal{O}(g)$'' szimbólumot írják. Ügyelnünk kell arra, hogy ez az egyenlőségjel \emph{nem szimmetrikus}, mivel $x=\mathcal{O}(x^2)$ ($x\to\infty$) nyilvánvalón igaz, de
$\mathcal{O}(x^2)=x$ nem. Az $\mathcal{O}$ halmazra érvényesek a következő állítások. 
\begin{itemize}
\item Ha $f_1\in\mathcal{O}(g_1)$ és $f_2\in\mathcal{O}(g_2)$, akkor $f_1f_2\in\mathcal{O}(g_1g_2)$.
\item $f\mathcal{O}(g)=\mathcal{O}(fg)$, speciálisan minden $c\neq 0$ konstansra $\mathcal{O}(cg)=c\mathcal{O}(g)$.
\item  Ha $f_1\in\mathcal{O}(g_1)$ és $f_2\in\mathcal{O}(g_2)$, akkor $f_1+f_2\in\mathcal{O}(|g_1|+|g_2|)$.
\item Ha $f\in\mathcal{O}(g)$, akkor $cf\in\mathcal{O}(g)$ minden $c\in\Real$ konstansra.
\item Ha $\Big|\dfrac{f(x)}{g(x)}\Big|\to C<+\infty$ ($x\to a$), akkor $f\in\mathcal{O}(g)$.
\end{itemize}
Az $\mathcal{O}$ jelölést mi leggyakrabban két mennyiség összehasonlítására fogjuk használni. Az $F=f+\mathcal{O}(g)$ jelölés alatt azt értjük, hogy $F-f\in\mathcal{O}(g)$.


Visszatérve a Taylor-polinomokhoz, \az{\eqref{tayexp}} formulát az
\[
u(a+h)=u(a)+u'(a)h + \frac{1}{2!} u''(a)h^2 +\ldots+\frac{1}{r!}u^{(r)}(a)h^r+\mathcal{O}(h^{r+1})
\]
kényelmes formában írhatjuk. 

\textbf{Többváltozós Taylor-formula.} Legyen $\Map{u}{\Rn}{\Real}$ skalárfüggvény. Ennek deriváltjai egy $a\in\Rn$ pontban
\begin{align*}
Du(a)&\in L(\Rn,\Real)\\
D^2u(a)&\in L(\Rn,L(\Rn,\Real))\\
D^3u(a)&\in L(\Rn,L(\Rn,L(\Rn,\Real)))\\
&\vdots
\end{align*}
ahol $L(X,Y)$ jelöli az $X$-ből $Y$-ba képező lineáris leképezések vektorterét. Világos, hogy $L(\Rn,\Real)\iso \Rn$. Továbbá $L(\Rn,L(\Rn,\Real))\iso \Real^{n\times n}$, valamint 
$L(\Rn,L(\Rn,L(\Rn,\Real)))\iso \Real^{n\times n\times n}$. A megfelelő mátrixok elemei a standard bázisban pedig, $(Du(a))_i=D_iu(a)$, $(D^2u(a))_{ij}=D_{ij}u(a)$ és $(D^2u(a))_{ijk}=D_{ijk}u(a)$.
Másképpen fogalmazva, $Du(a)$ \emph{lineáris} funkcionál (magtere egy hipersík), $D^2u(a)$ \emph{bilineáris funkcionál}, $D^3u(a)$ \emph{trilineáris fukncionál}, s.í.t.
Ennélfogva a deriváltak többparaméteres skalárfüggvények: helyettesítési értékük 
\[
Du(a)(v),\; D^2u(a)(v_1,v_2),\; D^3u(a)(v_1,v_2,v_3),\;\text{stb.}
\]

A többváltozóa Taylor-formula ennek megfelelően
\begin{btheorem}
Legyen $\Map{u}{\Rn}{\Real}$ $r+1$-szer folytonosan differenciálható az $a\in\Rn$ pontban. Ekkor
\[
u(a+h)=u(a)+Du(a)(h)+\frac{1}{2!}D^2u(a)(h,h)+\ldots+\frac{1}{r!}D^ru(a)(h,\ldots,h)+\mathcal{O}(|h|^{r+1})
\]
minden $h\in\Rn$ esetén.
\end{btheorem}
A tagokat expliciten is fel lehet írni konkrét bázisban, pl. $n=2$ esetén a másodrendű tag
\[
\frac{1}{2!}D^2u(a)(h,h)=\frac{1}{2} D^2u(a)h\cdot h=\frac{1}{2}\big( D_{11}u(a)h_1^2 + 2D_{12}u(a)h_1h_2 + D_{22}u(a)h_2^2 \big).
\]
A konkrét számolásoknál megpróbáljuk elkerülni, hogy expliciten ki kelljen fejteni a tagokat.

\section{Elliptikus operátorok és peremértékfeladatok}

\textbf{Divergencia-típusú operátorok.} Az olyan $L$ lineáris elliptikus operátort, amely
\[
Lu=-\dive (\mathbf{A}(x)Du)+\mathbf{b}(x)\cdot u+c(x)u=\sum_{i,j=1}^n D_i(a_{ij}(x)D_ju) + \sum_{i=1}^n b_i(x)D_iu + c(x)u
\]
alakú, \textbf{divergencia-típusúnak} nevezzük, ahol $\Map{\mathbf{A}}{\Omega}{\Real^{n\times n}}$ szimmetrikus, folytonos
és egyenletesen pozitív. Azért kiemelt az ilyen alakú operátorok szerepe, mert a $-\dive(\mathbf{A}(x)Du)=f$, $\restr{u}{\partial\Omega}=0$ \textbf{klasszikus} feladatból
a Green-tétel alkalmazásával a 
\[
\forall v\in H^1_0(\Omega) \;:\; \int_\Omega \mathbf{A}(x)Du\cdot Dv\,dx=\int_\Omega fv\,dx
\]
\textbf{gyenge-} (vagy \textbf{variációs-}) \textbf{alakot} kapjuk. Ennek $u\in H^1_0(\Omega)$ megoldása az $f$-re, az $\mathbf{A}$-ra és az $\Omega$-ra tett bizonyos megszorítások
esetén a klasszikus feladatnak is megoldása, azaz a $H^1$-simaságot $H^2$-, vagy $C^2$-simasággá tudjuk javítani. Az ilyen eredményeket \textbf{reguláritási tételeknek} nevezzük.
Erre példa Kadlec tétele, lásd az előadásjegyzet 1.5. Tételét.

\textbf{Nem-divergenciatípusú operátorok.} Tegyük fel, hogy a $\widetilde{L}$ lineáris elliptikus operátor
\[
\widetilde{L}u=\widetilde{\mathbf{A}}(x):D^2u+\widetilde{\mathbf{b}}(x)\cdot Du+c(x)=\sum_{i,j=1}^n \widetilde{a}_{ij}(x)D_{ij}u + \sum_{i=1}^n \widetilde{b}_i(x)D_iu+c(x)u
\]
alakú, ahol $\Map{\mathbf{A}}{\Omega}{\Real^{n\times n}}$ szimmetrikus és egyenletesen pozitív, bár nem feltétlenül sima.
Az $\widetilde{L}$ operátor divergenciaalakú, ha $\widetilde{\mathbf{A}}$ elég sima, illetve az $L$ operátor az iménti alakú, ha ,,ki lehet fejteni a divergenciát'', azaz
$\mathbf{A}$ elég sima. A nem-divergenciatípusú operátorok elméletét J. Schauder dolgozta ki, lásd Gilbarg--Trudinger.

\Exercises
\begin{Exercise}
Vizsgáljuk meg, hogy hogyan viselkedik az $L^p$-norma a 
\[
\Map{\delta_\lambda}{L^p(\Rn)}{L^p(\Rn)},\;\; (\delta_\lambda f)(x)=f(\lambda x)\;\; (\lambda>0, \;f\in L^p(\Rn))
\]
\textbf{dilatációra nézve}. [Segítség: Azt kell kapnunk, hogy $\|\delta_\lambda f\|_{L^p(\Rn)}=\lambda^{-n/p}\|f\|_{L^p(\Rn)}$.]
\end{Exercise}

\begin{Exercise}
Bizonyítsuk be \az{\ref{lpincl}}. Állítást.
\end{Exercise}

\begin{Exercise}
Az $|\Omega|<\infty$ feltevés lényeges, amint azt az $f(x)=\frac{\sin x}{x}$ függvény mutatja,
 erre ugyanis $f\notin L^1(\Real)$ ám $f\in L^2(\Real)$.
\end{Exercise}

\begin{Exercise}
Határozzuk meg azokat az $\alpha>0$ kitevőket, amelyre az $x\mapsto|x|^{-\alpha}$ függvény 
(i) lokálisan $p$-integrálható, azaz minden kompakt halmazon
$p$-integrálható, illetve 
(ii) a végtelenben $p$-integrálható, azaz $L^p(\Rn\setminus B(0,\epsilon))$-beli valamely $\epsilon>0$ esetén. 
\end{Exercise}

\begin{Exercise}
Az egyszerűség kedvéért legyen most $\Omega=\Rn$, és számítsuk ki a $\delta_\lambda$ dilatáció hatását a $H^1$ Szoboljev-normára és -félnormára nézve.
[Segítség: Azt kell kapnunk, hogy $|\delta_\lambda u|_{H^1}=\lambda^{n/2-1}|u|_{H^1}$.]
\end{Exercise}

\begin{Exercise}
Mutassuk meg, hogy az alábbi normák ekvivalensek $\|\cdot\|_{H^1(\Omega)}$-val:
\begin{align*}
\|u\|'&=\|u\|_{L^2(\Omega)}+\|Du\|_{L^2(\Omega,\Rn)}\\
\|u\|''&=\max\{\|u\|_{L^2(\Omega)},\|Du\|_{L^2(\Omega,\Rn)}\}
\end{align*}
\end{Exercise}

\begin{Exercise}
Bizonyítsuk be a Poincaré--Neumann-egyenlőtlenséget feltéve, hogy érvényes a Poincaré-egyenlőtlenség.
\end{Exercise}
% \begin{proof}
% Legyen $u\in H^1(\Omega)$ rögzített. A háromszög-egyenlőtlenség alapján
% \[
% \|u\|_{L^2(\Omega)}\le \|u-u_\Omega\|_{L^2(\Omega)} + \|u_\Omega\|_{L^2(\Omega)}.
% \]
% Az első tag becsülhető a Poincaré-egyenlőtlenséggel. A második tagban pedig egyszerűen csak egy konstans normáját 
% képezzük, így
% \begin{align*}
% \|u\|_{L^2(\Omega)}\le C_\Omega |u|_{H^1(\Omega)} + \frac{\sqrt{|\Omega|}}{|\partial\Omega|}\Big|\int_{\Omega} u_\Omega\Big|,
% \end{align*}
% amelyből következik a kívánt állítás a szokásos konvenció szerint: $C_\Omega$ egy általános, $\Omega$-tól függő konstanst jelöl.
% \end{proof}

\begin{Exercise}
Bizonyítsuk be, hogy a
\[
\left.
\begin{aligned}
\lapl u &=0 \;\;(\Omega\text{-n})\\
D_\nu u&=0 \;\; (\partial\Omega\text{-n})
\end{aligned}
\right\}
\]
feladat egy $u$ megoldására (itt $\nu(x)$ jelöli a kifelé mutató normálvekort egy $x\in\partial\Omega$ perempontban) szükségképpen $u_\Omega=0$
teljesül.
\end{Exercise}

\begin{Exercise}
Lássuk be, hogy $\mathring{H}^1(\Omega)$ merőleges kiegészítője (az $L^2$-skalárszorzatra nézve) a konstans függvények alterének.
\end{Exercise}
