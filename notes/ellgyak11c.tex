
\emph{Ez a gyakorlat teljes egészében opcionális, mivel olyan elméletet tartalmaz, amely nem része a tananyagnak. Főleg a becslések nehezek, ezeket nem
is tervezzük elmondani gyakorlaton.}

\section{Absztrakt additív Schwarz-prekondicionálók}

\begin{figure}
\begin{center}
\includegraphics[scale=0.5]{fig/grid1.eps}
\includegraphics[scale=0.5]{fig/grid2.eps}
\includegraphics[scale=0.5]{fig/grid3.eps}
\includegraphics[scale=0.5]{fig/grid4.eps}
\end{center}
\caption{Az egységkörlap egy durva $\mathcal{T}_1$ triangulációjának szukcesszív negyedelésével kapott $\mathcal{T}_2$, $\mathcal{T}_3$, $\mathcal{T}_4$ triangulációk.}
\end{figure}

\emph{A szakaszt a Brenner--Scott könyv 7. fejezete alapján írtuk.}

Legyen $V$ véges dimenziós vektortér, $V^*$ a duálisa. Mivel $V$ véges dimenziós,
$V$ valójában \emph{izomorf} $V^*$-gal, de mégis érdemes megkülönböztetni a két teret. Jelölje $\dua{}{}$ a duális párosítást $V$ és $V^*$ között, azaz
minden $v\in V$ és $\lambda\in V^*$ esetén $\dua{\lambda}{v}=\lambda(v)$. Ez tehát egy bilineáris funkcionál $V\times V^*$-on.
Azt mondjuk, hogy egy $\Map{\op{A}}{V}{V^*}$ lineáris leképezés szimmetrikus pozitív definit, ha $\dua{\op{A}v}{w}=\dua{\op{A}w}{v}$ ($v,w\in V$) és $\dua{\op{A}v}{v}>0$ ($v\neq 0$) teljesül.
Hasonlóan definiálható egy $\Map{\op{B}}{V^*}{V}$ leképezés szimmetrikus pozitív definitsége.

Gondoljunk bele egy pillantra, hogy mivel $V$ véges dimenziós, azonosítható $\Real^d$-nel valamilyen $d\ge 1$-re. Ezért az $\dua{}{}$ párosítás valójában a skaláris szorzat $\Real^d$-ben.

Legyen $\Map{\op{A}}{V}{V^*}$ szimmetrikus pozitív definit. Legyenek 
\[
V_0,V_1\ldots,V_n\subset V
\]
alterek és legyen $\Map{\op{B}_k}{V_k}{V_k^*}$ szimmetrikus pozitív definit ($0\le k\le n$).
Jelölje 
\[
\Map{\iop_k}{V_k}{V}
\]
a $V_k$ altér természetes beágyazását $V$-be, azaz $\iop_kv=v$ minden $v\in V_k$ esetén (szokás $\iop_k$-t prolongációs- vagy interpolációs operátornak is nevezni).
Ennek transzponáltja, 
\[
\Map{\iop_k^\trans}{V^*}{V_k^*}
\]
amelyet az $\dua{\iop_k^\trans\lambda}{v}=\dua{\lambda}{\iop_ku}$ ($\lambda\in V$, $v\in V_k$) reláció definiál (szokás ezt restrikciós operátornak is nevezni).
Tételezzük fel, hogy
\[
V=\sum_{k=0}^n \iop_kV_k.
\]

Az ún. \textbf{absztrakt additív Schwarz prekondicionáló} a $\Map{\op{B}}{V^{*}}{V}$ leképezés, amelyre
\[
\op{B}=\sum_{k=0}^n \iop_k\op{B}_k^{-1}\iop_k^\trans.
\]
Erre a következő, könnyen belátható tulajdonságok érvényesek.
\begin{enumerate}[(1)]
\item A $\Map{\op{B}}{V^*}{V}$ leképezés szimmetrikus pozitív definit, és ezért $\Map{\op{B}^{-1}}{V^*}{V}$ is.
\item Bármely $u\in V$ esetén
\[
\dua{\op{B}^{-1}v}{v}=\min\Big\{\sum_{k=0}^n \dua{\op{B}_kv_k}{v_k} \;:\; v=\sum_{k=0}^n \iop_kv_k, \; v_k\in V_k \Big\}.
\]
\item A $\Map{\op{B}\op{A}}{V}{V}$ leképezés sajátértékei pozitívak és 
\begin{align*}
\lambda_{\max}(\op{B}\op{A})&=\max_{\substack{v\in V\\v\neq 0}} \frac{\dua{\op{A}v}{v}}{\dua{\op{B}^{-1}v}{v}}\\
\lambda_{\min}(\op{B}\op{A})&=\min_{\substack{v\in V\\v\neq 0}} \frac{\dua{\op{A}v}{v}}{\dua{\op{B}^{-1}v}{v}}
\end{align*}
\end{enumerate}

Az iménti jelöléseket használva a Poisson-egyenlet végeselem-diszkretizációja során egy $\Map{\op{A}}{V}{V^*}$ leképezést konstruálunk meg egy konkrét bázisban 
(a félév során a $\{\phi_x\}$ csomóponti bázisban), amelyre
\[
\forall u,v\in V \;:\; \dua{\op{A}u}{v}=a(u,v)=\int_\Omega Du\cdot Dv\,dx.
\]
A szokásos $\ell(w)=\int_\Omega fw\,dx$ ($w\in V$) funkcionált bevezetve látszik, hogy a
\[
\op{A}u=\ell \;\; (V^*\text{-ban})
\]
absztrakt operátoregyenletet oldjuk meg egy konkrét bázisban: ez egyszerűen az $Au=b$ egyenletrendszer, ahol $A$ a merevségi mátrix és $b$ a terhelési vektor. Ezek persze ekvivalensek a
\[
\forall v\in V \;:\; a(u,v)=\ell(v)
\]
variációs egyenlettel. Korábban láttuk, hogy az $\op{A}$ operátor kondíciószáma ,,elszáll'', ahogyan a $h\to 0$.
 Ebben az absztrakt kontextusban a prekondicionálás feladata tehát egy olyan olcsón kiszámítható
$\Map{\op{B}}{V^*}{V}$ prekondicionáló operátor megkonstruálása $\op{A}$-hoz, amelyre a
\[
\kappa(\op{B}\op{A})=\frac{\lambda_{\max}(\op{B}\op{A})}{\lambda_{\min}(\op{B}\op{A})}
\]
kondíciószám lehetőleg független a $h_n$ rácsmérettől vagy legalábbis lassan növekszik $h_n$ függvényében. 
Ehhez tehát $\lambda_{\min}(\op{B}\op{A})$-t alulról és $\lambda_{\max}(\op{B}\op{A})$-t felülről kell becsülni.
A különböző eljárásokhoz tartozó becsléseket nem fogjuk bebizonyítani, csak közöljük; a bizonyításokért lásd az idézett Brenner--Scott könyvet.

\section{A csomóponti bázis}\label{nodalbasis}
Legyen $\Omega\subset\Real^2$ korlátos sokszögletű tartomány és tekintsük
\[
\mathcal{T}_1,\ldots,\mathcal{T}_n
\]
háromszögrácsok egy olyan sorozatát, amelyben $\mathcal{T}_k$ elemeit a $\mathcal{T}_{k-1}$ minden háromszögének szabályos felnegyedelésével kapjuk (szokás ezt diadikus felosztásnak is nevezni).
\textbf{Itt $\mathcal{T}_1$ a ,,bemeneti'' rács és a megoldást a $\mathcal{T}_n$ rácson fogjuk megkonstruálni.} Könnyen látható, hogy ha $h_k=\diam_{T\in\mathcal{T}_k} T$, akkor
$h_k=2^{n-k}h_n$ minden $1\le k\le n$ esetén. Jelölje
\[
\emptyset=\Omega_0\subset\Omega_1\subset\ldots\subset\Omega_n
\]
a rácsok csúcsainak sorozatát. Ha a $V_k$ tereket most a
\[
V_{k}=\Span\{\phi_x^{k}\;:\; x\in\Omega_{k}\}
\]
egyenlet szerint választjuk, akkor
\[
V_1\subset\ldots\subset V_n
\]
teljesül. Ezt a választást \textbf{csomóponti bázisnak nevezzük}.

\begin{figure}
\begin{center}
\includegraphics[scale=0.8]{fig/refine.pdf}
\end{center}
\caption{A csomóponti bázis konstrukciója.}
\label{nodalbase}
\end{figure}

Vizsgáljuk meg a $V_1,\ldots,V_n$ alterek bázisai közötti kapcsolatot. Egy rögzített $x\in\Omega_{k-1}$ pont esetén a konstrukció szerint $x\in\Omega_k$, és ha $\{y_i\}\subset\Omega_k$ jelöli
$x$ csúcs-környezetét, akkor a $\phi^{k-1}_x$ durva bázisfüggvény a
\[
\phi^{k-1}_x=\phi_x^k+\frac{1}{2}\sum_i \phi_{y_i}^k\in V_k\tag*{$(*)$}
\]
alakban írható fel a finom bázisban.

Az iménti $(*)$ felírásból következik, hogy $V_{k-1}\subset V_k$ altér. Ebből kiindulva, rendezzük úgy sorba a $\{\phi_x^k\}_{x\in\Omega_k}=\{\phi_i^k\}_{i=1}^{N_k}$ bázist, hogy előre a
$x\in\Omega_{k-1}$-hez tartozó (,,régi'') elemeket, majd az $x\in\Omega_k\setdif\Omega_{k-1}$-hez tartozókat rakjuk; lásd \az{\ref{nodalbase}}. ábrát.
Ebben bázisban az $\Map{\iop_{k-1}^k}{V_{k-1}}{V_k}$ interpolációs operátor mátrixa
\[
I_{k-1}^k=\begin{pmatrix}
1 &   &    \\
     & \ddots &  \\
     &  & 1 \\
     & H_k &
\end{pmatrix}\in \Real^{N_k\times N_{k-1}},
\]
ahol $H_k\in \Real^{(N_k-N_{k-1})\times N_{k-1}}$ olyan mátrix, amelyre
\[
\forall x\in\Omega_k\setdif\Omega_{k-1}\;\forall y\in\Omega_{k-1} \;:\; (H_k)_{xy}=\begin{cases}
\frac{1}{2}, & \text{ha $x$ és $y$ szomszédosak} \\
0, & \text{egyébként}
\end{cases}
\]
Ezek alapján már fel lehet írni az $\Map{\iop_k}{V_k}{V_n}$ operátor mátrixát, elvégre
\[
I_k=I_{n-1}^nI_{n-2}^{n-1}\cdots I_k^{k+1}\in\Real^{N_n\times N_k}.
\]
Hasonlóan írható fel az $\Map{\iop_k^\trans}{V_n^*}{V_k^*}$ operátor mátrixa:
\[
I_k^\trans=(I_k^{k+1})^\trans\cdots (I_{n-2}^{n-1})^\trans (I_{n-1}^n)^\trans \in\Real^{N_k\times N_n}.
\]

\section{A HB-prekondicionáló}
Az iménti csomóponti bázis helyett vezessük be az ún. \textbf{hierarchikus bázist}. Legyen
\[
W_k=\Span \{\phi^k_y\;:\;y\in \Omega_k\setdif\Omega_{k-1}\}=\{v\in V_k \;:\; v(x)=0 \;\;(\forall x\in\Omega_{k-1})\}.
\]
Ekkor az előző szakaszban bevezetett $V_k=\Span\{\phi_x^{k}\;:\; x\in\Omega_{k}\}$ alterekre
\[
V_k=V_{k-1}\oplus W_k
\]
teljesül. Ennélfogva érvényes a következő direktösszeg-felbontás, ahol $W_1:=V_1$.
\begin{prop}
$V_n=W_1\oplus\ldots\oplus W_n$
\end{prop}
\begin{proof}
Emlékeztetünk a $\Map{\Pi_k}{C(\overline{\Omega})}{V_k}$ interpolációs operátorra, amelyet a
\[
\forall v\in C(\overline{\Omega}) \;:\; \Pi_kv=\sum_{x\in\Omega_k} v(x)\psi_x^k
\]
utasítással definiáltunk. Az jobb oldali összeget szétszedve $\Omega_{k-1}$ és $\Omega_k\setdif\Omega_{k-1}$ fölöttiekre kapjuk, hogy
\[
\Pi_kv=\sum_{x\in\Omega_{k-1}} v(x)\psi_x^{k-1} + \sum_{x\in\Omega_k\setdif\Omega_{k-1}} v(x)\phi^k_x=\Pi_{k-1}v + \sum_{x\in\Omega_k\setdif\Omega_{k-1}} v(x)\phi^k_x.
\]
Ezek alapján indukcióval kapjuk, hogy
\begin{align*}
\forall v\in V_n \;:\; v&=\Pi_1v + (\Pi_2-\Pi_1)v + (\Pi_3-\Pi_2)v + \ldots + (\Pi_{n}-\Pi_{n-1})v\\
&=\sum_{k=1}^{n} (\Pi_{k}-\Pi_{k-1})v,
\end{align*}
ahol felhasználtuk, hogy $\Pi_nv=v$ és egyúttal a $\Pi_0v=0$ jelölést is bevezettük, amely összhangban van az $\Omega_0=\emptyset$ konvencióval. Ezzel igazoltuk az állítást.
\end{proof}

Legyen $\Map{\iop_k}{W_k}{V_n}$ a természetes beágyazás. Legyen $\Map{\op{B}_k}{W_k}{W_k^*}$ a
\[
\forall v,w\in W_k \;:\; \dua{\op{B}_kv}{w}=\sum_{x\in \Omega_k\setminus\Omega_{k-1}} v(x)w(x)
\]
utasítással megadva. Az $\op{A}_n$ operátorhoz tartozó ún. \textbf{hierarchikus bázis prekondicionáló} egy $\Map{\op{B}_{HB}}{V_n^*}{V_n}$ leképezés,
amelyre
\[
\op{B}_{HB}=\sum_{k=1}^n \iop_k \op{B}_k^{-1} \iop_k^\trans.
\]
Hosszadalmas számolással beláthatóak a
\[
\lambda_{\min}(\op{B}_{HB}\op{A}_n)\ge \frac{C_1}{1+|\log h_n|^2}
\]
és a
\[
\lambda_{\max}(\op{B}_{HB}\op{A}_n)\le C_2
\]
becslések. Ebből következik, hogy létezik egy $C>0$ $n$-től és $h_n$-től \emph{független konstans}, amellyel
\[
\kappa(\op{B}_{HB}\op{A}_n)=\frac{\lambda_{\max}(\op{B}_{HB}\op{A}_n)}{\lambda_{\min}(\op{B}_{HB}\op{A}_n)}\le C(1+|\log h_n|^2).
\]

\section{A BPX-prekondicionáló}

Most legyen $\Map{\iop_k}{V_k}{V_n}$ a természetes beágyazás és legyen
$\Map{\op{B}_k}{V_k}{V_k^*}$ az alábbi szimmetrikus pozitív definit operátor:
\[
\forall v,w\in V_k \;:\; \dua{\op{B}_kv}{w}=\sum_{x\in\Omega_k} v(x)w(x).
\]
Ezzel az $\op{A}_n$ operátorhoz tartozó \textbf{Bramble--Pasciak--Xu prekondicionáló} egy $\Map{\op{B}_{BPX}}{V_n^{*}}{V_n}$ leképezés, amelyre
\[
\op{B}_{BPX}=\sum_{k=1}^N \iop_k \op{B}_k^{-1} \iop_k^\trans.
\]
Erre beláthatóak a
\[
\lambda_{\max}(\op{B}_{BPX}\op{A}_n)\le C_1
\]
és
\[
\lambda_{\min}(\op{B}_{BPX}\op{A}_n)\ge C_2
\]
becslések, amiből következik a rácsfüggetlen
\[
\kappa(\op{B}_{BPX}\op{A}_n)=\frac{\lambda_{\max}(\op{B}_{BPX}\op{A}_n)}{\lambda_{\min}(\op{B}_{BPX}\op{A}_n)}\le C
\]
becslés.

\section{Implementáció}

A MATLAB \verb!pcg! prekondicionált konjugált gradiens függvénye lehetővé teszi, hogy a prekondicionálót egy külső függvényként adjuk meg. 
Emlékeztetünk, hogy a \verb!pcg! függvény az
\[
M^{-1}Ax=M^{-1}b
\]
egyenletrendszert oldja meg. Az általunk megadható \verb!mfun! paraméter egy olyan függvény, amely kiszámítja az $M^{-1}v$ értékét tetszőleges $v$ vektorra.
Ez azért hasznos, mert lehetőségünk nyílik $M^{-1}v$ gyorsabb meghatározására.

Mi egy másik megközelítést alkalmaztunk, és a prekondicionált egyenletrendszerünk
\[
BAx=Bb
\]
alakú, ahol $B$ a $\op{B}$ prekondicionálóoperátor mátrix-reprezentációja. Ennélfogva nekunk az \verb!mfun! függvényünkben a $Bv$ szorzatot kell gyorsan kiértékelnünk.

\textbf{BPX.} Csomóponti bázisban fogunk dolgozni; a $V_n$ legfinomabb szinten a szokásos $\{\phi_x\}_{x\in\Omega_n}$ kalapfüggvények alkotják a bázist, a \verb!tris! háromszögrácsnak megelelő sorbarendezésben.
Világos, hogy a durvább $V_1,\ldots,V_{n-1}$ rácsokat nem kell közvetlenül eltárolnunk: elég az $\Map{I_k}{V_k}{V_n}$ interpolációs mátrixot megkonstruálnunk. 
Ennek hatása egy $v\in V_k$ vektoron
\[
I_kv=\sum_{x\in\Omega_k} v(x)I_k\phi_x^k,
\]
ezért elég az $I_k\phi_x^k$ vektort meghatároznunk minden $1\le k\le n-1$ és $x\in\Omega_k$ esetén.
Ha most $\op{B}_k=I$, akkor 
\begin{align*}
B_{BPX}v=\sum_{k=1}^N I_kI_k^\trans v=\sum_{k=1}^N \sum_{x\in\Omega_k} (I_k^\trans v)(x)I_k\phi_x^k=\sum_{k=1}^N \sum_{x\in\Omega_k}\sum_{y\in\Omega_n} v(y)(I_k^\trans\phi_y)(x)I_k\phi_x^k
\end{align*}

A \verb!mesh2d! könyvtár \verb!refine! függvénye éppen a megfelelő háromszögrács-finomítást hajtja végre; sőt a finomabb rácson megörződnek a durvább rácson lévő csúcsok indexei.
Ennélfogvan a \verb!verts! tömb \az{\ref{nodalbasis}}. szakaszban leírt sémának megfelelően van indexelve: először a durva, majd a finom csúcsok szerepelnek.

\begin{algorithmic}
\STATE \boxed{\textbf{BPX}} -- \emph{A BPX prekondicionáló hatásának kiszámítása $\mathcal{O}(n)$ időben} 
\STATE \textbf{INPUT:} $v$ tetszőleges és az $I_{k-1}^k\in\Real^{N_k\times N_{k-1}}$ interpolációs mátrixok minden $1\le k\le n$-re.
\STATE \textbf{OUTPUT:} A $B_{BPX}v$ vektor.
\STATE Legyen $r_n=v$ \hfill \emph{Reziduális}
\FOR{$k=n\ldots 2$}
\STATE $e_{k}=B_k^{-1}r_k$ \hfill \emph{Hiba}
\STATE $r_{k-1}=(I_{k-1}^k)^\trans r_k$
\ENDFOR
\STATE Legyen $e_1=B_1^{-1}r_1$ \hfill \emph{Hibaegyenlet egzakt megoldása a durva rácson}
\FOR{$k=2\ldots n$}
\STATE  $e_k=e_k + I_{k-1}^k e_{k-1}$ \hfill\emph{Hiba interpolációja}
\ENDFOR
\STATE Legyen $B_{BPX}v=e_n$
\end{algorithmic}

\Exercises
\begin{Exercise}
Bizonyítsuk be, hogy a $k$-adik rácson felírt $\Map{\op{A}_k}{V_k}{V_k^*}$ és a legfinomabb rácson felírt $\Map{\op{A}_n}{V_n}{V_n^*}$
operátorokra érvényes az ún. Galjorkin-tulajdonság:
\[
\op{A}_k=\iop_k^\trans\op{A}_n\iop_k.
\]
\end{Exercise}
