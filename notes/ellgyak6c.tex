\section{Mátrix-összefűzés a $T_3$ elemekhez}
A végeselem-módszer receptje szerint adott $\Map{a(\ph,\ph)}{V\times V}{\Real}$ bilineáris forma és $\Map{\ell}{V}{\Real}$ lineáris funkcionál esetén
meg kell oldanunk a vetületi egyenlettel ekvivalens
\begin{equation}\label{femla}
\sum_{\fie{y}\in\overline{\Omega}_h} a(\phi_{\fie{y}},\phi_{\fie{x}})c_{\fie{y}}=\ell(\phi_{\fie{x}})\;\;(\fie{x}\in\overline{\Omega}_h)
\end{equation}
lineáris algebrai egyenletrendszert az ismeretlen $\{c_{\fie{x}}\}$ együtthatók meghatározásához, és ekkor a közelítő megoldás
\[
u_h(\fie{x})=\sum_{\fie{y}\in\overline{\Omega}_h}c_{\fie{y}}\phi_{\fie{y}}(\fie{x}).
\]
Az előző gyakorlaton meghatároztuk a $T_3$ Courant-elemekhez tartozó $\{\phi_{\fie{x}}\}$ bázist. 
Világos, hogy az $\fie{x}\in\overline{\Omega}_h$ csúcsok megfelelő index-leképezésével megkapható egy $\{\phi_j\}_{j=1}^N$ indexelés is, így \az{\eqref{femla}}
az ismerős
\[
\sum_{j=1}^N a(\phi_j,\phi_i)c_j=\ell(\phi_i)\;\;(i=1,\ldots,N)
\]
alakot ölti.

Két lehetőség mutatkozik \az{\eqref{femla}} egyenletekben szereplő $a(\phi_{\fie{y}},\phi_{\fie{x}})$ és $\ell(\phi_{\fie{x}})$
mennyiségek meghatározására, más szóval az $A^h=(a(\phi_j,\phi_i))_{i,j}$ \textbf{merevségi mátrix} és az $F^h=(\ell(\phi_i))_i$ \textbf{terhelési vektor} összefűzésére.
\footnote{Az elnevezések a végeselem-módszer mérnöki gyökereiből erednek; angolul \emph{stiffness matrix} és \emph{load vector}.}
\begin{center}
\includegraphics[scale=1.3]{fig/phixdisj.pdf}
\end{center}
\begin{enumerate}[(A)]\indented
\item \textbf{Csúcsonkénti összefűzés.} Minden $\fie{x},\fie{y}\in\overline{\Omega}_h$ rácspontpár esetén meghatározzuk $a(\phi_{\fie{y}},\phi_{\fie{x}})$-et és $\ell(\phi_{\fie{x}})$-et.
 Az ábrán háromszögrácsokon láthatóak különböző pontok kalapfüggvényeinek tartói és sötétítve azok metszetei.
Az világos, hogy $\supp \phi_{\fie{y}}\cap\supp \phi_{\fie{z}}=\emptyset$ és $\supp \phi_{\fie{x}}\cap\supp \phi_{\fie{z}}=\emptyset$, így 
$a(\phi_{\fie{y}},\phi_{\fie{z}})=0$ és $a(\phi_{\fie{z}},\phi_{\fie{x}})=0$. Viszont $\phi_{\fie{x}}$ és $\phi_{\fie{y}}$ tartói összemetszenek.

A fő hátránya a \emph{naív} csúcsonkénti összefűzésnek, hogy meg kell meghatározni azokat a $T\in\mathcal{T}_h$ háromszögeket,
amelyek a tartók metszetét alkotják.

\item \textbf{Elemenkénti összefűzés.} Vegyük észre, hogy ha megállapodunk abban, hogy a $\lambda_{T,\fie{x}}$ baricentrikus függvényeket azonosan 0-nak kiterjesztjük a $T$-n kívül, akkor
azt írhatjuk, hogy
\[
\phi_{\fie{x}}=\sum_{T\in\Star(\fie{x})} \lambda_{T,\fie{x}}.
\]
Az ilymódon kiterjesztett $\lambda_{T,\fie{x}}$ függvényeket \textbf{alakfüggvényeknek nevezzük}. Ezzel
\[
a(\phi_{\fie{y}},\phi_{\fie{x}})=\sum_{T\in\Star(\fie{x})}\sum_{S\in\Star(\fie{y})} a(\lambda_{S,\fie{y}},\lambda_{T,\fie{x}})=\sum_{T\in\Star(\fie{x})\cap\Star(\fie{y})} a(\lambda_{T,\fie{x}},\lambda_{T,\fie{y}}).
\]
\emph{Kifordítva a dolgot:}
 minden $T\in\mathcal{T}_h$ elem esetén összegyűjtve az $a(\lambda_{T,\fie{x}},\lambda_{T,\fie{y}})$ járulékokat ($\fie{x},\fie{y}\in\mathcal{V}(T)$), végül megkapjuk $a(\phi_{\fie{y}},\phi_{\fie{x}})$-t a fenti képlet szerint.
Hasonlóan,
\[
\ell(\phi_{\fie{x}})=\sum_{T\in\Star(\fie{x})} \ell(\lambda_{T,\fie{x}}).
\]
A módszer fő előnye, hogy elegendő $T$ csúcsain iterálni és nem kell fordított irányú lekérdezést végezni (nevezetesen: megmondani, hogy melyik elemhez tartozik egy adott csúcs), 
mint az előző esetben.
\textbf{A továbbiakban ezt a stratégiát fogjuk követni}, mert magasabb rendű elemeknél némileg egyszerűbb, mint a csúcsonkénti összefűzés.
\end{enumerate}

Az $a(\lambda_{T,\fie{x}},\lambda_{T,\fie{y}})$ mennyiségek meghatározásához (ahol $\fie{x},\fie{y}\in\mathcal{V}(T)$) az előző gyakorlaton elmondott \textbf{referenciaelem-technikát} alkalmazzuk. Az ottaniakat folytatva, 
ha $\Map{\bm{\zeta}}{R}{T}$, $\bm{\zeta}(\fie{x})=Z\fie{x}+\fie{b}$ affin leképezés az $R$ referenciaelemről, akkor
\begin{align*}
a(\lambda_{T,\fie{x}},\lambda_{T,\fie{y}})&=|\det Z|\int_R Z^{-1}D(\lambda_{T,\fie{x}}\circ\bm{\zeta}) \cdot Z^{-1}D(\lambda_{T,\fie{y}}\circ\bm{\zeta}) \\
&=|\det Z|\int_R Z^{-1}D\lambda_{R,\bm{\zeta}^{-1}(\fie{x})} \cdot Z^{-1}D\lambda_{R,\bm{\zeta}^{-1}(\fie{y})}\\
&=\frac{1}{2}|\det Z|Z^{-1}D\lambda_{R,\bm{\zeta}^{-1}(\fie{x})}\cdot Z^{-1}D\lambda_{R,\bm{\zeta}^{-1}(\fie{y})}
\end{align*}
ahol a $D\lambda_{R,\bm{\zeta}^{-1}(\fie{x})}$ gradienseket már meghatároztuk az $R$ referenciaháromszögön:
\[
D\lambda_1=\begin{pmatrix}
-1 \\
-1
\end{pmatrix},\quad
D\lambda_2=\begin{pmatrix}
1 \\
0
\end{pmatrix},\quad
D\lambda_3=\begin{pmatrix}
0 \\
1
\end{pmatrix}.
\]
A bonyolultnak tűnő $\lambda_{R,\bm{\zeta}^{-1}(\fie{x})}$ jelölés szavakkal: az $R$ referenciaelemen azon alakfüggvény, amely a $T$ ,,fizikai'' háromszög $\fie{x}$ csúcsához tartozik.

Ezzel az $A^h$ merevségi mátrix összefűzésének módját teljes egészében leírtuk, az merevségi mátrix elemenkénti összefűzésének pszeudokódja a következő.

\fbox{%
\begin{minipage}{\dimexpr\linewidth-2\fboxsep-2\fboxrule\relax}
\begin{algorithmic}
\STATE \boxed{\textbf{fe2\_stiff}} -- \emph{Elsőfokú merevségi mátrix elemenkénti összefűzése} 
\STATE \textbf{INPUT:} A $\mathcal{T}_h$ háromszögrács.
\STATE \textbf{OUTPUT:} Az $A^h$ mátrix.
\STATE $A^h=0$
\FOR{$T\in\mathcal{T}_h$}
\STATE Határozzuk meg a $\bm{\zeta}(\fie{x})=Z\fie{x}+\fie{b}$ affin leképezést, ami átvisz az $R$ referenciaelemről a $T$-re és
jelölje $\lambda_{R,\bm{\zeta}^{-1}(\fie{x})}$ az $\fie{x}\in T$ csúcsnak megfelelő alakfüggvényt az $R$ referemciaelemen.
\FOR{$\fie{x}\in\mathcal{V}(T)$}
\FOR{$\fie{y}\in\mathcal{V}(T)$}
\STATE $A^h_{\fie{x},\fie{y}}=A^h_{\fie{x},\fie{y}}+\frac{1}{2}|\det Z| Z^{-1}D\lambda_{R,\bm{\zeta}^{-1}(\fie{x})}\cdot Z^{-1}D\lambda_{R,\bm{\zeta}^{-1}(\fie{y})}$
\ENDFOR
\ENDFOR
\ENDFOR
\end{algorithmic}
\end{minipage}% 
}

A terhelési vektor elemenénti összefűzésének pszeudokódja az alábbiakban olvasható; az integrál approximációjával \az{cubature}. szakaszban foglalkozunk.

\fbox{%
\begin{minipage}{\dimexpr\linewidth-2\fboxsep-2\fboxrule\relax}
\begin{algorithmic}
\STATE \boxed{\textbf{fe2\_load}} -- \emph{Elsőfokú terhelési vektor összefűzése} 
\STATE \textbf{INPUT:} $\mathcal{T}_h$, $f$
\STATE \textbf{OUTPUT:} Az $F^h$ vektor.
\STATE $F^h=0$
\FOR{$T\in\mathcal{T}_h$}
\STATE Határozzuk meg a $\bm{\zeta}(\fie{x})=Z\fie{x}+\fie{b}$ affin leképezést, ami átvisz az $R$ referenciaelemről a $T$-re és
jelölje $\lambda_{R,\bm{\zeta}^{-1}(\fie{x})}$ az $\fie{x}\in T$ csúcsnak megfelelő alakfüggvényt az $R$-en.
\FOR{$\fie{x}\in\mathcal{V}(T)$}
\STATE $\displaystyle{F^h_{\fie{x}}=F^h_{\fie{x}}+|\det Z|\int_R f\circ\bm{\zeta}\,\lambda_{R,\bm{\zeta}^{-1}(\fie{x})}}$\hfill\emph{valamilyen kubatúrával}
\ENDFOR
\ENDFOR
\end{algorithmic}
\end{minipage}% 
}
\begin{figure}
\begin{center}
\includegraphics[scale=1.4]{fig/1dof.pdf}
\end{center}
\label{examass}
\end{figure}
\begin{exam} Ellenőrizzük le, hogy jól megértettük-e az algoritmusokat! Ehhez tekintsük a legegyszerűbb esetet: $\Omega=(0,2)^2$, a peremen homogén Dirichlet-peremfeltétel 
legyen előírva és a $\mathcal{T}_h$ háromszögrács pedig \az{\ref{examass}}. ábrán látható.
Egyetlen $\fie{x}$ szabadsági fok van: ezt az ábrán kis négyzettel jelöltük. Az ehhez tartozó $\phi_{\fie{x}}$ bázisfüggvény tartója szürkítve látható.
Szerencsére a $T_3$, $T_5$, $T_7$ háromszögek az $R$ referenciaelem eltoltjai, így az affin transzformációkabn szereplő $Z$ mátrix az identitás.
A $T_2$, $T_4$, $T_6$ háromszögek esetében könnyű meghatározni a szükséges $\lambda_{T_2,\fie{x}}$, $\lambda_{T_4,\fie{x}}$, $\lambda_{T_6,\fie{x}}$
alakfüggvényeket (több nincs a homogén Dirichlet-peremfeltétel miatt), így most nem bajlódunk az referenciaelemre való visszatéréssel. Ezek
\[
\lambda_{T_2,\fie{x}}(\xi_1,\xi_2)=\xi_1+\xi_2-1,\;\; \lambda_{T_4,\fie{x}}(\xi_1,\xi_2)=2-\xi_1,\;\; \lambda_{T_6,\fie{x}}(\xi_1,\xi_2)=2-\xi_2,
\]
és gradienseik
\[
D\lambda_{T_2,\fie{x}}=\begin{pmatrix}
1 \\
1
\end{pmatrix},\quad
D\lambda_{T_4,\fie{x}}=\begin{pmatrix}
-1 \\
0
\end{pmatrix}
\quad
D\lambda_{T_6,\fie{x}}=\begin{pmatrix}
0 \\
-1
\end{pmatrix}.
\]
A megfelelő elemenkénti járulékok tehát egyrészt a referenciaelemre visszahúzással:
\begin{align*}
a(\lambda_{T_3,\fie{x}},\lambda_{T_3,\fie{x}})=\int_R D\lambda_3\cdot D\lambda_3=1\\
a(\lambda_{T_5,\fie{x}},\lambda_{T_5,\fie{x}})=\int_R D\lambda_2\cdot D\lambda_2=\frac{1}{2}\\
a(\lambda_{T_7,\fie{x}},\lambda_{T_7,\fie{x}})=\int_R D\lambda_1\cdot D\lambda_1=\frac{1}{2}
\end{align*}
illetve közvetlen számolással
\begin{align*}
a(\lambda_{T_2,\fie{x}},\lambda_{T_2,\fie{x}})=\int_{T_2} D\lambda_{T_2,\fie{x}}\cdot D\lambda_{T_2,\fie{x}}=1\\
a(\lambda_{T_4,\fie{x}},\lambda_{T_4,\fie{x}})=\int_{T_4} D\lambda_{T_4,\fie{x}}\cdot D\lambda_{T_4,\fie{x}}=\frac{1}{2}\\
a(\lambda_{T_6,\fie{x}},\lambda_{T_5,\fie{x}})=\int_{T_6} D\lambda_{T_6,\fie{x}}\cdot D\lambda_{T_6,\fie{x}}=\frac{1}{2}
\end{align*}
Összefoglalva, $A^h=4$. A programok hibakereséséhez ilyen egyszerűen kiszámolható példákat szokás használni.
\end{exam}

\subsection{Implementáció} 
A MATLAB példaprogramokban a következő tárolási sémát használjuk a $\mathcal{T}_h$ rács tárolására, amely egyébként a háromszögrácsok
tárolásának leggyakoribb és legegyszerűbb módja. Egy \lstinline|tris| $N\times 3$ egész számokból álló mátrix $k$-adik sora megadja a $k$-adik háromszög csúcsainak \emph{indexeit}
egy \lstinline|verts| $M\times 2$ lebegőpontos mátrixban. Ezzel pl. a 10-ik háromszög első csúcsának koordinátái
\begin{lstlisting}
[verts(tris(10,1),1), verts(tris(10,1),2)]
\end{lstlisting}
A \lstinline|verts| tömb \emph{egyedi} pontokat tartalmaz, így tekinthetjük annak elemeit az $\Omega_h$ rácspontoknak.

A merevségi mátrix és a terhelési vektor elemenkénti összefűzésének naív megvalósítását az \lstinline|fe2_stiff_d| és az \lstinline|fe2_load_d|
programok tartalmazzák. \textbf{Figyelem!} A gyakorlatban az \lstinline|fe2_stiff_v| és az \lstinline|fe2_load_v| programokat használjuk.

\begin{rmrk}\label{stiffvec}
Az \verb!fe2_stiff_d! program nagyon lassú. Ennek oka, hogy minden elemen végig kell menni egy 
\verb!for! ciklus segítségével.
A baj nem az elemenkénti összefűzés elvével van: lényegesen gyorsítható a folyamat, ha megpróbáljuk elkerülni a nagy ciklus használatat. Ez most lehetséges, a módszert \textbf{vektorizálásnak} nevezik.
A hatékonyság informatikai háttere az, hogy a CPU/GPU-k  sokkal gyorsabban hajtják végre a nagy vektorokon végzett műveleteket, pl. összeadas, skalárszorzás,
elemenkénti szorzás, stb. Ennek az az oka, hogy párhuzamosan tudja elvégezni az áramkör az elemenkénti műveleteket.

 Próbáljuk megérteni, mi is történik a \verb!fe2_stiff_v! programban és miért!\footnote{Lásd 
\verb!http://www.math.uci.edu/~chenlong/226/Ch3FEMCode.pdf!.}
\begin{enumerate}[(i)]
\item Jelölje
\[
\bm{\eta}_1=\fie{a}_3-\fie{a}_2,\; \bm{\eta}_2=\fie{a}_1-\fie{a}_3,\;  \bm{\eta}_3=\fie{a}_2-\fie{a}_1
\]
a $T=\{\fie{a}_1,\fie{a}_2,\fie{a}_3\}^{co}$ háromszög éleit. Belátható, hogy
\begin{equation}\label{dlambdaintedge}
\int_T D\lambda_k\cdot D\lambda_\ell=\frac{1}{4|T|} \bm{\eta}_k\cdot \bm{\eta}_\ell.
\end{equation}
Valóban, emlékezzünk vissza a $D\lambda_k=-\dfrac{1}{d_k}\fie{n}_k$ formulára, ahol $\fie{n}_k$ az $\fie{a}_k$ csúccsal szemben fekvő oldal kifelé mutató normálvektora, és
$d_k$ az $\fie{a}_k$ csúcs az ettől az oldaltól vett távolsága. Legyen 
\[
R=\begin{pmatrix}
0 & -1 \\
1 & 0
\end{pmatrix}
\]
a $90^\circ$-os forgatás mátrixa. Ekkor könnyen láthatóan
\[
\fie{n}_k=R\frac{\bm{\eta}_k}{|\bm{\eta}_k|}
\]
érvényes, és felhasználva, hogy $R$ ortogonális, kapjuk
\[
\fie{n}_k\cdot\fie{n}_\ell=\dfrac{\bm{\eta}_k}{|\bm{\eta}_k|}\cdot \dfrac{\bm{\eta}_\ell}{|\bm{\eta}_\ell|}.
\]
A triviális $|T|=\frac{1}{2}d_k|\bm{\eta}_k|$ területképletet felhasználva:
\begin{align*}
\int_T &D\lambda_k\cdot D\lambda_\ell=\int_T \frac{1}{d_kd_\ell} \fie{n}_k\cdot\fie{n}_\ell=\frac{|T|}{d_kd_\ell}\fie{n}_k\cdot\fie{n}_\ell=
\frac{|T|}{d_k|\bm{\eta}_k|d_\ell|\bm{\eta}_\ell|} \bm{\eta}_k\cdot\bm{\eta}_\ell\\
&=\frac{|T|}{4\frac{1}{2}d_k|\bm{\eta}_k|\frac{1}{2}d_\ell|\bm{\eta}_\ell|} \bm{\eta}_k\cdot\bm{\eta}_\ell
=\frac{|T|}{4|T||T|} \bm{\eta}_k\cdot\bm{\eta}_\ell=\frac{1}{4|T|} \bm{\eta}_k\cdot\bm{\eta}_\ell.
\end{align*}
\item Igazoljuk, hogy
\[
|T|=\frac{1}{2}\Bigg|\det\begin{pmatrix}
\eta_{21} & \eta_{31} \\
\eta_{22} & \eta_{32}
\end{pmatrix}
\Bigg|
=\frac{1}{2}|\eta_{21}\eta_{32}-\eta_{31}\eta_{22}|
\]
\item Az \verb!edges(el,k,i)! megadja az \verb!el! háromszög \verb!i!-edik $\bm{\eta}_i$ élének $\eta_{ik}$ koordinátáját. Ezért az
\begin{lstlisting}
Aij = dot(edges(:,:,i), edges(:,:,j), 2) ./ ( 4*areas );
\end{lstlisting}
utasítás megadja az \emph{összes} háromszögre az
\[
\int_T D\lambda_i\cdot D\lambda_j
\]
integrál értékét (a \verb!dot! függvény harmadik paramétere megmondja, hogy melyik futó ``index'' szerint akarunk összegezni -- itt az $\eta_i$ koordinátái szerint)
a rögzített $\lambda_i$, $\lambda_j$ alakfüggvény-párra. Ezeket a \verb!Aij! mennyiséget meghatározzuk minden $i,j\in\{1,2,3\}$ párra.
\item Ha most kiadjuk minden $i,j\in\{1,2,3\}$ esetén az
\[
\verb!A = A + sparse(tris(:,i),tris(:,j),Aij,nv,nv);!
\]
utasítást (persze \verb!A=0! inicializálás után), akkor az elemenkénti járulékok összegyűjődnek az algoritmusnak megfelelően. 
A \verb!sparse! függvény első két paramétere két $\{i_1,\ldots,i_n\}$ és $\{j_1,\ldots,j_n\}$ indexsorozat, a harmadik paramétere
a beírandó $\{x_{k\ell}\}_{k,\ell=1,\ldots,n}$ számok: ezzel a kapott mátrix elemei $a_{i_k,i_\ell}=x_{k\ell}$.
\end{enumerate}
\end{rmrk}

\section{Inhomogén Dirichlet-peremfeltétel}\label{bd}

A \textbf{homogén} Dirichlet-peremfeltétel esetén $V$, és így annak altere $V_h$ olyan függvényekből áll amely az $\Omega$ poliéder-tartomány
peremén eltűnnek. Ezzel ekvivalensen, az $\fie{x}\in\Omega_h^\partial=\partial\Omega\cap\overline{\Omega}_h$ perempontokhoz tartozó 
$\phi_{\fie{x}}$ kalapfüggvényeket kivesszük $V_h$ bázisából. Ezzel gyakorlatilag eltűntettük az előző szakaszban megkonstruált $A^h$ mátrixból a ``szemetet'',
és kapunk egy kisebb $A^{h,0}$ mátrixot, \emph{amely olyan bázisban van felírva, amelyben nincsenek benne a peremen lévő kalapfüggvények.}
A bázist rendezzük úgy sorba, hogy először a belső pontoknak, majd a perempontoknak megfelelő kalapfüggvények szerepeljenek benne.
Ekkor egy $U^h$ megoldásvektor
\[
U^h=\begin{pmatrix}
U^{h,0}\\
0
\end{pmatrix}
\]
alakban írható. Ha már ismerjük az $F^h$ terhelési vektort, akkor az
\[
A^{h,0}U^{h,0}=F^h
\]
lineáris algebrai egyenletrendszert kell megoldani.
Ez utóbbi megfogalmazást könnyű programkódra lefordítani: a MATLAB-ban egyszerűen ki lehet választani egy megfelelő részmátrixot.
Például az $\texttt{A}\in\Real^{N\times N}$ merevségi mátrix homogén Dirichlet-peremfeltételnek megfelelő részmátrixa 
\lstinline|A(dof,dof)|,
ahol $\texttt{dof}\in\{1,\ldots,N\}^{N}$ a kiválasztandó elemeket felsoroló indexvektor. Kísérletezzünk ezzel!

Az \textbf{inhomogén} esetben egy kis trükkre van szükség. Legyen a Dirichlet-peremfeltétel $u=g$ ($\partial\Omega$-n).
A megfelelő homogenizált\footnote{azaz a kinullázott Dirichlet-peremfeltételű} feladat $u_h^0\in V_h$ megoldására vonatkozó vetületi egyenlet
\[
\forall v_h\in V_h \;:\; a(u_h^0,v_h)=\ell(v_h).
\]
Most legyen $g_h$ olyan $V_h$-beli függvény, amely az $\Omega_h$ belső pontokban azonosan zérus, és $g_h=g$ az $\Omega_h^\partial$-n. Ilyen függvényt könnyű megadni.
Ekkor, a
\begin{equation}\label{modrhs}
\forall v_h\in V_h \;:\; a(u_h^0,v_h)=\ell(v_h)-a(g_h,v_h)
\end{equation}
\emph{módosított jobb oldalú} homogén gyenge feladat megoldásával olyan $u_h:=u_h^0+g_h$ függvényt nyerünk amelyre
\[
\left.
\begin{aligned}
\forall v_h\in V_h \;:\; a(u_h,v_h)&=\ell(v_h)\\
u_h&=g \;\;(\Omega_h^\partial\text{-n})
\end{aligned}
\right\}
\]
Ezzel visszavezettük az inhomogén feladat megoldását \az{\eqref{modrhs}} módosított jobb oldalú \emph{homogén} feladat megoldására.

Mátrixokkal megfogalmazva, az \eqref{modrhs} módosított egyenlet az 
\[
\begin{pmatrix}
A^{h,0} & A^{h,\partial} \\
0 & I 
\end{pmatrix} 
\begin{pmatrix}
U^{h,0}\\
U^{h,\partial}
\end{pmatrix}
=\begin{pmatrix}
F^h\\
G^h
\end{pmatrix}
\]
alakot ölti, ahol $G^h$ egyszerűen a $g$-ből pontonkénti behelyettesítéssel kapott vektort jelöli. Ebből kiküszöböléssel (,,blokk Gauss-eliminációval'') kapjuk az
\[
A^{h,0}U^{h,0}=F^h-A^{h,\partial}G^h.
\]
módosított egyenletet. Ezt megoldva, és az $U^{h,0}$-t kiegészítve kapjuk az
\[
U^h=\begin{pmatrix}
U^{h,0}\\
G^h
\end{pmatrix}
\]
megoldást. Vegyük észre, hogy ez teljesen hasonló ahhoz, amit a véges differencia-módszernél csináltunk. Az eljárás pszeudokódja a következő.

\fbox{%
\begin{minipage}{\dimexpr\linewidth-2\fboxsep-2\fboxrule\relax}
\begin{algorithmic}
\STATE \boxed{\textbf{fe2\_boundary}} -- \emph{Dirichlet-peremfeltételek érvényesítése} 
\STATE \textbf{INPUT:} $\mathcal{T}_h$, $A^h$, $F^h$, $g$, $h$
\STATE \textbf{OUTPUT:} A módosított $F^h$ terhelési vektor, és a megfelelően feltöltött $U^h$ vektor.
\STATE Legyen $G^h_k=g(\fie{x}_k)$ minden $\fie{x}_k$ Dirichlet-peremen lévő csúcsban.
\STATE $F^h=F^h-A^hG^h$
\STATE $U^{h,\partial}=G^{h,\partial}$
\STATE \emph{Ezek után a megoldást $U^{h,0}=(A^{h,0})^{-1}F^{h,0}$ alakban kaphatjuk meg.}
\end{algorithmic}
\end{minipage}}

\subsection{Implementáció}\label{purediriimpl}
A MATLAB/Octave implementáció a következőképpen történhet:
\begin{lstlisting}
function [U,F,dof,nondof] = fe2_boundary_diri_only( verts, edges, edgemarker, A, F, dirifunc )
% csucsok szama
nv = size( verts, 1 );
% Dirichlet-peremelek
diriedges = find(edgemarker == 1);
% Dirichlet-csucsok
dirivertmask = zeros( nv, 1 );
dirivertmask(edges(edgemarker == 1, 1)) = 1;
dirivertmask(edges(edgemarker == 1, 2)) = 1;
% Dirichlet csucsok nem-dofok
nondof = find( dirivertmask );
% A tobbi csucs dof
dof = find( ~dirivertmask );
% Megoldasvektor kinullazasa
U = zeros( nv, 1 );
% A Dirichlet adat kiertekelese a Dirichlet-perempontokon
Gh = arrayfun( dirifunc, verts( nondof, 1 ), verts( nondof, 2 ) );
% A Dirichlet peremfeltetel ervenyesites
U(nondof) = Gh;
F = F - A*U;
end
\end{lstlisting}
ahol az \lstinline|edgemarker| tömböt a következő kóddal generáljuk:
\begin{lstlisting}
% Legyen az osszes peremel Dirichlet
edgemarker = [];
edgemarker = markbdedges( verts, edges, trineigh, edgemarker, [], [], 1 );
\end{lstlisting}
Ezek után az
\begin{lstlisting}
U(dof) = A(dof,dof)\F(dof);
\end{lstlisting}
utasítással tudjuk megoldani az ,,eliminált'' egyenletrendszert.

Az iménti kód csak a Dirichlet-peremfeltételt tudja kezelni, \az{\ref{mixedfem}}. szakaszban tárgyaljuk a vegyes peremfeltételt.

% Hogy ne csak az absztrakt mátrix-vektor szinten értsük meg a dolgot, gondoljuk meg,
%  hogy az elemenkénti összefűzésnél a jobb oldalból levonandó extra tag \textbf{csak olyan $x$ csúcsokban jelentkezik, amelynek szomszédja egy $x'$ perempont.}
% Ilyen $x$-ekre a járulék egyszerűen
% \[
% a(g_h,\phi_x)=\sum_{T\in\mathcal{N}(x)} a(g_h,\lambda_{T,x})=\sum_{T\in\mathcal{N}(x)\cap\mathcal{N}(x')} g(x')a(\lambda_{T,x'},\lambda_{T,x}),
% \]
% $g_h$ tulajdonságai miatt. Az alábbi ábra szemlélteti, hogy mely peremhez tartozó alakfüggvényekből és helyettesítési értékekből származnak a járulékok az elemenkénti összefűzés során.
% \begin{center}
% \includegraphics[scale=1.6]{bdrhs.pdf}
% \end{center}


\section{Kubatúra háromszögön}\label{cubature}
Hátra van még az $\ell(\lambda_{T,x})$ mennyiségek meghatározása. Tételezzük fel, hogy létezik $f\in L^2(\Omega)$ úgy, hogy
\[
\forall v\in V \;:\; \ell(v)=\int_\Omega fv.
\]
A Riesz reprezentációs tétel szerint ilyen $f$ van, ha $\Map{\ell}{L^2(\Omega)}{\Real}$ folytonos lineáris funkcionál.
Itt az integrálás már nem intézhető el olyan könnyedén, mint az $a(\lambda_{T,x},\lambda_{T,x'})$ esetében, ahol az integrandus konstans volt. Megjegyezzük,
hogy a függvényegyütthatós esetben már az $a(\ph,\ph)$ bilineáris forma kiértékelése is numerikus közelítést igényel,
lásd később.

Az integrálok numerikus közelétését \textbf{kubatúrának} ($n=1$ dimenzióban \textbf{kvadratúrának} nevezik).
Egy dimenzióban (az ún. tenzorszorzat-elemeken is alkalmazhatóak, lásd később) a kvadratúra-formuláknak nagyon szép, klasszikus elmélete van. 
Magasabb dimenzióban ilyen általános elmélet nincsen\footnote{De lásd pl. S. L. Sobolev, V. L. Vashkevich. \emph{The Theory of Cubature Formulas.} (1997)}.
Legyen most $f\in C(R)$, ahol $R$ a referenciaháromszög. Olyan $X=\{(\xi_k,\zeta_k)\}\subset R$ pontokat,
és $W=\{w_k\}\subset\Real$ együtthatókat keresünk, amelyekre
\begin{equation}\label{quadrule}
\int_R p=\sum_{k=1}^N w_kp(\xi_k,\zeta_k),
\end{equation}
minden olyan $p$ polinomra, amelyre $\deg(p)\le d$ adott $d$ pozitív egészre. A $d$ számot a formula \textbf{rendjének} nevezzük:
megmondja, legfeljebb milyen fokú polinomokra egzakt a kubatúraformula.

Az $\ell(v)$ mennyiséget tehát kubatúra alkalmazásával közelítjük. Ezzel hibát vétünk, mivel valójában egy másik, $\ell_h(v)$ mennyiséget értékelünk ki.
Az előadásjegyzet 3.6.3 pontja foglalkozik részletesen azzal, hogy milyen körülmények között őrizhető meg a végeselem-módszer várt rendje.

\begin{center}
\includegraphics[scale=1.1]{fig/cubature.pdf}
\end{center}

Emlékeztetünk az alábbi egyszerű kvadratúraformulákra, amelyeket numerikus analízisen tanultunk.
\begin{enumerate}[(1)]\indented
\item \textbf{Középponti szabály.} Elegendő megkövetelni, hogy a
\[
p_0(\xi,\zeta)=1,\quad p_1(\xi,\zeta)=\xi\quad \text{és}\;\; p_2(\xi,\zeta)=\zeta
\]
polinomokra legyen pontos \az{\eqref{quadrule}} egyenlet, mivel ezek bázist alkotnak $P^1(R)$-ben. Legyen $N=1$, ez három egyenletet
szolgáltat:
\[
\frac{1}{2}=w_1,\quad\frac{1}{6}=w_1p_1\Big(\frac{1}{3},\frac{1}{3}\Big),\quad\frac{1}{6}=w_1p_2\Big(\frac{1}{3},\frac{1}{3}\Big),
\]
amiből tehát a középponti szabály $X=((1/3,1/3))$, $W=(1/2)$. Egy elsőrendű formulát kaptunk.
\item \textbf{Hárompontos szabály.} Hasonlóan belátható, hogy
\begin{equation}\label{threepoint}
X=((1/2,1/2),\,(0,1/2),\,(1/2,0)), \quad W=(1/6,1/6,1/6)
\end{equation}
és
\[
X=((2/3,1/6),\,(1/6,2/3),\,(1/6,1/6)), \quad W=(1/6,1/6,1/6)
\]
másodrendű kubatúraformulák.
\item \textbf{Négypontos szabály.} Az alábbi harmadrendű szabályban már megjelenik egy negatív súly is,
\begin{align*}
X&=((1/3,1/3),\,(1/5,3/5),\,(1/5,1/5),\,(3/5,1.5))\\
 W&=(-27/96,25/96,25/96,25/96)
\end{align*}
ami kellemetlen numerikus pontatlanságot okozhat ha a helyettesítési értékek nagyok\footnote{ugyanis $a+(-a)\neq 0$ lebegőpontos számokra.}, illetve erre nem is alkalmazható a jegyzetben szereplő 
3.48. Tétel.
 Van viszont olyan négypontos kubatúraformula, amelyben
minden együttható pozitív.
\end{enumerate}

\section{Rácsfüggő normák}
\Az{\ref{discnorm}}. szakaszban a \emph{diszkrét} $H^1$-, és $L^2$ normákkal foglalkoztunk: ezek a véges differencia-módszerben
alkalmazott rácsfüggvények nagyságának mérésében igen hasznosnak bizonyultak. Ebben a szakaszban háromszögrácsokon vezetünk be diszkrét normákat.

Ismét egy alkalmas skalárszoratból indulunk ki: legyen $V_h\subset H^1_0(\Omega)$ a $\mathcal{T}_h$ trianguláción megadott (elsőfokú) $T_3$ Courant-elemek által indukált
approximáló altér és definiáljuk tetszőleges $u,v\in V_h$ függvények esetén az
\[
\dua{u}{v}_h=h^2\dua{u}{v} =h^2\sum_{\fie{x}\in\Omega_h} u(\fie{x})v(\fie{x})
\]
\textbf{rácsfüggő skalárszorzatot}, ahol 
\[
h=\max_{T\in\mathcal{T}_h}\diam T
\]
és emlékeztetünk, hogy $\Omega_h$ a $\mathcal{T}_h$ trianguláció belső rácspontjait jelöli.
Az eszerint a skalárszorzat szerint vett ,,Riesz-reprezentánsa'' az $a(\ph,\ph)$ bilineáris formának egy $\Map{B_h}{V_h}{V_h}$ lineáris operátor, azaz
\[
\dua{B_hu}{v}_h=a(u,v)
\]
minden $u,v\in V_h$ esetén. Ekkor $B_h=h^2A_h$, ahol $A_h$ merevségi mátrix. Később látni fogjuk (lásd \az{\ref{condstiffmass}}. Állítás bizonyítását), hogy $\|A_h\|=\mathcal{O}(h^{-2})$, és mivel
$A_h$ szimmetrikus pozitív definit, ezért $\lambda_{\max}(A_h)=\mathcal{O}(h^{-2})$. Ennélfogva $\lambda_{\max}(B_h)=\mathcal{O}(1)$,\footnote{Természetesen ez az egyszerű átskálázás
nem változtatja meg a kondiciószámot.} azaz $B_h$ spektruma mindig egy rácsfüggetlen intervallumba fog esni.

Az $\dua{}{}_h$ rácsfüggő skalárszorzat jelentősége abban áll, hogy mivel $B_h$ továbbra is szimmetrikus pozitív definit, bevezethetjük a
\[
\|u\|_{s,h}^2=\dua{B_h^su}{v}_h
\]
\textbf{diszkrét Szoboljev-normacsaládot}, ahol $s\in\Real$. Világos, hogy ekkor az energianorma kifejezhető az 
\[
\|u\|_{1,h}=\|u\|_E=\sqrt{a(u,u)}
\]
szerint, illetve 
\[
\|u\|_{0,h}=\|u\|_h=\sqrt{\dua{u}{u}_h}
\]
a \textbf{diszkrét $\pmb{L^2}$-norma}. A Tychonoff-tétel alapján tudjuk, hogy a $V_h$ véges dimenziós vektortéren minden norma ekvivalens, 
\emph{azonban az ekvivalenciában szereplő konstansok függenek a dimenziótól}, és emiatt $h$-tól is. Ezért némileg meglepő a következő eredmény, amihez emlékeztetünk az előadáson
szerepelt definícióra: a $\{\mathcal{T}_h\}_{h>0}$ triangulációcsalád \textbf{reguláris}, ha bármely $h>0$ és $T\in\mathcal{T}_h$ esetén a $h^2/|T|$ mennyiség (egyenletesen) korlátos.

\begin{bprop}\label{meshequ}
Tételezzük fel, hogy a $\{\mathcal{T}_h\}_{h>0}$ triangulációcsalád reguláris. Ekkor léteznek rácsfüggetlen $C,C'>0$ konstansok, hogy minden $u\in V_h$ esetén
\[
C\|u\|_{L^2(\Omega)}\le \|u\|_{0,h}\le C'\|u\|_{L^2(\Omega)}.
\]
\end{bprop}
\begin{proof}[Brenner--Scott]
Egyrészt világos, hogy
\[
\|u\|_{0,h}^2=h^2\sum_{\fie{x}\in\Omega_h} u(\fie{x})^2.
\]
Másrészt vegyük vigyelembe, hogy az $u^2$ függvény háromszögenként másodfokú polinom, ezért \az{\eqref{threepoint}} kubatúraformula
háromszögenként $u^2$ pontos integrálját adja meg, ezért
\[
\|u\|_{L^2(\Omega)}^2=\sum_{T\in\mathcal{T}_h}\int_T u(\fie{x})^2\,d\fie{x}=\sum_{T\in\mathcal{T}_h}\frac{|T|}{3}\sum_{i=1}^3 u(\fie{m}_i)^2,
\]
ahol $\fie{m}_i$ az $T$ háromszög $\fie{p}_i$ csúcsával szemben fekvő oldalfelező pontját jelöli. Minthogy $u$ lineáris $T$-n, ezért $u(\fie{m}_i)$ kifejezhető az $u(\fie{p}_i)$ csúcsokban
vett helyettesítési értékek lineáris kombinációjaként, mégpedig
\[
\begin{pmatrix}
u(\fie{m}_1)\\
u(\fie{m}_2)\\
u(\fie{m}_3)
\end{pmatrix}
=
\underbrace{\frac{1}{2}
\begin{pmatrix}
0 & 1 & 1\\
1 & 0 & 1\\
1 & 1 & 0
\end{pmatrix}}_{\displaystyle{M}}
\begin{pmatrix}
u(\fie{p}_1)\\
u(\fie{p}_2)\\
u(\fie{p}_3)
\end{pmatrix}.
\]
Vegyük észre, hogy a jobb oldalon szereplő $M$ mátrix könnyen láthatóan invertálható, és $\|M\|_2=1$, illetve $\|M^{-1}\|_2=2$. Ezért érvényes az
\[
\sum_{i=1}^3 u(\fie{m}_i)^2\le \sum_{i=1}^3 u(\fie{p}_i)^2 \le 2\sum_{i=1}^3 u(\fie{m}_i)^2
\]
egyenlőlenséglánc. Ha ezt összegezzük minden $T\in\mathcal{T}_h$-ra, és $h^2$-tel végigszorozzuk, akkor
\[
h^2\sum_{T\in\mathcal{T}_h}\sum_{i=1}^3 u(\fie{m}_i)^2\le \|u\|_{0,h}^2\le 2h^2\sum_{T\in\mathcal{T}_h}\sum_{i=1}^3 u(\fie{m}_i)^2
\]
A $\{\mathcal{T}_h\}$ triangulációcsalád reguláritása alapján létezik olyan $h$-tól független $K>0$ konstans, hogy
\[
\frac{\pi^2}{4}|T|\le h^2\le K'|T|
\]
minden $T\in\mathcal{T}_h$ esetén. Ezekből már következik az állítás a $C=\sqrt{\frac{3}{4}}\pi$ és $C'=\sqrt{\frac{3}{2}K'}$ választással.
\end{proof}

A szakasz zárásaként jegyezzük meg, hogy egy háromszögrácson definiált $\Map{u_h}{\overline{\Omega}}{\Real}$ 
folytonos és háromszögenként lineáris függvény maximumát (minimumát) könnyen láthatóan felveszi valamely rácspontban.
(Ez nincs így magasabb polinomfok esetén!) Ennélfogva az $\|u_h\|_\infty$ mennyiség meghatározásához elegendő az $u_h$-t a csomóponti bázisban
reprezentáló $U^h$ vektor maximumnormáját képezni.

\Exercises

\begin{Exercise}
Nézzük meg a \verb!fe2_stiff_s! programot és vessük össze a jegyzet végén található pszeudokóddal.
\end{Exercise}

\begin{Exercise}
Vessük össze \az{\ref{bd}}. szakaszban elmondottakat az \verb!fe2_boundary! programmal és figyeljük meg, mennyire transzparens a kapott kód!
\end{Exercise}

\begin{Exercise}
Az \verb!fe2_load_d! (és a vektorizált \verb!fe2_load_v!) program összefűzi az $F^h$ terhelési vektorokat. A programban a kubatúraszabályokat az $R$ referenciaháromszögön adjuk meg
egy \verb!XW! $N\times 3$ vektor formájában, amelynek első két oszlopa a kubatúrapontokat és harmadik oszlopa a súlyokat adja meg.
Felhívjuk a figyelmet, hogy a programok nem ellenőrzik, hogy az \verb!XW! kubatúraszabály ``helyes-e''. Nézzük meg a programokat!
\end{Exercise}