Example ** programs** and **course notes** for the course *"Numerical methods for elliptic PDEs"* held at ELTE, Budapest during the fall semester of 2014, 2015.

CC0
To the extent possible under law, the author has waived all copyright and related or neighboring rights to this file.
